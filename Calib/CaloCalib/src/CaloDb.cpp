#define _CALODB_DLL_

#ifdef __cplusplus
extern "C" {
#endif
#include <oci.h>
#include <orid.h> 
#ifdef __cplusplus
}
#endif

#include <iostream>
#include <math.h>
#include <string.h>
#include <stdio.h>

#include "calotyp_c.h"
#include "ecalmoduleitem_c.h"

#include "CaloDb.h"
  
double v0_ecal=0.0063; 
double v1_ecal=0.4214;

double stepx_ecal=121.7; //mm
double stepy_ecal=121.9; //mm

void checkerr(OCIError *errhp, sword status, char* erMess);
void display_attr_val(OCIEnv *envhp, OCIError *errhp, text *names, OCITypeCode typecode, dvoid *attr_value);
void dump_adt(OCIEnv *envhp, OCIError *errhp, OCISvcCtx *svchp, OCIType *tdo, dvoid *obj, dvoid *null_obj);
sword Connect(OCIEnv*& myenvhp,OCIServer*& mysrvhp1,OCISession*& myusrhp1, OCISvcCtx*& mysvchp, OCIError*& myerrhp, char* ErrorMessage); 
sword ConnectUser(const char* usr, const char* server, const char* pwd, OCIEnv*& myenvhp,OCIServer*& mysrvhp1,OCISession*& myusrhp1, OCISvcCtx*& mysvchp, OCIError*& myerrhp, char* ErrorMessage); 
sword Disconnect(OCIEnv* env,OCIError* err,OCIServer* mysrvhp1,OCISession* myusrhp1, OCISvcCtx* mysvchp, int count_free, char* ErrorMessage); 
sword all_tdos(char* erm);
sword calotypo(OCIEnv *env, OCIError *err);
sword ecalmoduleitemo(OCIEnv *env, OCIError *err);

bool m_connected=false;

static const int M_GetOld  =1;
static const int M_GetNew  =2;
static const int M_MarkNew =4;

OCIError* ociError=0; /* the error handle */ 
OCIEnv* ociEnv=0; /* the environment handle */ 
OCISvcCtx* ociHdbc=0; /* the context handle */
OCIServer *mysrvhp=0; /* the server handle */
OCISession *myusrhp=0; /* user session handle */

// chan tdo's
OCIType* ecal_chan_typ_tdo = (OCIType*) 0;
OCIType* hcal_chan_typ_tdo = (OCIType*) 0;
OCIType* ecal_pmcalib_typ_tdo = (OCIType*) 0;
OCIType* ecal_pmcalib_list_tdo = (OCIType*) 0;
OCIType* hcal_pmcalib_typ_tdo = (OCIType*) 0;
OCIType* hcal_pmcalib_list_tdo = (OCIType*) 0;
OCIType* dac_setting_typ_tdo = (OCIType*) 0;
OCIType* dac_setting_list_tdo = (OCIType*) 0;
OCIType* adc_setting_typ_tdo = (OCIType*) 0;
OCIType* adc_setting_list_tdo = (OCIType*) 0;
OCIType* hcal_cscalib_typ_tdo = (OCIType*) 0;
OCIType* hcal_cscalib_list_tdo = (OCIType*) 0;
OCIType* tile_curr_typ_tdo = (OCIType*) 0;
OCIType* tile_curr_list_tdo = (OCIType*) 0;
OCIType* pmgain_pnt_typ_tdo = (OCIType*) 0;
OCIType* pmgain_pnt_list_tdo = (OCIType*) 0;
// ledpin tdo's
OCIType* hcal_ledpin_typ_tdo = (OCIType*) 0;
OCIType* ecal_led_typ_tdo = (OCIType*) 0;
OCIType* ecal_pin_typ_tdo = (OCIType*) 0;
OCIType* ledpin_calib_typ_tdo = (OCIType*) 0;
OCIType* ledpin_calib_list_tdo = (OCIType*) 0;
OCIType* ledpin_pnt_typ_tdo = (OCIType*) 0;
OCIType* ledpin_pnt_list_tdo = (OCIType*) 0;
// more ledpin tdo's
OCIType* ecal_ledpin_calib_typ_tdo = (OCIType*) 0;
OCIType* ecal_ledpin_calib_list_tdo = (OCIType*) 0;
OCIType* leddac_setting_typ_tdo = (OCIType*) 0;
OCIType* leddac_setting_list_tdo = (OCIType*) 0;
OCIType* ledtsb_setting_typ_tdo = (OCIType*) 0;
OCIType* ledtsb_setting_list_tdo = (OCIType*) 0;
OCIType* hcal_ledpinref_typ_tdo = (OCIType*) 0;
OCIType* hcal_ledpinref_list_tdo = (OCIType*) 0;
OCIType* hcal_module_typ_tdo = (OCIType*) 0;
// Oleg's Ecal tdo's
// OCIType* ecal_module_item_tdo = (OCIType*) 0;
// OCIType* ecal_chan_item_tdo = (OCIType*) 0;
// OCIType* ecal_ldb_item_tdo = (OCIType*) 0;
OCIType* box_module_item_tdo = (OCIType*) 0;
OCIType* box_chan_item_tdo = (OCIType*) 0;
OCIType* ecal_chan_lst_tdo = (OCIType*) 0;
OCIType* box_chan_lst_tdo = (OCIType*) 0;
// Oleg's PMT tdo's
OCIType* pmt_item_tdo = (OCIType*) 0;
OCIType* pmt_res_item_tdo = (OCIType*) 0;
OCIType* pmt_val_tdo = (OCIType*) 0;
OCIType* cw_item_tdo = (OCIType*) 0;
OCIType* pmt_lst_res_tdo = (OCIType*) 0;
OCIType* pmt_val_tab_tdo = (OCIType*) 0;



EXTERN_CALODB int ECALConnByCell(char type, int x, int y, int &dac, int &cwpow, int &adc, char* PMname, char* erm){
  if(!m_connected){strcpy(erm, "ECALConnByCell: not connected");return(-1);}
  erm[0]=(char)0;
  
  sword status;
  
  OCIStmt* stmtp;
  
  char tip;
  sword ix,iy,idac,ipow,iadc;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  char pmnam[20];
  
  char sqlst[]="SELECT ch.dac_chan, ch.cw_pow_conn, ch.adc_chan, cal.pmt_id FROM ecal_chan_otb ch, TABLE(ch.pm_calibs) cal WHERE ch.chan_type=:1 AND ch.chan_x=:2 AND ch.chan_y=:3 AND cal.start_v = (SELECT MAX(cal1.start_v) FROM TABLE(ch.pm_calibs) cal1)";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmtp, ociError, (text*) sqlst,(ub4) strlen(sqlst),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByCell: Error preparing stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIBindByPos(stmtp, &bndp[0], ociError, (ub4)1, (dvoid*)&tip, (sb4)sizeof(tip), (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByCell: Error binding tip to stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIBindByPos(stmtp, &bndp[1], ociError, (ub4)2, (dvoid*)&ix, (sb4)sizeof(ix), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByCell: Error binding ix to stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIBindByPos(stmtp, &bndp[2], ociError, (ub4)3, (dvoid*)&iy, (sb4)sizeof(iy), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByCell: Error binding iy to stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[0], ociError, (ub4)1, (dvoid*)&idac, (sb4)sizeof(idac), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByCell: Error defining idac in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[1], ociError, (ub4)2, (dvoid*)&ipow, (sb4)sizeof(ipow), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByCell: Error defining ipow in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[2], ociError, (ub4)3, (dvoid*)&iadc, (sb4)sizeof(iadc), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByCell: Error defining iadc in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[3], ociError, (ub4)4, (dvoid*)&pmnam[0], (sb4)sizeof(pmnam), SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByCell: Error defining pmnam in stmtp "); checkerr(ociError, status, erm); return -1; }
  
  // get tip, ix, iy
  tip=type;
  ix=x;
  iy=y;
  
  // execute stmtp
  status=OCIStmtExecute(ociHdbc, stmtp, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByCell: Error executing stmtp "); checkerr(ociError, status, erm); return -1; }
  
  int nfound=0;
  while( (status=OCIStmtFetch2(stmtp, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
    ++nfound;
    dac=idac;
    cwpow=ipow;
    adc=iadc;
    sscanf(pmnam,"%s",PMname);
    
  }
  //status=OCIHandleFree((dvoid *) stmtp, (ub4) OCI_HTYPE_STMT);
  status=OCIStmtRelease(stmtp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  if(nfound==1)sprintf(erm,"ECALConnByCell: success ");
  else if(nfound==0)sprintf(erm,"ECALConnByCell: cell not found ");
  else if(nfound>1)sprintf(erm,"ECALConnByCell: WARNING: %d cells found ",nfound);
  return nfound;
}

EXTERN_CALODB int ECALConnByPM(char &type, int &x, int &y, int &dac, int &cwpow, int &adc, char* PMname, char* erm){
  if(!m_connected){strcpy(erm, "ECALConnByPM: not connected");return(-1);}
  erm[0]=(char)0;
  
  sword status;
  OCIStmt* stmtp;
  char tip;
  sword ix,iy,idac,ipow,iadc;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  char pmnam[20];
  
  char sqlst[]="SELECT ch.chan_type, ch.chan_x, ch.chan_y, ch.dac_chan, ch.cw_pow_conn, ch.adc_chan FROM ecal_chan_otb ch, TABLE(ch.pm_calibs) cal WHERE cal.pmt_id=:1 AND cal.start_v = (SELECT MAX(cal1.start_v) FROM TABLE(ch.pm_calibs) cal1)";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmtp, ociError, (text*) sqlst,(ub4) strlen(sqlst),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByPM: Error preparing stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIBindByPos(stmtp, &bndp[0], ociError, (ub4)1, (dvoid*)&pmnam[0], (sb4)8*sizeof(char), (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByPM: Error binding pmnam to stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[1], ociError, (ub4)1, (dvoid*)&tip, (sb4)sizeof(tip), SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByPM: Error defining tip in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[2], ociError, (ub4)2, (dvoid*)&ix, (sb4)sizeof(ix), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByPM: Error defining ix in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[3], ociError, (ub4)3, (dvoid*)&iy, (sb4)sizeof(iy), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByPM: Error defining iy in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[4], ociError, (ub4)4, (dvoid*)&idac, (sb4)sizeof(idac), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByPM: Error defining idac in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[5], ociError, (ub4)5, (dvoid*)&ipow, (sb4)sizeof(ipow), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByPM: Error defining ipow in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[6], ociError, (ub4)6, (dvoid*)&iadc, (sb4)sizeof(iadc), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByPM: Error defining iadc in stmtp "); checkerr(ociError, status, erm); return -1; }
  
  // get pmnam
  sscanf(PMname,"%s",pmnam);
  strcat(pmnam,"        ");
  pmnam[8]=(char)0;
  
  // execute stmtp
  status=OCIStmtExecute(ociHdbc, stmtp, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByPM: Error executing stmtp "); checkerr(ociError, status, erm); return -1; }
  
  int nfound=0;
  while( (status=OCIStmtFetch2(stmtp, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
    ++nfound;
    type=tip;
    x=ix;
    y=iy;
    dac=idac;
    cwpow=ipow;
    adc=iadc;
  }
  //status=OCIHandleFree((dvoid *) stmtp, (ub4) OCI_HTYPE_STMT);
  status=OCIStmtRelease(stmtp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  if(nfound==1)sprintf(erm,"ECALConnByPM: success ");
  else if(nfound==0)sprintf(erm,"ECALConnByPM: cell not found ");
  else if(nfound>1)sprintf(erm,"ECALConnByPM: WARNING: %d cells found ",nfound);
  return nfound;
}

EXTERN_CALODB int ECALConnByDAC(char &type, int &x, int &y, int dac, int &cwpow, int &adc, char* PMname, char* erm){
  if(!m_connected){strcpy(erm, "ECALConnByDAC: not connected");return(-1);}
  erm[0]=(char)0;
  
  sword status;
  OCIStmt* stmtp;
  char tip;
  sword ix,iy,idac,ipow,iadc;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  char pmnam[20];
  
  char sqlst[]="SELECT ch.chan_type, ch.chan_x, ch.chan_y, ch.cw_pow_conn, ch.adc_chan, cal.pmt_id FROM ecal_chan_otb ch, table(ch.pm_calibs) cal WHERE ch.dac_chan=:1 AND cal.start_v = (SELECT MAX(cal1.start_v) FROM TABLE(ch.pm_calibs) cal1)";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmtp, ociError, (text*) sqlst,(ub4) strlen(sqlst),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByDAC: Error preparing stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIBindByPos(stmtp, &bndp[0], ociError, (ub4)1, (dvoid*)&idac, (sb4)sizeof(idac), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByDAC: Error binding idac to stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[1], ociError, (ub4)1, (dvoid*)&tip, (sb4)sizeof(tip), SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByDAC: Error defining tip in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[2], ociError, (ub4)2, (dvoid*)&ix, (sb4)sizeof(ix), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByDAC: Error defining ix in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[3], ociError, (ub4)3, (dvoid*)&iy, (sb4)sizeof(iy), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByDAC: Error defining iy in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[4], ociError, (ub4)4, (dvoid*)&ipow, (sb4)sizeof(ipow), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByDAC: Error defining ipow in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[5], ociError, (ub4)5, (dvoid*)&iadc, (sb4)sizeof(iadc), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByDAC: Error defining iadc in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[6], ociError, (ub4)6, (dvoid*)&pmnam[0], (sb4)sizeof(pmnam), SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByDAC: Error defining pmnam in stmtp "); checkerr(ociError, status, erm); return -1; }
  
  // get DAC
  idac=dac;
  // execute stmtp
  status=OCIStmtExecute(ociHdbc, stmtp, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByDAC: Error executing stmtp "); checkerr(ociError, status, erm); return -1; }
  
  int nfound=0;
  while( (status=OCIStmtFetch2(stmtp, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
    ++nfound;
    type=tip;
    x=ix;
    y=iy;
    cwpow=ipow;
    adc=iadc;
    sscanf(pmnam,"%s",PMname);
  }
  status=OCIStmtRelease(stmtp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  if(nfound==1)sprintf(erm,"ECALConnByDAC: success ");
  else if(nfound==0)sprintf(erm,"ECALConnByDAC: cell not found ");
  else if(nfound>1)sprintf(erm,"ECALConnByDAC: WARNING: %d cells found ",nfound);
  return nfound;
}

EXTERN_CALODB int ECALConnByADC(char &type, int &x, int &y, int &dac, int &cwpow, int adc, char* PMname, char* erm){
  if(!m_connected){strcpy(erm, "ECALConnByADC: not connected");return(-1);}
  erm[0]=(char)0;
  
  sword status;
  OCIStmt* stmtp;
  char tip;
  sword ix,iy,idac,ipow,iadc;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  char pmnam[20];
  
  char sqlst[]="SELECT ch.chan_type, ch.chan_x, ch.chan_y, ch.dac_chan, ch.cw_pow_conn, cal.pmt_id FROM ecal_chan_otb ch, table(ch.pm_calibs) cal WHERE ch.adc_chan=:1 AND cal.start_v = (SELECT MAX(cal1.start_v) FROM TABLE(ch.pm_calibs) cal1)";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmtp, ociError, (text*) sqlst,(ub4) strlen(sqlst),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByADC: Error preparing stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIBindByPos(stmtp, &bndp[0], ociError, (ub4)1, (dvoid*)&iadc, (sb4)sizeof(iadc), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByADC: Error binding iadc to stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[1], ociError, (ub4)1, (dvoid*)&tip, (sb4)sizeof(tip), SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByADC: Error defining tip in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[2], ociError, (ub4)2, (dvoid*)&ix, (sb4)sizeof(ix), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByADC: Error defining ix in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[3], ociError, (ub4)3, (dvoid*)&iy, (sb4)sizeof(iy), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByADC: Error defining iy in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[4], ociError, (ub4)4, (dvoid*)&idac, (sb4)sizeof(idac), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByADC: Error defining idac in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[5], ociError, (ub4)5, (dvoid*)&ipow, (sb4)sizeof(ipow), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByADC: Error defining ipow in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[6], ociError, (ub4)6, (dvoid*)&pmnam[0], (sb4)sizeof(pmnam), SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByADC: Error defining pmnam in stmtp "); checkerr(ociError, status, erm); return -1; }
  
  // get ADC
  iadc=adc;
  // execute stmtp
  status=OCIStmtExecute(ociHdbc, stmtp, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnByADC: Error executing stmtp "); checkerr(ociError, status, erm); return -1; }
  
  int nfound=0;
  while( (status=OCIStmtFetch2(stmtp, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
    ++nfound;
    type=tip;
    x=ix;
    y=iy;
    cwpow=ipow;
    dac=idac;
    sscanf(pmnam,"%s",PMname);
  }
  if(status==OCI_ERROR){sprintf(erm,"ECALConnByADC: Error fetching data "); checkerr(ociError, status, erm);  }
  status=OCIStmtRelease(stmtp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  if(nfound==1)sprintf(erm,"ECALConnByADC: success ");
  else if(nfound==0)sprintf(erm,"ECALConnByADC: cell not found ");
  else if(nfound>1)sprintf(erm,"ECALConnByADC: WARNING: %d cells found ",nfound);
  return nfound;
}

EXTERN_CALODB int ECALGetCell(int Search, char &type, int &x, int &y, int &dac, int &cwpow, int &adc, char* PMname, char* erm){
  if(!m_connected){strcpy(erm, "ECALGetCell: not connected");return(-1);}
  erm[0]=(char)0;
  //
  // Search - search item:
  //   1 - (type, x, y)
  //   2 - iDAC
  //   3 - iADC
  //   4 - PMname
  //
  //   It invokes the PL/SQL procedure ecal_cell(isearch, type, x, y, idac, ipow, iadc, pm)
  //   where isearch means the same as Search 
  //	           
  
  sword status;
  OCIStmt* stmtp;
  int j=0;
  sword isearch;
  char tip;
  sword ix, iy, idac, ipow, iadc;
  char pmnam[20];
  
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  
  if(1==Search){ 
    isearch=1;
    tip=type;
    ix=x;
    iy=y;
  }else if(2==Search){
    isearch=2;
    idac=dac;
  }else if(3==Search){
    isearch=3;
    iadc=adc;
  }else if(4==Search){
    isearch=4;
    strncpy(pmnam,PMname,20);
    pmnam[19]='\0';
    j=strlen(pmnam);
    for(int i=j; i<19; ++i)pmnam[i]=' ';
  }else isearch=0;
  
  if(0==isearch){sprintf(erm, "ECALGetCell: search option %d: not implemented yet",Search); return(-1);}
  
  char sqlst[]="BEGIN ecal_cell(:1, :2, :3, :4, :5, :6, :7, :8); END;";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmtp, ociError, (text*) sqlst,(ub4) strlen(sqlst),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetCell: Error preparing stmtp "); checkerr(ociError, status, erm); return -1; }
  
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[1], ociError, (ub4)1, (dvoid*)&isearch, (sb4)sizeof(isearch), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[2], ociError, (ub4)2, (dvoid*)&tip,     (sb4)sizeof(tip),     (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[3], ociError, (ub4)3, (dvoid*)&ix,      (sb4)sizeof(ix),      (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[4], ociError, (ub4)4, (dvoid*)&iy,      (sb4)sizeof(iy),      (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[5], ociError, (ub4)5, (dvoid*)&idac,    (sb4)sizeof(idac),    (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[6], ociError, (ub4)6, (dvoid*)&ipow,    (sb4)sizeof(ipow),    (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[7], ociError, (ub4)7, (dvoid*)&iadc,    (sb4)sizeof(iadc),    (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[8], ociError, (ub4)8, (dvoid*)pmnam,    (sb4)8*sizeof(char),  (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetCell: Error binding stmtp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmtp
  status=OCIStmtExecute(ociHdbc, stmtp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetCell: Error executing stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmtp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  if(isearch<0){sprintf(erm,"ECALGetCell: not found "); return 0; }
  
  // copy info
  if(Search!=1){
    type=tip;
    x=ix;
    y=iy;
  }
  if(Search!=2)dac=idac;
  cwpow=ipow;
  if(Search!=3)adc=iadc;
  if(Search!=4){
    pmnam[8]=' ';
    pmnam[9]='\0';
    sscanf(pmnam,"%s",PMname);
  }
  
  sprintf(erm,"ECALGetCell: success ");
  return 1;
}

EXTERN_CALODB int ECALCellsAtPOW(char *type, int *x, int *y, int *dac, int cwpow, int *adc, char* PMname, int PMname_len, char* erm){
  if(!m_connected){strcpy(erm, "ECALCellsAtPOW: not connected");return(-1);}
  erm[0]=(char)0;
  
  sword status;
  OCIStmt* stmtp;
  char tip;
  sword ix,iy,idac,ipow,iadc;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  char pmnam[20];
  
  char sqlst[]="SELECT ch.chan_type, ch.chan_x, ch.chan_y, ch.dac_chan, ch.adc_chan, cal.pmt_id FROM ecal_chan_otb ch, table(ch.pm_calibs) cal WHERE ch.cw_pow_conn=:1 AND cal.start_v = (SELECT MAX(cal1.start_v) FROM TABLE(ch.pm_calibs) cal1) ORDER BY ch.dac_chan";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmtp, ociError, (text*) sqlst,(ub4) strlen(sqlst),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALCellsAtPOW: Error preparing stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIBindByPos(stmtp, &bndp[0], ociError, (ub4)1, (dvoid*)&ipow, (sb4)sizeof(ipow), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALCellsAtPOW: Error binding iadc to stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[1], ociError, (ub4)1, (dvoid*)&tip, (sb4)sizeof(tip), SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALCellsAtPOW: Error defining tip in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[2], ociError, (ub4)2, (dvoid*)&ix, (sb4)sizeof(ix), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALCellsAtPOW: Error defining ix in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[3], ociError, (ub4)3, (dvoid*)&iy, (sb4)sizeof(iy), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALCellsAtPOW: Error defining iy in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[4], ociError, (ub4)4, (dvoid*)&idac, (sb4)sizeof(idac), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALCellsAtPOW: Error defining idac in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[5], ociError, (ub4)5, (dvoid*)&iadc, (sb4)sizeof(iadc), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALCellsAtPOW: Error defining iadc in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[6], ociError, (ub4)6, (dvoid*)&pmnam[0], (sb4)sizeof(pmnam), SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALCellsAtPOW: Error defining pmnam in stmtp "); checkerr(ociError, status, erm); return -1; }
  
  // get POW
  ipow=cwpow;
  // execute stmtp
  status=OCIStmtExecute(ociHdbc, stmtp, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALCellsAtPOW: Error executing stmtp "); checkerr(ociError, status, erm); return -1; }
  
  int nfound=0;
  while( (status=OCIStmtFetch2(stmtp, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
    type[nfound]=tip;
    x[nfound]=ix;
    y[nfound]=iy;
    dac[nfound]=idac;
    adc[nfound]=iadc;
    sscanf(pmnam,"%s",&PMname[nfound*PMname_len]);
    ++nfound;
  }
  status=OCIStmtRelease(stmtp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  if(nfound>0)sprintf(erm,"ECALCellsAtPOW: success ");
  else if(nfound==0)sprintf(erm,"ECALCellsAtPOW: WARNING: no cells found ");
  return nfound;
}

EXTERN_CALODB int ECALSetDACUnapplied(int* d1, int* d2, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALSetDACUnapplied: not connected");return(-1);}
  
  sword status;
  OCIStmt* stmthp;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  
  // dates
  OCIDate date1; 
  OCIDateSetDate((OCIDate*)&date1, (sb2) d1[0], (ub1) d1[1], (ub1) d1[2]); 
  OCIDateSetTime((OCIDate*)&date1, d1[3],d1[4],d1[5]);
  OCIDate date2; 
  if(d2[0]>0){
    OCIDateSetDate((OCIDate*)&date2, (sb2) d2[0], (ub1) d2[1], (ub1) d2[2]); 
    OCIDateSetTime((OCIDate*)&date2, (ub1) d2[3], (ub1) d2[4], (ub1) d2[5]);
  }else{
    status=OCIDateSysDate(ociError, &date2);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetDACUnapplied: Error getting SysDate "); checkerr(ociError, status, erm); return -1; }
  }
  
  char sqlECAL[]="UPDATE /*+NESTED_TABLE_GET_REFS*/ dac_setting_otb SET d_applied=NULL WHERE d_set>:1 AND d_set<:2";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetDACUnapplied: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  // bind
  status=OCIBindByPos(stmthp, &bndp[0], ociError, (ub4)1, (dvoid*) &date1, (sb4) sizeof(OCIDate) , (ub2) SQLT_ODT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stmthp, &bndp[1], ociError, (ub4)2, (dvoid*) &date2, (sb4) sizeof(OCIDate), (ub2) SQLT_ODT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetDACUnapplied: Error binding stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetDACUnapplied: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALSetDACUnapplied: success ");
  return 0;
}

EXTERN_CALODB int ECALSetADCUnapplied(int* d1, int* d2, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALSetADCUnapplied: not connected");return(-1);}
  
  sword status;
  OCIStmt* stmthp;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  
  // dates
  OCIDate date1; 
  OCIDateSetDate((OCIDate*)&date1, (sb2) d1[0], (ub1) d1[1], (ub1) d1[2]); 
  OCIDateSetTime((OCIDate*)&date1, d1[3],d1[4],d1[5]);
  OCIDate date2; 
  if(d2[0]>0){
    OCIDateSetDate((OCIDate*)&date2, (sb2) d2[0], (ub1) d2[1], (ub1) d2[2]); 
    OCIDateSetTime((OCIDate*)&date2, (ub1) d2[3], (ub1) d2[4], (ub1) d2[5]);
  }else{
    status=OCIDateSysDate(ociError, &date2);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetADCUnapplied: Error getting SysDate "); checkerr(ociError, status, erm); return -1; }
  }
  
  char sqlECAL[]="UPDATE /*+NESTED_TABLE_GET_REFS*/ adc_setting_otb SET d_applied=NULL WHERE d_set>:1 AND d_set<:2";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetADCUnapplied: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  // bind
  status=OCIBindByPos(stmthp, &bndp[0], ociError, (ub4)1, (dvoid*) &date1, (sb4) sizeof(OCIDate) , (ub2) SQLT_ODT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stmthp, &bndp[1], ociError, (ub4)2, (dvoid*) &date2, (sb4) sizeof(OCIDate), (ub2) SQLT_ODT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetADCUnapplied: Error binding stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetADCUnapplied: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALSetADCUnapplied: success ");
  return 0;
}

EXTERN_CALODB int ECALSetDACApplied(char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALSetDACApplied: not connected");return(-1);}
  
  sword status;
  OCIStmt* stmthp;
  
  char sqlECAL[]="BEGIN ecal_timestamp_alldac(); END;"; //char sqlECAL[]="UPDATE /*+NESTED_TABLE_GET_REFS*/ dac_setting_otb SET d_applied=SYSDATE WHERE d_applied IS NULL";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetDACApplied: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetDACApplied: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALSetDACApplied: success ");
  return 0;
}

EXTERN_CALODB int ECALSetADCApplied(char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALSetADCApplied: not connected");return(-1);}
  
  sword status;
  OCIStmt* stmthp;
  
  char sqlECAL[]="BEGIN ecal_timestamp_alladc(); END;"; //char sqlECAL[]="UPDATE /*+NESTED_TABLE_GET_REFS*/ adc_setting_otb SET d_applied=SYSDATE WHERE d_applied IS NULL";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetADCApplied: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetADCApplied: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALSetADCApplied: success ");
  return 0;
}

EXTERN_CALODB int ECALClearDACUpds(char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALClearDACUpds: not connected");return(-1);}
  
  sword status;
  OCIStmt* stmthp;
  
  char sqlECAL[]="UPDATE ecal_chan_otb ch SET ch.dac_settings = CAST ( MULTISET( SELECT * FROM TABLE(ch.dac_settings) se WHERE se.d_applied IS NOT NULL) AS dac_setting_list) WHERE EXISTS (SELECT * FROM TABLE(ch.dac_settings) se1 WHERE se1.d_applied IS NULL)";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALClearDACUpds: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALClearDACUpds: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALClearDACUpds: success ");
  return 0;
}

EXTERN_CALODB int ECALClearADCUpds(char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALClearADCUpds: not connected");return(-1);}
  
  sword status;
  OCIStmt* stmthp;
  
  char sqlECAL[]="UPDATE ecal_chan_otb ch SET ch.adc_settings = CAST ( MULTISET( SELECT * FROM TABLE(ch.adc_settings) se WHERE se.d_applied IS NOT NULL) AS adc_setting_list) WHERE EXISTS (SELECT * FROM TABLE(ch.adc_settings) se1 WHERE se1.d_applied IS NULL)";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALClearADCUpds: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALClearADCUpds: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALClearADCUpds: success ");
  return 0;
}

EXTERN_CALODB int ECALStoreDACSettings(char* type, int* x, int* y, double* HVphys, double* HVstdb, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALStoreDACSettings: not connected");return(-1);}
  
  sword status;
  OCIDate maxd; OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
  OCIDate appd; OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
  boolean appd_null=FALSE;
  
  OCIStmt* stECALchan;
  OCIStmt* stECALinss;
  OCIStmt* stECALupds;
  int ich=0, nch=0;
  char tip;
  sword ix,iy;
  double hvph=0, hvst=0;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine *defn1p = (OCIDefine *) 0;
  dac_setting_list* dac_settings = (dac_setting_list*) 0;
  ub4 dac_settings_size=0;
  dac_setting_typ*      setng = (dac_setting_typ*) 0;
  dac_setting_typ_ind*  setng_ind = (dac_setting_typ_ind*)0;
  boolean exists=FALSE;
  // stECALchan
  char sqlECALchan[]="SELECT t.dac_settings FROM ecal_chan_otb t WHERE t.chan_type=:1 AND t.chan_x=:2 AND t.chan_y=:3";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALchan, ociError, (text*) sqlECALchan,(ub4) strlen(sqlECALchan),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreDACSettings: Error preparing stECALchan "); checkerr(ociError, status, erm); return -1; }
  
  status=OCIBindByPos(stECALchan, &bndp[0], ociError, (ub4)1, (dvoid*) &tip, (sb4) sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALchan, &bndp[1], ociError, (ub4)2, (dvoid*) & ix, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALchan, &bndp[2], ociError, (ub4)3, (dvoid*) & iy, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreDACSettings: Error binding stECALchan "); checkerr(ociError, status, erm); return -1; }
  // stECALinss
  char sqlECALinss[]="INSERT INTO TABLE (SELECT ch.dac_settings FROM ecal_chan_otb ch WHERE ch.chan_type=:1 AND ch.chan_x=:2 AND ch.chan_y=:3 ) VALUES(SYSDATE, NULL, :4, :5)";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALinss, ociError, (text*) sqlECALinss,(ub4) strlen(sqlECALinss),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreDACSettings: Error preparing stECALinss "); checkerr(ociError, status, erm); return -1; }
  
  status=OCIBindByPos(stECALinss, &bndp[3], ociError, (ub4)1, (dvoid*) &tip, (sb4) sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALinss, &bndp[4], ociError, (ub4)2, (dvoid*) & ix,   (sb4) sizeof(sword),  (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALinss, &bndp[5], ociError, (ub4)3, (dvoid*) & iy,   (sb4) sizeof(sword),  (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALinss, &bndp[6], ociError, (ub4)4, (dvoid*) & hvph, (sb4) sizeof(double), (ub2) SQLT_FLT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALinss, &bndp[7], ociError, (ub4)5, (dvoid*) & hvst, (sb4) sizeof(double), (ub2) SQLT_FLT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreDACSettings; Error binding stECALinss "); checkerr(ociError, status, erm); return -1; }
  // stECALupds
  char sqlECALupds[]="UPDATE TABLE (SELECT ch.dac_settings FROM ecal_chan_otb ch WHERE ch.chan_type=:1 AND ch.chan_x=:2 AND ch.chan_y=:3 ) r SET r.d_set=SYSDATE, r.hv_phys=:4, r.hv_stdb=:5 WHERE r.d_applied IS NULL";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALupds, ociError, (text*) sqlECALupds,(ub4) strlen(sqlECALupds),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreDACSettings: Error preparing stECALupds "); checkerr(ociError, status, erm); return -1; }
  
  status=OCIBindByPos(stECALupds, &bndp[8], ociError, (ub4)1, (dvoid*) &tip, (sb4) sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALupds, &bndp[9],  ociError, (ub4)2, (dvoid*) & ix,   (sb4) sizeof(sword),  (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALupds, &bndp[10], ociError, (ub4)3, (dvoid*) & iy,   (sb4) sizeof(sword),  (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALupds, &bndp[11], ociError, (ub4)4, (dvoid*) & hvph, (sb4) sizeof(double), (ub2) SQLT_FLT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALupds, &bndp[12], ociError, (ub4)5, (dvoid*) & hvst, (sb4) sizeof(double), (ub2) SQLT_FLT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreDACSettings: Error binding stECALupds "); checkerr(ociError, status, erm); return -1; }
  
  while(type[ich]=='O' || type[ich]=='I' || type[ich]=='M'){
    if(ECALCellExists(type[ich],x[ich],y[ich])){
      // instantiate the collection 
      status=OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_TABLE, (OCIType *) dac_setting_list_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &dac_settings);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreDACSettings: Error creating dac_settings instance "); checkerr(ociError, status, erm); return -1; }
      
      status=OCIDefineByPos(stECALchan, &defn1p, ociError, (ub4) 1, (dvoid *) 0, (sb4) 0, SQLT_NTY, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
      if(status==OCI_SUCCESS) 
	status=OCIDefineObject(defn1p, ociError, dac_setting_list_tdo, (dvoid**)&dac_settings, &dac_settings_size, (dvoid**)0, (ub4*)0 );
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreDACSettings: Error defining dac_settings "); checkerr(ociError, status, erm); return -1; }
      
      // set bound variables
      tip=type[ich];
      ix=x[ich];
      iy=y[ich];
      hvph=HVphys[ich];
      hvst=HVstdb[ich];
      // execute stmt
      status=OCIStmtExecute(ociHdbc, stECALchan, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreDACSettings: Error executing stECALchan "); checkerr(ociError, status, erm); return -1; }
      
      appd_null=FALSE;
      if( (status=OCIStmtFetch2(stECALchan, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	sb4 tsiz=0;
	status=OCICollSize(ociEnv, ociError, (OCIColl*)dac_settings, &tsiz); 
	OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
	OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
	for(int i=0; i<tsiz; ++i){
	  OCICollGetElem(ociEnv, ociError, (OCIColl*)dac_settings, (sb4)i, &exists, (dvoid**)&setng, (dvoid**)&setng_ind);
	  sword res=0;
	  OCIDateCompare(ociError, &maxd, &(setng->d_set), &res);
	  if(res<0){
	    status=OCIDateAssign(ociError, &(setng->d_set), &maxd);
	    if(setng_ind->d_applied==OCI_IND_NULL)appd_null=TRUE;
	    else{appd_null=FALSE; status=OCIDateAssign(ociError, &(setng->d_applied), &appd); }
	  }
	}
      }
      status=OCIObjectFree(ociEnv, ociError, dac_settings, OCI_OBJECTFREE_FORCE);
      if(appd_null){
	status=OCIStmtExecute(ociHdbc, stECALupds, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreDACSettings: Error executing stECALupds "); checkerr(ociError, status, erm); return -1; }
      }else{
	status=OCIStmtExecute(ociHdbc, stECALinss, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreDACSettings: Error executing stECALinss "); checkerr(ociError, status, erm); return -1; }
      }
      ++nch;
    }
    ++ich;
  }
  status=OCIStmtRelease(stECALchan, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  status=OCIStmtRelease(stECALupds, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  status=OCIStmtRelease(stECALinss, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALStoreDACSettings: success %d chan out of %d", nch, ich);
  return nch;
}

EXTERN_CALODB int ECALStoreADCSettings(char* type, int* x, int* y, double* delay, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALStoreADCSettings: not connected");return(-1);}
  
  sword status;
  OCIDate maxd; OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
  OCIDate appd; OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
  boolean appd_null=FALSE;
  OCIStmt* stECALchan;
  OCIStmt* stECALinss;
  OCIStmt* stECALupds;
  int ich=0, nch=0;
  char tip;
  sword ix,iy,   del;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine *defn1p = (OCIDefine *) 0;
  adc_setting_list* adc_settings = (adc_setting_list*) 0;
  ub4 adc_settings_size=0;
  adc_setting_typ*      setng = (adc_setting_typ*) 0;
  adc_setting_typ_ind*  setng_ind = (adc_setting_typ_ind*)0;
  boolean exists=FALSE;
  // stECALchan
  char sqlECALchan[]="SELECT t.adc_settings FROM ecal_chan_otb t WHERE t.chan_type=:1 AND t.chan_x=:2 AND t.chan_y=:3";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALchan, ociError, (text*) sqlECALchan,(ub4) strlen(sqlECALchan),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreADCSettings: Error preparing stECALchan "); checkerr(ociError, status, erm); return -1; }
  
  status=OCIBindByPos(stECALchan, &bndp[0], ociError, (ub4)1, (dvoid*) &tip, (sb4) sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALchan, &bndp[1], ociError, (ub4)2, (dvoid*) & ix, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALchan, &bndp[2], ociError, (ub4)3, (dvoid*) & iy, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreADCSettings: Error binding stECALchan "); checkerr(ociError, status, erm); return -1; }
  // stECALinss
  char sqlECALinss[]="INSERT INTO TABLE (SELECT ch.adc_settings FROM ecal_chan_otb ch WHERE ch.chan_type=:1 AND ch.chan_x=:2 AND ch.chan_y=:3 ) VALUES(SYSDATE, NULL, :4)";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALinss, ociError, (text*) sqlECALinss,(ub4) strlen(sqlECALinss),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreADCSettings: Error preparing stECALinss "); checkerr(ociError, status, erm); return -1; }
  
  status=OCIBindByPos(stECALinss, &bndp[3], ociError, (ub4)1, (dvoid*) &tip, (sb4) sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALinss, &bndp[4], ociError, (ub4)2, (dvoid*) & ix,  (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALinss, &bndp[5], ociError, (ub4)3, (dvoid*) & iy,  (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALinss, &bndp[6], ociError, (ub4)4, (dvoid*) & del, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreADCSettings: Error binding stECALinss "); checkerr(ociError, status, erm); return -1; }
  // stECALupds
  char sqlECALupds[]="UPDATE TABLE (SELECT ch.adc_settings FROM ecal_chan_otb ch WHERE ch.chan_type=:1 AND ch.chan_x=:2 AND ch.chan_y=:3 ) r SET r.d_set=SYSDATE, r.delay_adc=:4 WHERE r.d_applied IS NULL";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALupds, ociError, (text*) sqlECALupds,(ub4) strlen(sqlECALupds),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreADCSettings: Error preparing stECALupds "); checkerr(ociError, status, erm); return -1; }
  
  if(status==OCI_SUCCESS)status=OCIBindByPos(stECALupds, &bndp[ 8], ociError, (ub4)1, (dvoid*)&tip, (sb4) sizeof(char) , (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stECALupds, &bndp[ 9], ociError, (ub4)2, (dvoid*) &ix, (sb4) sizeof(sword), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stECALupds, &bndp[10], ociError, (ub4)3, (dvoid*) &iy, (sb4) sizeof(sword), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stECALupds, &bndp[11], ociError, (ub4)4, (dvoid*)&del, (sb4) sizeof(sword), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreADCSettings: Error binding stECALupds "); checkerr(ociError, status, erm); return -1; }
  
  while(type[ich]=='O' || type[ich]=='I' || type[ich]=='M'){
    if(ECALCellExists(type[ich],x[ich],y[ich]) && fabs(delay[ich])<99){
      // instantiate the collection 
      status=OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_TABLE, (OCIType *) adc_setting_list_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &adc_settings);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreADCSettings: Error creating adc_settings instance "); checkerr(ociError, status, erm); return -1; }
      
      status=OCIDefineByPos(stECALchan, &defn1p, ociError, (ub4) 1, (dvoid *) 0, (sb4) 0, SQLT_NTY, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
      if(status==OCI_SUCCESS) 
	status=OCIDefineObject(defn1p, ociError, adc_setting_list_tdo, (dvoid**)&adc_settings, &adc_settings_size, (dvoid**)0, (ub4*)0 );
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreADCSettings: Error defining adc_settings "); checkerr(ociError, status, erm); return -1; }
      
      // set bound variables
      tip=type[ich];
      ix=x[ich];
      iy=y[ich];
      //del=delay[ich];
      del=(sword)floor(delay[ich]*1e3);  // double delay is stored as (integer) number of picoseconds 
      // execute stmt
      status=OCIStmtExecute(ociHdbc, stECALchan, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreADCSettings: Error executing stECALchan "); checkerr(ociError, status, erm); return -1; }
      
      appd_null=FALSE;
      if( (status=OCIStmtFetch2(stECALchan, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	sb4 tsiz=0;
	status=OCICollSize(ociEnv, ociError, (OCIColl*)adc_settings, &tsiz); 
	OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
	OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
	for(int i=0; i<tsiz; ++i){
	  OCICollGetElem(ociEnv, ociError, (OCIColl*)adc_settings, (sb4)i, &exists, (dvoid**)&setng, (dvoid**)&setng_ind);
	  sword res=0;
	  OCIDateCompare(ociError, &maxd, &(setng->d_set), &res);
	  if(res<0){
	    status=OCIDateAssign(ociError, &(setng->d_set), &maxd);
	    if(setng_ind->d_applied==OCI_IND_NULL)appd_null=TRUE;
	    else{appd_null=FALSE; status=OCIDateAssign(ociError, &(setng->d_applied), &appd); }
	  }
	}
      }
      status=OCIObjectFree(ociEnv, ociError, adc_settings, OCI_OBJECTFREE_FORCE);
      if(appd_null){
	status=OCIStmtExecute(ociHdbc, stECALupds, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreADCSettings: Error executing stECALupds "); checkerr(ociError, status, erm); return -1; }
      }else{
	status=OCIStmtExecute(ociHdbc, stECALinss, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreADCSettings: Error executing stECALinss "); checkerr(ociError, status, erm); return -1; }
      }
      ++nch;
    }
    ++ich;
  }
  status=OCIStmtRelease(stECALchan, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  status=OCIStmtRelease(stECALupds, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  status=OCIStmtRelease(stECALinss, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALStoreADCSettings: success %d chan out of %d", nch, ich);
  return nch;
}

EXTERN_CALODB int ECALGetChanADC(int opt, char* type, int* x, int* y, int* adc, double* delay, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALGetChanADC: not connected");return(-1);}
  
  if( !(opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  if( !(opt & M_GetNew) && (opt & M_MarkNew) ) opt &= ~M_MarkNew;
  
  bool allCells=true;
  if( ('O'==type[0]) || ('M'==type[0]) || ('I'==type[0]) ) allCells=false;
  if(!allCells && (opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  
  bool getOld  = (opt & M_GetOld)  ? true:false;
  bool getNew  = (opt & M_GetNew)  ? true:false;
  bool markNew = (opt & M_MarkNew) ? true:false;
  
  sword status;
  OCIDate maxd; OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
  OCIDate appd; OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
  boolean appd_null=FALSE;
  OCIStmt* stECALchan;
  OCIStmt* stECALappl;
  OCIStmt* stmt_allSetng;
  int ich=0, nch=0;
  char tip;
  sword ix,iy;
  sword iadc=0, del=0;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine *defn1p = (OCIDefine *) 0, *defn2p = (OCIDefine *) 0;//, *defn3p = (OCIDefine *) 0;
  OCIDefine *defn4p = (OCIDefine *) 0, *defn5p = (OCIDefine *) 0, *defn6p = (OCIDefine *) 0;
  OCIDefine *defn7p = (OCIDefine *) 0, *defn8p = (OCIDefine *) 0;//, *defn9p = (OCIDefine *) 0;
  adc_setting_list* adc_settings = (adc_setting_list*) 0;
  ub4 adc_settings_size=0;
  adc_setting_typ*      setng = (adc_setting_typ*) 0;
  adc_setting_typ_ind*  setng_ind = (adc_setting_typ_ind*)0;
  boolean exists=FALSE;
  
  if(markNew){
    char sqlECALappl[]="BEGIN ecal_timestamp_adc(:1, :2, :3); END;"; 
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALappl, ociError, (text*) sqlECALappl,(ub4) strlen(sqlECALappl),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanADC: Error preparing stECALappl "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIBindByPos(stECALappl, &bndp[0], ociError, (ub4)1, (dvoid*) &tip, (sb4) sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALappl, &bndp[1], ociError, (ub4)2, (dvoid*) & ix, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALappl, &bndp[2], ociError, (ub4)3, (dvoid*) & iy, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanADC: Error binding ADC stECALappl "); checkerr(ociError, status, erm); return -1; }
  }
  
  if(!allCells){
    char sqlECALchan[]="SELECT t.adc_settings, t.adc_chan FROM ecal_chan_otb t WHERE t.chan_type=:1 AND t.chan_x=:2 AND t.chan_y=:3";
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALchan, ociError, (text*) sqlECALchan,(ub4) strlen(sqlECALchan),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanADC: Error preparing stECALchan "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIBindByPos(stECALchan, &bndp[3], ociError, (ub4)1, (dvoid*) &tip, (sb4) sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALchan, &bndp[4], ociError, (ub4)2, (dvoid*) & ix, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALchan, &bndp[5], ociError, (ub4)3, (dvoid*) & iy, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanADC: Error binding stECALchan "); checkerr(ociError, status, erm); return -1; }
    
    while(type[ich]=='O' || type[ich]=='I' || type[ich]=='M'){
      if(ECALCellExists(type[ich],x[ich],y[ich])){
	// instantiate the collection 
	status=OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_TABLE, (OCIType *) adc_setting_list_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &adc_settings);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanADC: Error creating adc_settings instance "); checkerr(ociError, status, erm); return -1; }
	
	status=OCIDefineByPos(stECALchan, &defn1p, ociError, (ub4) 1, (dvoid *) 0, (sb4) 0, SQLT_NTY, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
	if(status==OCI_SUCCESS) 
	  status=OCIDefineObject(defn1p, ociError, adc_setting_list_tdo, (dvoid**)&adc_settings, &adc_settings_size, (dvoid**)0, (ub4*)0 );
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanADC: Error defining adc_settings "); checkerr(ociError, status, erm); return -1; }
	
	status=OCIDefineByPos(stECALchan, &defn2p, ociError, (ub4) 2, (dvoid*)&iadc, (sb4)sizeof(iadc), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanADC: Error defining iadc "); checkerr(ociError, status, erm); return -1; }
	
	// set bound variables
	tip=type[ich];
	ix=x[ich];
	iy=y[ich];
	// execute stmt
	status=OCIStmtExecute(ociHdbc, stECALchan, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanADC: Error executing stECALchan "); checkerr(ociError, status, erm); return -1; }
	
	if( (status=OCIStmtFetch2(stECALchan, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	  adc[ich]=iadc;
	  sb4 tsiz=0;
	  status=OCICollSize(ociEnv, ociError, (OCIColl*)adc_settings, &tsiz); 
	  OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
	  OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
	  for(int i=0; i<tsiz; ++i){
	    OCICollGetElem(ociEnv, ociError, (OCIColl*)adc_settings, (sb4)i, &exists, (dvoid**)&setng, (dvoid**)&setng_ind);
	    sword res=0;
	    OCIDateCompare(ociError, &maxd, &(setng->d_set), &res);
	    if(res<0 && (getNew || (setng_ind->d_applied!=OCI_IND_NULL) ) ){
	      status=OCIDateAssign(ociError, &(setng->d_set), &maxd);
	      if(setng_ind->d_applied==OCI_IND_NULL)appd_null=TRUE;
	      else{
		appd_null=FALSE; 
		status=OCIDateAssign(ociError, &(setng->d_applied), &appd); 
	      }
	      del=0; 
	      if(setng_ind->delay_adc!=OCI_IND_NULL) OCINumberToInt(ociError, &(setng->delay_adc), (uword)sizeof(del), OCI_NUMBER_UNSIGNED, (dvoid*)&del);
	      delay[ich]=(double)del/1e3;
	    }
	  }
	  if(tsiz>0 &&appd_null && markNew){
	    //changes=true;
	    status=OCIStmtExecute(ociHdbc, stECALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanADC: Error executing ADC markNew "); checkerr(ociError, status, erm); return -1; }
	  }
	  if(tsiz>0)++nch;
	}
	status=OCIObjectFree(ociEnv, ociError, adc_settings, OCI_OBJECTFREE_FORCE);
      }
      ++ich;
    }
    status=OCIStmtRelease(stECALchan, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }else{
    char sql_allSetng[255];
    if( getNew && !getOld )
      strcpy(sql_allSetng, "SELECT t.adc_settings, t.adc_chan, t.chan_type, t.chan_x, t.chan_y FROM ecal_chan_otb t WHERE EXISTS (SELECT * FROM TABLE(t.adc_settings) se WHERE se.d_applied IS NULL) ORDER BY t.adc_chan ");
    else strcpy(sql_allSetng,"SELECT t.adc_settings, t.adc_chan, t.chan_type, t.chan_x, t.chan_y FROM ecal_chan_otb t ORDER BY t.adc_chan ");
    
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmt_allSetng, ociError, (text*) sql_allSetng,(ub4) strlen(sql_allSetng),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanADC: Error preparing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    // instantiate the collection 
    status=OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_TABLE, (OCIType *) adc_setting_list_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &adc_settings);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanADC: Error creating adc_settings instance "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIDefineByPos(stmt_allSetng, &defn4p, ociError, (ub4) 1, (dvoid *) 0, (sb4) 0, SQLT_NTY, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS)
      status=OCIDefineObject(defn4p, ociError, adc_setting_list_tdo, (dvoid**)&adc_settings, &adc_settings_size, (dvoid**)0, (ub4*)0 );
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanADC: Error defining adc_settings "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIDefineByPos(stmt_allSetng, &defn5p, ociError, (ub4) 2, (dvoid*)&iadc, (sb4)sizeof(iadc), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defn6p, ociError, (ub4) 3, (dvoid*)&tip,  (sb4)sizeof(tip),  SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defn7p, ociError, (ub4) 4, (dvoid*)&ix,   (sb4)sizeof(ix),   SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defn8p, ociError, (ub4) 5, (dvoid*)&iy,   (sb4)sizeof(iy),   SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanADC: Error defining iadc, tip, ix, iy "); checkerr(ociError, status, erm); return -1; }
    
    // execute stmt
    status=OCIStmtExecute(ociHdbc, stmt_allSetng, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanADC: Error executing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    while((status=OCIStmtFetch2(stmt_allSetng, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS || status==OCI_SUCCESS_WITH_INFO ){
      type[nch]=tip;
      x[nch]=ix;
      y[nch]=iy;
      adc[nch]=iadc;
      sb4 tsiz=0;
      status=OCICollSize(ociEnv, ociError, (OCIColl*)adc_settings, &tsiz); 
      OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
      OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
      for(int i=0; i<tsiz; ++i){
	OCICollGetElem(ociEnv, ociError, (OCIColl*)adc_settings, (sb4)i, &exists, (dvoid**)&setng, (dvoid**)&setng_ind);
	sword res=0;
	OCIDateCompare(ociError, &maxd, &(setng->d_set), &res);
	if(res<0 && (getNew || (setng_ind->d_applied!=OCI_IND_NULL) ) ){
	  status=OCIDateAssign(ociError, &(setng->d_set), &maxd);
	  if(setng_ind->d_applied==OCI_IND_NULL)appd_null=TRUE;
	  else{
	    appd_null=FALSE; 
	    status=OCIDateAssign(ociError, &(setng->d_applied), &appd); 
	  }
	  del=0; 
	  if(setng_ind->delay_adc!=OCI_IND_NULL) OCINumberToInt(ociError, &(setng->delay_adc), (uword)sizeof(del), OCI_NUMBER_UNSIGNED, (dvoid*)&del);
	  delay[nch]=(double)del/1e3;
	}
      }
      if(tsiz>0 && appd_null && markNew){
	//changes=true;
	status=OCIStmtExecute(ociHdbc, stECALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanADC: Error executing allADC markNew "); checkerr(ociError, status, erm); return -1; }
      }
      if(tsiz>0){
	++nch;
	status=OCICollTrim(ociEnv, ociError, (sb4)tsiz, (OCIColl*)adc_settings); 
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanADC: Error trimming collection "); checkerr(ociError, status, erm); return -1; }
      }
      ++ich;
    }
    
    status=OCIObjectFree(ociEnv, ociError, adc_settings, OCI_OBJECTFREE_FORCE);
    status=OCIStmtRelease(stmt_allSetng, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }
  if(markNew)status=OCIStmtRelease(stECALappl, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALGetChanADC: success %d chan out of %d", nch, ich);
  return nch;
}

EXTERN_CALODB int ECALGetChanDAC(int opt, char* type, int* x, int* y, int* dac, int* cwpow, double* HV, double* HVstdb, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALGetChanDAC: not connected");return(-1);}
  
  if( !(opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  if( !(opt & M_GetNew) && (opt & M_MarkNew) ) opt &= ~M_MarkNew;
  
  bool allCells=true;
  if( ('O'==type[0]) || ('M'==type[0]) || ('I'==type[0]) ) allCells=false;
  if(!allCells && (opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  
  bool getOld  = (opt & M_GetOld)  ? true:false;
  bool getNew  = (opt & M_GetNew)  ? true:false;
  bool markNew = (opt & M_MarkNew) ? true:false;
  
  sword status;
  
  OCIDate maxd; OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
  OCIDate appd; OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
  boolean appd_null=FALSE;
  OCIStmt* stECALchan;
  OCIStmt* stECALappl;
  OCIStmt* stmt_allSetng;
  int ich=0, nch=0;
  char tip;
  sword ix,iy;
  sword idac=0, ipow=0;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine *defn1p = (OCIDefine *) 0, *defn2p = (OCIDefine *) 0, *defn3p = (OCIDefine *) 0;
  OCIDefine *defn4p = (OCIDefine *) 0, *defn5p = (OCIDefine *) 0, *defn6p = (OCIDefine *) 0;
  OCIDefine *defn7p = (OCIDefine *) 0, *defn8p = (OCIDefine *) 0, *defn9p = (OCIDefine *) 0;
  dac_setting_list* dac_settings = (dac_setting_list*) 0;
  ub4 dac_settings_size=0;
  dac_setting_typ*      setng = (dac_setting_typ*) 0;
  dac_setting_typ_ind*  setng_ind = (dac_setting_typ_ind*)0;
  boolean exists=FALSE;
  
  if(markNew){
    char sqlECALappl[]="BEGIN ecal_timestamp_dac(:1, :2, :3); END;"; 
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALappl, ociError, (text*) sqlECALappl,(ub4) strlen(sqlECALappl),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC: Error preparing stECALappl "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIBindByPos(stECALappl, &bndp[0], ociError, (ub4)1, (dvoid*) &tip, (sb4) sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALappl, &bndp[1], ociError, (ub4)2, (dvoid*) & ix, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALappl, &bndp[2], ociError, (ub4)3, (dvoid*) & iy, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC: Error binding DAC stECALappl "); checkerr(ociError, status, erm); return -1; }
  }
  
  if(!allCells){
    char sqlECALchan[]="SELECT t.dac_settings, t.dac_chan, t.cw_pow_conn FROM ecal_chan_otb t WHERE t.chan_type=:1 AND t.chan_x=:2 AND t.chan_y=:3";
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALchan, ociError, (text*) sqlECALchan,(ub4) strlen(sqlECALchan),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC: Error preparing stECALchan "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIBindByPos(stECALchan, &bndp[3], ociError, (ub4)1, (dvoid*) &tip, (sb4) sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALchan, &bndp[4], ociError, (ub4)2, (dvoid*) & ix, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALchan, &bndp[5], ociError, (ub4)3, (dvoid*) & iy, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC: Error binding stECALchan "); checkerr(ociError, status, erm); return -1; }
    
    while(type[ich]=='O' || type[ich]=='I' || type[ich]=='M'){
      if(ECALCellExists(type[ich],x[ich],y[ich])){
	// instantiate the collection 
	status=OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_TABLE, (OCIType *) dac_setting_list_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &dac_settings);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC: Error creating dac_settings instance "); checkerr(ociError, status, erm); return -1; }
	
	status=OCIDefineByPos(stECALchan, &defn1p, ociError, (ub4) 1, (dvoid *) 0, (sb4) 0, SQLT_NTY, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
	if(status==OCI_SUCCESS) 
	  status=OCIDefineObject(defn1p, ociError, dac_setting_list_tdo, (dvoid**)&dac_settings, &dac_settings_size, (dvoid**)0, (ub4*)0 );
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC: Error defining dac_settings "); checkerr(ociError, status, erm); return -1; }
	
	status=OCIDefineByPos(stECALchan, &defn2p, ociError, (ub4) 2, (dvoid*)&idac, (sb4)sizeof(idac), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
	if(status==OCI_SUCCESS) 
	  status=OCIDefineByPos(stECALchan, &defn3p, ociError, (ub4) 3, (dvoid*)&ipow, (sb4)sizeof(ipow), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC: Error defining idac & ipow "); checkerr(ociError, status, erm); return -1; }
	
	// set bound variables
	tip=type[ich];
	ix=x[ich];
	iy=y[ich];
	// execute stmt
	status=OCIStmtExecute(ociHdbc, stECALchan, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC: Error executing stECALchan "); checkerr(ociError, status, erm); return -1; }
	
	if( (status=OCIStmtFetch2(stECALchan, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	  dac[ich]=idac;
	  cwpow[ich]=ipow;
	  sb4 tsiz=0;
	  status=OCICollSize(ociEnv, ociError, (OCIColl*)dac_settings, &tsiz); 
	  OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
	  OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
	  for(int i=0; i<tsiz; ++i){
	    OCICollGetElem(ociEnv, ociError, (OCIColl*)dac_settings, (sb4)i, &exists, (dvoid**)&setng, (dvoid**)&setng_ind);
	    sword res=0;
	    OCIDateCompare(ociError, &maxd, &(setng->d_set), &res);
	    if(res<0 && (getNew || (setng_ind->d_applied!=OCI_IND_NULL) ) ){
	      status=OCIDateAssign(ociError, &(setng->d_set), &maxd);
	      if(setng_ind->d_applied==OCI_IND_NULL)appd_null=TRUE;
	      else{
		appd_null=FALSE; 
		status=OCIDateAssign(ociError, &(setng->d_applied), &appd); 
	      }
	      HV[ich]=0; 
	      if(setng_ind->hv_phys!=OCI_IND_NULL) OCINumberToReal(ociError, &(setng->hv_phys), (uword)sizeof(HV[ich]), (dvoid*)&HV[ich]);
	      HVstdb[ich]=0.5*HV[ich]; 
	      if(setng_ind->hv_stdb!=OCI_IND_NULL) OCINumberToReal(ociError, &(setng->hv_stdb), (uword)sizeof(HVstdb[ich]), (dvoid*)&HVstdb[ich]);
	    }
	  }
	  if(tsiz>0 && appd_null && markNew){
	    //changes=true;
	    status=OCIStmtExecute(ociHdbc, stECALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC: Error executing DAC markNew "); checkerr(ociError, status, erm); return -1; }
	  }
	  if(tsiz>0)++nch;
	}
	status=OCIObjectFree(ociEnv, ociError, dac_settings, OCI_OBJECTFREE_FORCE);
      }
      ++ich;
    }
    status=OCIStmtRelease(stECALchan, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }else{
    char sql_allSetng[255];
    if( getNew && !getOld )
      strcpy(sql_allSetng, "SELECT t.dac_settings, t.dac_chan, t.cw_pow_conn, t.chan_type, t.chan_x, t.chan_y FROM ecal_chan_otb t WHERE EXISTS (SELECT * FROM TABLE(t.dac_settings) se WHERE se.d_applied IS NULL) ORDER BY t.dac_chan ");
    else strcpy(sql_allSetng,"SELECT t.dac_settings, t.dac_chan, t.cw_pow_conn, t.chan_type, t.chan_x, t.chan_y FROM ecal_chan_otb t ORDER BY t.dac_chan ");
    
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmt_allSetng, ociError, (text*) sql_allSetng,(ub4) strlen(sql_allSetng),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC: Error preparing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    // instantiate the collection 
    status=OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_TABLE, (OCIType *) dac_setting_list_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &dac_settings);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC: Error creating dac_settings instance "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIDefineByPos(stmt_allSetng, &defn4p, ociError, (ub4) 1, (dvoid *) 0, (sb4) 0, SQLT_NTY, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS)
      status=OCIDefineObject(defn4p, ociError, dac_setting_list_tdo, (dvoid**)&dac_settings, &dac_settings_size, (dvoid**)0, (ub4*)0 );
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC: Error defining dac_settings "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIDefineByPos(stmt_allSetng, &defn5p, ociError, (ub4) 2, (dvoid*)&idac, (sb4)sizeof(idac), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defn6p, ociError, (ub4) 3, (dvoid*)&ipow, (sb4)sizeof(ipow), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defn7p, ociError, (ub4) 4, (dvoid*)&tip,  (sb4)sizeof(tip),  SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defn8p, ociError, (ub4) 5, (dvoid*)&ix,   (sb4)sizeof(ix),   SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defn9p, ociError, (ub4) 6, (dvoid*)&iy,   (sb4)sizeof(iy),   SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC: Error defining idac, ipow, tip, ix, iy "); checkerr(ociError, status, erm); return -1; }
    
    // execute stmt
    status=OCIStmtExecute(ociHdbc, stmt_allSetng, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC: Error executing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    while((status=OCIStmtFetch2(stmt_allSetng, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS || status==OCI_SUCCESS_WITH_INFO ){
      type[nch]=tip;
      x[nch]=ix;
      y[nch]=iy;
      dac[nch]=idac;
      cwpow[nch]=ipow;
      sb4 tsiz=0;
      status=OCICollSize(ociEnv, ociError, (OCIColl*)dac_settings, &tsiz); 
      OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
      OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
      for(int i=0; i<tsiz; ++i){
	OCICollGetElem(ociEnv, ociError, (OCIColl*)dac_settings, (sb4)i, &exists, (dvoid**)&setng, (dvoid**)&setng_ind);
	sword res=0;
	OCIDateCompare(ociError, &maxd, &(setng->d_set), &res);
	if(res<0 && (getNew || (setng_ind->d_applied!=OCI_IND_NULL) ) ){
	  status=OCIDateAssign(ociError, &(setng->d_set), &maxd);
	  if(setng_ind->d_applied==OCI_IND_NULL)appd_null=TRUE;
	  else{
	    appd_null=FALSE; 
	    status=OCIDateAssign(ociError, &(setng->d_applied), &appd); 
	  }
	  HV[nch]=0; 
	  if(setng_ind->hv_phys!=OCI_IND_NULL) OCINumberToReal(ociError, &(setng->hv_phys), (uword)sizeof(HV[nch]), (dvoid*)&HV[nch]);
	  HVstdb[nch]=0.5*HV[nch]; 
	  if(setng_ind->hv_stdb!=OCI_IND_NULL) OCINumberToReal(ociError, &(setng->hv_stdb), (uword)sizeof(HVstdb[nch]), (dvoid*)&HVstdb[nch]);
	}
      }
      if(tsiz>0 && appd_null && markNew){
	//changes=true;
	status=OCIStmtExecute(ociHdbc, stECALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC: Error executing markNew "); checkerr(ociError, status, erm); return -1; }
      }
      if(tsiz>0){
	++nch;
	status=OCICollTrim(ociEnv, ociError, (sb4)tsiz, (OCIColl*)dac_settings); 
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC: Error trimming collection "); checkerr(ociError, status, erm); return -1; }
      }
      ++ich;
    }
    
    status=OCIObjectFree(ociEnv, ociError, dac_settings, OCI_OBJECTFREE_FORCE);
    status=OCIStmtRelease(stmt_allSetng, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }
  if(markNew)status=OCIStmtRelease(stECALappl, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALGetChanDAC: success %d chan out of %d", nch, ich);
  return nch;
}


EXTERN_CALODB int ECALGetChanDAC_date(const char* thedate, int opt, char* type, int* x, int* y, int* dac, int* cwpow, double* HV, double* HVstdb, char* erm){
  // thedate is supposed to be in the YYYY-MM-DD HH24:MI:SS format
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALGetChanDAC_date: not connected");return(-1);}
  
  if( !(opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  if( !(opt & M_GetNew) && (opt & M_MarkNew) ) opt &= ~M_MarkNew;
  
  bool allCells=true;
  if( ('O'==type[0]) || ('M'==type[0]) || ('I'==type[0]) ) allCells=false;
  if(!allCells && (opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  
  bool getOld  = (opt & M_GetOld)  ? true:false;
  bool getNew  = (opt & M_GetNew)  ? true:false;
  bool markNew = (opt & M_MarkNew) ? true:false;
  
  sword status;
  OCIDate thed; 
  if(NULL!=thedate){
    char fmt[]="YYYY-MM-DD HH24:MI:SS";
    status=OCIDateFromText(ociError, (oratext*)thedate, strlen(thedate), (oratext*)fmt, strlen(fmt), 0, 0, &thed); 
  }
  else status=OCIDateSysDate(ociError, &thed);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC_date: date error"); checkerr(ociError, status, erm); return -1; }
  
  OCIDate maxd; OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
  OCIDate appd; OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
  boolean appd_null=FALSE;
  OCIStmt* stECALchan;
  OCIStmt* stECALappl;
  OCIStmt* stmt_allSetng;
  int ich=0, nch=0;
  char tip;
  sword ix,iy;
  sword idac=0, ipow=0;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine *defn1p = (OCIDefine *) 0, *defn2p = (OCIDefine *) 0, *defn3p = (OCIDefine *) 0;
  OCIDefine *defn4p = (OCIDefine *) 0, *defn5p = (OCIDefine *) 0, *defn6p = (OCIDefine *) 0;
  OCIDefine *defn7p = (OCIDefine *) 0, *defn8p = (OCIDefine *) 0, *defn9p = (OCIDefine *) 0;
  dac_setting_list* dac_settings = (dac_setting_list*) 0;
  ub4 dac_settings_size=0;
  dac_setting_typ*      setng = (dac_setting_typ*) 0;
  dac_setting_typ_ind*  setng_ind = (dac_setting_typ_ind*)0;
  boolean exists=FALSE;
  
  if(markNew){
    char sqlECALappl[]="BEGIN ecal_timestamp_dac(:1, :2, :3); END;";
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALappl, ociError, (text*) sqlECALappl,(ub4) strlen(sqlECALappl),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC_date: Error preparing stECALappl "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIBindByPos(stECALappl, &bndp[0], ociError, (ub4)1, (dvoid*) &tip, (sb4) sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALappl, &bndp[1], ociError, (ub4)2, (dvoid*) & ix, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALappl, &bndp[2], ociError, (ub4)3, (dvoid*) & iy, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC_date: Error binding DAC stECALappl "); checkerr(ociError, status, erm); return -1; }
  }
  
  if(!allCells){
    char sqlECALchan[]="SELECT t.dac_settings, t.dac_chan, t.cw_pow_conn FROM ecal_chan_otb t WHERE t.chan_type=:1 AND t.chan_x=:2 AND t.chan_y=:3";
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALchan, ociError, (text*) sqlECALchan,(ub4) strlen(sqlECALchan),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC_date: Error preparing stECALchan "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIBindByPos(stECALchan, &bndp[3], ociError, (ub4)1, (dvoid*) &tip, (sb4) sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALchan, &bndp[4], ociError, (ub4)2, (dvoid*) & ix, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALchan, &bndp[5], ociError, (ub4)3, (dvoid*) & iy, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC_date: Error binding stECALchan "); checkerr(ociError, status, erm); return -1; }
    
    while(type[ich]=='O' || type[ich]=='I' || type[ich]=='M'){
      if(ECALCellExists(type[ich],x[ich],y[ich])){
	// instantiate the collection 
	status=OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_TABLE, (OCIType *) dac_setting_list_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &dac_settings);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC_date: Error creating dac_settings instance "); checkerr(ociError, status, erm); return -1; }
	
	status=OCIDefineByPos(stECALchan, &defn1p, ociError, (ub4) 1, (dvoid *) 0, (sb4) 0, SQLT_NTY, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
	if(status==OCI_SUCCESS) 
	  status=OCIDefineObject(defn1p, ociError, dac_setting_list_tdo, (dvoid**)&dac_settings, &dac_settings_size, (dvoid**)0, (ub4*)0 );
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC_date: Error defining dac_settings "); checkerr(ociError, status, erm); return -1; }
	
	status=OCIDefineByPos(stECALchan, &defn2p, ociError, (ub4) 2, (dvoid*)&idac, (sb4)sizeof(idac), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
	if(status==OCI_SUCCESS) 
	  status=OCIDefineByPos(stECALchan, &defn3p, ociError, (ub4) 3, (dvoid*)&ipow, (sb4)sizeof(ipow), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC_date: Error defining idac & ipow "); checkerr(ociError, status, erm); return -1; }
	
	// set bound variables
	tip=type[ich];
	ix=x[ich];
	iy=y[ich];
	// execute stmt
	status=OCIStmtExecute(ociHdbc, stECALchan, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC_date: Error executing stECALchan "); checkerr(ociError, status, erm); return -1; }
	
	if( (status=OCIStmtFetch2(stECALchan, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	  dac[ich]=idac;
	  cwpow[ich]=ipow;
	  sb4 tsiz=0;
	  status=OCICollSize(ociEnv, ociError, (OCIColl*)dac_settings, &tsiz); 
	  OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
	  OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
	  for(int i=0; i<tsiz; ++i){
	    OCICollGetElem(ociEnv, ociError, (OCIColl*)dac_settings, (sb4)i, &exists, (dvoid**)&setng, (dvoid**)&setng_ind);
	    sword res=0, resd=0;
	    OCIDateCompare(ociError, &maxd, &(setng->d_set), &res);
	    OCIDateCompare(ociError, &thed, &(setng->d_set), &resd);
	    if(res<0 && resd>=0 && (getNew || (setng_ind->d_applied!=OCI_IND_NULL) ) ){
	      status=OCIDateAssign(ociError, &(setng->d_set), &maxd);
	      if(setng_ind->d_applied==OCI_IND_NULL)appd_null=TRUE;
	      else{
		appd_null=FALSE; 
		status=OCIDateAssign(ociError, &(setng->d_applied), &appd); 
	      }
	      HV[ich]=0; 
	      if(setng_ind->hv_phys!=OCI_IND_NULL) OCINumberToReal(ociError, &(setng->hv_phys), (uword)sizeof(HV[ich]), (dvoid*)&HV[ich]);
	      HVstdb[ich]=0.5*HV[ich]; 
	      if(setng_ind->hv_stdb!=OCI_IND_NULL) OCINumberToReal(ociError, &(setng->hv_stdb), (uword)sizeof(HVstdb[ich]), (dvoid*)&HVstdb[ich]);
	    }
	  }
	  if(tsiz>0 && appd_null && markNew){
	    //changes=true;
	    status=OCIStmtExecute(ociHdbc, stECALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC_date: Error executing DAC markNew "); checkerr(ociError, status, erm); return -1; }
	  }
	  if(tsiz>0)++nch;
	}
	status=OCIObjectFree(ociEnv, ociError, dac_settings, OCI_OBJECTFREE_FORCE);
      }
      ++ich;
    }
    status=OCIStmtRelease(stECALchan, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }else{
    char sql_allSetng[255];
    if( getNew && !getOld )
      strcpy(sql_allSetng, "SELECT t.dac_settings, t.dac_chan, t.cw_pow_conn, t.chan_type, t.chan_x, t.chan_y FROM ecal_chan_otb t WHERE EXISTS (SELECT * FROM TABLE(t.dac_settings) se WHERE se.d_applied IS NULL) ORDER BY t.dac_chan ");
    else strcpy(sql_allSetng,"SELECT t.dac_settings, t.dac_chan, t.cw_pow_conn, t.chan_type, t.chan_x, t.chan_y FROM ecal_chan_otb t ORDER BY t.dac_chan ");
    
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmt_allSetng, ociError, (text*) sql_allSetng,(ub4) strlen(sql_allSetng),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC_date: Error preparing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    // instantiate the collection 
    status=OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_TABLE, (OCIType *) dac_setting_list_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &dac_settings);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC_date: Error creating dac_settings instance "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIDefineByPos(stmt_allSetng, &defn4p, ociError, (ub4) 1, (dvoid *) 0, (sb4) 0, SQLT_NTY, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS)
      status=OCIDefineObject(defn4p, ociError, dac_setting_list_tdo, (dvoid**)&dac_settings, &dac_settings_size, (dvoid**)0, (ub4*)0 );
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC_date: Error defining dac_settings "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIDefineByPos(stmt_allSetng, &defn5p, ociError, (ub4) 2, (dvoid*)&idac, (sb4)sizeof(idac), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defn6p, ociError, (ub4) 3, (dvoid*)&ipow, (sb4)sizeof(ipow), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defn7p, ociError, (ub4) 4, (dvoid*)&tip,  (sb4)sizeof(tip),  SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defn8p, ociError, (ub4) 5, (dvoid*)&ix,   (sb4)sizeof(ix),   SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defn9p, ociError, (ub4) 6, (dvoid*)&iy,   (sb4)sizeof(iy),   SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC_date: Error defining idac, ipow, tip, ix, iy "); checkerr(ociError, status, erm); return -1; }
    
    // execute stmt
    status=OCIStmtExecute(ociHdbc, stmt_allSetng, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC_date: Error executing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    while((status=OCIStmtFetch2(stmt_allSetng, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS || status==OCI_SUCCESS_WITH_INFO ){
      type[nch]=tip;
      x[nch]=ix;
      y[nch]=iy;
      dac[nch]=idac;
      cwpow[nch]=ipow;
      sb4 tsiz=0;
      status=OCICollSize(ociEnv, ociError, (OCIColl*)dac_settings, &tsiz); 
      OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
      OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
      for(int i=0; i<tsiz; ++i){
	OCICollGetElem(ociEnv, ociError, (OCIColl*)dac_settings, (sb4)i, &exists, (dvoid**)&setng, (dvoid**)&setng_ind);
	sword res=0, resd=0;
	OCIDateCompare(ociError, &maxd, &(setng->d_set), &res);
	OCIDateCompare(ociError, &thed, &(setng->d_set), &resd);
	if(res<0 && resd>=0 && (getNew || (setng_ind->d_applied!=OCI_IND_NULL) ) ){
	  status=OCIDateAssign(ociError, &(setng->d_set), &maxd);
	  if(setng_ind->d_applied==OCI_IND_NULL)appd_null=TRUE;
	  else{
	    appd_null=FALSE; 
	    status=OCIDateAssign(ociError, &(setng->d_applied), &appd); 
	  }
	  HV[nch]=0; 
	  if(setng_ind->hv_phys!=OCI_IND_NULL) OCINumberToReal(ociError, &(setng->hv_phys), (uword)sizeof(HV[nch]), (dvoid*)&HV[nch]);
	  HVstdb[nch]=0.5*HV[nch]; 
	  if(setng_ind->hv_stdb!=OCI_IND_NULL) OCINumberToReal(ociError, &(setng->hv_stdb), (uword)sizeof(HVstdb[nch]), (dvoid*)&HVstdb[nch]);
	}
      }
      if(tsiz>0 && appd_null && markNew){
	//changes=true;
	status=OCIStmtExecute(ociHdbc, stECALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC_date: Error executing markNew "); checkerr(ociError, status, erm); return -1; }
      }
      if(tsiz>0){
	++nch;
	status=OCICollTrim(ociEnv, ociError, (sb4)tsiz, (OCIColl*)dac_settings); 
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanDAC_date: Error trimming collection "); checkerr(ociError, status, erm); return -1; }
      }
      ++ich;
    }
    
    status=OCIObjectFree(ociEnv, ociError, dac_settings, OCI_OBJECTFREE_FORCE);
    status=OCIStmtRelease(stmt_allSetng, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }
  if(markNew)status=OCIStmtRelease(stECALappl, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALGetChanDAC_date: success %d chan out of %d", nch, ich);
  return nch;
}


EXTERN_CALODB int HCALGetChanDAC_date(const char* thedate, int opt, char* type, int* x, int* y, int* dac, int* cwpow, double* HV, double* HVstdb, char* erm){
  // thedate is supposed to be in the YYYY-MM-DD HH24:MI:SS format
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "HCALGetChanDAC_date: not connected");return(-1);}
  
  if( !(opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  if( !(opt & M_GetNew) && (opt & M_MarkNew) ) opt &= ~M_MarkNew;
  
  bool allCells=true;
  if( ('O'==type[0]) || ('I'==type[0]) ) allCells=false;
  if(!allCells && (opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  
  bool getOld  = (opt & M_GetOld)  ? true:false;
  bool getNew  = (opt & M_GetNew)  ? true:false;
  bool markNew = (opt & M_MarkNew) ? true:false;
  
  sword status;
  
  OCIDate thed; 
  if (NULL!=thedate) {
    char fmt[] = "YYYY-MM-DD HH24:MI:SS";
    status = OCIDateFromText(ociError, (oratext*)thedate, strlen(thedate), (oratext*)fmt, strlen(fmt), 0, 0, &thed); 
    if (status==OCI_ERROR) status = OCIDateSysDate(ociError, &thed);
  } else status = OCIDateSysDate(ociError, &thed);
  if (status!=OCI_SUCCESS) { sprintf(erm,"HCALGetChanDAC_date: date error"); checkerr(ociError, status, erm); return -1; }
  
  OCIDate maxd; OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
  OCIDate appd; OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
  boolean appd_null=FALSE;
  
  OCIStmt* stHCALchan;
  OCIStmt* stHCALappl;
  OCIStmt* stmt_allSetng;
  //bool changes=false;
  int ich=0, nch=0;
  char tip;
  sword ix,iy;
  sword idac=0, ipow=0;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine *defn1p = (OCIDefine *) 0, *defn2p = (OCIDefine *) 0, *defn3p = (OCIDefine *) 0;
  OCIDefine *defn4p = (OCIDefine *) 0, *defn5p = (OCIDefine *) 0, *defn6p = (OCIDefine *) 0;
  OCIDefine *defn7p = (OCIDefine *) 0, *defn8p = (OCIDefine *) 0, *defn9p = (OCIDefine *) 0;
  dac_setting_list* dac_settings = (dac_setting_list*) 0;
  ub4 dac_settings_size=0;
  dac_setting_typ*      setng = (dac_setting_typ*) 0;
  dac_setting_typ_ind*  setng_ind = (dac_setting_typ_ind*)0;
  boolean exists=FALSE;
  
  if (markNew) {
    char sqlHCALappl[] = "BEGIN hcal_timestamp_dac(:1, :2, :3); END;";
    status = OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stHCALappl, ociError, (text*) sqlHCALappl,(ub4) strlen(sqlHCALappl),NULL, 0, (ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if (status!=OCI_SUCCESS) { sprintf(erm,"HCALGetChanDAC_date: Error preparing stHCALappl "); checkerr(ociError, status, erm); return -1; }
    if (status==OCI_SUCCESS) status = OCIBindByPos(stHCALappl, &bndp[0], ociError, (ub4)1, (dvoid*) &tip, (sb4) sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if (status==OCI_SUCCESS) status = OCIBindByPos(stHCALappl, &bndp[1], ociError, (ub4)2, (dvoid*) & ix, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if (status==OCI_SUCCESS) status = OCIBindByPos(stHCALappl, &bndp[2], ociError, (ub4)3, (dvoid*) & iy, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if (status!=OCI_SUCCESS) { sprintf(erm,"HCALGetChanDAC_date: Error binding DAC stHCALappl "); checkerr(ociError, status, erm); return -1; }
  }
  
  if (!allCells) {
    char sqlHCALchan[] = "SELECT t.dac_settings, t.dac_chan, m.cw_pow_conn FROM hcal_chan_otb t, hcal_module_otb m WHERE t.chan_type=:1 AND t.chan_x=:2 AND t.chan_y=:3 AND t.mod_id=m.mod_id";
    status = OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stHCALchan, ociError, (text*) sqlHCALchan,(ub4) strlen(sqlHCALchan),NULL, 0, (ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if (status!=OCI_SUCCESS) { sprintf(erm,"HCALGetChanDAC_date: Error preparing stHCALchan "); checkerr(ociError, status, erm); return -1; }
    status = OCIBindByPos(stHCALchan, &bndp[3], ociError, (ub4)1, (dvoid*) &tip, (sb4) sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if (status==OCI_SUCCESS) status = OCIBindByPos(stHCALchan, &bndp[4], ociError, (ub4)2, (dvoid*) & ix, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if (status==OCI_SUCCESS) status = OCIBindByPos(stHCALchan, &bndp[5], ociError, (ub4)3, (dvoid*) & iy, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if (status!=OCI_SUCCESS) { sprintf(erm,"HCALGetChanDAC_date: Error binding stHCALchan "); checkerr(ociError, status, erm); return -1; }
    
    while (type[ich]=='O' || type[ich]=='I') {
      if (HCALCellExists(type[ich],x[ich],y[ich])) {
	// instantiate the collection 
	status = OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_TABLE, (OCIType *) dac_setting_list_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &dac_settings);
	if (status!=OCI_SUCCESS) { sprintf(erm,"HCALGetChanDAC_date: Error creating dac_settings instance "); checkerr(ociError, status, erm); return -1; }	
	status = OCIDefineByPos(stHCALchan, &defn1p, ociError, (ub4) 1, (dvoid *) 0, (sb4) 0, SQLT_NTY, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
	if (status==OCI_SUCCESS) 
	  status = OCIDefineObject(defn1p, ociError, dac_setting_list_tdo, (dvoid**)&dac_settings, &dac_settings_size, (dvoid**)0, (ub4*)0 );
	if (status!=OCI_SUCCESS) { sprintf(erm,"HCALGetChanDAC_date: Error defining dac_settings "); checkerr(ociError, status, erm); return -1; }
	
	status = OCIDefineByPos(stHCALchan, &defn2p, ociError, (ub4) 2, (dvoid*)&idac, (sb4)sizeof(idac), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
	if (status==OCI_SUCCESS) 
	  status = OCIDefineByPos(stHCALchan, &defn3p, ociError, (ub4) 3, (dvoid*)&ipow, (sb4)sizeof(ipow), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
	if (status!=OCI_SUCCESS) { sprintf(erm,"HCALGetChanDAC_date: Error defining idac & ipow "); checkerr(ociError, status, erm); return -1; }
	
	// set bound variables
	tip = type[ich];
	ix = x[ich];
	iy = y[ich];
	// execute stmt
	status = OCIStmtExecute(ociHdbc, stHCALchan, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if (status!=OCI_SUCCESS) { sprintf(erm,"HCALGetChanDAC_date: Error executing stHCALchan "); checkerr(ociError, status, erm); return -1; }
	
	if ( (status=OCIStmtFetch2(stHCALchan, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS) {
	  dac[ich] = idac;
	  cwpow[ich] = ipow;
	  sb4 tsiz = 0;
	  status = OCICollSize(ociEnv, ociError, (OCIColl*)dac_settings, &tsiz); 
	  OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
	  OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
	  for (int i=0; i<tsiz; ++i) {
	    OCICollGetElem(ociEnv, ociError, (OCIColl*)dac_settings, (sb4)i, &exists, (dvoid**)&setng, (dvoid**)&setng_ind);
	    sword res=0, resd=0;
	    OCIDateCompare(ociError, &maxd, &(setng->d_set), &res);
	    OCIDateCompare(ociError, &thed, &(setng->d_set), &resd);
	    if (res<0 && resd>=0 && (getNew || (setng_ind->d_applied!=OCI_IND_NULL) ) ) {
	      status = OCIDateAssign(ociError, &(setng->d_set), &maxd);
	      if (setng_ind->d_applied==OCI_IND_NULL) appd_null = TRUE;
	      else {
		appd_null=FALSE; 
		status = OCIDateAssign(ociError, &(setng->d_applied), &appd); 
	      }
	      HV[ich] = 0; 
	      if (setng_ind->hv_phys!=OCI_IND_NULL) OCINumberToReal(ociError, &(setng->hv_phys), (uword)sizeof(HV[ich]), (dvoid*)&HV[ich]);
	      HVstdb[ich] = 0.5*HV[ich]; 
	      if (setng_ind->hv_stdb!=OCI_IND_NULL) OCINumberToReal(ociError, &(setng->hv_stdb), (uword)sizeof(HVstdb[ich]), (dvoid*)&HVstdb[ich]);
	    }
	  }
	  if (tsiz>0 && appd_null && markNew) {
	    //changes=true;
	    status = OCIStmtExecute(ociHdbc, stHCALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	    if (status!=OCI_SUCCESS) { sprintf(erm,"HCALGetChanDAC_date: Error executing DAC markNew "); checkerr(ociError, status, erm); return -1; }
	  }
	  if (tsiz>0) ++nch;
	}
	status = OCIObjectFree(ociEnv, ociError, dac_settings, OCI_OBJECTFREE_FORCE);
      }
      ++ich;
    }
    status = OCIStmtRelease(stHCALchan, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  } else {
    char sql_allSetng[255];
    if ( getNew && !getOld )
      strcpy(sql_allSetng, "SELECT t.dac_settings, t.dac_chan, m.cw_pow_conn, t.chan_type, t.chan_x, t.chan_y FROM hcal_chan_otb t, hcal_module_otb m WHERE EXISTS (SELECT * FROM TABLE(t.dac_settings) se WHERE se.d_applied IS NULL) AND t.mod_id=m.mod_id ORDER BY t.dac_chan ");
    else strcpy(sql_allSetng,"SELECT t.dac_settings, t.dac_chan, m.cw_pow_conn, t.chan_type, t.chan_x, t.chan_y FROM hcal_chan_otb t, hcal_module_otb m WHERE t.mod_id=m.mod_id ORDER BY t.dac_chan ");
    status = OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmt_allSetng, ociError, (text*) sql_allSetng,(ub4) strlen(sql_allSetng),NULL, 0, (ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if (status!=OCI_SUCCESS) { sprintf(erm,"HCALGetChanDAC_date: Error preparing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    // instantiate the collection 
    status = OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_TABLE, (OCIType *) dac_setting_list_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &dac_settings);
    if (status!=OCI_SUCCESS) { sprintf(erm,"HCALGetChanDAC_date: Error creating dac_settings instance "); checkerr(ociError, status, erm); return -1; }
    
    status = OCIDefineByPos(stmt_allSetng, &defn4p, ociError, (ub4) 1, (dvoid *) 0, (sb4) 0, SQLT_NTY, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if (status==OCI_SUCCESS)
      status = OCIDefineObject(defn4p, ociError, dac_setting_list_tdo, (dvoid**)&dac_settings, &dac_settings_size, (dvoid**)0, (ub4*)0 );
    if (status!=OCI_SUCCESS) { sprintf(erm,"HCALGetChanDAC_date: Error defining dac_settings "); checkerr(ociError, status, erm); return -1; }
    
    status = OCIDefineByPos(stmt_allSetng, &defn5p, ociError, (ub4) 2, (dvoid*)&idac, (sb4)sizeof(idac), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if (status==OCI_SUCCESS) status = OCIDefineByPos(stmt_allSetng, &defn6p, ociError, (ub4) 3, (dvoid*)&ipow, (sb4)sizeof(ipow), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if (status==OCI_SUCCESS) status = OCIDefineByPos(stmt_allSetng, &defn7p, ociError, (ub4) 4, (dvoid*)&tip,  (sb4)sizeof(tip),  SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if (status==OCI_SUCCESS) status = OCIDefineByPos(stmt_allSetng, &defn8p, ociError, (ub4) 5, (dvoid*)&ix,   (sb4)sizeof(ix),   SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if (status==OCI_SUCCESS) status = OCIDefineByPos(stmt_allSetng, &defn9p, ociError, (ub4) 6, (dvoid*)&iy,   (sb4)sizeof(iy),   SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if (status!=OCI_SUCCESS) { sprintf(erm,"HCALGetChanDAC_date: Error defining idac, ipow, tip, ix, iy "); checkerr(ociError, status, erm); return -1; }
    
    // execute stmt
    status = OCIStmtExecute(ociHdbc, stmt_allSetng, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
    if (status!=OCI_SUCCESS) { sprintf(erm,"HCALGetChanDAC_date: Error executing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    while ((status=OCIStmtFetch2(stmt_allSetng, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS || status==OCI_SUCCESS_WITH_INFO ) {
      type[nch] = tip;
      x[nch] = ix;
      y[nch] = iy;
      dac[nch] = idac;
      cwpow[nch] = ipow;
      sb4 tsiz = 0;
      status = OCICollSize(ociEnv, ociError, (OCIColl*)dac_settings, &tsiz); 
      OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
      OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
      for (int i=0; i<tsiz; ++i) {
	OCICollGetElem(ociEnv, ociError, (OCIColl*)dac_settings, (sb4)i, &exists, (dvoid**)&setng, (dvoid**)&setng_ind);
	sword res=0, resd=0;
	OCIDateCompare(ociError, &maxd, &(setng->d_set), &res);
	OCIDateCompare(ociError, &thed, &(setng->d_set), &resd);
	if (res<0 && resd>=0 && (getNew || (setng_ind->d_applied!=OCI_IND_NULL) ) ) {
	  status = OCIDateAssign(ociError, &(setng->d_set), &maxd);
	  if (setng_ind->d_applied==OCI_IND_NULL) appd_null = TRUE;
	  else {
	    appd_null = FALSE; 
	    status = OCIDateAssign(ociError, &(setng->d_applied), &appd); 
	  }
	  HV[nch]=0; 
	  if (setng_ind->hv_phys!=OCI_IND_NULL) OCINumberToReal(ociError, &(setng->hv_phys), (uword)sizeof(HV[nch]), (dvoid*)&HV[nch]);
	  HVstdb[nch] = 0.5*HV[nch]; 
	  if (setng_ind->hv_stdb!=OCI_IND_NULL) OCINumberToReal(ociError, &(setng->hv_stdb), (uword)sizeof(HVstdb[nch]), (dvoid*)&HVstdb[nch]);
	}
      }
      if (tsiz>0 && appd_null && markNew) {
	//changes=true;
	status = OCIStmtExecute(ociHdbc, stHCALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if (status!=OCI_SUCCESS) { sprintf(erm,"HCALGetChanDAC_date: Error executing markNew "); checkerr(ociError, status, erm); return -1; }
      }
      if (tsiz>0) {
	++nch;
	status = OCICollTrim(ociEnv, ociError, (sb4)tsiz, (OCIColl*)dac_settings); 
	if (status!=OCI_SUCCESS) { sprintf(erm,"HCALGetChanDAC_date: Error trimming collection "); checkerr(ociError, status, erm); return -1; }
      }
      ++ich;
    }
    
    status = OCIObjectFree(ociEnv, ociError, dac_settings, OCI_OBJECTFREE_FORCE);
    status = OCIStmtRelease(stmt_allSetng, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }
  if (markNew) status = OCIStmtRelease(stHCALappl, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"HCALGetChanDAC_date: success %d chan out of %d", nch, ich);
  return nch;
}

EXTERN_CALODB bool HCALCellExists(char typ, int x, int y){
  if(typ!='O' && typ!='I')return false;
  if('O'==typ){
    if(x<0 || x>31 || y<3 || y>28)return false;
    if(x>=8 && x<=23 && y>=9 && y<=22)return false;
  }
  else if('I'==typ){
    if(x<0 || x>31 || y<2 || y>29)return false;
    if(x>=14 && x<=17 && y>=14 && y<=17)return false;
  }
  
  return true;
}

EXTERN_CALODB int HCALGetPM_date(const char* thedate, char* type, int* x, int* y, char* PMname, int PMname_len, double* g0, double* alpha, char* erm){
  // the_date is supposed to be in the YYYY-MM-DD HH24:MI:SS format
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "HCALGetPM_date: not connected");return(-1);}
  
  bool allCells=true;
  if( ('O'==type[0]) || ('I'==type[0]) ) allCells=false;
  
  sword status;
  
  OCIDate thed; 
  if(NULL!=thedate){
    char fmt[]="YYYY-MM-DD HH24:MI:SS";
    status=OCIDateFromText(ociError, (oratext*)thedate, strlen(thedate), (oratext*)fmt, strlen(fmt), 0, 0, &thed); 
    if(status==OCI_ERROR)status=OCIDateSysDate(ociError, &thed);
  }
  else status=OCIDateSysDate(ociError, &thed);
  if(status!=OCI_SUCCESS){sprintf(erm,"HCALGetPM_date: date error"); checkerr(ociError, status, erm); return -1; }
  
  OCIDate maxd; OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
  
  OCIStmt* stHCALchan;
  OCIStmt* stmt_allSetng;
  int ich=0, nch=0;
  char tip;
  sword ix,iy;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine *defn1p = (OCIDefine *) 0; 
  OCIDefine *defn4p = (OCIDefine *) 0;
  OCIDefine *defn7p = (OCIDefine *) 0, *defn8p = (OCIDefine *) 0, *defn9p = (OCIDefine *) 0;
  hcal_pmcalib_list* pm_calibs = (hcal_pmcalib_list*) 0;
  ub4 pm_calibs_size=0;
  hcal_pmcalib_typ*      calb = (hcal_pmcalib_typ*) 0;
  hcal_pmcalib_typ_ind*   calb_ind = (hcal_pmcalib_typ_ind*)0;
  boolean exists=FALSE;
  
  if(!allCells){
    char sqlHCALchan[]="SELECT t.pm_calibs FROM hcal_chan_otb t WHERE t.chan_type=:1 AND t.chan_x=:2 AND t.chan_y=:3";
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stHCALchan, ociError, (text*) sqlHCALchan,(ub4) strlen(sqlHCALchan),NULL, 0, (ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"HCALGetPM_date: Error preparing stHCALchan "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS) status=OCIBindByPos(stHCALchan, &bndp[3], ociError, (ub4)1, (dvoid*)&tip, (sb4)sizeof(char) , (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stHCALchan, &bndp[4], ociError, (ub4)2, (dvoid*)& ix, (sb4)sizeof(sword), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stHCALchan, &bndp[5], ociError, (ub4)3, (dvoid*)& iy, (sb4)sizeof(sword), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"HCALGetPM_date: Error binding stHCALchan "); checkerr(ociError, status, erm); return -1; }
    
    while(type[ich]=='O' || type[ich]=='I'){
      g0[ich]=0; 
      alpha[ich]=0; 
      PMname[ich*PMname_len]=(char)0;
      if(HCALCellExists(type[ich],x[ich],y[ich])){
	// instantiate the collection 
	status=OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_TABLE, (OCIType *) hcal_pmcalib_list_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &pm_calibs);
	if(status!=OCI_SUCCESS){sprintf(erm,"HCALGetPM_date: Error creating pm_calibs instance "); checkerr(ociError, status, erm); return -1; }
	
	status=OCIDefineByPos(stHCALchan, &defn1p, ociError, (ub4) 1, (dvoid *) 0, (sb4) 0, SQLT_NTY, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
	if(status==OCI_SUCCESS) 
	  status=OCIDefineObject(defn1p, ociError, hcal_pmcalib_list_tdo, (dvoid**)&pm_calibs, &pm_calibs_size, (dvoid**)0, (ub4*)0 );
	if(status!=OCI_SUCCESS){sprintf(erm,"HCALGetPM_date: Error defining pm_calibs "); checkerr(ociError, status, erm); return -1; }
	
	// set bound variables
	tip=type[ich];
	ix=x[ich];
	iy=y[ich];
	// execute stmt
	status=OCIStmtExecute(ociHdbc, stHCALchan, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"HCALGetPM_date: Error executing stHCALchan "); checkerr(ociError, status, erm); return -1; }
	
	if( (status=OCIStmtFetch2(stHCALchan, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	  sb4 tsiz=0;
	  status=OCICollSize(ociEnv, ociError, (OCIColl*)pm_calibs, &tsiz); 
	  OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
	  for(int i=0; i<tsiz; ++i){
	    OCICollGetElem(ociEnv, ociError, (OCIColl*)pm_calibs, (sb4)i, &exists, (dvoid**)&calb, (dvoid**)&calb_ind);
	    sword res=0, resd=0;
	    OCIDateCompare(ociError, &maxd, &(calb->start_v), &res);
	    OCIDateCompare(ociError, &thed, &(calb->start_v), &resd);
	    if(res<0 && resd>=0){
	      status=OCIDateAssign(ociError, &(calb->start_v), &maxd);
	      if(calb_ind->g0   !=OCI_IND_NULL) OCINumberToReal(ociError, &(calb->g0),    (uword)sizeof(g0[ich]),    (dvoid*)&g0[ich]);
	      if(calb_ind->alpha!=OCI_IND_NULL) OCINumberToReal(ociError, &(calb->alpha), (uword)sizeof(alpha[ich]), (dvoid*)&alpha[ich]);
	      if(calb_ind->pmt_id!=OCI_IND_NULL)strcpy(&PMname[ich*PMname_len], (char*)OCIStringPtr(ociEnv, calb->pmt_id)); 
	    }
	  }
	  if(tsiz>0)++nch;
	}
	status=OCIObjectFree(ociEnv, ociError, pm_calibs, OCI_OBJECTFREE_FORCE);
      }
      ++ich;
    }
    status=OCIStmtRelease(stHCALchan, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }
  else{
    char sql_allSetng[]="SELECT t.pm_calibs, t.chan_type, t.chan_x, t.chan_y FROM hcal_chan_otb t ORDER BY t.chan_type, t.chan_x, t.chan_y";
    
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmt_allSetng, ociError, (text*) sql_allSetng,(ub4) strlen(sql_allSetng),NULL, 0, (ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"HCALGetPM_date: Error preparing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    // instantiate the collection 
    status=OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_TABLE, (OCIType *) hcal_pmcalib_list_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &pm_calibs);
    if(status!=OCI_SUCCESS){sprintf(erm,"HCALGetPM_date: Error creating pm_calibs instance "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIDefineByPos(stmt_allSetng, &defn4p, ociError, (ub4) 1, (dvoid *) 0, (sb4) 0, SQLT_NTY, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS)
      status=OCIDefineObject(defn4p, ociError, hcal_pmcalib_list_tdo, (dvoid**)&pm_calibs, &pm_calibs_size, (dvoid**)0, (ub4*)0 );
    if(status!=OCI_SUCCESS){sprintf(erm,"HCALGetPM_date: Error defining pm_calibs "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIDefineByPos(stmt_allSetng, &defn7p, ociError, (ub4) 2, (dvoid*)&tip,  (sb4)sizeof(tip),  SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defn8p, ociError, (ub4) 3, (dvoid*)&ix,   (sb4)sizeof(ix),   SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defn9p, ociError, (ub4) 4, (dvoid*)&iy,   (sb4)sizeof(iy),   SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"HCALGetPM_date: Error defining tip, ix, iy "); checkerr(ociError, status, erm); return -1; }
    
    // execute stmt
    status=OCIStmtExecute(ociHdbc, stmt_allSetng, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"HCALGetPM_date: Error executing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    while((status=OCIStmtFetch2(stmt_allSetng, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS || status==OCI_SUCCESS_WITH_INFO ){
      type[nch]=tip;
      x[nch]=ix;
      y[nch]=iy;
      g0[nch]=0; 
      alpha[nch]=0; 
      PMname[nch*PMname_len]=(char)0;
      sb4 tsiz=0;
      status=OCICollSize(ociEnv, ociError, (OCIColl*)pm_calibs, &tsiz); 
      OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
      for(int i=0; i<tsiz; ++i){
	OCICollGetElem(ociEnv, ociError, (OCIColl*)pm_calibs, (sb4)i, &exists, (dvoid**)&calb, (dvoid**)&calb_ind);
	sword res=0, resd=0;
	OCIDateCompare(ociError, &maxd, &(calb->start_v), &res);
	OCIDateCompare(ociError, &thed, &(calb->start_v), &resd);
	if(res<0 && resd>=0){
	  status=OCIDateAssign(ociError, &(calb->start_v), &maxd);
	  if(calb_ind->g0   !=OCI_IND_NULL) OCINumberToReal(ociError, &(calb->g0),    (uword)sizeof(g0[nch]),    (dvoid*)&g0[nch]);
	  if(calb_ind->alpha!=OCI_IND_NULL) OCINumberToReal(ociError, &(calb->alpha), (uword)sizeof(alpha[nch]), (dvoid*)&alpha[nch]);
	  if(calb_ind->pmt_id!=OCI_IND_NULL)strcpy(&PMname[nch*PMname_len], (char*) OCIStringPtr(ociEnv, calb->pmt_id) ); 
	}
      }
      if(tsiz>0){
	++nch;
	status=OCICollTrim(ociEnv, ociError, (sb4)tsiz, (OCIColl*)pm_calibs); 
	if(status!=OCI_SUCCESS){sprintf(erm,"HCALGetPM_date: Error trimming collection "); checkerr(ociError, status, erm); return -1; }
      }
      ++ich;
    }
    
    status=OCIObjectFree(ociEnv, ociError, pm_calibs, OCI_OBJECTFREE_FORCE);
    status=OCIStmtRelease(stmt_allSetng, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }
  
  sprintf(erm,"HCALGetPM_date: success %d chan out of %d", nch, ich);
  return nch;
}

EXTERN_CALODB int HCALStoreDACSettings(char* type, int* x, int* y, double* HVphys, double* HVstdb, char* erm){
	erm[0]=(char)0;
	if(!m_connected){strcpy(erm, "HCALStoreDACSettings: not connected");return(-1);}

	sword status;

	OCIDate maxd; OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
	OCIDate appd; OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
	boolean appd_null=FALSE;

	OCIStmt* stHCALchan;
	OCIStmt* stHCALinss;
	OCIStmt* stHCALupds;
	int ich=0, nch=0;

	char tip;
	sword ix,iy;
	double hvph=0, hvst=0;
	OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
	OCIDefine *defn1p = (OCIDefine *) 0;
	dac_setting_list* dac_settings = (dac_setting_list*) 0;
	ub4 dac_settings_size=0;
	dac_setting_typ*      setng = (dac_setting_typ*) 0;
	dac_setting_typ_ind*  setng_ind = (dac_setting_typ_ind*)0;
	boolean exists=FALSE;
// stHCALchan
	char sqlHCALchan[]="SELECT t.dac_settings FROM hcal_chan_otb t WHERE t.chan_type=:1 AND t.chan_x=:2 AND t.chan_y=:3";
	status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stHCALchan, ociError, (text*) sqlHCALchan,(ub4) strlen(sqlHCALchan),NULL, 0, (ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"HCALStoreDACSettings: Error preparing stHCALchan "); checkerr(ociError, status, erm); return -1; }

	status=OCIBindByPos(stHCALchan, &bndp[0], ociError, (ub4)1, (dvoid*) &tip, (sb4) sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
	if(status==OCI_SUCCESS) status=OCIBindByPos(stHCALchan, &bndp[1], ociError, (ub4)2, (dvoid*) & ix, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
	if(status==OCI_SUCCESS) status=OCIBindByPos(stHCALchan, &bndp[2], ociError, (ub4)3, (dvoid*) & iy, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"HCALStoreDACSettings: Error binding stHCALchan "); checkerr(ociError, status, erm); return -1; }
// stHCALinss
	char sqlHCALinss[]="INSERT INTO TABLE (SELECT ch.dac_settings FROM hcal_chan_otb ch WHERE ch.chan_type=:1 AND ch.chan_x=:2 AND ch.chan_y=:3 ) VALUES(SYSDATE, NULL, :4, :5)";
	status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stHCALinss, ociError, (text*) sqlHCALinss,(ub4) strlen(sqlHCALinss),NULL, 0, (ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"HCALStoreDACSettings: Error preparing stHCALinss "); checkerr(ociError, status, erm); return -1; }

	status=OCIBindByPos(stHCALinss, &bndp[3], ociError, (ub4)1, (dvoid*) &tip, (sb4) sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
	if(status==OCI_SUCCESS) status=OCIBindByPos(stHCALinss, &bndp[4], ociError, (ub4)2, (dvoid*) & ix,   (sb4) sizeof(sword),  (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
	if(status==OCI_SUCCESS) status=OCIBindByPos(stHCALinss, &bndp[5], ociError, (ub4)3, (dvoid*) & iy,   (sb4) sizeof(sword),  (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
	if(status==OCI_SUCCESS) status=OCIBindByPos(stHCALinss, &bndp[6], ociError, (ub4)4, (dvoid*) & hvph, (sb4) sizeof(double), (ub2) SQLT_FLT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
	if(status==OCI_SUCCESS) status=OCIBindByPos(stHCALinss, &bndp[7], ociError, (ub4)5, (dvoid*) & hvst, (sb4) sizeof(double), (ub2) SQLT_FLT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"HCALStoreDACSettings; Error binding stHCALinss "); checkerr(ociError, status, erm); return -1; }
// stHCALupds
	char sqlHCALupds[]="UPDATE TABLE (SELECT ch.dac_settings FROM hcal_chan_otb ch WHERE ch.chan_type=:1 AND ch.chan_x=:2 AND ch.chan_y=:3 ) r SET r.d_set=SYSDATE, r.hv_phys=:4, r.hv_stdb=:5 WHERE r.d_applied IS NULL";
	status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stHCALupds, ociError, (text*) sqlHCALupds,(ub4) strlen(sqlHCALupds),NULL, 0, (ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"HCALStoreDACSettings: Error preparing stHCALupds "); checkerr(ociError, status, erm); return -1; }

	status=OCIBindByPos(stHCALupds, &bndp[8], ociError, (ub4)1, (dvoid*) &tip, (sb4) sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
	if(status==OCI_SUCCESS) status=OCIBindByPos(stHCALupds, &bndp[9],  ociError, (ub4)2, (dvoid*) & ix,   (sb4) sizeof(sword),  (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
	if(status==OCI_SUCCESS) status=OCIBindByPos(stHCALupds, &bndp[10], ociError, (ub4)3, (dvoid*) & iy,   (sb4) sizeof(sword),  (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
	if(status==OCI_SUCCESS) status=OCIBindByPos(stHCALupds, &bndp[11], ociError, (ub4)4, (dvoid*) & hvph, (sb4) sizeof(double), (ub2) SQLT_FLT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
	if(status==OCI_SUCCESS) status=OCIBindByPos(stHCALupds, &bndp[12], ociError, (ub4)5, (dvoid*) & hvst, (sb4) sizeof(double), (ub2) SQLT_FLT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"HCALStoreDACSettings: Error binding stHCALupds "); checkerr(ociError, status, erm); return -1; }

	while(type[ich]=='O' || type[ich]=='I' ){
		if(HCALCellExists(type[ich],x[ich],y[ich])){
			// instantiate the collection 
			status=OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_TABLE, (OCIType *) dac_setting_list_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &dac_settings);
			if(status!=OCI_SUCCESS){sprintf(erm,"HCALStoreDACSettings: Error creating dac_settings instance "); checkerr(ociError, status, erm); return -1; }

			status=OCIDefineByPos(stHCALchan, &defn1p, ociError, (ub4) 1, (dvoid *) 0, (sb4) 0, SQLT_NTY, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
			if(status==OCI_SUCCESS) 
				status=OCIDefineObject(defn1p, ociError, dac_setting_list_tdo, (dvoid**)&dac_settings, &dac_settings_size, (dvoid**)0, (ub4*)0 );
			if(status!=OCI_SUCCESS){sprintf(erm,"HCALStoreDACSettings: Error defining dac_settings "); checkerr(ociError, status, erm); return -1; }

			// set bound variables
			tip=type[ich];
			ix=x[ich];
			iy=y[ich];
			hvph=HVphys[ich];
			hvst=HVstdb[ich];
			// execute stmt
			status=OCIStmtExecute(ociHdbc, stHCALchan, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
			if(status!=OCI_SUCCESS){sprintf(erm,"HCALStoreDACSettings: Error executing stHCALchan "); checkerr(ociError, status, erm); return -1; }

			appd_null=FALSE;
			if( (status=OCIStmtFetch2(stHCALchan, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
				sb4 tsiz=0;
				status=OCICollSize(ociEnv, ociError, (OCIColl*)dac_settings, &tsiz); 
				OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
				OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
				for(int i=0; i<tsiz; ++i){
					OCICollGetElem(ociEnv, ociError, (OCIColl*)dac_settings, (sb4)i, &exists, (dvoid**)&setng, (dvoid**)&setng_ind);
					sword res=0;
					OCIDateCompare(ociError, &maxd, &(setng->d_set), &res);
					if(res<0){
						status=OCIDateAssign(ociError, &(setng->d_set), &maxd);
						if(setng_ind->d_applied==OCI_IND_NULL)appd_null=TRUE;
						else{appd_null=FALSE; status=OCIDateAssign(ociError, &(setng->d_applied), &appd); }
					}
				}
			}
			status=OCIObjectFree(ociEnv, ociError, dac_settings, OCI_OBJECTFREE_FORCE);
			if(appd_null){
				status=OCIStmtExecute(ociHdbc, stHCALupds, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
				if(status!=OCI_SUCCESS){sprintf(erm,"HCALStoreDACSettings: Error executing stHCALupds "); checkerr(ociError, status, erm); return -1; }
			}
			else{
				status=OCIStmtExecute(ociHdbc, stHCALinss, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
				if(status!=OCI_SUCCESS){sprintf(erm,"HCALStoreDACSettings: Error executing stHCALinss "); checkerr(ociError, status, erm); return -1; }
			}
			++nch;
		}
		++ich;
	}
	status=OCIStmtRelease(stHCALchan, ociError, NULL, 0, (ub4) OCI_DEFAULT);
	status=OCIStmtRelease(stHCALupds, ociError, NULL, 0, (ub4) OCI_DEFAULT);
	status=OCIStmtRelease(stHCALinss, ociError, NULL, 0, (ub4) OCI_DEFAULT);

	sprintf(erm,"HCALStoreDACSettings: success %d chan out of %d", nch, ich);
	return nch;
}






EXTERN_CALODB int ECALGetPM(char* type, int* x, int* y, char* PMname, int PMname_len, double* g0, double* alpha, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALGetPM: not connected");return(-1);}
  
  bool allCells=true;
  if( ('O'==type[0]) || ('M'==type[0]) || ('I'==type[0]) ) allCells=false;
  
  sword status;
  OCIDate maxd; OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
  
  OCIStmt* stECALchan;
  OCIStmt* stmt_allSetng;
  int ich=0, nch=0; 
  char tip;
  sword ix,iy;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine *defn1p = (OCIDefine *) 0;//, *defn2p = (OCIDefine *) 0, *defn3p = (OCIDefine *) 0;
  OCIDefine *defn4p = (OCIDefine *) 0;//, *defn5p = (OCIDefine *) 0, *defn6p = (OCIDefine *) 0;
  OCIDefine *defn7p = (OCIDefine *) 0, *defn8p = (OCIDefine *) 0, *defn9p = (OCIDefine *) 0;
  ecal_pmcalib_list* pm_calibs = (ecal_pmcalib_list*) 0;
  ub4 pm_calibs_size=0;
  ecal_pmcalib_typ*      calb = (ecal_pmcalib_typ*) 0;
  ecal_pmcalib_typ_ind*   calb_ind = (ecal_pmcalib_typ_ind*)0;
  boolean exists=FALSE;
  
  if(!allCells){
    char sqlECALchan[]="SELECT t.pm_calibs FROM ecal_chan_otb t WHERE t.chan_type=:1 AND t.chan_x=:2 AND t.chan_y=:3";
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALchan, ociError, (text*) sqlECALchan,(ub4) strlen(sqlECALchan),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM: Error preparing stECALchan "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIBindByPos(stECALchan, &bndp[3], ociError, (ub4)1, (dvoid*) &tip, (sb4) sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALchan, &bndp[4], ociError, (ub4)2, (dvoid*) & ix, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALchan, &bndp[5], ociError, (ub4)3, (dvoid*) & iy, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM: Error binding stECALchan "); checkerr(ociError, status, erm); return -1; }
    
    while(type[ich]=='O' || type[ich]=='I' || type[ich]=='M'){
      g0[ich]=0; 
      alpha[ich]=0; 
      PMname[ich*PMname_len]=(char)0;
      if(ECALCellExists(type[ich],x[ich],y[ich])){
	// instantiate the collection 
	status=OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_TABLE, (OCIType *) ecal_pmcalib_list_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &pm_calibs);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM: Error creating pm_calibs instance "); checkerr(ociError, status, erm); return -1; }
	
	status=OCIDefineByPos(stECALchan, &defn1p, ociError, (ub4) 1, (dvoid *) 0, (sb4) 0, SQLT_NTY, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
	if(status==OCI_SUCCESS) 
	  status=OCIDefineObject(defn1p, ociError, ecal_pmcalib_list_tdo, (dvoid**)&pm_calibs, &pm_calibs_size, (dvoid**)0, (ub4*)0 );
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM: Error defining pm_calibs "); checkerr(ociError, status, erm); return -1; }
	
	// set bound variables
	tip=type[ich];
	ix=x[ich];
	iy=y[ich];
	// execute stmt
	status=OCIStmtExecute(ociHdbc, stECALchan, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM: Error executing stECALchan "); checkerr(ociError, status, erm); return -1; }
	
	if( (status=OCIStmtFetch2(stECALchan, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	  sb4 tsiz=0;
	  status=OCICollSize(ociEnv, ociError, (OCIColl*)pm_calibs, &tsiz); 
	  OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
	  for(int i=0; i<tsiz; ++i){
	    OCICollGetElem(ociEnv, ociError, (OCIColl*)pm_calibs, (sb4)i, &exists, (dvoid**)&calb, (dvoid**)&calb_ind);
	    sword res=0;
	    OCIDateCompare(ociError, &maxd, &(calb->start_v), &res);
	    if(res<0){
	      status=OCIDateAssign(ociError, &(calb->start_v), &maxd);
	      if(calb_ind->g0   !=OCI_IND_NULL) OCINumberToReal(ociError, &(calb->g0),    (uword)sizeof(g0[ich]),    (dvoid*)&g0[ich]);
	      if(calb_ind->alpha!=OCI_IND_NULL) OCINumberToReal(ociError, &(calb->alpha), (uword)sizeof(alpha[ich]), (dvoid*)&alpha[ich]);
	      if(calb_ind->pmt_id!=OCI_IND_NULL)strcpy(&PMname[ich*PMname_len], (char*)OCIStringPtr(ociEnv, calb->pmt_id)); 
	    }
	  }
	  if(tsiz>0)++nch;
	}
	status=OCIObjectFree(ociEnv, ociError, pm_calibs, OCI_OBJECTFREE_FORCE);
      }
      ++ich;
    }
    status=OCIStmtRelease(stECALchan, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }else{
    char sql_allSetng[]="SELECT t.pm_calibs, t.chan_type, t.chan_x, t.chan_y FROM ecal_chan_otb t ORDER BY t.chan_type, t.chan_x, t.chan_y ";
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmt_allSetng, ociError, (text*) sql_allSetng,(ub4) strlen(sql_allSetng),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM: Error preparing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    // instantiate the collection 
    status=OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_TABLE, (OCIType *) ecal_pmcalib_list_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &pm_calibs);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM: Error creating pm_calibs instance "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIDefineByPos(stmt_allSetng, &defn4p, ociError, (ub4) 1, (dvoid *) 0, (sb4) 0, SQLT_NTY, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS)
      status=OCIDefineObject(defn4p, ociError, ecal_pmcalib_list_tdo, (dvoid**)&pm_calibs, &pm_calibs_size, (dvoid**)0, (ub4*)0 );
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM: Error defining pm_calibs "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIDefineByPos(stmt_allSetng, &defn7p, ociError, (ub4) 2, (dvoid*)&tip,  (sb4)sizeof(tip),  SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defn8p, ociError, (ub4) 3, (dvoid*)&ix,   (sb4)sizeof(ix),   SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defn9p, ociError, (ub4) 4, (dvoid*)&iy,   (sb4)sizeof(iy),   SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM: Error defining tip, ix, iy "); checkerr(ociError, status, erm); return -1; }
    
    // execute stmt
    status=OCIStmtExecute(ociHdbc, stmt_allSetng, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM: Error executing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    while((status=OCIStmtFetch2(stmt_allSetng, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS || status==OCI_SUCCESS_WITH_INFO ){
      type[nch]=tip;
      x[nch]=ix;
      y[nch]=iy;
      g0[nch]=0; 
      alpha[nch]=0; 
      PMname[nch*PMname_len]=(char)0;
      sb4 tsiz=0;
      status=OCICollSize(ociEnv, ociError, (OCIColl*)pm_calibs, &tsiz); 
      OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
      for(int i=0; i<tsiz; ++i){
	OCICollGetElem(ociEnv, ociError, (OCIColl*)pm_calibs, (sb4)i, &exists, (dvoid**)&calb, (dvoid**)&calb_ind);
	sword res=0;
	OCIDateCompare(ociError, &maxd, &(calb->start_v), &res);
	if(res<0){
	  status=OCIDateAssign(ociError, &(calb->start_v), &maxd);
	  if(calb_ind->g0   !=OCI_IND_NULL) OCINumberToReal(ociError, &(calb->g0),    (uword)sizeof(g0[nch]),    (dvoid*)&g0[nch]);
	  if(calb_ind->alpha!=OCI_IND_NULL) OCINumberToReal(ociError, &(calb->alpha), (uword)sizeof(alpha[nch]), (dvoid*)&alpha[nch]);
	  if(calb_ind->pmt_id!=OCI_IND_NULL)strcpy(&PMname[nch*PMname_len], (char*) OCIStringPtr(ociEnv, calb->pmt_id) ); 
	}
      }
      ++ich;
      if(tsiz>0){
	++nch;
	status=OCICollTrim(ociEnv, ociError, (sb4)tsiz, (OCIColl*)pm_calibs); 
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM: Error trimming collection "); checkerr(ociError, status, erm); return -1; }
      }
    }
    
    status=OCIObjectFree(ociEnv, ociError, pm_calibs, OCI_OBJECTFREE_FORCE);
    status=OCIStmtRelease(stmt_allSetng, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }
  
  sprintf(erm,"ECALGetPM: success %d chan out of %d", nch, ich);
  return nch;
}

EXTERN_CALODB int ECALGetPM_date(const char* thedate, char* type, int* x, int* y, char* PMname, int PMname_len, double* g0, double* alpha, char* erm){
  // the_date is supposed to be in the YYYY-MM-DD HH24:MI:SS format
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALGetPM_date: not connected");return(-1);}
  
  bool allCells=true;
  if( ('O'==type[0]) || ('M'==type[0]) || ('I'==type[0]) ) allCells=false;
  
  sword status;
  OCIDate thed; 
  if(NULL!=thedate){
    char fmt[]="YYYY-MM-DD HH24:MI:SS";
    status=OCIDateFromText(ociError, (oratext*)thedate, strlen(thedate), (oratext*)fmt, strlen(fmt), 0, 0, &thed); 
  }
  else status=OCIDateSysDate(ociError, &thed);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM_date: date error"); checkerr(ociError, status, erm); return -1; }

  OCIDate maxd; OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
  
  OCIStmt* stECALchan;
  OCIStmt* stmt_allSetng;
  int ich=0, nch=0;
  char tip;
  sword ix,iy;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine *defn1p = (OCIDefine *) 0;//, *defn2p = (OCIDefine *) 0, *defn3p = (OCIDefine *) 0;
  OCIDefine *defn4p = (OCIDefine *) 0;//, *defn5p = (OCIDefine *) 0, *defn6p = (OCIDefine *) 0;
  OCIDefine *defn7p = (OCIDefine *) 0, *defn8p = (OCIDefine *) 0, *defn9p = (OCIDefine *) 0;
  ecal_pmcalib_list* pm_calibs = (ecal_pmcalib_list*) 0;
  ub4 pm_calibs_size=0;
  ecal_pmcalib_typ*      calb = (ecal_pmcalib_typ*) 0;
  ecal_pmcalib_typ_ind*   calb_ind = (ecal_pmcalib_typ_ind*)0;
  boolean exists=FALSE;
  
  if(!allCells){
    char sqlECALchan[]="SELECT t.pm_calibs FROM ecal_chan_otb t WHERE t.chan_type=:1 AND t.chan_x=:2 AND t.chan_y=:3";
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALchan, ociError, (text*) sqlECALchan,(ub4) strlen(sqlECALchan),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM_date: Error preparing stECALchan "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIBindByPos(stECALchan, &bndp[3], ociError, (ub4)1, (dvoid*) &tip, (sb4) sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALchan, &bndp[4], ociError, (ub4)2, (dvoid*) & ix, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALchan, &bndp[5], ociError, (ub4)3, (dvoid*) & iy, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM_date: Error binding stECALchan "); checkerr(ociError, status, erm); return -1; }
    
    while(type[ich]=='O' || type[ich]=='I' || type[ich]=='M'){
      g0[ich]=0; 
      alpha[ich]=0; 
      PMname[ich*PMname_len]=(char)0;
      if(ECALCellExists(type[ich],x[ich],y[ich])){
	// instantiate the collection 
	status=OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_TABLE, (OCIType *) ecal_pmcalib_list_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &pm_calibs);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM_date: Error creating pm_calibs instance "); checkerr(ociError, status, erm); return -1; }
	
	status=OCIDefineByPos(stECALchan, &defn1p, ociError, (ub4) 1, (dvoid *) 0, (sb4) 0, SQLT_NTY, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
	if(status==OCI_SUCCESS) 
	  status=OCIDefineObject(defn1p, ociError, ecal_pmcalib_list_tdo, (dvoid**)&pm_calibs, &pm_calibs_size, (dvoid**)0, (ub4*)0 );
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM_date: Error defining pm_calibs "); checkerr(ociError, status, erm); return -1; }
	
	// set bound variables
	tip=type[ich];
	ix=x[ich];
	iy=y[ich];
	// execute stmt
	status=OCIStmtExecute(ociHdbc, stECALchan, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM_date: Error executing stECALchan "); checkerr(ociError, status, erm); return -1; }
	
	if( (status=OCIStmtFetch2(stECALchan, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	  sb4 tsiz=0;
	  status=OCICollSize(ociEnv, ociError, (OCIColl*)pm_calibs, &tsiz); 
	  OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
	  for(int i=0; i<tsiz; ++i){
	    OCICollGetElem(ociEnv, ociError, (OCIColl*)pm_calibs, (sb4)i, &exists, (dvoid**)&calb, (dvoid**)&calb_ind);
	    sword res=0, resd=0;
	    OCIDateCompare(ociError, &maxd, &(calb->start_v), &res);
	    OCIDateCompare(ociError, &thed, &(calb->start_v), &resd);
	    if(res<0 && resd>=0){
	      status=OCIDateAssign(ociError, &(calb->start_v), &maxd);
	      if(calb_ind->g0   !=OCI_IND_NULL) OCINumberToReal(ociError, &(calb->g0),    (uword)sizeof(g0[ich]),    (dvoid*)&g0[ich]);
	      if(calb_ind->alpha!=OCI_IND_NULL) OCINumberToReal(ociError, &(calb->alpha), (uword)sizeof(alpha[ich]), (dvoid*)&alpha[ich]);
	      if(calb_ind->pmt_id!=OCI_IND_NULL)strcpy(&PMname[ich*PMname_len], (char*)OCIStringPtr(ociEnv, calb->pmt_id)); 
	    }
	  }
	  if(tsiz>0)++nch;
	}
	status=OCIObjectFree(ociEnv, ociError, pm_calibs, OCI_OBJECTFREE_FORCE);
      }
      ++ich;
    }
    status=OCIStmtRelease(stECALchan, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }else{
    char sql_allSetng[]="SELECT t.pm_calibs, t.chan_type, t.chan_x, t.chan_y FROM ecal_chan_otb t ORDER BY t.chan_type, t.chan_x, t.chan_y ";
    
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmt_allSetng, ociError, (text*) sql_allSetng,(ub4) strlen(sql_allSetng),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM_date: Error preparing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    // instantiate the collection 
    status=OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_TABLE, (OCIType *) ecal_pmcalib_list_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &pm_calibs);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM_date: Error creating pm_calibs instance "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIDefineByPos(stmt_allSetng, &defn4p, ociError, (ub4) 1, (dvoid *) 0, (sb4) 0, SQLT_NTY, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS)
      status=OCIDefineObject(defn4p, ociError, ecal_pmcalib_list_tdo, (dvoid**)&pm_calibs, &pm_calibs_size, (dvoid**)0, (ub4*)0 );
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM_date: Error defining pm_calibs "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIDefineByPos(stmt_allSetng, &defn7p, ociError, (ub4) 2, (dvoid*)&tip,  (sb4)sizeof(tip),  SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defn8p, ociError, (ub4) 3, (dvoid*)&ix,   (sb4)sizeof(ix),   SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defn9p, ociError, (ub4) 4, (dvoid*)&iy,   (sb4)sizeof(iy),   SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM_date: Error defining tip, ix, iy "); checkerr(ociError, status, erm); return -1; }
    
    // execute stmt
    status=OCIStmtExecute(ociHdbc, stmt_allSetng, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM_date: Error executing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    while((status=OCIStmtFetch2(stmt_allSetng, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS || status==OCI_SUCCESS_WITH_INFO ){
      type[nch]=tip;
      x[nch]=ix;
      y[nch]=iy;
      g0[nch]=0; 
      alpha[nch]=0; 
      PMname[nch*PMname_len]=(char)0;
      sb4 tsiz=0;
      status=OCICollSize(ociEnv, ociError, (OCIColl*)pm_calibs, &tsiz); 
      OCIDateSetDate((OCIDate*)&maxd, (sb2) 1980, (ub1) 7, (ub1) 1);
      for(int i=0; i<tsiz; ++i){
	OCICollGetElem(ociEnv, ociError, (OCIColl*)pm_calibs, (sb4)i, &exists, (dvoid**)&calb, (dvoid**)&calb_ind);
	sword res=0, resd=0;
	OCIDateCompare(ociError, &maxd, &(calb->start_v), &res);
	OCIDateCompare(ociError, &thed, &(calb->start_v), &resd);
	if(res<0 && resd>=0){
	  status=OCIDateAssign(ociError, &(calb->start_v), &maxd);
	  if(calb_ind->g0   !=OCI_IND_NULL) OCINumberToReal(ociError, &(calb->g0),    (uword)sizeof(g0[nch]),    (dvoid*)&g0[nch]);
	  if(calb_ind->alpha!=OCI_IND_NULL) OCINumberToReal(ociError, &(calb->alpha), (uword)sizeof(alpha[nch]), (dvoid*)&alpha[nch]);
	  if(calb_ind->pmt_id!=OCI_IND_NULL)strcpy(&PMname[nch*PMname_len], (char*) OCIStringPtr(ociEnv, calb->pmt_id) ); 
	}
      }
      ++ich;
      if(tsiz>0){
	++nch;
	status=OCICollTrim(ociEnv, ociError, (sb4)tsiz, (OCIColl*)pm_calibs); 
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPM_date: Error trimming collection "); checkerr(ociError, status, erm); return -1; }
      }
    }
    
    status=OCIObjectFree(ociEnv, ociError, pm_calibs, OCI_OBJECTFREE_FORCE);
    status=OCIStmtRelease(stmt_allSetng, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }
  
  sprintf(erm,"ECALGetPM_date: success %d chan out of %d", nch, ich);
  return nch;
}

EXTERN_CALODB int ECALGetCellFiberLengths(char* type, int* x, int* y, double* LED_len, double* PIN_len, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALGetCellFiberLengths: not connected");return(-1);}
  
  bool allCells=true;
  if( ('O'==type[0]) || ('M'==type[0]) || ('I'==type[0]) ) allCells=false;
  
  sword status;
  OCIStmt* stECALchan;
  OCIStmt* stmt_allSetng;
  int ich=0, nch=0;
  char tip;
  sword ix,iy;
  double ledlen=0, pinlen=0;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine *defn1p = (OCIDefine *) 0, *defn2p = (OCIDefine *) 0;//, *defn3p = (OCIDefine *) 0;
  OCIDefine *defn5p = (OCIDefine *) 0, *defn6p = (OCIDefine *) 0;
  OCIDefine *defn7p = (OCIDefine *) 0, *defn8p = (OCIDefine *) 0, *defn9p = (OCIDefine *) 0;
  //boolean exists=FALSE;
  
  if(!allCells){
    char sqlECALchan[]="SELECT ch.ledfib_len, l.pinfib_len FROM ecal_chan_otb ch, ecal_module_table m, ecal_led_otb l WHERE ch.mod_id=m.mod_id AND m.pin_id=l.pin_id AND m.led_num=l.led_num AND ch.chan_type=:1 AND ch.chan_x=:2 AND ch.chan_y=:3";
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALchan, ociError, (text*) sqlECALchan,(ub4) strlen(sqlECALchan),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetCellFiberLengths: Error preparing stECALchan "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS)status=OCIBindByPos(stECALchan, &bndp[3], ociError, (ub4)1, (dvoid*)&tip, (sb4) sizeof(tip), (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIBindByPos(stECALchan, &bndp[4], ociError, (ub4)2, (dvoid*)& ix, (sb4) sizeof(ix) , (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIBindByPos(stECALchan, &bndp[5], ociError, (ub4)3, (dvoid*)& iy, (sb4) sizeof(iy) , (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetCellFiberLengths: Error binding stECALchan "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stECALchan, &defn1p, ociError, (ub4)1, (dvoid*)&ledlen, (sb4)sizeof(ledlen), (ub2)SQLT_FLT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stECALchan, &defn2p, ociError, (ub4)2, (dvoid*)&pinlen, (sb4)sizeof(pinlen), (ub2)SQLT_FLT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetCellFiberLengths: Error defining led_len and pin_len in stECALchan "); checkerr(ociError, status, erm); return -1; }
    
    while(type[ich]=='O' || type[ich]=='I' || type[ich]=='M'){
      LED_len[ich]=0;
      PIN_len[ich]=0;
      if(ECALCellExists(type[ich],x[ich],y[ich])){
	// set bound variables
	tip=type[ich];
	ix=x[ich];
	iy=y[ich];
	// execute stmt
	status=OCIStmtExecute(ociHdbc, stECALchan, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetCellFiberLengths: Error executing stECALchan "); checkerr(ociError, status, erm); return -1; }
	
	if( (status=OCIStmtFetch2(stECALchan, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	  LED_len[ich]=ledlen;
	  PIN_len[ich]=pinlen;
	  ++nch;
	}
      }
      ++ich;
    }
    status=OCIStmtRelease(stECALchan, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }else{
    char sql_allSetng[]="SELECT ch.ledfib_len, l.pinfib_len, ch.chan_type, ch.chan_x, ch.chan_y FROM ecal_chan_otb ch, ecal_module_table m, ecal_led_otb l WHERE ch.mod_id=m.mod_id AND m.pin_id=l.pin_id AND m.led_num=l.led_num AND ch.adc_chan>0 ORDER BY ch.chan_type, ch.chan_x, ch.chan_y";
    
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmt_allSetng, ociError, (text*) sql_allSetng,(ub4) strlen(sql_allSetng),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetCellFiberLengths: Error preparing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmt_allSetng, &defn5p, ociError, (ub4)1, (dvoid*)&ledlen, (sb4)sizeof(ledlen), (ub2)SQLT_FLT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmt_allSetng, &defn6p, ociError, (ub4)2, (dvoid*)&pinlen, (sb4)sizeof(pinlen), (ub2)SQLT_FLT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmt_allSetng, &defn7p, ociError, (ub4)3, (dvoid*)&tip,    (sb4)sizeof(tip),    (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmt_allSetng, &defn8p, ociError, (ub4)4, (dvoid*)&ix,     (sb4)sizeof(ix),     (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmt_allSetng, &defn9p, ociError, (ub4)5, (dvoid*)&iy,     (sb4)sizeof(iy),     (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetCellFiberLengths: Error defining stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    // execute stmt
    status=OCIStmtExecute(ociHdbc, stmt_allSetng, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetCellFiberLengths: Error executing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    while((status=OCIStmtFetch2(stmt_allSetng, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS || status==OCI_SUCCESS_WITH_INFO ){
      type[ich]=tip;
      x[ich]=ix;
      y[ich]=iy;
      LED_len[ich]=ledlen;
      PIN_len[ich]=pinlen;
      ++ich;
      ++nch;
    }
    status=OCIStmtRelease(stmt_allSetng, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }
  
  sprintf(erm,"ECALGetCellFiberLengths: success %d chan out of %d", nch, ich);
  return nch;
}

EXTERN_CALODB int ECALSetCellFiberLengths(char* type, int* x, int* y, double* LED_len, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALSetCellFiberLengthsECALSetCellFiberLengths: not connected");return(-1);}
  
  bool allCells=true;
  if( ('O'==type[0]) || ('M'==type[0]) || ('I'==type[0]) ) allCells=false;
  
  sword status;
  OCIStmt* stECALchan;
  int ich=0, nch=0;
  char tip;
  sword ix,iy;
  double ledlen=0;//, pinlen=0;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  //boolean exists=FALSE;
  
  if(!allCells){
    char sqlECALchan[]="UPDATE ecal_chan_otb ch SET ch.ledfib_len=:1 WHERE ch.chan_type=:2 AND ch.chan_x=:3 AND ch.chan_y=:4";
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALchan, ociError, (text*) sqlECALchan,(ub4) strlen(sqlECALchan),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetCellFiberLengths: Error preparing stECALchan "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS)status=OCIBindByPos(stECALchan, &bndp[1], ociError, (ub4)1, (dvoid*)&ledlen, (sb4)sizeof(ledlen), (ub2)SQLT_FLT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIBindByPos(stECALchan, &bndp[2], ociError, (ub4)2, (dvoid*)&   tip, (sb4)sizeof(tip)   , (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIBindByPos(stECALchan, &bndp[3], ociError, (ub4)3, (dvoid*)&    ix, (sb4)sizeof(ix)    , (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIBindByPos(stECALchan, &bndp[4], ociError, (ub4)4, (dvoid*)&    iy, (sb4)sizeof(iy)    , (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetCellFiberLengths: Error binding stECALchan "); checkerr(ociError, status, erm); return -1; }
    
    while(type[ich]=='O' || type[ich]=='I' || type[ich]=='M'){
      if(ECALCellExists(type[ich],x[ich],y[ich])){
	// set bound variables
	ledlen=LED_len[ich];
	tip=type[ich];
	ix=x[ich];
	iy=y[ich];
	// execute stmt
	status=OCIStmtExecute(ociHdbc, stECALchan, ociError, (ub4)1, (ub4)0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetCellFiberLengths: Error executing stECALchan "); checkerr(ociError, status, erm); return -1; }
	++nch;
      }
      ++ich;
    }
    status=OCIStmtRelease(stECALchan, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }
  
  sprintf(erm,"ECALSetCellFiberLengths: success %d chan out of %d", nch, ich);
  return nch;
}

EXTERN_CALODB int ECALSetPM(char* type, int* x, int* y, char* PMname, int PMname_len, double* g0, double* alpha, char* erm){
  if(!m_connected){strcpy(erm, "ECALSetPM: not connected");return(-1);}
  
  erm[0]=(char)0;
  
  sword status;
  OCIStmt* stPM;			// fetch data from pmt_otab
  OCIStmt* stECALPM;		// put data into ecal_chan_otb
  //OCIStmt* stOG;			// change ecal_modules_otab accordingly (OG == Oleg Guschin)
  int ich=0, nch=0;
  char tip;
  sword ix,iy;//,  mx,my;   // ix, iy - cell coords; mx, my - module coords
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  ecal_pmcalib_typ*      calb = (ecal_pmcalib_typ*) 0;
  ecal_pmcalib_typ_ind*   calb_ind = (ecal_pmcalib_typ_ind*)0;
  ub4 calb_size=0, calb_ind_size=0;
  pmgain_pnt_typ* pmpnt = (pmgain_pnt_typ*) 0;
  pmgain_pnt_typ_ind* pmpnt_ind = (pmgain_pnt_typ_ind*) 0;
  pmt_val_tab* g_data = (pmt_val_tab*) 0;
  ub4 g_data_size = 0;
  boolean exists;
  pmt_val* pmval = (pmt_val*)0;
  pmt_val_ind* pmval_ind = (pmt_val_ind*)0;
  
  char pmnam[20], pmna1[20];
  // pmnam[0]=(char)0;
  // pmnam contains PMT name blank padded to 8 characters (as PMT_ID is declared VARCHAR2(8) in the DB)
  // pmna1 contains PMT name 0-terminated. 
  // pmnam is used in SELECT
  // pmna1 is used in INSERT
  // -- ORACLE idioticity!
  double gg=0, aa=0, cc=0;

  char sqlPM[]="SELECT res.g0, res.alpha, res.g_data FROM pmt_otab pm, TABLE(pm.results) res WHERE pm.pmt_id = :1 AND res.src LIKE 'HAM%'";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stPM, ociError, (text*) sqlPM,(ub4) strlen(sqlPM),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error preparing stPM "); checkerr(ociError, status, erm); return -1; }
  status=OCIBindByPos(stPM, &bndp[0], ociError, (ub4)1, (dvoid*)&pmnam[0], (sb4)8*sizeof(char), (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error binding pmnam to stPM "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stPM, &defnp[1], ociError, (ub4)1, (dvoid*)&gg, (sb4)sizeof(gg), SQLT_FLT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error defining g0 in stPM "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stPM, &defnp[2], ociError, (ub4)2, (dvoid*)&aa, (sb4)sizeof(aa), SQLT_FLT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error defining alpha in stPM "); checkerr(ociError, status, erm); return -1; }
  
  char sqlECALPM[]="INSERT INTO TABLE (SELECT e.pm_calibs FROM ecal_chan_otb e WHERE e.chan_type = :1 AND e.chan_x = :2 AND e.chan_y = :3) VALUES(:4)";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALPM, ociError, (text*) sqlECALPM,(ub4) strlen(sqlECALPM),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error preparing stECALPM "); checkerr(ociError, status, erm); return -1; }
  status=OCIBindByPos(stECALPM, &bndp[3], ociError, (ub4)1, (dvoid*) &tip, (sb4) sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error binding tip to stECALPM "); checkerr(ociError, status, erm); return -1; }
  status=OCIBindByPos(stECALPM, &bndp[4], ociError, (ub4)2, (dvoid*) & ix, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error binding x to stECALPM "); checkerr(ociError, status, erm); return -1; }
  status=OCIBindByPos(stECALPM, &bndp[5], ociError, (ub4)3, (dvoid*) & iy, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error binding y to stECALPM "); checkerr(ociError, status, erm); return -1; }
    
  while(type[ich]=='O' || type[ich]=='I' || type[ich]=='M'){
    g0[ich]=-1;
    alpha[ich]=-1;
    if(ECALCellExists(type[ich],x[ich],y[ich])){
      // instantiate pmt_val_tab
      status=OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_TABLE, (OCIType *) pmt_val_tab_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &g_data);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: ECALGetChanADC: Error creating pmt_val_tab instance g_data "); checkerr(ociError, status, erm); return -1; }
      
      // instantiate ecal_pmcalib_typ
      status=OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_OBJECT, (OCIType *) ecal_pmcalib_typ_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &calb);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error creating ecal_pmcalib_typ instance calb "); checkerr(ociError, status, erm); return -1; }
      status=OCIObjectGetInd(ociEnv, ociError, (dvoid*)calb, (dvoid**)&calb_ind);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error obtaining calb_ind "); checkerr(ociError, status, erm); return -1; }
      
      // instantiate pmgain_pnt_typ
      status=OCIObjectNew(ociEnv, ociError, ociHdbc, (OCITypeCode)OCI_TYPECODE_OBJECT, (OCIType *) pmgain_pnt_typ_tdo, (dvoid *) 0, OCI_DURATION_SESSION, TRUE, (dvoid **) &pmpnt);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error creating pmgain_pnt_typ instance pmpnt "); checkerr(ociError, status, erm); return -1; }
      status=OCIObjectGetInd(ociEnv, ociError, (dvoid*)pmpnt, (dvoid**)&pmpnt_ind);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error obtaining pmpnt_ind "); checkerr(ociError, status, erm); return -1; }
      
      // set bound variables
      strcpy(pmnam,&PMname[ich*PMname_len]);
      strcat(pmnam,"        ");
      pmnam[8]=(char)0;
      tip=type[ich];
      ix=x[ich];
      iy=y[ich];
      
      // define g_data
      status=OCIDefineByPos(stPM, &defnp[3], ociError, (ub4)3, (dvoid*)0, (sb4)0, SQLT_NTY, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
      if(status==OCI_SUCCESS)
	status=OCIDefineObject(defnp[3], ociError, pmt_val_tab_tdo, (dvoid**)&g_data, &g_data_size, (dvoid**)0, (ub4*)0 );
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error defining g_data in stPM "); checkerr(ociError, status, erm); return -1; }
      // execute stPM
      status=OCIStmtExecute(ociHdbc, stPM, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error executing stPM "); checkerr(ociError, status, erm); return -1; }
      
      if( (status=OCIStmtFetch2(stPM, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	g0[ich]=gg;
	alpha[ich]=aa;
	
	calb_ind->_atomic=OCI_IND_NOTNULL;
	calb_ind->pmt_id=OCI_IND_NOTNULL;
	sscanf(pmnam,"%s",pmna1);
	status=OCIStringAssignText(ociEnv, ociError, (text*)pmna1, (ub4)strlen(pmna1), &(calb->pmt_id) );
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error setting pmnam = %s",pmnam); checkerr(ociError, status, erm); return -1; }
	calb_ind->g0=OCI_IND_NOTNULL;
	status=OCINumberFromReal(ociError, &gg, sizeof(gg), &(calb->g0) );
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error setting g0 "); checkerr(ociError, status, erm); return -1; }
	calb_ind->alpha=OCI_IND_NOTNULL;
	status=OCINumberFromReal(ociError, &aa, sizeof(aa), &(calb->alpha) );
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error setting alpha "); checkerr(ociError, status, erm); return -1; }
	calb_ind->chrg=OCI_IND_NOTNULL;
	cc=0;
	status=OCINumberFromReal(ociError, &cc, sizeof(cc), &(calb->chrg) );
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error setting chrg=0 "); checkerr(ociError, status, erm); return -1; }
	calb_ind->start_v=OCI_IND_NOTNULL;
	status=OCIDateSysDate(ociError, &(calb->start_v) );
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error setting start_v "); checkerr(ociError, status, erm); return -1; }
	
	sb4 tsiz=0;
	status=OCICollSize(ociEnv, ociError, (OCIColl*)g_data, &tsiz); 
	for(int i=0; i<tsiz; ++i){
	  pmpnt_ind->_atomic=OCI_IND_NULL;
	  pmpnt_ind->hv=OCI_IND_NULL;
	  pmpnt_ind->gain=OCI_IND_NULL;
	  OCICollGetElem(ociEnv, ociError, (OCIColl*)g_data, (sb4)i, &exists, (dvoid**)&pmval, (dvoid**)&pmval_ind);
	  text* descr_ptr=OCIStringPtr(ociEnv, pmval->descr);
	  if(0==strcmp( (char*)descr_ptr, "ghv5k") ){
	    pmpnt_ind->_atomic=OCI_IND_NOTNULL;
	    pmpnt_ind->hv=OCI_IND_NOTNULL;
	    pmpnt_ind->gain=OCI_IND_NOTNULL;
	    gg=5000;
	    status=OCINumberFromReal(ociError, &gg, sizeof(gg), &(pmpnt->gain) );
	    if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error setting gain=5000"); checkerr(ociError, status, erm); return -1; }
	    status=OCINumberAssign(ociError, &(pmval->value), &(pmpnt->hv));
	    if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error setting hv5000"); checkerr(ociError, status, erm); return -1; }
	  }else if(0==strcmp( (char*)descr_ptr, "ghv10k") ){
	    pmpnt_ind->_atomic=OCI_IND_NOTNULL;
	    pmpnt_ind->hv=OCI_IND_NOTNULL;
	    pmpnt_ind->gain=OCI_IND_NOTNULL;
	    gg=10000;
	    status=OCINumberFromReal(ociError, &gg, sizeof(gg), &(pmpnt->gain) );
	    if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error setting gain=10000"); checkerr(ociError, status, erm); return -1; }
	    status=OCINumberAssign(ociError, &(pmval->value), &(pmpnt->hv));
	    if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error setting hv10000"); checkerr(ociError, status, erm); return -1; }
	  }else if(0==strcmp( (char*)descr_ptr, "ghv50k") ){
	    pmpnt_ind->_atomic=OCI_IND_NOTNULL;
	    pmpnt_ind->hv=OCI_IND_NOTNULL;
	    pmpnt_ind->gain=OCI_IND_NOTNULL;
	    gg=50000;
	    status=OCINumberFromReal(ociError, &gg, sizeof(gg), &(pmpnt->gain) );
	    if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error setting gain=50000"); checkerr(ociError, status, erm); return -1; }
	    status=OCINumberAssign(ociError, &(pmval->value), &(pmpnt->hv));
	    if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error setting hv50000"); checkerr(ociError, status, erm); return -1; }
	  }else if(0==strcmp( (char*)descr_ptr, "ghv1m") ){
	    pmpnt_ind->_atomic=OCI_IND_NOTNULL;
	    pmpnt_ind->hv=OCI_IND_NOTNULL;
	    pmpnt_ind->gain=OCI_IND_NOTNULL;
	    gg=1000000;
	    status=OCINumberFromReal(ociError, &gg, sizeof(gg), &(pmpnt->gain) );
	    if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error setting gain=1000000"); checkerr(ociError, status, erm); return -1; }
	    status=OCINumberAssign(ociError, &(pmval->value), &(pmpnt->hv));
	    if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error setting hv1000000"); checkerr(ociError, status, erm); return -1; }
	  }
	  if(pmpnt_ind->_atomic == OCI_IND_NOTNULL){
	    calb_ind->pmgain_pnts=OCI_IND_NOTNULL;
	    status=OCICollAppend(ociEnv, ociError, (dvoid*)pmpnt, (dvoid*)pmpnt_ind, (OCIColl*)calb->pmgain_pnts); 
	  }
	}
	// bind calb
	status=OCIBindByPos(stECALPM, &bndp[6], ociError, (ub4)4, (dvoid*)0, (sb4)0, (ub2) SQLT_NTY, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4) 0, (ub4*) 0, (ub4) OCI_DEFAULT);
	if(status==OCI_SUCCESS) status=OCIBindObject(bndp[6], ociError, ecal_pmcalib_typ_tdo, (dvoid**)&calb, (ub4*)&calb_size, (dvoid**)&calb_ind, (ub4*)&calb_ind_size);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error binding calb to stECALPM "); checkerr(ociError, status, erm); return -1; }
	// execute stECALPM
	status=OCIStmtExecute(ociHdbc, stECALPM, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPM: Error executing stECALPM "); checkerr(ociError, status, erm); return -1; }
	++nch;
      }
      status=OCIObjectFree(ociEnv, ociError, g_data, OCI_OBJECTFREE_FORCE);
      status=OCIObjectFree(ociEnv, ociError, calb, OCI_OBJECTFREE_FORCE);
      status=OCIObjectFree(ociEnv, ociError, pmpnt, OCI_OBJECTFREE_FORCE);
    }
    ++ich;
  }
  status=OCIStmtRelease(stECALPM, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  status=OCIStmtRelease(stPM, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALSetPM: success %d chan out of %d", nch, ich);
  return nch;
}

EXTERN_CALODB int ECALCheckUniqPM(char* PMname, int PMname_len, char* type1, int* x1, int* y1, char* type2, int* x2, int* y2, char* erm){
  if(!m_connected){strcpy(erm, "ECALCheckUniqPM: not connected");return(-1);}
  erm[0]=(char)0;
  
  sword status;
  OCIStmt* stmtp;
  int nch=0;
  char tip1, tip2;
  sword ix1, ix2,iy1,iy2;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  char pmnam[20];
  
  char sqlst[]="SELECT cal1.pmt_id, ch1.chan_type, ch1.chan_x, ch1.chan_y, ch2.chan_type, ch2.chan_x, ch2.chan_y "
    " FROM ecal_chan_otb ch1, ecal_chan_otb ch2, TABLE(ch1.pm_calibs) cal1, TABLE(ch2.pm_calibs) cal2 "
    " WHERE cal1.start_v=(SELECT MAX(calp1.start_v) FROM TABLE(ch1.pm_calibs) calp1) "
    " AND   cal2.start_v=(SELECT MAX(calp2.start_v) FROM table(ch2.pm_calibs) calp2) "
    " AND   cal1.pmt_id=cal2.pmt_id "
    " AND   ch1.dac_chan > ch2.dac_chan "
    " AND   ch2.dac_chan > 0 "
    " AND  (ch1.chan_type != ch2.chan_type OR ch1.chan_x != ch2.chan_x OR ch1.chan_y != ch2.chan_y)";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmtp, ociError, (text*) sqlst,(ub4) strlen(sqlst),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALCheckUniqPM: Error preparing stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[1], ociError, (ub4)1, (dvoid*)&pmnam[0], (sb4)sizeof(pmnam), SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALCheckUniqPM: Error defining pmnam in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[2], ociError, (ub4)2, (dvoid*)&tip1, (sb4)sizeof(tip1), SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALCheckUniqPM: Error defining tip1 in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[3], ociError, (ub4)3, (dvoid*)&ix1, (sb4)sizeof(ix1), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALCheckUniqPM: Error defining ix1 in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[4], ociError, (ub4)4, (dvoid*)&iy1, (sb4)sizeof(iy1), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALCheckUniqPM: Error defining iy1 in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[5], ociError, (ub4)5, (dvoid*)&tip2, (sb4)sizeof(tip2), SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALCheckUniqPM: Error defining tip2 in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[6], ociError, (ub4)6, (dvoid*)&ix2, (sb4)sizeof(ix2), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALCheckUniqPM: Error defining ix2 in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[7], ociError, (ub4)7, (dvoid*)&iy2, (sb4)sizeof(iy2), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALCheckUniqPM: Error defining iy2 in stmtp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmtp
  status=OCIStmtExecute(ociHdbc, stmtp, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALCheckUniqPM: Error executing stmtp "); checkerr(ociError, status, erm); return -1; }
  
  while( (status=OCIStmtFetch2(stmtp, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
    sscanf(pmnam,"%s",&PMname[PMname_len*nch]);
    type1[nch]=tip1;
    type2[nch]=tip2;
    x1[nch]=ix1;
    x2[nch]=ix2;
    y1[nch]=iy1;
    y2[nch]=iy2;
    ++nch;
  }
  type1[nch]=type2[nch]=(char)0;

  status=OCIStmtRelease(stmtp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  if(nch==0)sprintf(erm,"ECALCheckUniqPM: no duplicated PMs ");
  else sprintf(erm,"ECALCheckUniqPM: %d duplicated PMs ",nch);
  return nch;
}

EXTERN_CALODB int LHCBCALOdbConnect(char* erm){
  sword status;
  if(!m_connected){
    status=ConnectUser("LHCB_ECAL", "LHCB_ECAL", "tests2007", ociEnv, mysrvhp, myusrhp, ociHdbc, ociError,erm);	
    if(status!=0){
      ociError=0; /* the error handle */ 
      ociEnv=0; /* the environment handle */ 
      ociHdbc=0; /* the context handle */
      mysrvhp=0; /* the server handle */
      myusrhp=0; /* user session handle */
      return status;
    }
    status=all_tdos(erm);
    if(status!=OCI_SUCCESS){ return -1; }
    
    m_connected=true;
  }else{
    sprintf(erm,"LHCBCALOdbConnect: already connected "); 
    return OCI_SUCCESS_WITH_INFO;
  }
  
  sprintf(erm,"LHCBCALOdbConnect: success");
  
  return(OCI_SUCCESS);
}

EXTERN_CALODB int LHCBCALOdbConnectUser(const char* usr, const char* server, const char* pwd, char* erm){

  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
    sword status;
  
  if(!m_connected){
    status=ConnectUser(usr, server, pwd, ociEnv, mysrvhp, myusrhp, ociHdbc, ociError, erm);	
    if(status!=0){
      ociError=0; /* the error handle */ 
      ociEnv=0; /* the environment handle */ 
      ociHdbc=0; /* the context handle */
      mysrvhp=0; /* the server handle */
      myusrhp=0; /* user session handle */
      return status;
    }
    m_connected=true;
    
    // getting the schema name
    OCIStmt* stmthp;
    char sch[50], shema[50];
    char tmpstmt[]="SELECT distinct table_schema FROM all_tab_privs WHERE table_name='HCAL_CHAN_OTB' ";
    status=OCIStmtPrepare2(ociHdbc, (OCIStmt**)&stmthp, ociError, (text*)tmpstmt, (ub4)strlen(tmpstmt), NULL, 0, (ub4)OCI_NTV_SYNTAX, (ub4)OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"LHCBCALOdbConnectUser: Error preparing statement SELECT table_schema"); checkerr(ociError, status, erm); return -1; }
    
    status=OCIDefineByPos (stmthp, &defnp[1], ociError, (ub4)1, (dvoid*)sch, (sb4)30*sizeof(char), SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"LHCBCALOdbConnectUser: Error defining sch "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIStmtExecute(ociHdbc, stmthp, ociError, 0, 0, (OCISnapshot*)0, (OCISnapshot*)0, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"LHCBCALOdbConnectUser: Error executing statement SELECT table_schema "); checkerr(ociError, status, erm); return -1; }
    status=OCIStmtFetch2(stmthp,ociError, 1, OCI_FETCH_NEXT, 1, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"LHCBCALOdbConnectUser: Error fetching result "); checkerr(ociError, status, erm); return -1; }
    sch[30]='\0';
    int nit=sscanf(sch,"%s",shema);
    if(nit<1){sprintf(erm,"LHCBCALOdbConnectUser: Wrong schema name "); return -1; }
    status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"LHCBCALOdbConnectUser: Error releasing handle "); checkerr(ociError, status, erm); return -1; }
    
    // setting current_schema
    status=OCIAttrSet((dvoid *)myusrhp, OCI_HTYPE_SESSION, (dvoid*)shema, (ub4)strlen((char*)shema), OCI_ATTR_CURRENT_SCHEMA, ociError);
    if(status!=OCI_SUCCESS){sprintf(erm,"LHCBCALOdbConnectUser: Error setting CURRENT_SCHEMA"); checkerr(ociError, status, erm); return -1; }
    
    // creating tdo's
    status=all_tdos(erm);
    if(status!=OCI_SUCCESS){ return -1; }
  }else{
    sprintf(erm,"LHCBCALOdbConnect: already connected "); 
    return OCI_SUCCESS_WITH_INFO;
  }
  
  sprintf(erm,"LHCBCALOdbConnect: success");
  
  return(OCI_SUCCESS);
}

EXTERN_CALODB int LHCBCALOdbClose(char* erm){
  int status=0;
  if(m_connected){
    status=Disconnect(ociEnv,ociError,mysrvhp,myusrhp, ociHdbc,5,erm);
    ociError=0; /* the error handle */ 
    ociEnv=0; /* the environment handle */ 
    ociHdbc=0; /* the context handle */
    mysrvhp=0; /* the server handle */
    myusrhp=0; /* user session handle */
    m_connected=false;
  }else{
    strcpy(erm,"LHCBCALOdbClose: not connected");
  }
  return status;
}

EXTERN_CALODB int LHCBCALOCommit(char* erm){
  if(!m_connected){strcpy(erm, "LHCBCALOCommit: not connected");return(-1);}
  
  int status=OCITransCommit(ociHdbc, ociError, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"LHCBCALOCommit: Error "); checkerr(ociError, status, erm); return -1; }
  strcpy(erm,"LHCBCALOCommit: success");
  return status;
}

EXTERN_CALODB int LHCBCALORollBack(char* erm){
  if(!m_connected){strcpy(erm, "LHCBCALORollBack: not connected");return(-1);}
  
  int status=OCITransRollback(ociHdbc, ociError, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"LHCBCALORollBack: Error "); checkerr(ociError, status, erm); return -1; }
  strcpy(erm,"LHCBCALORollBack: success");
  return status;
}


EXTERN_CALODB int isLHCBCALOdbConnected(){
  if(m_connected)return 1; 
  else return 0;
}


//internal fct to disconnect to DB
sword Disconnect(OCIEnv* env,OCIError* err,OCIServer* mysrvhp1,OCISession* myusrhp1, OCISvcCtx* mysvchp, int count_free, char* ErrorMessage)   
{
  sword status=OCI_SUCCESS;
  
  if(count_free>4)status=OCISessionEnd (mysvchp,err,myusrhp1,OCI_DEFAULT);
  if(count_free>3)status=OCIServerDetach (mysrvhp1,err,OCI_DEFAULT);
  
  if(count_free>4)status=OCIHandleFree(myusrhp1,OCI_HTYPE_SESSION);
  if(count_free>3)status+=OCIHandleFree(mysvchp,OCI_HTYPE_SVCCTX);
  if(count_free>2)status+=OCIHandleFree (err, OCI_HTYPE_ERROR);
  if(count_free>1)status+=OCIHandleFree(mysrvhp1,OCI_HTYPE_SERVER);
  if(count_free>0)status+=OCIHandleFree (env, OCI_HTYPE_ENV);
  
  if(status== OCI_SUCCESS)sprintf(ErrorMessage,"OCI connection terminated successfully");
  else sprintf(ErrorMessage,"BAD termination of OCI connection");
  
  return status;
}

//internal fct to connect to DB
sword Connect(OCIEnv*& myenvhp,OCIServer*& mysrvhp1,OCISession*& myusrhp1, OCISvcCtx*& mysvchp, OCIError*& myerrhp, char* ErrorMessage)      
{
  sword status=OCI_SUCCESS;
  int count_free=0;
  int rescode=0;
  char errmessg[1024];
  char server[]="LHCB_ECAL";
  char usr[]="LHCB_ECAL";
  char pwd[]="calo2006";
  
  /* initialize the mode to be not threaded and object environment */
  status=OCIEnvCreate(&myenvhp, OCI_OBJECT, (dvoid *)0,0, 0, 0, (size_t) 0, (dvoid **)0);
  if(status!= OCI_SUCCESS){ 
    count_free++; //1
    status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
    if(status==OCI_SUCCESS)	status=-1;
    rescode= -1;
  }else{	/* allocate a server handle */
    count_free++; //1
    status=OCIHandleAlloc ((dvoid *)myenvhp, (dvoid **)&mysrvhp1,OCI_HTYPE_SERVER, 0, (dvoid **) 0);
  }
  
  if(status!= OCI_SUCCESS ){ 
    if(rescode==0)	{
      count_free++;  //2
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else{	/* allocate an error handle */
    count_free++;
    status=OCIHandleAlloc ((dvoid *)myenvhp, (dvoid **)&myerrhp,OCI_HTYPE_ERROR, 0, (dvoid **) 0);
  }
  
  if(status!= OCI_SUCCESS){ 
    if(rescode==0){
      count_free++;  //3
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else{/* execute mappings */
    status=calotypo(myenvhp, myerrhp);
    if(status== OCI_SUCCESS)status=ecalmoduleitemo(myenvhp, myerrhp);
  }
  
  if(status!= OCI_SUCCESS){ 
    if(rescode==0){
      // count_free++;  // still ==3
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else{/* create a server context */
    count_free++;
    status=OCIServerAttach (mysrvhp1, myerrhp, (text *)server, (sb4)strlen(server), OCI_DEFAULT);
  }
  
  if(status!= OCI_SUCCESS){ 
    if(rescode==0){
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else{/* allocate a service handle */
    status=OCIHandleAlloc ((dvoid *)myenvhp, (dvoid **)&mysvchp,OCI_HTYPE_SVCCTX, 0, (dvoid **) 0);
  }
  
  if(status!= OCI_SUCCESS){ 
    if(rescode==0){
      count_free++;  //4
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else{/* set the server attribute in the service context handle*/
    count_free++;
    status=OCIAttrSet ((dvoid *)mysvchp, OCI_HTYPE_SVCCTX,(dvoid *)mysrvhp1, (ub4) 0, OCI_ATTR_SERVER, myerrhp);
  }
  
  if(status!= OCI_SUCCESS){ 
    if(rescode==0){
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else{/* allocate a user session handle */
    status=OCIHandleAlloc ((dvoid *)myenvhp, (dvoid **)&myusrhp1,OCI_HTYPE_SESSION, 0, (dvoid **) 0);
  }
  
  if(status!= OCI_SUCCESS){ 
    if(rescode==0){
      count_free++;//5
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else{/* set user name attribute in user session handle */
    status=OCIAttrSet ((dvoid *)myusrhp1, OCI_HTYPE_SESSION,(dvoid *)usr, (ub4)strlen(usr),OCI_ATTR_USERNAME, myerrhp);
    count_free++;
  }
  
  
  if(status!= OCI_SUCCESS){ 
    if(rescode==0){
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else{/* set password attribute in user session handle */
    status=OCIAttrSet ((dvoid *)myusrhp1, OCI_HTYPE_SESSION,(dvoid *)pwd, (ub4)strlen(pwd),OCI_ATTR_PASSWORD, myerrhp);
  }
  
  if(status!= OCI_SUCCESS){ 
    if(rescode==0){
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else	status=OCISessionBegin(mysvchp, myerrhp, myusrhp1, OCI_CRED_RDBMS, OCI_STMT_CACHE);

  if(status!= OCI_SUCCESS){ 
    if(rescode==0){
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else{/* set the user session attribute in the service context handle*/
    status=OCIAttrSet ( (dvoid *)mysvchp, OCI_HTYPE_SVCCTX,(dvoid *)myusrhp1, (ub4) 0, OCI_ATTR_SESSION, myerrhp);
  }
  
  if(status!= OCI_SUCCESS){ 
    if(rescode==0){
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }
  
  ub4 stmtCacheSiz=0;
  status=OCIAttrGet((dvoid*)mysvchp, (ub4)OCI_HTYPE_SVCCTX, (ub4*)&stmtCacheSiz, (ub4)0, OCI_ATTR_STMTCACHESIZE, myerrhp);
  if(20==stmtCacheSiz){
    stmtCacheSiz=100;
    status=OCIAttrSet((dvoid*)mysvchp, (ub4)OCI_HTYPE_SVCCTX, (ub4*)&stmtCacheSiz, (ub4)0, OCI_ATTR_STMTCACHESIZE, myerrhp);
    status=OCIAttrGet((dvoid*)mysvchp, (ub4)OCI_HTYPE_SVCCTX, (ub4*)&stmtCacheSiz, (ub4)0, OCI_ATTR_STMTCACHESIZE, myerrhp);
  }
  
  if(0==rescode)sprintf(ErrorMessage,"OCI connection - success");
  else sprintf(ErrorMessage,"OCI connection failed");
  
  return (rescode);
}

//internal fct to connect to DB
sword ConnectUser(const char* usr, const char* server, const char* pwd, OCIEnv*& myenvhp, OCIServer*& mysrvhp1, OCISession*& myusrhp1, OCISvcCtx*& mysvchp, OCIError*& myerrhp, char* ErrorMessage)      
{
  sword status=OCI_SUCCESS;
  int count_free=0;
  int rescode=0;
  char errmessg[1024]; errmessg[0]='\0';
  
  /* initialize the mode to be not threaded and object environment */
  status=OCIEnvCreate(&myenvhp, OCI_OBJECT, (dvoid *)0,0, 0, 0, (size_t) 0, (dvoid **)0);
  if(status!= OCI_SUCCESS){ 
    count_free++; //1
    status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
    if(status==OCI_SUCCESS)	status=-1;
    rescode= -1;
  }else{	/* allocate a server handle */
    count_free++; //1
    status=OCIHandleAlloc ((dvoid *)myenvhp, (dvoid **)&mysrvhp1,OCI_HTYPE_SERVER, 0, (dvoid **) 0);
  }
  
  if(status!= OCI_SUCCESS ){ 
    if(rescode==0)	{
      count_free++;  //2
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else{	/* allocate an error handle */
    count_free++;
    status=OCIHandleAlloc ((dvoid *)myenvhp, (dvoid **)&myerrhp,OCI_HTYPE_ERROR, 0, (dvoid **) 0);
  }
  
  if(status!= OCI_SUCCESS){ 
    if(rescode==0){
      count_free++;  //3
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else{/* execute mappings */
    status=calotypo(myenvhp, myerrhp);
    if(status== OCI_SUCCESS)status=ecalmoduleitemo(myenvhp, myerrhp);
  }
  
  if(status!= OCI_SUCCESS){ 
    if(rescode==0){
      // count_free++;  // still ==3
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else{/* create a server context */
    count_free++;
    status=OCIServerAttach (mysrvhp1, myerrhp, (text *)server, (sb4)strlen(server), OCI_DEFAULT);
  }
  
  if(status!= OCI_SUCCESS){ 
    if(rescode==0){
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else{/* allocate a service handle */
    status=OCIHandleAlloc ((dvoid *)myenvhp, (dvoid **)&mysvchp,OCI_HTYPE_SVCCTX, 0, (dvoid **) 0);
  }
  
  if(status!= OCI_SUCCESS){ 
    if(rescode==0){
      count_free++;  //4
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else{/* set the server attribute in the service context handle*/
    count_free++;
    status=OCIAttrSet ((dvoid *)mysvchp, OCI_HTYPE_SVCCTX,(dvoid *)mysrvhp1, (ub4) 0, OCI_ATTR_SERVER, myerrhp);
  }
  
  if(status!= OCI_SUCCESS){ 
    if(rescode==0){
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else{/* allocate a user session handle */
    status=OCIHandleAlloc ((dvoid *)myenvhp, (dvoid **)&myusrhp1,OCI_HTYPE_SESSION, 0, (dvoid **) 0);
  }
  
  if(status!= OCI_SUCCESS){ 
    if(rescode==0){
      count_free++;//5
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else{/* set user name attribute in user session handle */
    status=OCIAttrSet ((dvoid *)myusrhp1, OCI_HTYPE_SESSION,(dvoid *)usr, (ub4)strlen(usr),OCI_ATTR_USERNAME, myerrhp);
    count_free++;
  }
  
  
  if(status!= OCI_SUCCESS){ 
    if(rescode==0){
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else{/* set password attribute in user session handle */
    status=OCIAttrSet ((dvoid *)myusrhp1, OCI_HTYPE_SESSION,(dvoid *)pwd, (ub4)strlen(pwd),OCI_ATTR_PASSWORD, myerrhp);
  }
  
  if(status!= OCI_SUCCESS){ 
    if(rescode==0){
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else	status=OCISessionBegin(mysvchp, myerrhp, myusrhp1, OCI_CRED_RDBMS, OCI_STMT_CACHE);
  
  if(status!= OCI_SUCCESS){ 
    checkerr(ociError, status, errmessg); 
    if(rescode==0){
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else{/* set the user session attribute in the service context handle*/
    status=OCIAttrSet ( (dvoid *)mysvchp, OCI_HTYPE_SVCCTX,(dvoid *)myusrhp1, (ub4) 0, OCI_ATTR_SESSION, myerrhp);
  }
  
  if(status!= OCI_SUCCESS){ 
    if(rescode==0){
      status=Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
      if(status==OCI_SUCCESS)	status=-1;
      rescode= -1;
    }
  }else{  // enlarge cache if default (=20)
    ub4 stmtCacheSiz=0;
    status=OCIAttrGet((dvoid*)mysvchp, (ub4)OCI_HTYPE_SVCCTX, (ub4*)&stmtCacheSiz, (ub4)0, OCI_ATTR_STMTCACHESIZE, myerrhp);
    if(20==stmtCacheSiz){
      stmtCacheSiz=100;
      status=OCIAttrSet((dvoid*)mysvchp, (ub4)OCI_HTYPE_SVCCTX, (ub4*)&stmtCacheSiz, (ub4)0, OCI_ATTR_STMTCACHESIZE, myerrhp);
      status=OCIAttrGet((dvoid*)mysvchp, (ub4)OCI_HTYPE_SVCCTX, (ub4*)&stmtCacheSiz, (ub4)0, OCI_ATTR_STMTCACHESIZE, myerrhp);
    }
  }

  if(0==rescode)sprintf(ErrorMessage,"OCI connection - success");
  else sprintf(ErrorMessage,"OCI connection failed");
  
  return (rescode);
}

sword all_tdos(char* erm){
  sword status=0;
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "ECAL_CHAN_TYP", (ub4) strlen("ECAL_CHAN_TYP"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &ecal_chan_typ_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating ecal_chan_typ_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "HCAL_CHAN_TYP", (ub4) strlen("HCAL_CHAN_TYP"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &hcal_chan_typ_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating hcal_chan_typ_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "ECAL_PMCALIB_TYP", (ub4) strlen("ECAL_PMCALIB_TYP"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &ecal_pmcalib_typ_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating ecal_pmcalib_typ_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "ECAL_PMCALIB_LIST", (ub4) strlen("ECAL_PMCALIB_LIST"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &ecal_pmcalib_list_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating ecal_pmcalib_list_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "HCAL_PMCALIB_TYP", (ub4) strlen("HCAL_PMCALIB_TYP"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &hcal_pmcalib_typ_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating hcal_pmcalib_typ_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "HCAL_PMCALIB_LIST", (ub4) strlen("HCAL_PMCALIB_LIST"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &hcal_pmcalib_list_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating hcal_pmcalib_list_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "DAC_SETTING_TYP", (ub4) strlen("DAC_SETTING_TYP"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &dac_setting_typ_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating dac_setting_typ_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "DAC_SETTING_LIST", (ub4) strlen("DAC_SETTING_LIST"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &dac_setting_list_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating dac_setting_list_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "ADC_SETTING_TYP", (ub4) strlen("ADC_SETTING_TYP"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &adc_setting_typ_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating adc_setting_typ_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "ADC_SETTING_LIST", (ub4) strlen("ADC_SETTING_LIST"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &adc_setting_list_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating adc_setting_list_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "HCAL_CSCALIB_TYP", (ub4) strlen("HCAL_CSCALIB_TYP"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &hcal_cscalib_typ_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating hcal_cscalib_typ_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "HCAL_CSCALIB_LIST", (ub4) strlen("HCAL_CSCALIB_LIST"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &hcal_cscalib_list_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating hcal_cscalib_list_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "TILE_CURR_TYP", (ub4) strlen("TILE_CURR_TYP"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &tile_curr_typ_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating tile_curr_typ_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "TILE_CURR_LIST", (ub4) strlen("TILE_CURR_LIST"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &tile_curr_list_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating tile_curr_list_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "PMGAIN_PNT_TYP", (ub4) strlen("PMGAIN_PNT_TYP"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &pmgain_pnt_typ_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating pmgain_pnt_typ_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "PMGAIN_PNT_LIST", (ub4) strlen("PMGAIN_PNT_LIST"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &pmgain_pnt_list_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating pmgain_pnt_list_tdo "); checkerr(ociError, status, erm); return -1; }

  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "HCAL_LEDPIN_TYP", (ub4) strlen("HCAL_LEDPIN_TYP"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &hcal_ledpin_typ_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating hcal_ledpin_typ_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "ECAL_LED_TYP", (ub4) strlen("ECAL_LED_TYP"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &ecal_led_typ_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating ecal_led_typ_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "ECAL_PIN_TYP", (ub4) strlen("ECAL_PIN_TYP"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &ecal_pin_typ_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating ecal_pin_typ_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "LEDPIN_CALIB_TYP", (ub4) strlen("LEDPIN_CALIB_TYP"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &ledpin_calib_typ_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating ledpin_calib_typ_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "LEDPIN_CALIB_LIST", (ub4) strlen("LEDPIN_CALIB_LIST"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &ledpin_calib_list_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating ledpin_calib_list_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "LEDPIN_PNT_TYP", (ub4) strlen("LEDPIN_PNT_TYP"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &ledpin_pnt_typ_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating ledpin_pnt_typ_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "LEDPIN_PNT_LIST", (ub4) strlen("LEDPIN_PNT_LIST"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &ledpin_pnt_list_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating ledpin_pnt_list_tdo "); checkerr(ociError, status, erm); return -1; }
  //
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "ECAL_LEDPIN_CALIB_TYP", (ub4) strlen("ECAL_LEDPIN_CALIB_TYP"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &ecal_ledpin_calib_typ_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating ecal_ledpin_calib_typ_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "ECAL_LEDPIN_CALIB_LIST", (ub4) strlen("ECAL_LEDPIN_CALIB_LIST"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &ecal_ledpin_calib_list_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating ecal_ledpin_calib_list_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "LEDDAC_SETTING_TYP", (ub4) strlen("LEDDAC_SETTING_TYP"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &leddac_setting_typ_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating leddac_setting_typ_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "LEDDAC_SETTING_LIST", (ub4) strlen("LEDDAC_SETTING_LIST"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &leddac_setting_list_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating leddac_setting_list_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "LEDTSB_SETTING_TYP", (ub4) strlen("LEDTSB_SETTING_TYP"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &ledtsb_setting_typ_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating ledtsb_setting_typ_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "LEDTSB_SETTING_LIST", (ub4) strlen("LEDTSB_SETTING_LIST"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &ledtsb_setting_list_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating ledtsb_setting_list_tdo "); checkerr(ociError, status, erm); return -1; }

  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "HCAL_LEDPINREF_TYP", (ub4) strlen("HCAL_LEDPINREF_TYP"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &hcal_ledpinref_typ_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating hcal_ledpinref_typ_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "HCAL_LEDPINREF_LIST", (ub4) strlen("HCAL_LEDPINREF_LIST"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &hcal_ledpinref_list_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating hcal_ledpinref_list_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "HCAL_MODULE_TYP", (ub4) strlen("HCAL_MODULE_TYP"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &hcal_module_typ_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating hcal_module_typ_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "PMT_ITEM", (ub4) strlen("PMT_ITEM"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &pmt_item_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating pmt_item_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "PMT_RES_ITEM", (ub4) strlen("PMT_RES_ITEM"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &pmt_res_item_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating pmt_res_item_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "PMT_VAL", (ub4) strlen("PMT_VAL"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &pmt_val_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating pmt_val_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "CW_ITEM", (ub4) strlen("CW_ITEM"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &cw_item_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating cw_item_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "PMT_LST_RES", (ub4) strlen("PMT_LST_RES"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &pmt_lst_res_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating pmt_lst_res_tdo "); checkerr(ociError, status, erm); return -1; }
  
  status=OCITypeByName(ociEnv, ociError, ociHdbc, (CONST text *)"", (ub4)0,
		       (CONST text *) "PMT_VAL_TAB", (ub4) strlen("PMT_VAL_TAB"),
		       (CONST text *) 0, (ub4) 0, OCI_DURATION_SESSION,
		       OCI_TYPEGET_HEADER, &pmt_val_tab_tdo);
  if(status!=OCI_SUCCESS){sprintf(erm,"all_tdos: Error creating pmt_val_tab_tdo "); checkerr(ociError, status, erm); return -1; }
  
  return OCI_SUCCESS;
}

EXTERN_CALODB bool ECALCellExists(char typ, int x, int y){
  if(typ!='O' && typ!='M' && typ!='I')return false;
  if('O'==typ){
    if(x<0 || x>63 || y<6 || y>57)return false;
    if(x>=16 && x<=47 && y>=22 && y<=41)return false;
  }else if('M'==typ){
    if(x<0 || x>63 || y<12 || y>51)return false;
    if(x>=16 && x<=47 && y>=20 && y<=43)return false;
  }else if('I'==typ){
    if(x<8 || x>55 || y<14 || y>49)return false;
    if(x>=26 && x<=37 && y>=26 && y<=37)return false;
  }
  return true;
}

EXTERN_CALODB bool ECALCellActive(char typ, int x, int y){
  bool exis=ECALCellExists(typ,x,y);
  bool hole=('I'==typ && x>=24 && x<=39 && y>=26 && y<=37);
  bool actv=(exis && !hole);
  return actv;
}

///////////////////////// LED-PIN functions //////////////


EXTERN_CALODB int ECALGetLEDDAC(int opt, char* PINname, int PINname_len, int* LEDnum, int* DAC, double* uLED, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALGetLEDDAC: not connected");return(-1);}
  
  if( !(opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  if( !(opt & M_GetNew) && (opt & M_MarkNew) ) opt &= ~M_MarkNew;
  
  bool allCells=true;
  if( ('A'==PINname[0]) || ('C'==PINname[0]) ) allCells=false;
  if(!allCells && (opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  
  bool getOld  = (opt & M_GetOld)  ? true:false;
  bool getNew  = (opt & M_GetNew)  ? true:false;
  bool markNew = (opt & M_MarkNew) ? true:false;
  
  sword status;
  OCIDate appd; OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
  sb2 appd_ind;
  boolean appd_null=FALSE;
  OCIStmt* stECALled;
  OCIStmt* stECALappl;
  OCIStmt* stmt_allSetng;
  bool changes=false;
  int iled=0, nled=0;
  char pinnam[20];
  sword lednum=0;
  sword idac=0;
  double volt=0;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  
  if(!allCells){
    char sqlECALled[255];
    if(getNew)strcpy(sqlECALled,"SELECT l.led_dac_chan, d.uled, d.d_applied FROM ecal_led_otb l, table(l.leddac_settings) d WHERE l.pin_id=:1 AND l.led_num = :2 AND d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(l.leddac_settings) d1)");
    else      strcpy(sqlECALled,"SELECT l.led_dac_chan, d.uled, d.d_applied FROM ecal_led_otb l, table(l.leddac_settings) d WHERE l.pin_id=:1 AND l.led_num = :2 AND d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(l.leddac_settings) d1 WHERE d1.d_applied IS NOT NULL)");
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALled, ociError, (text*) sqlECALled,(ub4) strlen(sqlECALled),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC: Error preparing stECALled "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIBindByPos(stECALled, &bndp[3], ociError, (ub4)1, (dvoid*) &pinnam, (sb4) 4*sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALled, &bndp[4], ociError, (ub4)2, (dvoid*) &lednum, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC: Error binding stECALled "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIDefineByPos(stECALled, &defnp[1], ociError, (ub4)1, (dvoid*)&idac, (sb4)sizeof(idac), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stECALled, &defnp[2], ociError, (ub4)2, (dvoid*)&volt, (sb4)sizeof(volt), SQLT_FLT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stECALled, &defnp[3], ociError, (ub4)3, (dvoid*)&appd, (sb4)sizeof(appd), SQLT_ODT, (dvoid *) &appd_ind, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC: Error defining idac & uled in stECALled"); checkerr(ociError, status, erm); return -1; }
    
    if(markNew){
      char sqlECALappl[]="BEGIN ecal_timestamp_leddac(:1, :2); END;";
      status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALappl, ociError, (text*) sqlECALappl,(ub4) strlen(sqlECALappl),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC: Error preparing stECALappl "); checkerr(ociError, status, erm); return -1; }
      
      status=OCIBindByPos(stECALappl, &bndp[0], ociError, (ub4)1, (dvoid*) &pinnam, (sb4) 4*sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
      if(status==OCI_SUCCESS) status=OCIBindByPos(stECALappl, &bndp[1], ociError, (ub4)2, (dvoid*) &lednum, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC: Error binding stECALappl "); checkerr(ociError, status, erm); return -1; }
    }
    
    while('A'==PINname[iled*PINname_len] || 'C'==PINname[iled*PINname_len]){
      // set defaults
      DAC[iled]=0;
      uLED[iled]=0;
      // set bound variables
      for(int j=0; j<4; ++j)pinnam[j]=PINname[iled*PINname_len+j];
      pinnam[4]='\0';
      lednum=LEDnum[iled];
      // execute stmt
      status=OCIStmtExecute(ociHdbc, stECALled, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC: Error executing stECALled "); checkerr(ociError, status, erm); return -1; }
      
      if( (status=OCIStmtFetch2(stECALled, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	DAC[iled]=idac;
	uLED[iled]=volt;
	appd_null= (appd_ind==-1);
	if(appd_null && markNew){
	  changes=true;
	  status=OCIStmtExecute(ociHdbc, stECALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC: Error executing DAC markNew "); checkerr(ociError, status, erm); return -1; }
	}
	++nled;
      }
      ++iled;
    }
    status=OCIStmtRelease(stECALled, ociError, NULL, 0, (ub4) OCI_DEFAULT);
    if(markNew)status=OCIStmtRelease(stECALappl, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }else{
    char sql_allSetng[255];
    if     ( getNew && getOld)strcpy(sql_allSetng,"SELECT l.pin_id, l.led_num, l.led_dac_chan, d.uled, d.d_applied FROM ecal_led_otb l, table(l.leddac_settings) d WHERE d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(l.leddac_settings) d1) ORDER BY l.pin_id, l.led_num");
    else if( getNew &&!getOld)strcpy(sql_allSetng,"SELECT l.pin_id, l.led_num, l.led_dac_chan, d.uled, d.d_applied FROM ecal_led_otb l, table(l.leddac_settings) d WHERE d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(l.leddac_settings) d1 WHERE d1.d_applied IS NULL) ORDER BY l.pin_id, l.led_num");
    else if(!getNew && getOld)strcpy(sql_allSetng,"SELECT l.pin_id, l.led_num, l.led_dac_chan, d.uled, d.d_applied FROM ecal_led_otb l, table(l.leddac_settings) d WHERE d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(l.leddac_settings) d1 WHERE d1.d_applied IS NOT NULL) ORDER BY l.pin_id, l.led_num");
    
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmt_allSetng, ociError, (text*) sql_allSetng,(ub4) strlen(sql_allSetng),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC: Error preparing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[5], ociError, (ub4) 1, (dvoid*)&pinnam, (sb4)4*sizeof(char), SQLT_CHR, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[6], ociError, (ub4) 2, (dvoid*)&lednum, (sb4)sizeof(lednum), SQLT_INT, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[7], ociError, (ub4) 3, (dvoid*)&idac,   (sb4)sizeof(idac),   SQLT_INT, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[8], ociError, (ub4) 4, (dvoid*)&volt,   (sb4)sizeof(volt),   SQLT_FLT, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[9], ociError, (ub4) 5, (dvoid*)&appd,   (sb4)sizeof(appd),   SQLT_ODT, (dvoid *)&appd_ind, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC: Error defining pinnam, lednum, idac, uled, appd in stmt_allSetng"); checkerr(ociError, status, erm); return -1; }
    
    if(markNew){
      char sqlECALappl[]="BEGIN ecal_timestamp_allleddac(); END;";
      status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALappl, ociError, (text*) sqlECALappl,(ub4) strlen(sqlECALappl),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC: Error preparing stECALappl "); checkerr(ociError, status, erm); return -1; }
    }
    
    // execute stmt
    status=OCIStmtExecute(ociHdbc, stmt_allSetng, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC: Error executing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    while((status=OCIStmtFetch2(stmt_allSetng, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS || status==OCI_SUCCESS_WITH_INFO ){
      for(int i=0; i<4; ++i) PINname[iled*PINname_len+i]=pinnam[i];
      PINname[iled*PINname_len+4]='\0';
      LEDnum[iled]=lednum;
      DAC[iled]=idac;
      uLED[iled]=volt;
      appd_null= (appd_ind==-1);
      if(appd_null && markNew){
	changes=true;
      }
      ++iled;
      ++nled;
    }
    
    if(changes){
      status=OCIStmtExecute(ociHdbc, stECALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC: Error executing markNew "); checkerr(ociError, status, erm); return -1; }
    }
    status=OCIStmtRelease(stmt_allSetng, ociError, NULL, 0, (ub4) OCI_DEFAULT);
    if(markNew)status=OCIStmtRelease(stECALappl, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }
  
  sprintf(erm,"ECALGetLEDDAC: success %d chan out of %d", nled, iled);
  return nled;
}

EXTERN_CALODB int ECALGetLEDDAC_date(const char* thedate, int opt, char* PINname, int PINname_len, int* LEDnum, int* DAC, double* uLED, char* erm){
  // thedate is supposed to be in the YYYY-MM-DD HH24:MI:SS format
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALGetLEDDAC_date: not connected");return(-1);}
  
  if( !(opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  if( !(opt & M_GetNew) && (opt & M_MarkNew) ) opt &= ~M_MarkNew;
  
  bool allCells=true;
  if( ('A'==PINname[0]) || ('C'==PINname[0]) ) allCells=false;
  if(!allCells && (opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  
  bool getOld  = (opt & M_GetOld)  ? true:false;
  bool getNew  = (opt & M_GetNew)  ? true:false;
  bool markNew = (opt & M_MarkNew) ? true:false;
  
  sword status;
  OCIDate thed; 
  if(NULL!=thedate){
    char fmt[]="YYYY-MM-DD HH24:MI:SS";
    status=OCIDateFromText(ociError, (oratext*)thedate, strlen(thedate), (oratext*)fmt, strlen(fmt), 0, 0, &thed); 
  }
  else status=OCIDateSysDate(ociError, &thed);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC_date: date error"); checkerr(ociError, status, erm); return -1; }
  
  OCIDate appd; OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
  sb2 appd_ind;
  boolean appd_null=FALSE;
  OCIStmt* stECALled;
  OCIStmt* stECALappl;
  OCIStmt* stmt_allSetng;
  bool changes=false;
  int iled=0, nled=0;
  char pinnam[20];
  sword lednum=0;
  sword idac=0;
  double volt=0;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  
  if(!allCells){
    char sqlECALled[255];
    if(getNew)strcpy(sqlECALled,"SELECT l.led_dac_chan, d.uled, d.d_applied FROM ecal_led_otb l, table(l.leddac_settings) d WHERE l.pin_id=:1 AND l.led_num = :2 AND d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(l.leddac_settings) d1 WHERE d1.d_set<=:3)");
    else      strcpy(sqlECALled,"SELECT l.led_dac_chan, d.uled, d.d_applied FROM ecal_led_otb l, table(l.leddac_settings) d WHERE l.pin_id=:1 AND l.led_num = :2 AND d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(l.leddac_settings) d1 WHERE d1.d_set<=:3 AND d1.d_applied IS NOT NULL)");
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALled, ociError, (text*) sqlECALled,(ub4) strlen(sqlECALled),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC_date: Error preparing stECALled "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALled, &bndp[1], ociError, (ub4)1, (dvoid*)&pinnam, (sb4)4*sizeof(char), (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALled, &bndp[2], ociError, (ub4)2, (dvoid*)&lednum, (sb4)sizeof(sword),  (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALled, &bndp[3], ociError, (ub4)3, (dvoid*)&thed,   (sb4)sizeof(thed),   (ub2)SQLT_ODT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC_date: Error binding stECALled "); checkerr(ociError, status, erm); return -1; }

    if(status==OCI_SUCCESS) status=OCIDefineByPos(stECALled, &defnp[1], ociError, (ub4)1, (dvoid*)&idac, (sb4)sizeof(idac), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stECALled, &defnp[2], ociError, (ub4)2, (dvoid*)&volt, (sb4)sizeof(volt), SQLT_FLT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stECALled, &defnp[3], ociError, (ub4)3, (dvoid*)&appd, (sb4)sizeof(appd), SQLT_ODT, (dvoid *) &appd_ind, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC_date: Error defining idac & uled in stECALled"); checkerr(ociError, status, erm); return -1; }
    
    if(markNew){
      char sqlECALappl[]="BEGIN ecal_timestamp_leddac(:1, :2); END;";
      status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALappl, ociError, (text*)sqlECALappl,(ub4)strlen(sqlECALappl), NULL, 0, (ub4)OCI_NTV_SYNTAX, (ub4)OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC_date: Error preparing stECALappl "); checkerr(ociError, status, erm); return -1; }
      
      if(status==OCI_SUCCESS) status=OCIBindByPos(stECALappl, &bndp[4], ociError, (ub4)1, (dvoid*) &pinnam, (sb4)4*sizeof(char), (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
      if(status==OCI_SUCCESS) status=OCIBindByPos(stECALappl, &bndp[5], ociError, (ub4)2, (dvoid*) &lednum, (sb4)sizeof(sword),  (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC_date: Error binding stECALappl "); checkerr(ociError, status, erm); return -1; }
    }
    
    while('A'==PINname[iled*PINname_len] || 'C'==PINname[iled*PINname_len]){
      // set defaults
      DAC[iled]=0;
      uLED[iled]=0;
      // set bound variables
      for(int j=0; j<4; ++j)pinnam[j]=PINname[iled*PINname_len+j];
      pinnam[4]='\0';
      lednum=LEDnum[iled];
      // execute stmt
      status=OCIStmtExecute(ociHdbc, stECALled, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC_date: Error executing stECALled "); checkerr(ociError, status, erm); return -1; }
      
      if( (status=OCIStmtFetch2(stECALled, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	DAC[iled]=idac;
	uLED[iled]=volt;
	appd_null= (appd_ind==-1);
	if(appd_null && markNew){
	  changes=true;
	  status=OCIStmtExecute(ociHdbc, stECALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC_date: Error executing DAC markNew "); checkerr(ociError, status, erm); return -1; }
	}
	++nled;
      }
      ++iled;
    }
    status=OCIStmtRelease(stECALled, ociError, NULL, 0, (ub4) OCI_DEFAULT);
    if(markNew)status=OCIStmtRelease(stECALappl, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }else{
    char sql_allSetng[255];
    if     ( getNew && getOld)strcpy(sql_allSetng,"SELECT l.pin_id, l.led_num, l.led_dac_chan, d.uled, d.d_applied FROM ecal_led_otb l, table(l.leddac_settings) d WHERE d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(l.leddac_settings) d1 WHERE d1.d_set<=:1) ORDER BY l.pin_id, l.led_num");
    else if( getNew &&!getOld)strcpy(sql_allSetng,"SELECT l.pin_id, l.led_num, l.led_dac_chan, d.uled, d.d_applied FROM ecal_led_otb l, table(l.leddac_settings) d WHERE d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(l.leddac_settings) d1 WHERE d1.d_applied IS NULL AND d1.d_set<=:1) ORDER BY l.pin_id, l.led_num");
    else if(!getNew && getOld)strcpy(sql_allSetng,"SELECT l.pin_id, l.led_num, l.led_dac_chan, d.uled, d.d_applied FROM ecal_led_otb l, table(l.leddac_settings) d WHERE d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(l.leddac_settings) d1 WHERE d1.d_applied IS NOT NULL AND d1.d_set<=:1) ORDER BY l.pin_id, l.led_num");
    
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmt_allSetng, ociError, (text*) sql_allSetng,(ub4) strlen(sql_allSetng),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC_date: Error preparing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    // bind
    if(status==OCI_SUCCESS)status=OCIBindByPos(stmt_allSetng, &bndp[11], ociError, (ub4)1, (dvoid*)&thed, (sb4)sizeof(thed), (ub2)SQLT_ODT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    //define
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[5], ociError, (ub4)1, (dvoid*)&pinnam, (sb4)4*sizeof(char), SQLT_CHR, (dvoid*)        0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[6], ociError, (ub4)2, (dvoid*)&lednum, (sb4)sizeof(lednum), SQLT_INT, (dvoid*)        0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[7], ociError, (ub4)3, (dvoid*)&idac,   (sb4)sizeof(idac),   SQLT_INT, (dvoid*)        0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[8], ociError, (ub4)4, (dvoid*)&volt,   (sb4)sizeof(volt),   SQLT_FLT, (dvoid*)        0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[9], ociError, (ub4)5, (dvoid*)&appd,   (sb4)sizeof(appd),   SQLT_ODT, (dvoid*)&appd_ind, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC_date: Error binding date or defining pinnam, lednum, idac, uled, appd in stmt_allSetng"); checkerr(ociError, status, erm); return -1; }
    
    if(markNew){
      char sqlECALappl[]="BEGIN ecal_timestamp_allleddac(); END;";
      status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALappl, ociError, (text*) sqlECALappl,(ub4) strlen(sqlECALappl),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC_date: Error preparing stECALappl "); checkerr(ociError, status, erm); return -1; }
    }
    
    // execute stmt
    status=OCIStmtExecute(ociHdbc, stmt_allSetng, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC_date: Error executing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    while((status=OCIStmtFetch2(stmt_allSetng, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS || status==OCI_SUCCESS_WITH_INFO ){
      for(int i=0; i<4; ++i) PINname[iled*PINname_len+i]=pinnam[i];
      PINname[iled*PINname_len+4]='\0';
      LEDnum[iled]=lednum;
      DAC[iled]=idac;
      uLED[iled]=volt;
      appd_null= (appd_ind==-1);
      if(appd_null && markNew){
	changes=true;
      }
      ++iled;
      ++nled;
    }
    
    if(changes){
      status=OCIStmtExecute(ociHdbc, stECALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC_date: Error executing markNew "); checkerr(ociError, status, erm); return -1; }
    }
    status=OCIStmtRelease(stmt_allSetng, ociError, NULL, 0, (ub4) OCI_DEFAULT);
    if(markNew)status=OCIStmtRelease(stECALappl, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }
  
  sprintf(erm,"ECALGetLEDDAC_date: success %d chan out of %d", nled, iled);
  return nled;
}

EXTERN_CALODB int ECALGetPINADC(int opt, char* PINname, int PINname_len, int* ADC, double* delay, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALGetPINADC: not connected");return(-1);}
  
  if( !(opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  if( !(opt & M_GetNew) && (opt & M_MarkNew) ) opt &= ~M_MarkNew;
  
  bool allCells=true;
  if( ('A'==PINname[0]) || ('C'==PINname[0]) ) allCells=false;
  if(!allCells && (opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  
  bool getOld  = (opt & M_GetOld)  ? true:false;
  bool getNew  = (opt & M_GetNew)  ? true:false;
  bool markNew = (opt & M_MarkNew) ? true:false;
  
  sword status;
  OCIDate appd; OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
  sb2 appd_ind;
  boolean appd_null=FALSE;
  OCIStmt* stECALpin;
  OCIStmt* stECALappl;
  OCIStmt* stmt_allSetng;
  bool changes=false;
  int ipin=0, npin=0;
  char pinnam[20];
  sword iadc=0;
  int del=0;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  
  if(!allCells){
    char sqlECALpin[255];
    if(getNew)strcpy(sqlECALpin,"SELECT p.adc_chan, d.delay_adc, d.d_applied FROM ecal_pin_otb p, table(p.pinadc_settings) d WHERE p.pin_id=:1 AND d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(p.pinadc_settings) d1)");
    else      strcpy(sqlECALpin,"SELECT p.adc_chan, d.delay_adc, d.d_applied FROM ecal_pin_otb p, table(p.pinadc_settings) d WHERE p.pin_id=:1 AND d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(p.pinadc_settings) d1 WHERE d1.d_applied IS NOT NULL)");
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALpin, ociError, (text*) sqlECALpin,(ub4) strlen(sqlECALpin),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC: Error preparing stECALpin "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIBindByPos(stECALpin, &bndp[3], ociError, (ub4)1, (dvoid*) &pinnam, (sb4) 4*sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC: Error binding stECALpin "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stECALpin, &defnp[1], ociError, (ub4)1, (dvoid*)&iadc, (sb4)sizeof(iadc), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stECALpin, &defnp[2], ociError, (ub4)2, (dvoid*)&del,  (sb4)sizeof(del),  SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stECALpin, &defnp[3], ociError, (ub4)3, (dvoid*)&appd, (sb4)sizeof(appd), SQLT_ODT, (dvoid *) &appd_ind, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC: Error defining iadc & del in stECALpin"); checkerr(ociError, status, erm); return -1; }
    
    if(markNew){
      char sqlECALappl[]="BEGIN ecal_timestamp_pinadc(:1); END;";
      status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALappl, ociError, (text*) sqlECALappl,(ub4) strlen(sqlECALappl),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC: Error preparing stECALappl "); checkerr(ociError, status, erm); return -1; }
      
      status=OCIBindByPos(stECALappl, &bndp[0], ociError, (ub4)1, (dvoid*) &pinnam, (sb4) 4*sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC: Error binding stECALappl "); checkerr(ociError, status, erm); return -1; }
    }
    
    while('A'==PINname[ipin*PINname_len] || 'C'==PINname[ipin*PINname_len]){
      // set defaults
      ADC[ipin]=0;
      delay[ipin]=0;
      // set bound variables
      for(int j=0; j<4; ++j)pinnam[j]=PINname[ipin*PINname_len+j];
      pinnam[4]='\0';
      // execute stmt
      status=OCIStmtExecute(ociHdbc, stECALpin, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC: Error executing stECALpin "); checkerr(ociError, status, erm); return -1; }
      
      if( (status=OCIStmtFetch2(stECALpin, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	ADC[ipin]=iadc;
	delay[ipin]=(double)del/1e3;
	appd_null= (appd_ind==-1);
	if(appd_null && markNew){
	  changes=true;
	  status=OCIStmtExecute(ociHdbc, stECALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC: Error executing PINADC markNew "); checkerr(ociError, status, erm); return -1; }
	}
	++npin;
      }
      ++ipin;
    }
    status=OCIStmtRelease(stECALpin, ociError, NULL, 0, (ub4) OCI_DEFAULT);
    if(markNew)status=OCIStmtRelease(stECALappl, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }else{
    char sql_allSetng[255];
    if     ( getNew && getOld)strcpy(sql_allSetng,"SELECT  p.pin_id, p.adc_chan, d.delay_adc, d.d_applied FROM ecal_pin_otb p, table(p.pinadc_settings) d WHERE d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(p.pinadc_settings) d1) ORDER BY p.pin_id");
    else if( getNew &&!getOld)strcpy(sql_allSetng,"SELECT  p.pin_id, p.adc_chan, d.delay_adc, d.d_applied FROM ecal_pin_otb p, table(p.pinadc_settings) d WHERE d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(p.pinadc_settings) d1 WHERE d1.d_applied IS NULL) ORDER BY p.pin_id");
    else if(!getNew && getOld)strcpy(sql_allSetng,"SELECT  p.pin_id, p.adc_chan, d.delay_adc, d.d_applied FROM ecal_pin_otb p, table(p.pinadc_settings) d WHERE d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(p.pinadc_settings) d1 WHERE d1.d_applied IS NOT NULL) ORDER BY p.pin_id");
    
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmt_allSetng, ociError, (text*) sql_allSetng,(ub4) strlen(sql_allSetng),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC: Error preparing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[5], ociError, (ub4) 1, (dvoid*)&pinnam, (sb4)4*sizeof(char), SQLT_CHR, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[6], ociError, (ub4) 2, (dvoid*)&iadc,   (sb4)sizeof(iadc),   SQLT_INT, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[7], ociError, (ub4) 3, (dvoid*)&del,    (sb4)sizeof(del),    SQLT_INT, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[8], ociError, (ub4) 4, (dvoid*)&appd,   (sb4)sizeof(appd),   SQLT_ODT, (dvoid *)&appd_ind, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC: Error defining pinnam, iadc, del, appd in stmt_allSetng"); checkerr(ociError, status, erm); return -1; }
    
    if(markNew){
      char sqlECALappl[]="BEGIN ecal_timestamp_allpinadc(); END;";
      status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALappl, ociError, (text*) sqlECALappl,(ub4) strlen(sqlECALappl),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC: Error preparing stECALappl "); checkerr(ociError, status, erm); return -1; }
    }
    
    // execute stmt
    status=OCIStmtExecute(ociHdbc, stmt_allSetng, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC: Error executing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    while((status=OCIStmtFetch2(stmt_allSetng, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS || status==OCI_SUCCESS_WITH_INFO ){
      for(int i=0; i<4; ++i) PINname[ipin*PINname_len+i]=pinnam[i];
      PINname[ipin*PINname_len+4]='\0';
      ADC[ipin]=iadc;
      delay[ipin]=(double)del/1e3;
      appd_null= (appd_ind==-1);
      if(appd_null && markNew){
	changes=true;
      }
      ++ipin;
      ++npin;
    }
    
    if(changes){
      status=OCIStmtExecute(ociHdbc, stECALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC: Error executing markNew "); checkerr(ociError, status, erm); return -1; }
    }
    status=OCIStmtRelease(stmt_allSetng, ociError, NULL, 0, (ub4) OCI_DEFAULT);
    if(markNew)status=OCIStmtRelease(stECALappl, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }
  
  sprintf(erm,"ECALGetPINADC: success %d chan out of %d", npin, ipin);
  return npin;
}

EXTERN_CALODB int ECALGetPINADC_date(const char* thedate, int opt, char* PINname, int PINname_len, int* ADC, double* delay, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALGetPINADC_date: not connected");return(-1);}
  
  if( !(opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  if( !(opt & M_GetNew) && (opt & M_MarkNew) ) opt &= ~M_MarkNew;
  
  bool allCells=true;
  if( ('A'==PINname[0]) || ('C'==PINname[0]) ) allCells=false;
  if(!allCells && (opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  
  bool getOld  = (opt & M_GetOld)  ? true:false;
  bool getNew  = (opt & M_GetNew)  ? true:false;
  bool markNew = (opt & M_MarkNew) ? true:false;
  
  sword status;
  OCIDate thed; 
  if(NULL!=thedate){
    char fmt[]="YYYY-MM-DD HH24:MI:SS";
    status=OCIDateFromText(ociError, (oratext*)thedate, strlen(thedate), (oratext*)fmt, strlen(fmt), 0, 0, &thed); 
  }
  else status=OCIDateSysDate(ociError, &thed);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC_date: date error"); checkerr(ociError, status, erm); return -1; }
  
  OCIDate appd; OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
  sb2 appd_ind;
  boolean appd_null=FALSE;
  OCIStmt* stECALpin;
  OCIStmt* stECALappl;
  OCIStmt* stmt_allSetng;
  bool changes=false;
  int ipin=0, npin=0;
  char pinnam[20];
  sword iadc=0;
  int del=0;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  
  if(!allCells){
    char sqlECALpin[255];
    if(getNew)strcpy(sqlECALpin,"SELECT p.adc_chan, d.delay_adc, d.d_applied FROM ecal_pin_otb p, table(p.pinadc_settings) d WHERE p.pin_id=:1 AND d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(p.pinadc_settings) d1 WHERE d1.d_set<=:2)");
    else      strcpy(sqlECALpin,"SELECT p.adc_chan, d.delay_adc, d.d_applied FROM ecal_pin_otb p, table(p.pinadc_settings) d WHERE p.pin_id=:1 AND d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(p.pinadc_settings) d1 WHERE d1.d_set<=:2 AND d1.d_applied IS NOT NULL)");
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALpin, ociError, (text*) sqlECALpin,(ub4) strlen(sqlECALpin),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC_date: Error preparing stECALpin "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALpin, &bndp[1], ociError, (ub4)1, (dvoid*)&pinnam, (sb4)4*sizeof(char) ,(ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALpin, &bndp[2], ociError, (ub4)2, (dvoid*)&thed,   (sb4)sizeof(thed),   (ub2)SQLT_ODT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC_date: Error binding stECALpin "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stECALpin, &defnp[1], ociError, (ub4)1, (dvoid*)&iadc, (sb4)sizeof(iadc), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stECALpin, &defnp[2], ociError, (ub4)2, (dvoid*)&del,  (sb4)sizeof(del),  SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stECALpin, &defnp[3], ociError, (ub4)3, (dvoid*)&appd, (sb4)sizeof(appd), SQLT_ODT, (dvoid *) &appd_ind, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC_date: Error defining iadc & del in stECALpin"); checkerr(ociError, status, erm); return -1; }
    
    if(markNew){
      char sqlECALappl[]="BEGIN ecal_timestamp_pinadc(:1); END;";
      status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALappl, ociError, (text*) sqlECALappl,(ub4) strlen(sqlECALappl),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC_date: Error preparing stECALappl "); checkerr(ociError, status, erm); return -1; }
      
      status=OCIBindByPos(stECALappl, &bndp[0], ociError, (ub4)1, (dvoid*) &pinnam, (sb4) 4*sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC_date: Error binding stECALappl "); checkerr(ociError, status, erm); return -1; }
    }
    
    while('A'==PINname[ipin*PINname_len] || 'C'==PINname[ipin*PINname_len]){
      // set defaults
      ADC[ipin]=0;
      delay[ipin]=0;
      // set bound variables
      for(int j=0; j<4; ++j)pinnam[j]=PINname[ipin*PINname_len+j];
      pinnam[4]='\0';
      // execute stmt
      status=OCIStmtExecute(ociHdbc, stECALpin, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC_date: Error executing stECALpin "); checkerr(ociError, status, erm); return -1; }
      
      if( (status=OCIStmtFetch2(stECALpin, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	ADC[ipin]=iadc;
	delay[ipin]=(double)del/1e3;
	appd_null= (appd_ind==-1);
	if(appd_null && markNew){
	  changes=true;
	  status=OCIStmtExecute(ociHdbc, stECALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC_date: Error executing PINADC markNew "); checkerr(ociError, status, erm); return -1; }
	}
	++npin;
      }
      ++ipin;
    }
    status=OCIStmtRelease(stECALpin, ociError, NULL, 0, (ub4) OCI_DEFAULT);
    if(markNew)status=OCIStmtRelease(stECALappl, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }else{
    char sql_allSetng[255];
    if     ( getNew && getOld)strcpy(sql_allSetng,"SELECT  p.pin_id, p.adc_chan, d.delay_adc, d.d_applied FROM ecal_pin_otb p, table(p.pinadc_settings) d WHERE d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(p.pinadc_settings) d1 WHERE d1.d_set<=:1) ORDER BY p.pin_id");
    else if( getNew &&!getOld)strcpy(sql_allSetng,"SELECT  p.pin_id, p.adc_chan, d.delay_adc, d.d_applied FROM ecal_pin_otb p, table(p.pinadc_settings) d WHERE d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(p.pinadc_settings) d1 WHERE d1.d_set<=:1 AND d1.d_applied IS NULL) ORDER BY p.pin_id");
    else if(!getNew && getOld)strcpy(sql_allSetng,"SELECT  p.pin_id, p.adc_chan, d.delay_adc, d.d_applied FROM ecal_pin_otb p, table(p.pinadc_settings) d WHERE d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(p.pinadc_settings) d1 WHERE d1.d_set<=:1 AND d1.d_applied IS NOT NULL) ORDER BY p.pin_id");
    
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmt_allSetng, ociError, (text*) sql_allSetng,(ub4) strlen(sql_allSetng),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC_date: Error preparing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    // bind
    if(status==OCI_SUCCESS)status=OCIBindByPos(stmt_allSetng, &bndp[11], ociError, (ub4)1, (dvoid*)&thed, (sb4)sizeof(thed), (ub2)SQLT_ODT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    //define
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[5], ociError, (ub4) 1, (dvoid*)&pinnam, (sb4)4*sizeof(char), SQLT_CHR, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[6], ociError, (ub4) 2, (dvoid*)&iadc,   (sb4)sizeof(iadc),   SQLT_INT, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[7], ociError, (ub4) 3, (dvoid*)&del,    (sb4)sizeof(del),    SQLT_INT, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[8], ociError, (ub4) 4, (dvoid*)&appd,   (sb4)sizeof(appd),   SQLT_ODT, (dvoid *)&appd_ind, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC_date: Error defining pinnam, iadc, del, appd in stmt_allSetng"); checkerr(ociError, status, erm); return -1; }
    
    if(markNew){
      char sqlECALappl[]="BEGIN ecal_timestamp_allpinadc(); END;";
      status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALappl, ociError, (text*) sqlECALappl,(ub4) strlen(sqlECALappl),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC_date: Error preparing stECALappl "); checkerr(ociError, status, erm); return -1; }
    }
    
    // execute stmt
    status=OCIStmtExecute(ociHdbc, stmt_allSetng, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC_date: Error executing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    while((status=OCIStmtFetch2(stmt_allSetng, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS || status==OCI_SUCCESS_WITH_INFO ){
      for(int i=0; i<4; ++i) PINname[ipin*PINname_len+i]=pinnam[i];
      PINname[ipin*PINname_len+4]='\0';
      ADC[ipin]=iadc;
      delay[ipin]=(double)del/1e3;
      appd_null= (appd_ind==-1);
      if(appd_null && markNew){
	changes=true;
      }
      ++ipin;
      ++npin;
    }
    
    if(changes){
      status=OCIStmtExecute(ociHdbc, stECALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetPINADC_date: Error executing markNew "); checkerr(ociError, status, erm); return -1; }
    }
    status=OCIStmtRelease(stmt_allSetng, ociError, NULL, 0, (ub4) OCI_DEFAULT);
    if(markNew)status=OCIStmtRelease(stECALappl, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }
  
  sprintf(erm,"ECALGetPINADC_date: success %d chan out of %d", npin, ipin);
  return npin;
}

EXTERN_CALODB int ECALGetLEDTSB(int opt, char* PINname, int PINname_len, int* LEDnum, int* TSB, int* delay, char* tune, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALGetLEDTSB: not connected");return(-1);}
  
  if( !(opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  if( !(opt & M_GetNew) && (opt & M_MarkNew) ) opt &= ~M_MarkNew;
  
  bool allCells=true;
  if( ('A'==PINname[0]) || ('C'==PINname[0]) ) allCells=false;
  if(!allCells && (opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  
  bool getOld  = (opt & M_GetOld)  ? true:false;
  bool getNew  = (opt & M_GetNew)  ? true:false;
  bool markNew = (opt & M_MarkNew) ? true:false;
  
  sword status;
  OCIDate appd; OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
  sb2 appd_ind;
  boolean appd_null=FALSE;
  OCIStmt* stECALled;
  OCIStmt* stECALappl;
  OCIStmt* stmt_allSetng;
  bool changes=false;
  int iled=0, nled=0;
  char pinnam[20];
  sword lednum=0;
  sword itsb=0;
  int del=0;
  char seq[100];
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  
  if(!allCells){
    char sqlECALled[255];
    if(getNew)strcpy(sqlECALled,"SELECT l.led_ledtsb_chan, t.delay, t.tune, t.d_applied FROM ecal_led_otb l, table(l.ledtsb_settings) t WHERE l.pin_id=:1 AND l.led_num = :2 AND t.d_set = (SELECT MAX(t1.d_set) FROM TABLE(l.ledtsb_settings) t1)");
    else      strcpy(sqlECALled,"SELECT l.led_ledtsb_chan, t.delay, t.tune, t.d_applied FROM ecal_led_otb l, table(l.ledtsb_settings) t WHERE l.pin_id=:1 AND l.led_num = :2 AND t.d_set = (SELECT MAX(t1.d_set) FROM TABLE(l.ledtsb_settings) t1 WHERE t1.d_applied IS NOT NULL)");
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALled, ociError, (text*) sqlECALled,(ub4) strlen(sqlECALled),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB: Error preparing stECALled "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIBindByPos(stECALled, &bndp[3], ociError, (ub4)1, (dvoid*) &pinnam, (sb4) 4*sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALled, &bndp[4], ociError, (ub4)2, (dvoid*) &lednum, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB: Error binding stECALled "); checkerr(ociError, status, erm); return -1; }
    
    status=OCIDefineByPos(stECALled, &defnp[1], ociError, (ub4)1, (dvoid*)&itsb, (sb4)sizeof(itsb), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stECALled, &defnp[2], ociError, (ub4)2, (dvoid*)&del , (sb4)sizeof(del) , SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stECALled, &defnp[3], ociError, (ub4)3, (dvoid*)&seq[0], (sb4)sizeof(seq), SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stECALled, &defnp[4], ociError, (ub4)4, (dvoid*)&appd, (sb4)sizeof(appd), SQLT_ODT, (dvoid *) &appd_ind, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB: Error defining itsb, del & tune in stECALled"); checkerr(ociError, status, erm); return -1; }
    
    if(markNew){
      char sqlECALappl[]="BEGIN ecal_timestamp_ledtsb(:1, :2); END;";
      status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALappl, ociError, (text*) sqlECALappl,(ub4) strlen(sqlECALappl),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB: Error preparing stECALappl "); checkerr(ociError, status, erm); return -1; }
      
      status=OCIBindByPos(stECALappl, &bndp[0], ociError, (ub4)1, (dvoid*) &pinnam, (sb4) 4*sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
      if(status==OCI_SUCCESS) status=OCIBindByPos(stECALappl, &bndp[1], ociError, (ub4)2, (dvoid*) &lednum, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB: Error binding stECALappl "); checkerr(ociError, status, erm); return -1; }
    }
    
    while('A'==PINname[iled*PINname_len] || 'C'==PINname[iled*PINname_len]){
      // set defaults
      TSB[iled]=0;
      delay[iled]=0;
      memset(&tune[iled*64],0,64*sizeof(char));
      // set bound variables
      for(int j=0; j<4; ++j)pinnam[j]=PINname[iled*PINname_len+j];
      pinnam[4]='\0';
      lednum=LEDnum[iled];
      // execute stmt
      status=OCIStmtExecute(ociHdbc, stECALled, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB: Error executing stECALled "); checkerr(ociError, status, erm); return -1; }
      
      if( (status=OCIStmtFetch2(stECALled, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	TSB[iled]=itsb;
	delay[iled]=del;
	for(int j=0; j<64; ++j)tune[iled*64+j]=seq[j];
	appd_null= (-1==appd_ind);
	if(appd_null && markNew){
	  changes=true;
	  status=OCIStmtExecute(ociHdbc, stECALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB: Error executing markNew "); checkerr(ociError, status, erm); return -1; }
	}
	++nled;
      }
      ++iled;
    }
    status=OCIStmtRelease(stECALled, ociError, NULL, 0, (ub4) OCI_DEFAULT);
    if(markNew)status=OCIStmtRelease(stECALappl, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }else{
    char sql_allSetng[255];
    if     ( getNew && getOld)strcpy(sql_allSetng,"SELECT  l.pin_id, l.led_num, l.led_ledtsb_chan, t.delay, t.tune, t.d_applied FROM ecal_led_otb l, table(l.ledtsb_settings) t WHERE t.d_set = (SELECT MAX(t1.d_set) FROM TABLE(l.ledtsb_settings) t1) ORDER BY l.pin_id, l.led_num");
    else if( getNew &&!getOld)strcpy(sql_allSetng,"SELECT  l.pin_id, l.led_num, l.led_ledtsb_chan, t.delay, t.tune, t.d_applied FROM ecal_led_otb l, table(l.ledtsb_settings) t WHERE t.d_set = (SELECT MAX(t1.d_set) FROM TABLE(l.ledtsb_settings) t1 WHERE t1.d_applied IS NULL) ORDER BY l.pin_id, l.led_num");
    else if(!getNew && getOld)strcpy(sql_allSetng,"SELECT  l.pin_id, l.led_num, l.led_ledtsb_chan, t.delay, t.tune, t.d_applied FROM ecal_led_otb l, table(l.ledtsb_settings) t WHERE t.d_set = (SELECT MAX(t1.d_set) FROM TABLE(l.ledtsb_settings) t1 WHERE t1.d_applied IS NOT NULL) ORDER BY l.pin_id, l.led_num");
    
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmt_allSetng, ociError, (text*) sql_allSetng,(ub4) strlen(sql_allSetng),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB: Error preparing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[5] , ociError, (ub4) 1, (dvoid*)&pinnam, (sb4)4*sizeof(char), SQLT_CHR, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[6] , ociError, (ub4) 2, (dvoid*)&lednum, (sb4)sizeof(lednum), SQLT_INT, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[7] , ociError, (ub4) 3, (dvoid*)&itsb,   (sb4)sizeof(itsb),   SQLT_INT, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[8] , ociError, (ub4) 4, (dvoid*)&del,    (sb4)sizeof(del),    SQLT_INT, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[9] , ociError, (ub4) 5, (dvoid*)&seq[0], (sb4)sizeof(seq),    SQLT_CHR, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[10], ociError, (ub4) 6, (dvoid*)&appd,   (sb4)sizeof(appd),   SQLT_ODT, (dvoid *)&appd_ind, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB: Error defining pinnam, lednum, itsb, del, seq, appd in stmt_allSetng"); checkerr(ociError, status, erm); return -1; }
    
    if(markNew){
      char sqlECALappl[]="BEGIN ecal_timestamp_allledtsb(); END;";
      status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALappl, ociError, (text*) sqlECALappl,(ub4) strlen(sqlECALappl),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB: Error preparing stECALappl "); checkerr(ociError, status, erm); return -1; }
    }
    
    // execute stmt
    status=OCIStmtExecute(ociHdbc, stmt_allSetng, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB: Error executing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    while((status=OCIStmtFetch2(stmt_allSetng, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS || status==OCI_SUCCESS_WITH_INFO ){
      for(int i=0; i<4; ++i) PINname[iled*PINname_len+i]=pinnam[i];
      PINname[iled*PINname_len+4]='\0';
      LEDnum[iled]=lednum;
      TSB[iled]=itsb;
      delay[iled]=del;
      for(int j=0; j<64; ++j)tune[iled*64+j]=seq[j];
      appd_null= (-1==appd_ind);
      if(appd_null && markNew){
	changes=true;
      }
      ++iled;
      ++nled;
    }
    
    if(changes){
      status=OCIStmtExecute(ociHdbc, stECALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB: Error executing markNew "); checkerr(ociError, status, erm); return -1; }
    }
    status=OCIStmtRelease(stmt_allSetng, ociError, NULL, 0, (ub4) OCI_DEFAULT);
    if(markNew)status=OCIStmtRelease(stECALappl, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }
  
  sprintf(erm,"ECALGetLEDTSB: success %d chan out of %d", nled, iled);
  return nled;
}

EXTERN_CALODB int ECALGetLEDTSB_date(const char* thedate, int opt, char* PINname, int PINname_len, int* LEDnum, int* TSB, int* delay, char* tune, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALGetLEDTSB_date: not connected");return(-1);}
  
  if( !(opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  if( !(opt & M_GetNew) && (opt & M_MarkNew) ) opt &= ~M_MarkNew;
  
  bool allCells=true;
  if( ('A'==PINname[0]) || ('C'==PINname[0]) ) allCells=false;
  if(!allCells && (opt & M_GetNew) && !(opt & M_GetOld) ) opt |= M_GetOld;
  
  bool getOld  = (opt & M_GetOld)  ? true:false;
  bool getNew  = (opt & M_GetNew)  ? true:false;
  bool markNew = (opt & M_MarkNew) ? true:false;
  
  sword status;
  OCIDate thed; 
  if(NULL!=thedate){
    char fmt[]="YYYY-MM-DD HH24:MI:SS";
    status=OCIDateFromText(ociError, (oratext*)thedate, strlen(thedate), (oratext*)fmt, strlen(fmt), 0, 0, &thed); 
  }else status=OCIDateSysDate(ociError, &thed);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC_date: date error"); checkerr(ociError, status, erm); return -1; }
  
  OCIDate appd; OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
  sb2 appd_ind;
  boolean appd_null=FALSE;
  OCIStmt* stECALled;
  OCIStmt* stECALappl;
  OCIStmt* stmt_allSetng;
  bool changes=false;
  int iled=0, nled=0;
  char pinnam[20];
  sword lednum=0;
  sword itsb=0;
  int del=0;
  char seq[100];
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  
  if(!allCells){
    char sqlECALled[255];
    if(getNew)strcpy(sqlECALled,"SELECT l.led_ledtsb_chan, t.delay, t.tune, t.d_applied FROM ecal_led_otb l, table(l.ledtsb_settings) t WHERE l.pin_id=:1 AND l.led_num = :2 AND t.d_set = (SELECT MAX(t1.d_set) FROM TABLE(l.ledtsb_settings) t1 WHERE d1.d_set<=:3)");
    else      strcpy(sqlECALled,"SELECT l.led_ledtsb_chan, t.delay, t.tune, t.d_applied FROM ecal_led_otb l, table(l.ledtsb_settings) t WHERE l.pin_id=:1 AND l.led_num = :2 AND t.d_set = (SELECT MAX(t1.d_set) FROM TABLE(l.ledtsb_settings) t1 WHERE d1.d_set<=:3 AND t1.d_applied IS NOT NULL)");
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALled, ociError, (text*) sqlECALled,(ub4) strlen(sqlECALled),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB_date: Error preparing stECALled "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALled, &bndp[1], ociError, (ub4)1, (dvoid*)&pinnam, (sb4)4*sizeof(char), (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALled, &bndp[2], ociError, (ub4)2, (dvoid*)&lednum, (sb4)sizeof(sword),  (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIBindByPos(stECALled, &bndp[3], ociError, (ub4)3, (dvoid*)&thed,   (sb4)sizeof(thed),   (ub2)SQLT_ODT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB_date: Error binding stECALled "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stECALled, &defnp[1], ociError, (ub4)1, (dvoid*)&itsb, (sb4)sizeof(itsb), SQLT_INT, (dvoid*) 0, (ub2*)0, (ub2*)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stECALled, &defnp[2], ociError, (ub4)2, (dvoid*)&del , (sb4)sizeof(del) , SQLT_INT, (dvoid*) 0, (ub2*)0, (ub2*)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stECALled, &defnp[3], ociError, (ub4)3, (dvoid*)&seq[0], (sb4)sizeof(seq), SQLT_CHR, (dvoid*) 0, (ub2*)0, (ub2*)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stECALled, &defnp[4], ociError, (ub4)4, (dvoid*)&appd, (sb4)sizeof(appd), SQLT_ODT, (dvoid*) &appd_ind, (ub2*)0, (ub2*)0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB_date: Error defining itsb, del & tune in stECALled"); checkerr(ociError, status, erm); return -1; }
    
    if(markNew){
      char sqlECALappl[]="BEGIN ecal_timestamp_ledtsb(:1, :2); END;";
      status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALappl, ociError, (text*) sqlECALappl,(ub4) strlen(sqlECALappl),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB_date: Error preparing stECALappl "); checkerr(ociError, status, erm); return -1; }
      
      if(status==OCI_SUCCESS) status=OCIBindByPos(stECALappl, &bndp[0], ociError, (ub4)1, (dvoid*)&pinnam, (sb4)4*sizeof(char), (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
      if(status==OCI_SUCCESS) status=OCIBindByPos(stECALappl, &bndp[1], ociError, (ub4)2, (dvoid*)&lednum, (sb4)sizeof(sword),  (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB_date: Error binding stECALappl "); checkerr(ociError, status, erm); return -1; }
    }
    
    while('A'==PINname[iled*PINname_len] || 'C'==PINname[iled*PINname_len]){
      // set defaults
      TSB[iled]=0;
      delay[iled]=0;
      memset(&tune[iled*64],0,64*sizeof(char));
      // set bound variables
      for(int j=0; j<4; ++j)pinnam[j]=PINname[iled*PINname_len+j];
      pinnam[4]='\0';
      lednum=LEDnum[iled];
      // execute stmt
      status=OCIStmtExecute(ociHdbc, stECALled, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB_date: Error executing stECALled "); checkerr(ociError, status, erm); return -1; }
      
      if( (status=OCIStmtFetch2(stECALled, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	TSB[iled]=itsb;
	delay[iled]=del;
	for(int j=0; j<64; ++j)tune[iled*64+j]=seq[j];
	appd_null= (-1==appd_ind);
	if(appd_null && markNew){
	  changes=true;
	  status=OCIStmtExecute(ociHdbc, stECALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB_date: Error executing markNew "); checkerr(ociError, status, erm); return -1; }
	}
	++nled;
      }
      ++iled;
    }
    status=OCIStmtRelease(stECALled, ociError, NULL, 0, (ub4) OCI_DEFAULT);
    if(markNew)status=OCIStmtRelease(stECALappl, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }else{
    char sql_allSetng[255];
    if     ( getNew && getOld)strcpy(sql_allSetng,"SELECT  l.pin_id, l.led_num, l.led_ledtsb_chan, t.delay, t.tune, t.d_applied FROM ecal_led_otb l, table(l.ledtsb_settings) t WHERE t.d_set = (SELECT MAX(t1.d_set) FROM TABLE(l.ledtsb_settings) t1 WHERE d1.d_set<=:1) ORDER BY l.pin_id, l.led_num");
    else if( getNew &&!getOld)strcpy(sql_allSetng,"SELECT  l.pin_id, l.led_num, l.led_ledtsb_chan, t.delay, t.tune, t.d_applied FROM ecal_led_otb l, table(l.ledtsb_settings) t WHERE t.d_set = (SELECT MAX(t1.d_set) FROM TABLE(l.ledtsb_settings) t1 WHERE t1.d_applied IS NULL AND d1.d_set<=:1) ORDER BY l.pin_id, l.led_num");
    else if(!getNew && getOld)strcpy(sql_allSetng,"SELECT  l.pin_id, l.led_num, l.led_ledtsb_chan, t.delay, t.tune, t.d_applied FROM ecal_led_otb l, table(l.ledtsb_settings) t WHERE t.d_set = (SELECT MAX(t1.d_set) FROM TABLE(l.ledtsb_settings) t1 WHERE t1.d_applied IS NOT NULL AND d1.d_set<=:1) ORDER BY l.pin_id, l.led_num");
    
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmt_allSetng, ociError, (text*) sql_allSetng,(ub4) strlen(sql_allSetng),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB_date: Error preparing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC_date: Error preparing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    // bind
    if(status==OCI_SUCCESS)status=OCIBindByPos(stmt_allSetng, &bndp[11], ociError, (ub4)1, (dvoid*)&thed, (sb4)sizeof(thed), (ub2)SQLT_ODT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    //define
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[5] , ociError, (ub4) 1, (dvoid*)&pinnam, (sb4)4*sizeof(char), SQLT_CHR, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[6] , ociError, (ub4) 2, (dvoid*)&lednum, (sb4)sizeof(lednum), SQLT_INT, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[7] , ociError, (ub4) 3, (dvoid*)&itsb,   (sb4)sizeof(itsb),   SQLT_INT, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[8] , ociError, (ub4) 4, (dvoid*)&del,    (sb4)sizeof(del),    SQLT_INT, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[9] , ociError, (ub4) 5, (dvoid*)&seq[0], (sb4)sizeof(seq),    SQLT_CHR, (dvoid *)        0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[10], ociError, (ub4) 6, (dvoid*)&appd,   (sb4)sizeof(appd),   SQLT_ODT, (dvoid *)&appd_ind, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB_date: Error binding date or defining pinnam, lednum, itsb, del, seq, appd in stmt_allSetng"); checkerr(ociError, status, erm); return -1; }
    
    if(markNew){
      char sqlECALappl[]="BEGIN ecal_timestamp_allledtsb(); END;";
      status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALappl, ociError, (text*) sqlECALappl,(ub4) strlen(sqlECALappl),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB_date: Error preparing stECALappl "); checkerr(ociError, status, erm); return -1; }
    }
    
    // execute stmt
    status=OCIStmtExecute(ociHdbc, stmt_allSetng, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB_date: Error executing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    while((status=OCIStmtFetch2(stmt_allSetng, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS || status==OCI_SUCCESS_WITH_INFO ){
      for(int i=0; i<4; ++i) PINname[iled*PINname_len+i]=pinnam[i];
      PINname[iled*PINname_len+4]='\0';
      LEDnum[iled]=lednum;
      TSB[iled]=itsb;
      delay[iled]=del;
      for(int j=0; j<64; ++j)tune[iled*64+j]=seq[j];
      appd_null= (-1==appd_ind);
      if(appd_null && markNew){
	changes=true;
      }
      ++iled;
      ++nled;
    }
    
    if(changes){
      status=OCIStmtExecute(ociHdbc, stECALappl, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDTSB_date: Error executing markNew "); checkerr(ociError, status, erm); return -1; }
    }
    status=OCIStmtRelease(stmt_allSetng, ociError, NULL, 0, (ub4) OCI_DEFAULT);
    if(markNew)status=OCIStmtRelease(stECALappl, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }
  
  sprintf(erm,"ECALGetLEDTSB_date: success %d chan out of %d", nled, iled);
  return nled;
}

EXTERN_CALODB int ECALStoreLEDDAC(char* PINname, int PINname_len, int* LEDnum, double* uLED, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALStoreLEDDAC: not connected");return(-1);}
  
  sword status;
  OCIDate appd; OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
  sb2 appd_ind;
  OCIStmt* stECALled;
  OCIStmt* stECALinss;
  OCIStmt* stECALupds;
  int iled=0, nled=0;
  char pinnam[20];
  sword lednum=0;
  double volt=0;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  
  // stECALled
  char sqlECALled[]="SELECT d.d_applied FROM ecal_led_otb l, table(l.leddac_settings) d WHERE l.pin_id=:1 AND l.led_num=:2 AND d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(l.leddac_settings) d1)";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALled, ociError, (text*) sqlECALled,(ub4) strlen(sqlECALled),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreLEDDAC: Error preparing stECALled "); checkerr(ociError, status, erm); return -1; }
  
  status=OCIBindByPos(stECALled, &bndp[1], ociError, (ub4)1, (dvoid*) &pinnam, (sb4) 4*sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALled, &bndp[2], ociError, (ub4)2, (dvoid*) &lednum, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreLEDDAC: Error binding stECALled "); checkerr(ociError, status, erm); return -1; }
  
  status=OCIDefineByPos(stECALled, &defnp[1], ociError, (ub4)1, (dvoid*)&appd, (sb4)sizeof(appd), SQLT_ODT, (dvoid *) &appd_ind, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreLEDDAC: Error defining appd in stECALled"); checkerr(ociError, status, erm); return -1; }
  
  // stECALinss
  char sqlECALinss[]="INSERT INTO TABLE (SELECT l.leddac_settings FROM ecal_led_otb l WHERE l.pin_id=:1 AND l.led_num=:2 ) VALUES(SYSDATE, NULL, :3)";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALinss, ociError, (text*) sqlECALinss,(ub4) strlen(sqlECALinss),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreLEDDAC: Error preparing stECALinss "); checkerr(ociError, status, erm); return -1; }
  
  status=OCIBindByPos(stECALinss, &bndp[11], ociError, (ub4)1, (dvoid*) &pinnam, (sb4) 4*sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALinss, &bndp[12], ociError, (ub4)2, (dvoid*) &lednum, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALinss, &bndp[13], ociError, (ub4)3, (dvoid*) & volt,   (sb4) sizeof(double),  (ub2) SQLT_FLT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreLEDDAC; Error binding stECALinss "); checkerr(ociError, status, erm); return -1; }
  // stECALupds
  char sqlECALupds[]="UPDATE TABLE (SELECT l.leddac_settings FROM ecal_led_otb l WHERE l.pin_id=:1 AND l.led_num=:2 ) r SET r.d_set=SYSDATE, r.uled=:3 WHERE r.d_applied IS NULL";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALupds, ociError, (text*) sqlECALupds,(ub4) strlen(sqlECALupds),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreLEDDAC: Error preparing stECALupds "); checkerr(ociError, status, erm); return -1; }
  
  status=OCIBindByPos(stECALupds, &bndp[21], ociError, (ub4)1, (dvoid*) &pinnam, (sb4) 4*sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALupds, &bndp[22], ociError, (ub4)2, (dvoid*) &lednum, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALupds, &bndp[23], ociError, (ub4)3, (dvoid*) & volt,   (sb4) sizeof(double),  (ub2) SQLT_FLT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreLEDDAC: Error binding stECALupds "); checkerr(ociError, status, erm); return -1; }
  
  while(PINname[iled*PINname_len]=='A' || PINname[iled*PINname_len]=='C'){
    // set bound variables
    for(int j=0; j<4; ++j)pinnam[j]=PINname[iled*PINname_len+j];
    pinnam[4]='\0';
    lednum=LEDnum[iled];
    volt=uLED[iled];
    // execute stmt
    status=OCIStmtExecute(ociHdbc, stECALled, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreLEDDAC: Error executing stECALled "); checkerr(ociError, status, erm); return -1; }
    
    appd_ind=0;
    if( (status=OCIStmtFetch2(stECALled, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
      if(-1==appd_ind){
	status=OCIStmtExecute(ociHdbc, stECALupds, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreLEDDAC: Error executing stECALupds "); checkerr(ociError, status, erm); return -1; }
      }else{
	status=OCIStmtExecute(ociHdbc, stECALinss, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreLEDDAC: Error executing stECALinss "); checkerr(ociError, status, erm); return -1; }
      }
      ++nled;
    }else{
      status=OCIStmtExecute(ociHdbc, stECALinss, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status==OCI_SUCCESS)	++nled;
    }
    
    ++iled;
  }
  status=OCIStmtRelease(stECALled, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  status=OCIStmtRelease(stECALupds, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  status=OCIStmtRelease(stECALinss, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALStoreLEDDAC: success %d chan out of %d", nled, iled);
  return nled;
}

EXTERN_CALODB int ECALStorePINADC(char* PINname, int PINname_len, double* delay, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALStorePINADC: not connected");return(-1);}
  
  sword status;
  OCIDate appd; OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
  sb2 appd_ind;
  OCIStmt* stECALpin;
  OCIStmt* stECALinss;
  OCIStmt* stECALupds;
  int ipin=0, npin=0;
  char pinnam[20];
  sword del=0;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  
  // stECALpin
  char sqlECALpin[]="SELECT d.d_applied FROM ecal_pin_otb p, table(p.pinadc_settings) d WHERE p.pin_id=:1 AND d.d_set = (SELECT MAX(d1.d_set) FROM TABLE(p.pinadc_settings) d1)";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALpin, ociError, (text*) sqlECALpin,(ub4) strlen(sqlECALpin),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStorePINADC: Error preparing stECALpin "); checkerr(ociError, status, erm); return -1; }
  
  status=OCIBindByPos(stECALpin, &bndp[1], ociError, (ub4)1, (dvoid*) &pinnam, (sb4) 4*sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStorePINADC: Error binding stECALpin "); checkerr(ociError, status, erm); return -1; }
  
  status=OCIDefineByPos(stECALpin, &defnp[1], ociError, (ub4)1, (dvoid*)&appd, (sb4)sizeof(appd), SQLT_ODT, (dvoid *) &appd_ind, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStorePINADC: Error defining appd in stECALpin"); checkerr(ociError, status, erm); return -1; }
  
  // stECALinss
  char sqlECALinss[]="INSERT INTO TABLE (SELECT p.pinadc_settings FROM ecal_pin_otb p WHERE p.pin_id=:1 ) VALUES(SYSDATE, NULL, :2)";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALinss, ociError, (text*) sqlECALinss,(ub4) strlen(sqlECALinss),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStorePINADC: Error preparing stECALinss "); checkerr(ociError, status, erm); return -1; }
  
  if(status==OCI_SUCCESS)status=OCIBindByPos(stECALinss, &bndp[11], ociError, (ub4)1, (dvoid*) &pinnam, (sb4) 4*sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stECALinss, &bndp[12], ociError, (ub4)2, (dvoid*) &del,    (sb4) sizeof(del),     (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStorePINADC; Error binding stECALinss "); checkerr(ociError, status, erm); return -1; }
  // stECALupds
  char sqlECALupds[]="UPDATE TABLE (SELECT p.pinadc_settings FROM ecal_pin_otb p WHERE p.pin_id=:1 ) r SET r.d_set=SYSDATE, r.delay_adc=:2 WHERE r.d_applied IS NULL";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALupds, ociError, (text*) sqlECALupds,(ub4) strlen(sqlECALupds),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStorePINADC: Error preparing stECALupds "); checkerr(ociError, status, erm); return -1; }
  
  if(status==OCI_SUCCESS)status=OCIBindByPos(stECALupds, &bndp[21], ociError, (ub4)1, (dvoid*) &pinnam, (sb4) 4*sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stECALupds, &bndp[22], ociError, (ub4)2, (dvoid*) &del,    (sb4) sizeof(del),     (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStorePINADC: Error binding stECALupds "); checkerr(ociError, status, erm); return -1; }
  
  while(PINname[ipin*PINname_len]=='A' || PINname[ipin*PINname_len]=='C'){
    // set bound variables
    for(int j=0; j<4; ++j)pinnam[j]=PINname[ipin*PINname_len+j];
    pinnam[4]='\0';
    del=(sword)floor(delay[ipin]*1e3);
    // execute stmt
    if(del<99000 && del>-99000){
      status=OCIStmtExecute(ociHdbc, stECALpin, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALStorePINADC: Error executing stECALpin "); checkerr(ociError, status, erm); return -1; }
      
      appd_ind=0;
      if( (status=OCIStmtFetch2(stECALpin, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	if(-1==appd_ind){
	  status=OCIStmtExecute(ociHdbc, stECALupds, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStorePINADC: Error executing stECALupds "); checkerr(ociError, status, erm); return -1; }
	}else{
	  status=OCIStmtExecute(ociHdbc, stECALinss, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStorePINADC: Error executing stECALinss "); checkerr(ociError, status, erm); return -1; }
	}
	++npin;
      }else{
	status=OCIStmtExecute(ociHdbc, stECALinss, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status==OCI_SUCCESS)	++npin;
      }
    }
    
    ++ipin;
  }
  status=OCIStmtRelease(stECALpin, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  status=OCIStmtRelease(stECALupds, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  status=OCIStmtRelease(stECALinss, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALStorePINADC: success %d chan out of %d", npin, ipin);
  return npin;
}

EXTERN_CALODB int ECALStoreLEDTSB (char* PINname, int PINname_len, int* LEDnum, int* delay, char* tune, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALStoreLEDTSB: not connected");return(-1);}
  
  sword status;
  OCIDate appd; OCIDateSetDate((OCIDate*)&appd, (sb2) 1980, (ub1) 7, (ub1) 1);
  sb2 appd_ind;
  OCIStmt* stECALled;
  OCIStmt* stECALinss;
  OCIStmt* stECALupds;
  int iled=0, nled=0;
  char pinnam[20];
  sword lednum=0;
  sword del=0;
  char seq[100];
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  
  // stECALled
  char sqlECALled[]="SELECT t.d_applied FROM ecal_led_otb l, table(l.ledtsb_settings) t WHERE l.pin_id=:1 AND l.led_num=:2 AND t.d_set = (SELECT MAX(t1.d_set) FROM TABLE(l.ledtsb_settings) t1)";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALled, ociError, (text*) sqlECALled,(ub4) strlen(sqlECALled),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC: Error preparing stECALled "); checkerr(ociError, status, erm); return -1; }
  
  status=OCIBindByPos(stECALled, &bndp[1], ociError, (ub4)1, (dvoid*) &pinnam, (sb4) 4*sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALled, &bndp[2], ociError, (ub4)2, (dvoid*) &lednum, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC: Error binding stECALled "); checkerr(ociError, status, erm); return -1; }
  
  status=OCIDefineByPos(stECALled, &defnp[1], ociError, (ub4)1, (dvoid*)&appd, (sb4)sizeof(appd), SQLT_ODT, (dvoid *) &appd_ind, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDDAC: Error defining appd in stECALled"); checkerr(ociError, status, erm); return -1; }
  
  // stECALinss
  char sqlECALinss[]="INSERT INTO TABLE (SELECT l.ledtsb_settings FROM ecal_led_otb l WHERE l.pin_id=:1 AND l.led_num=:2 ) VALUES(SYSDATE, NULL, :3, :4)";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALinss, ociError, (text*) sqlECALinss,(ub4) strlen(sqlECALinss),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreLEDTSB: Error preparing stECALinss "); checkerr(ociError, status, erm); return -1; }
  
  status=OCIBindByPos(stECALinss, &bndp[11], ociError, (ub4)1, (dvoid*) &pinnam, (sb4) 4*sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALinss, &bndp[12], ociError, (ub4)2, (dvoid*) &lednum, (sb4) sizeof(sword)  , (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALinss, &bndp[13], ociError, (ub4)3, (dvoid*) &del   , (sb4) sizeof(del)    , (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALinss, &bndp[14], ociError, (ub4)4, (dvoid*) &seq[0], (sb4) 64*sizeof(char), (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreLEDTSB; Error binding stECALinss "); checkerr(ociError, status, erm); return -1; }
  // stECALupds
  char sqlECALupds[]="UPDATE TABLE (SELECT l.ledtsb_settings FROM ecal_led_otb l WHERE l.pin_id=:1 AND l.led_num=:2 ) r SET r.d_set=SYSDATE, r.delay=:3, r.tune=:4 WHERE r.d_applied IS NULL";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALupds, ociError, (text*) sqlECALupds,(ub4) strlen(sqlECALupds),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreLEDTSB: Error preparing stECALupds "); checkerr(ociError, status, erm); return -1; }
  
  status=OCIBindByPos(stECALupds, &bndp[21], ociError, (ub4)1, (dvoid*) &pinnam, (sb4) 4*sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALupds, &bndp[22], ociError, (ub4)2, (dvoid*) &lednum, (sb4) sizeof(sword)  , (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALupds, &bndp[23], ociError, (ub4)3, (dvoid*) &del  ,  (sb4) sizeof(del)    , (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stECALupds, &bndp[24], ociError, (ub4)4, (dvoid*) &seq[0], (sb4) 64*sizeof(char), (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreLEDTSB: Error binding stECALupds "); checkerr(ociError, status, erm); return -1; }
  
  while(PINname[iled*PINname_len]=='A' || PINname[iled*PINname_len]=='C'){
    // set bound variables
    for(int j=0; j<4; ++j)pinnam[j]=PINname[iled*PINname_len+j];
    pinnam[4]='\0';
    lednum=LEDnum[iled];
    del=delay[iled];
    for(int j=0; j<64; ++j)seq[j]=tune[iled*64+j];
    seq[64]='\0';
    // execute stmt
    status=OCIStmtExecute(ociHdbc, stECALled, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreLEDTSB: Error executing stECALled "); checkerr(ociError, status, erm); return -1; }
    
    if( (status=OCIStmtFetch2(stECALled, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
      if(-1==appd_ind){
	status=OCIStmtExecute(ociHdbc, stECALupds, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreLEDTSB: Error executing stECALupds "); checkerr(ociError, status, erm); return -1; }
      }else{
	status=OCIStmtExecute(ociHdbc, stECALinss, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALStoreLEDTSB: Error executing stECALinss "); checkerr(ociError, status, erm); return -1; }
      }
      ++nled;
    }else{
      status=OCIStmtExecute(ociHdbc, stECALinss, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status!=OCI_SUCCESS)++nled;
    }
    ++iled;
  }
  status=OCIStmtRelease(stECALled, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  status=OCIStmtRelease(stECALupds, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  status=OCIStmtRelease(stECALinss, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALStoreLEDTSB: success %d chan out of %d", nled, iled);
  return nled;
}

EXTERN_CALODB int ECALClearLEDDACUpds(char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALClearLEDDACUpds: not connected");return(-1);}
  
  sword status;
  OCIStmt* stmthp;
  
  char sqlECAL[]="UPDATE ecal_led_otb l SET l.leddac_settings = CAST ( MULTISET( SELECT * FROM TABLE(l.leddac_settings) se WHERE se.d_applied IS NOT NULL) AS leddac_setting_list) WHERE EXISTS (SELECT * FROM TABLE(l.leddac_settings) se1 WHERE se1.d_applied IS NULL)";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALClearLEDDACUpds: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALClearLEDDACUpds: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALClearLEDDACUpds: success ");
  return 0;
}

EXTERN_CALODB int ECALClearPINADCUpds(char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALClearPINADCUpds: not connected");return(-1);}
  
  sword status;
  OCIStmt* stmthp;
  
  char sqlECAL[]="UPDATE ecal_pin_otb p SET p.pinadc_settings = CAST ( MULTISET( SELECT * FROM TABLE(p.pinadc_settings) se WHERE se.d_applied IS NOT NULL) AS adc_setting_list) WHERE EXISTS (SELECT * FROM TABLE(p.pinadc_settings) se1 WHERE se1.d_applied IS NULL)";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALClearPINADCUpds: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALClearPINADCUpds: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALClearPINADCUpds: success ");
  return 0;
}

EXTERN_CALODB int ECALClearLEDTSBUpds(char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALClearLEDTSBUpds: not connected");return(-1);}
  
  sword status;
  OCIStmt* stmthp;
  
  char sqlECAL[]="UPDATE ecal_led_otb l SET l.ledtsb_settings = CAST ( MULTISET( SELECT * FROM TABLE(l.ledtsb_settings) se WHERE se.d_applied IS NOT NULL) AS ledtsb_setting_list) WHERE EXISTS (SELECT * FROM TABLE(l.ledtsb_settings) se1 WHERE se1.d_applied IS NULL)";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALClearLEDTSBUpds: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALClearLEDTSBUpds: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALClearLEDTSBUpds: success ");
  return 0;
}

EXTERN_CALODB int ECALSetLEDDACApplied(char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALSetLEDDACApplied: not connected");return(-1);}
  
  sword status;
  OCIStmt* stmthp;
  
  char sqlECAL[]="BEGIN ecal_timestamp_allleddac(); END;";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetLEDDACApplied: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetLEDDACApplied: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALSetLEDDACApplied: success ");
  return 0;
}

EXTERN_CALODB int ECALSetPINADCApplied(char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALSetPINADCApplied: not connected");return(-1);}
  
  sword status;
  OCIStmt* stmthp;
  
  char sqlECAL[]="BEGIN ecal_timestamp_allpinadc(); END;";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPINADCApplied: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPINADCApplied: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALSetPINADCApplied: success ");
  return 0;
}

EXTERN_CALODB int ECALSetLEDTSBApplied(char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALSetLEDTSBApplied: not connected");return(-1);}
  
  sword status;
  OCIStmt* stmthp;

  char sqlECAL[]="BEGIN ecal_timestamp_allledtsb(); END;";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetLEDTSBApplied: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetLEDTSBApplied: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALSetLEDTSBApplied: success ");
  return 0;
}

EXTERN_CALODB int ECALSetLEDDACUnapplied(int* d1, int* d2, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALSetLEDDACUnapplied: not connected");return(-1);}
  
  sword status;
  OCIStmt* stmthp;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  
  // dates
  OCIDate date1; 
  OCIDateSetDate((OCIDate*)&date1, (sb2) d1[0], (ub1) d1[1], (ub1) d1[2]); 
  OCIDateSetTime((OCIDate*)&date1, d1[3],d1[4],d1[5]);
  OCIDate date2; 
  if(d2[0]>0){
    OCIDateSetDate((OCIDate*)&date2, (sb2) d2[0], (ub1) d2[1], (ub1) d2[2]); 
    OCIDateSetTime((OCIDate*)&date2, (ub1) d2[3], (ub1) d2[4], (ub1) d2[5]);
  }else{
    status=OCIDateSysDate(ociError, &date2);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetLEDDACUnapplied: Error getting SysDate "); checkerr(ociError, status, erm); return -1; }
  }
  
  char sqlECAL[]="UPDATE /*+NESTED_TABLE_GET_REFS*/ ecal_leddac_setting_otb SET d_applied=NULL WHERE d_set>:1 AND d_set<:2";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetLEDDACUnapplied: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  // bind
  status=OCIBindByPos(stmthp, &bndp[0], ociError, (ub4)1, (dvoid*) &date1, (sb4) sizeof(OCIDate) , (ub2) SQLT_ODT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stmthp, &bndp[1], ociError, (ub4)2, (dvoid*) &date2, (sb4) sizeof(OCIDate), (ub2) SQLT_ODT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetLEDDACUnapplied: Error binding stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetLEDDACUnapplied: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALSetLEDDACUnapplied: success ");
  return 0;
}

EXTERN_CALODB int ECALSetPINADCUnapplied(int* d1, int* d2, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALSetPINADCUnapplied: not connected");return(-1);}
  
  sword status;  
  OCIStmt* stmthp;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  
  // dates
  OCIDate date1; 
  OCIDateSetDate((OCIDate*)&date1, (sb2) d1[0], (ub1) d1[1], (ub1) d1[2]); 
  OCIDateSetTime((OCIDate*)&date1, d1[3],d1[4],d1[5]);
  OCIDate date2; 
  if(d2[0]>0){
    OCIDateSetDate((OCIDate*)&date2, (sb2) d2[0], (ub1) d2[1], (ub1) d2[2]); 
    OCIDateSetTime((OCIDate*)&date2, (ub1) d2[3], (ub1) d2[4], (ub1) d2[5]);
  }else{
    status=OCIDateSysDate(ociError, &date2);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPINADCUnapplied: Error getting SysDate "); checkerr(ociError, status, erm); return -1; }
  }
  
  char sqlECAL[]="UPDATE /*+NESTED_TABLE_GET_REFS*/ ecal_pinadc_setting_otb SET d_applied=NULL WHERE d_set>:1 AND d_set<:2";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPINADCUnapplied: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  // bind
  status=OCIBindByPos(stmthp, &bndp[0], ociError, (ub4)1, (dvoid*) &date1, (sb4) sizeof(OCIDate) , (ub2) SQLT_ODT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stmthp, &bndp[1], ociError, (ub4)2, (dvoid*) &date2, (sb4) sizeof(OCIDate), (ub2) SQLT_ODT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPINADCUnapplied: Error binding stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPINADCUnapplied: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALSetPINADCUnapplied: success ");
  return 0;
}

EXTERN_CALODB int ECALSetLEDTSBUnapplied(int* d1, int* d2, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALSetLEDTSBUnapplied: not connected");return(-1);}
  
  sword status;
  OCIStmt* stmthp;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  
  // dates
  OCIDate date1; 
  OCIDateSetDate((OCIDate*)&date1, (sb2) d1[0], (ub1) d1[1], (ub1) d1[2]); 
  OCIDateSetTime((OCIDate*)&date1, d1[3],d1[4],d1[5]);
  OCIDate date2; 
  if(d2[0]>0){
    OCIDateSetDate((OCIDate*)&date2, (sb2) d2[0], (ub1) d2[1], (ub1) d2[2]); 
    OCIDateSetTime((OCIDate*)&date2, (ub1) d2[3], (ub1) d2[4], (ub1) d2[5]);
  }else{
    status=OCIDateSysDate(ociError, &date2);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetLEDTSBUnapplied: Error getting SysDate "); checkerr(ociError, status, erm); return -1; }
  }
  
  char sqlECAL[]="UPDATE /*+NESTED_TABLE_GET_REFS*/ ecal_ledtsb_setting_otb SET d_applied=NULL WHERE d_set>:1 AND d_set<:2";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetLEDTSBUnapplied: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  // bind
  status=OCIBindByPos(stmthp, &bndp[0], ociError, (ub4)1, (dvoid*) &date1, (sb4) sizeof(OCIDate) , (ub2) SQLT_ODT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stmthp, &bndp[1], ociError, (ub4)2, (dvoid*) &date2, (sb4) sizeof(OCIDate), (ub2) SQLT_ODT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetLEDTSBUnapplied: Error binding stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetLEDTSBUnapplied: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALSetLEDTSBUnapplied: success ");
  return 0;
}


EXTERN_CALODB int ECALGetLEDChans(char* type, int* x, int* y, const char* PINname, const int LEDnum, char* erm){
  if(!m_connected){strcpy(erm, "ECALGetLEDChans: not connected");return(-1);}
  erm[0]=(char)0;
  
  sword status;
  OCIStmt* stmtp;
  char pinnam[20];
  sword lednum=0;  
  char tip;
  sword ix,iy;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  
  char sqlst[]="SELECT ch.chan_type, ch.chan_x, ch.chan_y FROM ecal_chan_otb ch, ecal_module_table m WHERE m.pin_id=:1 AND m.led_num=:2 AND ch.mod_id=m.mod_id AND ch.dac_chan>0 ORDER BY ch.chan_type, ch.chan_x, ch.chan_y";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmtp, ociError, (text*) sqlst,(ub4) strlen(sqlst),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDChans: Error preparing stmtp "); checkerr(ociError, status, erm); return -1; }
  
  status=OCIBindByPos(stmtp, &bndp[1], ociError, (ub4)1, (dvoid*) &pinnam, (sb4) 4*sizeof(char) , (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS) status=OCIBindByPos(stmtp, &bndp[2], ociError, (ub4)2, (dvoid*) &lednum, (sb4) sizeof(sword), (ub2) SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) 0, (ub4 *) 0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDChans: Error binding stmtp "); checkerr(ociError, status, erm); return -1; }
  
  status=OCIDefineByPos(stmtp, &defnp[1], ociError, (ub4)1, (dvoid*)&tip, (sb4)sizeof(tip), SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDChans: Error defining tip in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[2], ociError, (ub4)2, (dvoid*)&ix, (sb4)sizeof(ix), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDChans: Error defining ix in stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIDefineByPos(stmtp, &defnp[3], ociError, (ub4)3, (dvoid*)&iy, (sb4)sizeof(iy), SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDChans: Error defining iy in stmtp "); checkerr(ociError, status, erm); return -1; }
  
  // get bind values
  for(int j=0; j<4; ++j)pinnam[j]=PINname[j];
  pinnam[4]='\0';
  lednum=LEDnum;
  // execute stmtp
  status=OCIStmtExecute(ociHdbc, stmtp, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDChans: Error executing stmtp "); checkerr(ociError, status, erm); return -1; }

  int nfound=0;
  while( (status=OCIStmtFetch2(stmtp, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
    type[nfound]=tip;
    x[nfound]=ix;
    y[nfound]=iy;
    ++nfound;
  }
  status=OCIStmtRelease(stmtp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  if(nfound>0)sprintf(erm,"ECALGetLEDChans: success ");
  else if(nfound==0)sprintf(erm,"ECALGetLEDChans: WARNING: no cells found ");
  return nfound;
}

EXTERN_CALODB int ECALGetChanLED(char type, int x, int y, char* PINname, int& LEDnum, char* erm){
  if(!m_connected){strcpy(erm, "ECALGetChanLED: not connected");return(-1);}
  erm[0]=(char)0;
  
  sword status;
  OCIStmt* stmtp;
  char pinnam[20];
  sword lednum=0;  
  char tip;
  sword ix,iy;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  
  char sqlst[]="SELECT m.pin_id, m.led_num FROM ecal_chan_otb ch, ecal_module_table m WHERE ch.chan_type=:1 AND ch.chan_x=:2 AND ch.chan_y=:3 AND ch.mod_id=m.mod_id";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmtp, ociError, (text*) sqlst,(ub4) strlen(sqlst),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanLED: Error preparing stmtp "); checkerr(ociError, status, erm); return -1; }
  
  status=OCIBindByPos(stmtp, &bndp[0], ociError, (ub4)1, (dvoid*)&tip, (sb4)sizeof(tip), (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanLED: Error binding tip to stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIBindByPos(stmtp, &bndp[1], ociError, (ub4)2, (dvoid*)&ix, (sb4)sizeof(ix), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanLED: Error binding ix to stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIBindByPos(stmtp, &bndp[2], ociError, (ub4)3, (dvoid*)&iy, (sb4)sizeof(iy), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanLED: Error binding iy to stmtp "); checkerr(ociError, status, erm); return -1; }
  
  if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[1] , ociError, (ub4) 1, (dvoid*)&pinnam, (sb4)4*sizeof(char), SQLT_CHR, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[2] , ociError, (ub4) 2, (dvoid*)&lednum, (sb4)sizeof(lednum), SQLT_INT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanLED: Error defining stmtp "); checkerr(ociError, status, erm); return -1; }
  
  // get bind values
  tip=type;
  ix=x;
  iy=y;
  
  // execute stmtp
  status=OCIStmtExecute(ociHdbc, stmtp, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanLED: Error executing stmtp "); checkerr(ociError, status, erm); return -1; }
  
  int nfound=0;
  while( (status=OCIStmtFetch2(stmtp, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
    for(int j=0; j<4; ++j)PINname[j]=pinnam[j];
    PINname[4]='\0';
    LEDnum=lednum;
    ++nfound;
  }
  status=OCIStmtRelease(stmtp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  if(1==nfound)sprintf(erm,"ECALGetChanLED: success ");
  else if(0==nfound)sprintf(erm,"ECALGetChanLED: WARNING: no LEDs found ");
  else if(nfound>1)sprintf(erm,"ECALGetChanLED: WARNING: more than one LED found ");
  return nfound;
}

EXTERN_CALODB int ECALGetChanLEDs(char* type, int* x, int* y, char* PINname, int PINname_len, int* LEDnum, char* erm){
  if(!m_connected){strcpy(erm, "ECALGetChanLEDs: not connected");return(-1);}
  erm[0]=(char)0;
  
  sword status;
  bool allCells=true;
  if( ('O'==type[0]) || ('M'==type[0]) || ('I'==type[0]) ) allCells=false;
  
  OCIStmt* stmtp;
  char pinnam[20];
  sword lednum=0;
  int ich=0, nch=0;  
  char tip;
  sword ix,iy;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  
  if(!allCells){
    char sqlst[]="SELECT m.pin_id, m.led_num FROM ecal_chan_otb ch, ecal_module_table m WHERE ch.chan_type=:1 AND ch.chan_x=:2 AND ch.chan_y=:3 AND ch.mod_id=m.mod_id";
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmtp, ociError, (text*) sqlst,(ub4) strlen(sqlst),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanLEDs: Error preparing stmtp "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[0], ociError, (ub4)1, (dvoid*)&tip, (sb4)sizeof(tip), (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[1], ociError, (ub4)2, (dvoid*) &ix, (sb4)sizeof(ix) , (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[2], ociError, (ub4)3, (dvoid*) &iy, (sb4)sizeof(iy) , (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanLEDs: Error binding stmtp "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[1], ociError, (ub4) 1, (dvoid*)&pinnam, (sb4)4*sizeof(char), (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[2], ociError, (ub4) 2, (dvoid*)&lednum, (sb4)sizeof(lednum), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanLEDs: Error defining stmtp "); checkerr(ociError, status, erm); return -1; }
    
    while('O'==type[ich] || 'M'==type[ich] || 'I'==type[ich]){
      PINname[PINname_len*ich]='\0';
      LEDnum[ich]=-1;
      if(ECALCellExists(type[ich],x[ich],y[ich])){
	// get bind values
	tip=type[ich];
	ix=x[ich];
	iy=y[ich];
	
	// execute stmtp
	status=OCIStmtExecute(ociHdbc, stmtp, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
	if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanLEDs: Error executing stmtp "); checkerr(ociError, status, erm); return -1; }
	
	if( (status=OCIStmtFetch2(stmtp, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	  for(int j=0; j<4; ++j)PINname[PINname_len*ich+j]=pinnam[j];
	  PINname[PINname_len*ich+4]='\0';
	  LEDnum[ich]=lednum;
	  ++nch;
	}
      }
      ++ich;
    }
    status=OCIStmtRelease(stmtp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
    sprintf(erm,"ECALGetChanLEDs: %d cells found out of %d",nch,ich);
  }else{
    char sqlst[]="SELECT ch.chan_type, ch.chan_x, ch.chan_y, m.pin_id, m.led_num FROM ecal_chan_otb ch, ecal_module_table m WHERE ch.mod_id=m.mod_id ORDER BY m.pin_id, m.led_num";
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmtp, ociError, (text*) sqlst,(ub4) strlen(sqlst),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanLEDs: Error preparing stmtp "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[1], ociError, (ub4)1, (dvoid*)&tip,    (sb4)sizeof(tip),    (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[2], ociError, (ub4)2, (dvoid*)&ix,     (sb4)sizeof(ix),     (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[3], ociError, (ub4)3, (dvoid*)&iy,     (sb4)sizeof(iy),     (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[4], ociError, (ub4)4, (dvoid*)&pinnam, (sb4)4*sizeof(char), (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[5], ociError, (ub4)5, (dvoid*)&lednum, (sb4)sizeof(lednum), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanLEDs: Error defining stmtp "); checkerr(ociError, status, erm); return -1; }
    
    // execute stmtp
    status=OCIStmtExecute(ociHdbc, stmtp, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetChanLEDs: Error executing stmtp "); checkerr(ociError, status, erm); return -1; }
    
    while( (status=OCIStmtFetch2(stmtp, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
      type[ich]=tip;
      x[ich]=ix;
      y[ich]=iy;
      for(int j=0; j<4; ++j)PINname[PINname_len*ich+j]=pinnam[j];
      PINname[PINname_len*ich+4]='\0';
      LEDnum[ich]=lednum;
      ++ich;
    }
    status=OCIStmtRelease(stmtp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
    nch=ich;
    sprintf(erm,"ECALGetChanLEDs: %d cells found",ich);
  }
  
  return nch;
}

EXTERN_CALODB int ECALGetLEDPIN(int Search, char* PINname, int& Iled, int& iDAC, int& iTSB, int& iADC, char* erm){
  if(!m_connected){strcpy(erm, "ECALGetLEDPIN: not connected");return(-1);}
  erm[0]=(char)0;
  //
  // Search - search item:
  //   1 - (PINname, Iled)
  //   2 - iDAC
  //   3 - iTSB
  //   4 - iADC
  //
  //   It invokes the PL/SQL procedure ecal_led(isearch, pinname, iled, idac, itsb, iadc)
  //   where isearch means the same as Search 
  //	           
  
  sword status;
  OCIStmt* stmtp;
  sword isearch;
  char pinnam[8];
  sword iled, idac, itsb, iadc;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  
  if(1==Search){ 
    isearch=1;
    strncpy(pinnam,PINname,8);
    pinnam[7]='\0';
    iled=Iled;
  }else if(2==Search){
    isearch=2;
    idac=iDAC;
  }else if(3==Search){
    isearch=3;
    itsb=iTSB;
  }else if(4==Search){
    isearch=4;
    iadc=iADC;
  }else isearch=0;
  
  if(0==isearch){sprintf(erm, "ECALGetLEDPIN: search %d: not implemented yet",Search); return(-1);}
  
  char sqlst[]="BEGIN ecal_led(:1, :2, :3, :4, :5, :6); END;";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmtp, ociError, (text*) sqlst,(ub4) strlen(sqlst),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDPIN: Error preparing stmtp "); checkerr(ociError, status, erm); return -1; }
  
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[1], ociError, (ub4)1, (dvoid*)&isearch, (sb4)sizeof(isearch), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[2], ociError, (ub4)2, (dvoid*)&pinnam,  (sb4)4*sizeof(char),  (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[3], ociError, (ub4)3, (dvoid*)&iled,    (sb4)sizeof(iled),    (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[4], ociError, (ub4)4, (dvoid*)&idac,    (sb4)sizeof(idac),    (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[5], ociError, (ub4)5, (dvoid*)&itsb,    (sb4)sizeof(itsb),    (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[6], ociError, (ub4)6, (dvoid*)&iadc,    (sb4)sizeof(iadc),    (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDPIN: Error binding stmtp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmtp
  status=OCIStmtExecute(ociHdbc, stmtp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDPIN: Error executing stmtp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmtp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  if(isearch<0){sprintf(erm,"ECALGetLEDPIN: not found "); return 0; }
  
  // copy info
  if(Search!=1){
    pinnam[4]='\0';
    strcpy(PINname, pinnam);
    Iled=iled;
  }
  if(Search!=2)iDAC=idac;
  if(Search!=3)iTSB=itsb;
  if(Search!=4)iADC=iadc;
  
  sprintf(erm,"ECALGetLEDPIN: success ");
  return 1;
}

EXTERN_CALODB int ECALGetLEDFiberLength(char* PINname, int LEDnum, double& LED_len, double& PIN_len, char* erm){
  if(!m_connected){strcpy(erm, "ECALGetLEDFiberLength: not connected");return(-1);}
  erm[0]=(char)0;
  
  sword status;
  OCIStmt* stmtp;
  char pinnam[20];
  sword lednum=0;  
  double ledlen=0,pinlen=0;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  
  char sqlst[]="SELECT l.ledfib_len, l.pinfib_len FROM ecal_led_otb l WHERE l.pin_id=:1 AND l.led_num=:2";
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmtp, ociError, (text*) sqlst,(ub4) strlen(sqlst),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDFiberLength: Error preparing stmtp "); checkerr(ociError, status, erm); return -1; }
  
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[1], ociError, (ub4)1, (dvoid*)&pinnam, (sb4)4*sizeof(char), (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[2], ociError, (ub4)2, (dvoid*)&lednum, (sb4)sizeof(sword),  (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDFiberLength: Error binding stmtp "); checkerr(ociError, status, erm); return -1; }
  
  if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[1], ociError, (ub4)1, (dvoid*)&ledlen, (sb4)sizeof(ledlen), (ub2)SQLT_FLT, (dvoid*) 0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[2], ociError, (ub4)2, (dvoid*)&pinlen, (sb4)sizeof(pinlen), (ub2)SQLT_FLT, (dvoid*) 0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDFiberLength: Error defining stmtp "); checkerr(ociError, status, erm); return -1; }
  
  // get bind values
  for(int j=0; j<4; ++j)pinnam[j]=PINname[j];
  pinnam[4]='\0';
  lednum=LEDnum;
  // execute stmtp
  status=OCIStmtExecute(ociHdbc, stmtp, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDFiberLength: Error executing stmtp "); checkerr(ociError, status, erm); return -1; }
  
  int nfound=0;
  while( (status=OCIStmtFetch2(stmtp, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
    LED_len=ledlen;
    PIN_len=pinlen;
    ++nfound;
  }
  status=OCIStmtRelease(stmtp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  if(nfound>0)sprintf(erm,"ECALGetLEDFiberLength: success ");
  else if(nfound==0)sprintf(erm,"ECALGetLEDFiberLength: WARNING: LED not found ");
  return nfound;
}

EXTERN_CALODB int ECALSetLEDFiberLength(char* PINname, int LEDnum, double LED_len, double PIN_len, char* erm){
  if(!m_connected){strcpy(erm, "ECALSetLEDFiberLength: not connected");return(-1);}
  erm[0]=(char)0;
  
  sword status;
  char pinnam[20];
  sword lednum=0;
  double ledlen=0, pinlen=0;
  OCIStmt* stmtp;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  
  char sqlst[]="UPDATE ecal_led_otb l SET l.ledfib_len=:1, l.pinfib_len=:2 WHERE l.pin_id=:3 AND l.led_num=:4";
  status=OCIStmtPrepare2(ociHdbc, (OCIStmt**)&stmtp, ociError, (text*)sqlst, (ub4)strlen(sqlst), NULL, 0, (ub4)OCI_NTV_SYNTAX, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetLEDFiberLength: Error preparing stmtp "); checkerr(ociError, status, erm); return -1; }
  
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[1], ociError, (ub4)1, (dvoid*)&ledlen, (sb4)sizeof(double), (ub2)SQLT_FLT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[2], ociError, (ub4)2, (dvoid*)&pinlen, (sb4)sizeof(double), (ub2)SQLT_FLT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[3], ociError, (ub4)3, (dvoid*)&pinnam, (sb4)4*sizeof(char), (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[4], ociError, (ub4)4, (dvoid*)&lednum, (sb4)sizeof(sword) , (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetLEDFiberLength: Error binding stmtp "); checkerr(ociError, status, erm); return -1; }
  
  if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[1], ociError, (ub4)1, (dvoid*)&ledlen, (sb4)sizeof(ledlen), (ub2)SQLT_FLT, (dvoid*) 0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[2], ociError, (ub4)2, (dvoid*)&pinlen, (sb4)sizeof(pinlen), (ub2)SQLT_FLT, (dvoid*) 0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetLEDFiberLength: Error defining stmtp "); checkerr(ociError, status, erm); return -1; }
  
  // get bind values
  ledlen=LED_len;
  pinlen=PIN_len;
  for(int j=0; j<4; ++j)pinnam[j]=PINname[j]; pinnam[4]='\0';
  lednum=LEDnum;
  // execute stmtp
  status=OCIStmtExecute(ociHdbc, stmtp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetLEDFiberLength: Error executing stmtp "); checkerr(ociError, status, erm); return -1; }
  
  status=OCIStmtRelease(stmtp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  sprintf(erm,"ECALSetLEDFiberLength: success ");
  return 0;
}

EXTERN_CALODB int ECALGetLEDFiberLengths(char* PINname, int PINname_len, int* LEDnum, double* LED_len, double* PIN_len, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALGetLEDFiberLengths: not connected");return(-1);}
  
  bool allCells=true;
  if( ('A'==PINname[0]) || ('C'==PINname[0]) ) allCells=false;
  
  sword status;
  OCIStmt* stECALled;
  OCIStmt* stmt_allSetng;
  int iled=0, nled=0;
  char pinnam[20];
  sword lednum=0;
  double ledlen=0, pinlen=0;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  
  if(!allCells){
    char sqlECALled[]="SELECT l.ledfib_len, l.pinfib_len FROM ecal_led_otb l WHERE l.pin_id=:1 AND l.led_num=:2";
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stECALled, ociError, (text*) sqlECALled,(ub4) strlen(sqlECALled),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDFiberLengths: Error preparing stECALled "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS)status=OCIBindByPos(stECALled, &bndp[3], ociError, (ub4)1, (dvoid*)&pinnam, (sb4)4*sizeof(char)  , (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIBindByPos(stECALled, &bndp[4], ociError, (ub4)2, (dvoid*)&lednum, (sb4)  sizeof(lednum), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDFiberLengths: Error binding stECALled "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stECALled, &defnp[1], ociError, (ub4)1, (dvoid*)&ledlen, (sb4)sizeof(ledlen), (ub2)SQLT_FLT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stECALled, &defnp[2], ociError, (ub4)2, (dvoid*)&pinlen, (sb4)sizeof(pinlen), (ub2)SQLT_FLT, (dvoid *) 0, (ub2 *)0, (ub2 *)0, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDFiberLengths: Error defining stECALled"); checkerr(ociError, status, erm); return -1; }
    
    while('A'==PINname[iled*PINname_len] || 'C'==PINname[iled*PINname_len]){
      // set defaults
      LED_len[iled]=0;
      PIN_len[iled]=0;
      // set bound variables
      for(int j=0; j<4; ++j)pinnam[j]=PINname[iled*PINname_len+j];
      pinnam[4]='\0';
      lednum=LEDnum[iled];
      // execute stmt
      status=OCIStmtExecute(ociHdbc, stECALled, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDFiberLengths: Error executing stECALled "); checkerr(ociError, status, erm); return -1; }
      
      if( (status=OCIStmtFetch2(stECALled, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	LED_len[iled]=ledlen;
	PIN_len[iled]=pinlen;
	++nled;
      }
      ++iled;
    }
    status=OCIStmtRelease(stECALled, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }else{
    char sql_allSetng[]="SELECT l.pin_id, l.led_num, l.ledfib_len, l.pinfib_len FROM ecal_led_otb l ORDER BY l.pin_id, l.led_num";
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmt_allSetng, ociError, (text*) sql_allSetng,(ub4) strlen(sql_allSetng),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDFiberLengths: Error preparing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[5], ociError, (ub4)1, (dvoid*)&pinnam, (sb4)4*sizeof(char), (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[6], ociError, (ub4)2, (dvoid*)&lednum, (sb4)sizeof(lednum), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[7], ociError, (ub4)3, (dvoid*)&ledlen, (sb4)sizeof(ledlen), (ub2)SQLT_FLT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS) status=OCIDefineByPos(stmt_allSetng, &defnp[8], ociError, (ub4)4, (dvoid*)&pinlen, (sb4)sizeof(pinlen), (ub2)SQLT_FLT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDFiberLengths: Error defining stmt_allSetng"); checkerr(ociError, status, erm); return -1; }
    
    // execute stmt
    status=OCIStmtExecute(ociHdbc, stmt_allSetng, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALGetLEDFiberLengths: Error executing stmt_allSetng "); checkerr(ociError, status, erm); return -1; }
    
    while((status=OCIStmtFetch2(stmt_allSetng, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS || status==OCI_SUCCESS_WITH_INFO ){
      for(int i=0; i<4; ++i) PINname[iled*PINname_len+i]=pinnam[i];
      PINname[iled*PINname_len+4]='\0';
      LEDnum[iled]=lednum;
      LED_len[iled]=ledlen;
      PIN_len[iled]=pinlen;
      ++iled;
      ++nled;
    }
    status=OCIStmtRelease(stmt_allSetng, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }
  
  sprintf(erm,"ECALGetLEDFiberLengths: success %d chan out of %d", nled, iled);
  return nled;
}




EXTERN_CALODB int ECALConnDefault(const char* act, const char* what, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALConnDefault: not connected");return(-1);}
  
  sword status;
  OCIStmt* stmthp;
  
  char sqlECAL[200];
  if(0==strcmp(act, "SET")){
    if     (0==strcmp(what,"DAC"))      strcpy(sqlECAL,"BEGIN ecal_chan_dac_setdef(); END;");
    else if(0==strcmp(what,"ADC"))	strcpy(sqlECAL,"BEGIN ecal_chan_adc_setdef(); END;");
    else if(0==strcmp(what,"POW"))	strcpy(sqlECAL,"BEGIN ecal_chan_pow_setdef(); END;");
    else if(0==strcmp(what,"LEDDAC"))	strcpy(sqlECAL,"BEGIN ecal_led_dac_setdef(); END;");
    else if(0==strcmp(what,"LEDTSB"))	strcpy(sqlECAL,"BEGIN ecal_led_tsb_setdef(); END;");
    else if(0==strcmp(what,"PINADC"))	strcpy(sqlECAL,"BEGIN ecal_pin_adc_setdef(); END;");
    else {sprintf(erm,"ECALConnDefault: %s %s ? ", act, what); return -1;}
  }else if(0==strcmp(act, "RESTORE")){
    if     (0==strcmp(what,"DAC"))	strcpy(sqlECAL,"BEGIN ecal_chan_dac_restoredef(); END;");
    else if(0==strcmp(what,"ADC"))	strcpy(sqlECAL,"BEGIN ecal_chan_adc_restoredef(); END;");
    else if(0==strcmp(what,"POW"))	strcpy(sqlECAL,"BEGIN ecal_chan_pow_restoredef(); END;");
    else if(0==strcmp(what,"LEDDAC"))	strcpy(sqlECAL,"BEGIN ecal_led_dac_restoredef(); END;");
    else if(0==strcmp(what,"LEDTSB"))	strcpy(sqlECAL,"BEGIN ecal_led_tsb_restoredef(); END;");
    else if(0==strcmp(what,"PINADC"))	strcpy(sqlECAL,"BEGIN ecal_pin_adc_restoredef(); END;");
    else {sprintf(erm,"ECALConnDefault: %s %s ? ", act, what); return -1;}
  }else {sprintf(erm,"ECALConnDefault: %s %s ? ", act, what); return -1;}
  
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnDefault: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALConnDefault: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALConnDefault: success ");
  return 0;
}

EXTERN_CALODB int ECALSetChanConn(char type, int x, int y, const char* what, const void* val, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALSetChanConn: not connected");return(-1);}
  
  char tip;
  sword ix,iy;
  char ar[16];
  sb4 dsiz;
  ub2 dtyp;  
  sword status;
  OCIStmt* stmthp;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  
  if(0==strcmp(what,"PIN")){
    dtyp=SQLT_CHR;
    dsiz=4*sizeof(char);
    char* chp=(char*)val;
    for(int i=0; i<4;++i){
      if('\0'==chp[i])break; 
      ar[i]=chp[i];
    }
    ar[4]='\0';
  }else if(0==strcmp(what,"ADC") || 0==strcmp(what,"DAC") || 0==strcmp(what,"POW") || 0==strcmp(what,"LED")){
    dtyp=SQLT_INT;
    dsiz=sizeof(sword);
    memcpy(ar,val,sizeof(sword));
  } else {sprintf(erm,"ECALSetChanConn: %s ? ", what); return -1;}
  
  char sqlECAL[200];
  if     (0==strcmp(what,"DAC"))		strcpy(sqlECAL,"BEGIN ecal_chan_setdac(:1, :2, :3, :4); END;");
  else if(0==strcmp(what,"ADC"))		strcpy(sqlECAL,"BEGIN ecal_chan_setadc(:1, :2, :3, :4); END;");
  else if(0==strcmp(what,"POW"))		strcpy(sqlECAL,"BEGIN ecal_chan_setpow(:1, :2, :3, :4); END;");
  else if(0==strcmp(what,"PIN"))		strcpy(sqlECAL,"BEGIN ecal_cell_setpin(:1, :2, :3, :4); END;");
  else if(0==strcmp(what,"LED"))		strcpy(sqlECAL,"BEGIN ecal_cell_setled(:1, :2, :3, :4); END;");
  
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*)sqlECAL, (ub4)strlen(sqlECAL), NULL, 0, (ub4)OCI_NTV_SYNTAX, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetChanConn: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  // bind
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[1], ociError, (ub4)1, (dvoid*)& tip, (sb4)sizeof(char) , (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[2], ociError, (ub4)2, (dvoid*)&  ix, (sb4)sizeof(sword), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[3], ociError, (ub4)3, (dvoid*)&  iy, (sb4)sizeof(sword), (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[4], ociError, (ub4)4, (dvoid*)   ar,               dsiz,          dtyp, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetChanConn: Error binding stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // set bind variables
  tip=type;
  ix=x; 
  iy=y;
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetChanConn: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALSetChanConn: success ");
  return 0;
}

EXTERN_CALODB int ECALSetLEDConn(const char* pinnam, int lednum, const char* what, int val, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALSetLEDConn: not connected");return(-1);}
  
  char pin[20];
  sword iled,ival;
  sword status;  
  OCIStmt* stmthp;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  
  char sqlECAL[200];
  if     (0==strcmp(what,"LEDDAC"))		strcpy(sqlECAL,"BEGIN ecal_led_setdac(:1, :2, :3); END;");
  else if(0==strcmp(what,"LEDTSB"))		strcpy(sqlECAL,"BEGIN ecal_led_settsb(:1, :2, :3); END;");
  else {sprintf(erm,"ECALSetLEDConn: %s ? ", what); return -1;}
  
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetLEDConn: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  // bind
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[1], ociError, (ub4)1, (dvoid*)  pin, (sb4)4*sizeof(char), (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[2], ociError, (ub4)2, (dvoid*)&iled, (sb4)sizeof(sword) , (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[3], ociError, (ub4)3, (dvoid*)&ival, (sb4)sizeof(sword) , (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetLEDConn: Error binding stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // set bind variables
  for(int i=0; i<4; ++i)pin[i]=pinnam[i];
  pin[4]='\0';
  iled=lednum;
  ival=val;
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetLEDConn: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALSetLEDConn: success ");
  return 0;
}

EXTERN_CALODB int ECALSetPINConn(const char* pinnam, const char* what, int val, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALSetPINConn: not connected");return(-1);}
  
  char pin[20];
  sword ival;
  sword status;  
  OCIStmt* stmthp;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  
  char sqlECAL[200];
  if     (0==strcmp(what,"PINADC"))		strcpy(sqlECAL,"BEGIN ecal_pin_setadc(:1, :2); END;");
  else {sprintf(erm,"ECALSetPINConn: %s ? ", what); return -1;}
  
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPINConn: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  // bind
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[1], ociError, (ub4)1, (dvoid*)  pin, (sb4)4*sizeof(char), (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[2], ociError, (ub4)2, (dvoid*)&ival, (sb4)sizeof(sword) , (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPINConn: Error binding stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // set bind variables
  for(int i=0; i<4; ++i)pin[i]=pinnam[i];
  pin[4]='\0';
  ival=val;
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4) 1, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPINConn: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  
  sprintf(erm,"ECALSetPINConn: success ");
  return 0;
}

EXTERN_CALODB int ECALSetPMCalib(char tip, int x, int y, double g, double alpha, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALSetPMCalib: not connected");return(-1);}
  
  OCIStmt* stmthp;
  OCIBind* bndp[8];  for(int i=0; i<8; ++i) bndp[i] = (OCIBind*) 0;
  
  sword status;
  char sqlECAL[]="BEGIN ecal_pmcalib_update(:1,:2,:3,:4,:5); END;";
  
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPMCalib: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  // bind
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[1], ociError, (ub4) 1, (dvoid*)&tip,   (sb4)sizeof(tip),    (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[2], ociError, (ub4) 2, (dvoid*)&x,     (sb4)sizeof(x),      (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[3], ociError, (ub4) 3, (dvoid*)&y,     (sb4)sizeof(y),      (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[4], ociError, (ub4) 4, (dvoid*)&g,     (sb4)sizeof(g),      (ub2)SQLT_FLT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[5], ociError, (ub4) 5, (dvoid*)&alpha, (sb4)sizeof(alpha),  (ub2)SQLT_FLT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPMCalib: Error binding stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4)1, (ub4)0, (OCISnapshot*)NULL, (OCISnapshot*)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALSetPMCalib: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4)OCI_DEFAULT);
  
  sprintf(erm,"ECALSetPMCalib: success ");
  return 0;
}

EXTERN_CALODB int ECALAddPMCalib(char tip, int x, int y, char* PMnam, double g, double alpha, char* erm){
  erm[0]=(char)0;
  if(!m_connected){strcpy(erm, "ECALAddPMCalib: not connected");return(-1);}
  
  OCIStmt* stmthp;
  OCIBind* bndp[8];  for(int i=0; i<8; ++i) bndp[i] = (OCIBind*) 0;
  
  sword status;
  char pmnam[20];	
  strncpy(pmnam,PMnam,8);
  for(int i=strlen(pmnam); i<20; ++i)pmnam[i]=' ';
  pmnam[8]='\0';
  
  char sqlECAL[]="BEGIN ecal_pmcalib_add(:1,:2,:3,:4,:5,:6); END;";
  
  status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmthp, ociError, (text*) sqlECAL,(ub4) strlen(sqlECAL),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALAddPMCalib: Error preparing stmthp "); checkerr(ociError, status, erm); return -1; }
  // bind
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[1], ociError, (ub4) 1, (dvoid*)&tip,   (sb4)sizeof(tip),    (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[2], ociError, (ub4) 2, (dvoid*)&x,     (sb4)sizeof(x),      (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[3], ociError, (ub4) 3, (dvoid*)&y,     (sb4)sizeof(y),      (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[4], ociError, (ub4) 4, (dvoid*)pmnam,  (sb4)sizeof(char)*8, (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[5], ociError, (ub4) 5, (dvoid*)&g,     (sb4)sizeof(g),      (ub2)SQLT_FLT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status==OCI_SUCCESS)status=OCIBindByPos(stmthp, &bndp[6], ociError, (ub4) 6, (dvoid*)&alpha, (sb4)sizeof(alpha),  (ub2)SQLT_FLT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALAddPMCalib: Error binding stmthp "); checkerr(ociError, status, erm); return -1; }
  
  // execute stmt
  status=OCIStmtExecute(ociHdbc, stmthp, ociError, (ub4)1, (ub4)0, (OCISnapshot*)NULL, (OCISnapshot*)NULL, OCI_DEFAULT);
  if(status!=OCI_SUCCESS){sprintf(erm,"ECALAddPMCalib: Error executing stmthp "); checkerr(ociError, status, erm); return -1; }
  status=OCIStmtRelease(stmthp, ociError, NULL, 0, (ub4)OCI_DEFAULT);
  
  sprintf(erm,"ECALAddPMCalib: success ");
  return 0;
}

EXTERN_CALODB int ECALgetDACforGain(double gain, char* type, int* x, int* y, int* DAC, double* hv, char* erm){
  if(!m_connected){strcpy(erm, "ECALgetDACforGain: not connected");return(-1);}
  erm[0]=(char)0;
  
  sword status;
  OCIStmt* stmtp;
  char tip;
  int ix,iy, dac;
  double g0,alpha,hvphys;
  OCIBind* bndp[30];  for(int i=0; i<30; ++i) bndp[i] = (OCIBind*) 0;
  OCIDefine* defnp[30]; for(int i=0; i<30; ++i) defnp[i] = (OCIDefine *) 0;
  bool allCells=false;
  if(!ECALCellExists(type[0],x[0],y[0]))allCells=true;
  
  int ich=0;
  int nfound=0;
  if(allCells){
    char sqlstAll[]="SELECT ch.chan_type, ch.chan_x, ch.chan_y, ch.dac_chan, cal.g0, cal.alpha, d.hv_phys FROM ecal_chan_otb ch, TABLE(ch.pm_calibs) cal, TABLE(ch.dac_settings) d WHERE d.d_set=(SELECT MAX(d1.d_set) FROM TABLE(ch.dac_settings) d1) AND cal.start_v=(SELECT MAX(cal1.start_v) FROM TABLE(ch.pm_calibs) cal1) AND cal.g0>0 AND cal.alpha>0 ORDER BY ch.dac_chan";
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmtp, ociError, (text*) sqlstAll,(ub4) strlen(sqlstAll),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALgetDACforGain: Error preparing stmtp "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[1], ociError, (ub4) 1, (dvoid*)&tip,   (sb4)sizeof(char),  SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[2], ociError, (ub4) 2, (dvoid*)&ix,    (sb4)sizeof(ix),    SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[3], ociError, (ub4) 3, (dvoid*)&iy,    (sb4)sizeof(iy),    SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[4], ociError, (ub4) 4, (dvoid*)&dac,   (sb4)sizeof(dac),   SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[5], ociError, (ub4) 5, (dvoid*)&g0,    (sb4)sizeof(g0),    SQLT_FLT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[6], ociError, (ub4) 6, (dvoid*)&alpha, (sb4)sizeof(alpha), SQLT_FLT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[7], ociError, (ub4) 7, (dvoid*)&hvphys,(sb4)sizeof(hvphys),SQLT_FLT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALgetDACforGain: Error binding "); checkerr(ociError, status, erm); return -1; }
    
    // execute stmtp
    status=OCIStmtExecute(ociHdbc, stmtp, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALgetDACforGain: Error executing stmtp "); checkerr(ociError, status, erm); return -1; }
    
    while( (status=OCIStmtFetch2(stmtp, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
      type[nfound]=tip;
      x[nfound]=ix;
      y[nfound]=iy;
      DAC[nfound]=dac;
      hv[nfound]=pow(gain/g0,1./alpha);
      if(hvphys<=0.001)hv[nfound]=0;
      ++nfound;
    }
    status=OCIStmtRelease(stmtp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }else{
    char sqlst[]="SELECT ch.dac_chan, cal.g0, cal.alpha, d.hv_phys FROM ecal_chan_otb ch, TABLE(ch.pm_calibs) cal, TABLE(ch.dac_settings) d WHERE ch.chan_type=:1 AND ch.chan_x=:2 AND ch.chan_y=:3 AND d.d_set=(SELECT MAX(d1.d_set) FROM TABLE(ch.dac_settings) d1) AND cal.start_v=(SELECT MAX(cal1.start_v) FROM TABLE(ch.pm_calibs) cal1) AND cal.g0>0 AND cal.alpha>0 ";
    status=OCIStmtPrepare2(ociHdbc,(OCIStmt**)&stmtp, ociError, (text*) sqlst,(ub4) strlen(sqlst),NULL,0,(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALgetDACforGain: Error preparing stmtp "); checkerr(ociError, status, erm); return -1; }
    
    if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[1], ociError, (ub4) 1, (dvoid*)&tip, (sb4)sizeof(tip), (ub2)SQLT_CHR, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[2], ociError, (ub4) 2, (dvoid*)&ix,  (sb4)sizeof(ix),  (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIBindByPos(stmtp, &bndp[3], ociError, (ub4) 3, (dvoid*)&iy,  (sb4)sizeof(iy),  (ub2)SQLT_INT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)0, (ub4*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[1], ociError, (ub4) 1, (dvoid*)&dac,   (sb4)sizeof(dac),   SQLT_INT, (dvoid*) 0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[2], ociError, (ub4) 2, (dvoid*)&g0,    (sb4)sizeof(g0),    SQLT_FLT, (dvoid*) 0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[3], ociError, (ub4) 3, (dvoid*)&alpha, (sb4)sizeof(alpha), SQLT_FLT, (dvoid*) 0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status==OCI_SUCCESS)status=OCIDefineByPos(stmtp, &defnp[4], ociError, (ub4) 4, (dvoid*)&hvphys,(sb4)sizeof(hvphys),SQLT_FLT, (dvoid*)0, (ub2*)0, (ub2*)0, (ub4)OCI_DEFAULT);
    if(status!=OCI_SUCCESS){sprintf(erm,"ECALgetDACforGain: Error binding/defining "); checkerr(ociError, status, erm); return -1; }
    
    while(ECALCellExists(type[ich],x[ich],y[ich])){
      // execute stmtp
      tip=type[ich];
      ix=x[ich];
      iy=y[ich];
      status=OCIStmtExecute(ociHdbc, stmtp, ociError, (ub4) 0, (ub4) 0, (OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);
      if(status!=OCI_SUCCESS){sprintf(erm,"ECALgetDACforGain: Error executing stmtp "); checkerr(ociError, status, erm); return -1; }
      
      DAC[ich]=0;
      hv[ich]=0;
      if( (status=OCIStmtFetch2(stmtp, ociError, (ub4)1, OCI_FETCH_NEXT, (sb4)0, OCI_DEFAULT))==OCI_SUCCESS){
	DAC[ich]=dac;
	hv[ich]=pow(gain/g0,1./alpha);
	if(hvphys<=0.001)hv[nfound]=0;
	++nfound;
      }else{
	checkerr(ociError, status, erm); 
      }
      ++ich;
    }
    status=OCIStmtRelease(stmtp, ociError, NULL, 0, (ub4) OCI_DEFAULT);
  }
  
  sprintf(erm,"ECALgetDACforGain: %d cells found ", nfound);
  return nfound;
}








// Utility routines m--------------------------------------------

void checkerr(OCIError *errhp, sword status, char* erMess){
  text errbuf[512];
  ub4 errcode;
  
  char* smsg = &erMess[strlen(erMess)];
  
  switch (status)
    {
    case OCI_SUCCESS:
      break;
    case OCI_SUCCESS_WITH_INFO:
      sprintf(smsg, " - OCI_SUCCESS_WITH_INFO");
      break;
    case OCI_NEED_DATA:
      sprintf(smsg, " - OCI_NEED_DATA");
      break;
    case OCI_NO_DATA:
      sprintf(smsg, " - OCI_NO_DATA");
      break;
    case OCI_ERROR:
      OCIErrorGet ((dvoid *) errhp, (ub4) 1, (text *) NULL, (sb4 *)&errcode,
		   errbuf, (ub4) sizeof(errbuf), (ub4) OCI_HTYPE_ERROR);
      sprintf(smsg, " - %s", errbuf);
      break;
    case OCI_INVALID_HANDLE:
      sprintf(smsg, " - OCI_INVALID_HANDLE");
      break;
    case OCI_STILL_EXECUTING:
      sprintf(smsg, " - OCI_STILL_EXECUTE");
      break;
    case OCI_CONTINUE:
      sprintf(smsg, " - OCI_CONTINUE");
      break;
    default:
      break;
    }
}

//  Dump the info of any ADT                                                 *
void dump_adt(OCIEnv *envhp, OCIError *errhp, OCISvcCtx *svchp, OCIType *tdo, dvoid *obj, dvoid *null_obj){
  ub2             count, pos;
  ub4             str_len;
  OCITypeCode           typecode;
  OCIInd          attr_null_status;
  dvoid          *attr_null_struct;
  dvoid          *attr_value;
  OCIType         *attr_tdo, *element_type;
  dvoid          *object;
  dvoid          *null_object;
  OCIType         *object_tdo;
  ub1             status;
  OCIRef         *type_ref;
  dvoid          *element = (dvoid *) 0, *null_element = (dvoid *) 0;
  boolean        exist, eoc;
  sb4            index;
  OCIDescribe    *dschp = (OCIDescribe *) 0, *dschp1 = (OCIDescribe *) 0;
  text           *namep, *typenamep;
  dvoid          *list_attr;
  OCIIter        *itr = (OCIIter *) 0;
  dvoid          *parmp = (dvoid *) 0,
    *parmdp = (dvoid *) 0,
    *parmp1 = (dvoid *) 0,
    *parmp2 = (dvoid *) 0;
  OCIRef         *elem_ref = (OCIRef *) 0;
  
  char erm[1024];
  
  checkerr(errhp, OCIHandleAlloc((dvoid *) envhp, (dvoid **) &dschp,
				 (ub4) OCI_HTYPE_DESCRIBE,
				 (size_t) 0, (dvoid **) 0), erm);
  
  checkerr(errhp, OCIDescribeAny(svchp, errhp, (dvoid *) tdo,
				 (ub4) 0, OCI_OTYPE_PTR, (ub1)1,
				 (ub1) OCI_PTYPE_TYPE, dschp), erm);
  
  checkerr(errhp, OCIAttrGet((dvoid *) dschp, (ub4) OCI_HTYPE_DESCRIBE,
			     (dvoid *)&parmp, (ub4 *)0, (ub4)OCI_ATTR_PARAM, errhp), erm);
  
  checkerr(errhp, OCIAttrGet((dvoid*) parmp,(ub4) OCI_DTYPE_PARAM,
			     (dvoid*) &typenamep, (ub4 *) &str_len,
			     (ub4) OCI_ATTR_NAME, (OCIError *) errhp), erm);
  typenamep[str_len] = '\0';
  
  printf("starting displaying instance of type '%s'\n", typenamep);
  
  /* loop through all attributes in the type */
  checkerr(errhp, OCIAttrGet((dvoid*) parmp, (ub4) OCI_DTYPE_PARAM,
			     (dvoid*) &count, (ub4 *) 0,
			     (ub4) OCI_ATTR_NUM_TYPE_ATTRS, (OCIError *) errhp), erm);
  
  checkerr(errhp, OCIAttrGet((dvoid *) parmp, (ub4) OCI_DTYPE_PARAM,
			     (dvoid *)&list_attr, (ub4 *)0,
			     (ub4)OCI_ATTR_LIST_TYPE_ATTRS, (OCIError *)errhp), erm);
  
  /* loop through all attributes in the type */
  for (pos = 1; pos <= count; pos++)
    {
      
      checkerr(errhp, OCIParamGet((dvoid *) list_attr,
				  (ub4) OCI_DTYPE_PARAM, errhp,
				  (dvoid **)&parmdp, (ub4) pos), erm);
      
      checkerr(errhp, OCIAttrGet((dvoid*) parmdp, (ub4) OCI_DTYPE_PARAM,
				 (dvoid*) &namep, (ub4 *) &str_len,
				 (ub4) OCI_ATTR_NAME, (OCIError *) errhp), erm);
      namep[str_len] = '\0';
      
      /* get the attribute */
      if (OCIObjectGetAttr(envhp, errhp, obj, null_obj, tdo,
			   (CONST oratext **)&namep, &str_len, 1,
			   (ub4 *)0, 0, &attr_null_status, &attr_null_struct,
			   &attr_value, &attr_tdo) != OCI_SUCCESS)
	(void) printf("BUG -- OCIObjectGetAttr, expect OCI_SUCCESS.\n");
      
      /* get the type code of the attribute */
      checkerr(errhp, OCIAttrGet((dvoid*) parmdp, (ub4) OCI_DTYPE_PARAM,
				 (dvoid*) &typecode, (ub4 *) 0,
				 (ub4) OCI_ATTR_TYPECODE,
				 (OCIError *) errhp), erm);

      /* support only fixed length string, ref and embedded ADT */
      switch (typecode)
	{
	case OCI_TYPECODE_OBJECT :                            /* embedded ADT */
	  printf("attribute %s is an embedded ADT. Display instance ....\n",
		 namep);
	  /* recursive call to dump nested ADT data */
	  dump_adt(envhp, errhp, svchp, attr_tdo, attr_value,
		   attr_null_struct);
	  break;
	case OCI_TYPECODE_REF :                               /* embedded ADT */
	  printf("attribute %s is a ref. Pin and display instance ...\n",
		 namep);
	  /* pin the object */
	  if (OCIObjectPin(envhp, errhp, *(OCIRef **)attr_value,
			   (OCIComplexObject *)0, OCI_PIN_ANY,
			   OCI_DURATION_SESSION, OCI_LOCK_NONE,
			   (dvoid **)&object) != OCI_SUCCESS)
	    (void) printf("BUG -- OCIObjectPin, expect OCI_SUCCESS.\n");
	  /* allocate the ref */
	  if (( status = OCIObjectNew(envhp, errhp, svchp,
				      OCI_TYPECODE_REF, (OCIType *)0,
				      (dvoid *)0, OCI_DURATION_DEFAULT, TRUE,
				      (dvoid **) &type_ref)) != OCI_SUCCESS)
	    (void) printf("BUG -- OCIObjectNew, expect OCI_SUCCESS.\n");
	  /* get the ref of the type from the object */
	  if (( status = OCIObjectGetTypeRef(envhp, errhp, object, type_ref))
	      != OCI_SUCCESS)
	    (void) printf("BUG -- ORIOGTR, expect OCI_SUCCESS.\n");
	  /* pin the type ref to get the type object */
	  if (OCIObjectPin(envhp, errhp, type_ref,  (OCIComplexObject *)0,
			   OCI_PIN_ANY, OCI_DURATION_SESSION, OCI_LOCK_NONE,
			   (dvoid **) &object_tdo) != OCI_SUCCESS)
	    (void) printf("BUG -- OCIObjectPin, expect OCI_SUCCESS.\n");
	  /* get null struct of the object */
	  if (( status = OCIObjectGetInd(envhp, errhp, object,
					 &null_object)) != OCI_SUCCESS)
	    (void) printf("BUG -- ORIOGNS, expect OCI_SUCCESS.\n");
	  /* call the function recursively to dump the pinned object */
	  dump_adt(envhp, errhp, svchp, object_tdo, object,
		   null_object);
	case OCI_TYPECODE_NAMEDCOLLECTION :
	  checkerr(errhp, OCIHandleAlloc((dvoid *) envhp, (dvoid **) &dschp1,
					 (ub4) OCI_HTYPE_DESCRIBE,
					 (size_t) 0, (dvoid **) 0), erm);
	  
	  checkerr(errhp, OCIDescribeAny(svchp, errhp, (dvoid *) attr_tdo,
					 (ub4) 0, OCI_OTYPE_PTR, (ub1)1,
					 (ub1) OCI_PTYPE_TYPE, dschp1), erm);
	  
	  checkerr(errhp, OCIAttrGet((dvoid *) dschp1,
				     (ub4) OCI_HTYPE_DESCRIBE,
				     (dvoid *)&parmp1, (ub4 *)0, (ub4)OCI_ATTR_PARAM, errhp), erm);
	  
	  /* get the collection type code of the attribute */
	  checkerr(errhp, OCIAttrGet((dvoid*) parmp1, (ub4) OCI_DTYPE_PARAM,
				     (dvoid*) &typecode, (ub4 *) 0,
				     (ub4) OCI_ATTR_COLLECTION_TYPECODE,
				     (OCIError *) errhp), erm);
	  switch (typecode)
	    {
	    case OCI_TYPECODE_VARRAY :                    /* variable array */
	      (void) printf
		("\n---> Dump the table from the top to the bottom.\n");
	      checkerr(errhp, OCIAttrGet((dvoid*) parmp1,
					 (ub4) OCI_DTYPE_PARAM,
					 (dvoid*) &parmp2, (ub4 *) 0,
					 (ub4) OCI_ATTR_COLLECTION_ELEMENT,
					 (OCIError *) errhp), erm);
	      checkerr(errhp, OCIAttrGet((dvoid*) parmp2,
					 (ub4) OCI_DTYPE_PARAM,
					 (dvoid*) &elem_ref, (ub4 *) 0,
					 (ub4) OCI_ATTR_REF_TDO,
					 (OCIError *) errhp), erm);
	      checkerr(errhp, OCITypeByRef(envhp, errhp, elem_ref,
					   OCI_PIN_DEFAULT, (OCITypeGetOpt)0, &element_type), erm);
	      /* initialize the iterator */
	      checkerr(errhp, OCIIterCreate(envhp, errhp,
					    (CONST OCIColl*) attr_value, &itr), erm);
	      /* loop through the iterator */
	      for(eoc = FALSE;!OCIIterNext(envhp, errhp, itr,
					   (dvoid **) &element,
					   (dvoid **)&null_element, &eoc) && !eoc;)
		{
		  /* if type is named type, call the same function recursively
		   */
		  if (typecode == OCI_TYPECODE_OBJECT)
		    dump_adt(envhp, errhp, svchp, element_type, element,
			     null_element);
		  else  /* else, display the scaler type attribute */
		    display_attr_val(envhp, errhp, namep, typecode, element);
		}
	      break;
	      
	    case OCI_TYPECODE_TABLE :                       /* nested table */
	      (void) printf
		("\n---> Dump the table from the top to the bottom.\n");
	      /* go to the first element and print out the index */
	      checkerr(errhp, OCIAttrGet((dvoid*) parmp1,
					 (ub4) OCI_DTYPE_PARAM,
					 (dvoid*) &parmp2, (ub4 *) 0,
					 (ub4) OCI_ATTR_COLLECTION_ELEMENT,
					 (OCIError *) errhp), erm);
	      checkerr(errhp, OCIAttrGet((dvoid*) parmp2,
					 (ub4) OCI_DTYPE_PARAM,
					 (dvoid*) &elem_ref, (ub4 *) 0,
					 (ub4) OCI_ATTR_REF_TDO,
					 (OCIError *) errhp), erm);
	      checkerr(errhp, OCITypeByRef(envhp, errhp, elem_ref,
					   OCI_DURATION_SESSION,
					   OCI_TYPEGET_HEADER, &element_type), erm);
	      attr_value = *(dvoid **)attr_value;
	      /* move to the first element in the nested table */
	      checkerr(errhp, OCITableFirst(envhp, errhp,
					    (CONST OCITable*) attr_value, &index), erm);
	      (void) printf
		("     The index of the first element is : %d.\n", index);
	      /* print out the element */
	      checkerr(errhp, OCICollGetElem(envhp, errhp,
					     (CONST OCIColl *) attr_value, index,
					     &exist, (dvoid **) &element,
					     (dvoid **) &null_element), erm);
	      /* if it is named type, recursively call the same function */
	      checkerr(errhp, OCIAttrGet((dvoid*) parmp2,
					 (ub4) OCI_DTYPE_PARAM,
					 (dvoid*) &typecode, (ub4 *) 0,
					 (ub4) OCI_ATTR_TYPECODE,
					 (OCIError *) errhp), erm);
	      if (typecode == OCI_TYPECODE_OBJECT)
		dump_adt(envhp, errhp, svchp, element_type,
			 (dvoid *)element, (dvoid *)null_element);
	      else
		display_attr_val(envhp, errhp, namep, typecode, element);
	      
	      for(;!OCITableNext(envhp, errhp, index,
				 (CONST OCITable *) attr_value,
				 &index, &exist) && exist;)
		{
                  checkerr(errhp, OCICollGetElem(envhp, errhp,
						 (CONST OCIColl *) attr_value, index,
						 &exist, (dvoid **) &element,
						 (dvoid **) &null_element), erm);
                  if (typecode == OCI_TYPECODE_OBJECT)
		    dump_adt(envhp, errhp, svchp, element_type,
			     (dvoid *)element, (dvoid *)null_element);
                  else
		    display_attr_val(envhp, errhp, namep, typecode, element);
		}
	      break;
	    default:
	      break;
	    }
	  checkerr(errhp, OCIHandleFree((dvoid *) dschp1,
                                        (ub4) OCI_HTYPE_DESCRIBE), erm);
	  break;
	default:   /* scaler type, display the attribute value */
	  if (attr_null_status == OCI_IND_NOTNULL)
	    {
              display_attr_val(envhp, errhp, namep, typecode, attr_value);
	    }
	  else
	    printf("attr %s is null\n", namep);
	  break;
	}
    }
  
  printf("finishing displaying instance of type '%s'\n", typenamep);
  checkerr(errhp, OCIHandleFree((dvoid *) dschp, (ub4) OCI_HTYPE_DESCRIBE), erm);
}


/****************************************************************************
 *  Display attribute value of an ADT                                        *
 ****************************************************************************/
void display_attr_val(OCIEnv *envhp, OCIError *errhp, text *names, OCITypeCode typecode, dvoid *attr_value){
  text           str_buf[200];
  double         dnum;
  ub4            str_len;
  OCIRaw         *raw = (OCIRaw *) 0;
  OCIString        *vs = (OCIString *) 0;
  ub1            *temp = (ub1 *)0;
  ub4            rawsize = 0;
  ub4            i = 0;
  
  /* display the data based on the type code */
  switch (typecode)
    {
    case OCI_TYPECODE_DATE :                         /* fixed length string */
      str_len = 200;
      (void) OCIDateToText(errhp, (CONST OCIDate *) attr_value,
			   (CONST text*) "Month dd, SYYYY, HH:MI A.M.",
			   (ub1) 27, (CONST text*) "American", (ub4) 8,
			   (ub4 *)&str_len, str_buf);
      str_buf[str_len+1] = '\0';
      (void) printf("attr %s = %s\n", names, (text *) str_buf);
      break;
    case OCI_TYPECODE_RAW :                                          /* RAW */
      raw = *(OCIRaw **) attr_value;
      temp = OCIRawPtr(envhp, raw);
      rawsize = OCIRawSize (envhp, raw);
      (void) printf("attr %s = ", names);
      for (i=0; i < rawsize; i++)
	{
	  (void) printf("0x%x ", temp[i]);
	}
      (void) printf("\n");
      break;
    case OCI_TYPECODE_CHAR :                         /* fixed length string */
    case OCI_TYPECODE_VARCHAR :                                 /* varchar  */
    case OCI_TYPECODE_VARCHAR2 :                                /* varchar2 */
      vs = *(OCIString **) attr_value;
      (void) printf("attr %s = %s\n",
		    names, (text *) OCIStringPtr(envhp, vs));
      break;
    case OCI_TYPECODE_SIGNED8 :                              /* BYTE - sb1  */
      (void) printf("attr %s = %d\n", names, *(sb1 *) attr_value);
      break;
    case OCI_TYPECODE_UNSIGNED8 :                   /* UNSIGNED BYTE - ub1  */
      (void) printf("attr %s = %d\n", names, *(ub1 *) attr_value);
      break;
    case OCI_TYPECODE_OCTET :                                       /* OCT  */
      (void) printf("attr %s = %d\n", names, *(ub1 *) attr_value);
      break;
    case OCI_TYPECODE_UNSIGNED16 :                       /* UNSIGNED SHORT  */
    case OCI_TYPECODE_UNSIGNED32 :                        /* UNSIGNED LONG  */
    case OCI_TYPECODE_REAL :                                     /* REAL    */
    case OCI_TYPECODE_DOUBLE :                                   /* DOUBLE  */
    case OCI_TYPECODE_INTEGER :                                     /* INT  */
    case OCI_TYPECODE_SIGNED16 :                                  /* SHORT  */
    case OCI_TYPECODE_SIGNED32 :                                   /* LONG  */
    case OCI_TYPECODE_DECIMAL :                                 /* DECIMAL  */
    case OCI_TYPECODE_FLOAT :                                   /* FLOAT    */
    case OCI_TYPECODE_NUMBER :                                  /* NUMBER   */
    case OCI_TYPECODE_SMALLINT :                                /* SMALLINT */
      (void) OCINumberToReal(errhp, (CONST OCINumber *) attr_value,
			     (uword) sizeof(dnum), (dvoid *) &dnum);
      (void) printf("attr %s = %f\n", names, dnum);
      break;
    default:
      (void) printf("attr %s - typecode %d\n", names, typecode);
      break;
    }
}



sword calotypo(OCIEnv *env, OCIError *err)
{
  sword status = OCITypeVTInit(env, err);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "HCAL_CHAN_TYP", 13,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "HCAL_PMCALIB_TYP", 16,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "HCAL_CSCALIB_TYP", 16,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "TILE_CURR_TYP", 13,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "ECAL_CHAN_TYP", 13,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "ECAL_PMCALIB_TYP", 16,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "PMGAIN_PNT_TYP", 14,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "HCAL_LEDPIN_TYP", 15,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "ECAL_LED_TYP", 12,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "ECAL_PIN_TYP", 12,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "HCAL_MODULE_TYP", 15,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "HCAL_LEDPINREF_TYP", 18,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "LEDDAC_SETTING_TYP", 18,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "LEDTSB_SETTING_TYP", 18,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "ECAL_LEDPIN_CALIB_TYP", 21,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "LEDPIN_CALIB_TYP", 16,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "LEDPIN_PNT_TYP", 14,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "HCAL_CSCALIB_LIST", 17,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "HCAL_PMCALIB_LIST", 17,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "DAC_SETTING_LIST", 16,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "ADC_SETTING_LIST", 16,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "PMGAIN_PNT_LIST", 15,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "TILE_CURR_LIST", 14,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "ECAL_PMCALIB_LIST", 17,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "DAC_SETTING_TYP", 15,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "ADC_SETTING_TYP", 15,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "LEDPIN_CALIB_LIST", 17,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "LEDDAC_SETTING_LIST", 19,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "LEDTSB_SETTING_LIST", 19,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "ECAL_LEDPIN_CALIB_LIST", 22,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "HCAL_LEDPINREF_LIST", 19,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "LEDPIN_PNT_LIST", 15,
			     (unsigned char *) "$8.0", 4);
  return status;
}

sword ecalmoduleitemo(OCIEnv *env, OCIError *err)
{
  sword status = OCITypeVTInit(env, err);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "BOX_MODULE_ITEM", 15,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "BOX_CHAN_ITEM", 13,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "PMT_ITEM", 8,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "PMT_RES_ITEM", 12,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "PMT_VAL", 7,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "CW_ITEM", 7,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "BOX_CHAN_LST", 12,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "PMT_LST_RES", 11,
			     (unsigned char *) "$8.0", 4);
  if (status == OCI_SUCCESS)
    status = OCITypeVTInsert(env, err,
			     (unsigned char *) "CALODB_ADMIN", 12,
			     (unsigned char *) "PMT_VAL_TAB", 11,
			     (unsigned char *) "$8.0", 4);
  return status;
}
