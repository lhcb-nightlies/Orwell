#pragma once
#define EXTERN_CALODB 

extern double v0_ecal; 
extern double v1_ecal;

extern double v0_hcal;
extern double v1_hcal;

extern double stepx_ecal;
extern double stepy_ecal;

extern double stepx_hcal;
extern double stepy_hcal;


EXTERN_CALODB int LHCBCALOdbConnect(char* erm);
EXTERN_CALODB int LHCBCALOdbConnectUser(const char* usr, const char* server, const char* pwd, char* erm);
EXTERN_CALODB int LHCBCALOdbClose(char* erm);
EXTERN_CALODB int isLHCBCALOdbConnected();
EXTERN_CALODB int LHCBCALOCommit(char* erm);
EXTERN_CALODB int LHCBCALORollBack(char* erm);

EXTERN_CALODB int ECALGetChanDAC(int opt, char* type, int* x, int* y, int* dac, int* cwpow, double* HV, double* HVstdb, char* erm);
EXTERN_CALODB int ECALGetChanDAC_date(const char* thedate, int opt, char* type, int* x, int* y, int* dac, int* cwpow, double* HV, double* HVstdb, char* erm);
EXTERN_CALODB int HCALGetChanDAC(int opt, char* type, int* x, int* y, int* dac, int* cwpow, double* HV, double* HVstdb, char* erm);
EXTERN_CALODB int HCALGetChanDAC_date(const char* thedate, int opt, char* type, int* x, int* y, int* dac, int* cwpow, double* HV, double* HVstdb, char* erm);
EXTERN_CALODB int ECALGetChanADC(int opt, char* type, int* x, int* y, int* adc, double* delay, char* erm);
EXTERN_CALODB int HCALGetChanADC(int opt, char* type, int* x, int* y, int* adc, double* delay, char* erm);

EXTERN_CALODB int ECALGetPM(char* type, int* x, int* y, char* PMname, int PMname_len, double* g0, double* alpha, char* erm);
EXTERN_CALODB int ECALGetPM_date(const char* thedate, char* type, int* x, int* y, char* PMname, int PMname_len, double* g0, double* alpha, char* erm);
EXTERN_CALODB int HCALGetPM(char* type, int* x, int* y, char* PMname, int PMname_len, double* g0, double* alpha, char* erm);
EXTERN_CALODB int HCALGetPM_date(const char* thedate, char* type, int* x, int* y, char* PMname, int PMname_len, double* g0, double* alpha, char* erm);
EXTERN_CALODB int ECALSetPM(char* type, int* x, int* y, char* PMname, int PMname_len, double* g0, double* alpha, char* erm);
EXTERN_CALODB int HCALSetPM(char* type, int* x, int* y, char* PMname, int PMname_len, double* g0, double* alpha, char* erm);

EXTERN_CALODB int ECALGetCellFiberLengths(char* type, int* x, int* y, double* LED_len, double* PIN_len, char* erm);
EXTERN_CALODB int ECALSetCellFiberLengths(char* type, int* x, int* y, double* LED_len, char* erm);

EXTERN_CALODB int ECALStoreDACSettings(char* type, int* x, int* y, double* HVphys, double* HVstdb, char* erm);
EXTERN_CALODB int HCALStoreDACSettings(char* type, int* x, int* y, double* HVphys, double* HVstdb, char* erm);
EXTERN_CALODB int ECALStoreADCSettings(char* type, int* x, int* y, double* delay, char* erm);
EXTERN_CALODB int HCALStoreADCSettings(char* type, int* x, int* y, double* delay, char* erm);

EXTERN_CALODB int ECALClearDACUpds(char* erm);
EXTERN_CALODB int HCALClearDACUpds(char* erm);
EXTERN_CALODB int ECALClearADCUpds(char* erm);
EXTERN_CALODB int HCALClearADCUpds(char* erm);

EXTERN_CALODB int ECALSetDACApplied(char* erm);
EXTERN_CALODB int HCALSetDACApplied(char* erm);
EXTERN_CALODB int ECALSetADCApplied(char* erm);
EXTERN_CALODB int HCALSetADCApplied(char* erm);

EXTERN_CALODB int ECALSetDACUnapplied(int* d1, int* d2, char* erm);
EXTERN_CALODB int HCALSetDACUnapplied(int* d1, int* d2, char* erm);
EXTERN_CALODB int ECALSetADCUnapplied(int* d1, int* d2, char* erm);
EXTERN_CALODB int HCALSetADCUnapplied(int* d1, int* d2, char* erm);

EXTERN_CALODB int ECALConnByCell(char  type, int  x, int  y, int &dac, int &cwpow, int &adc, char* PMname, char* erm);
EXTERN_CALODB int ECALConnByPM  (char &type, int &x, int &y, int &dac, int &cwpow, int &adc, char* PMname, char* erm);
EXTERN_CALODB int ECALConnByADC (char &type, int &x, int &y, int &dac, int &cwpow, int  adc, char* PMname, char* erm);
EXTERN_CALODB int ECALConnByDAC (char &type, int &x, int &y, int  dac, int &cwpow, int &adc, char* PMname, char* erm);
EXTERN_CALODB int ECALGetCell(int Search, char &type, int &x, int &y, int &dac, int &cwpow, int &adc, char* PMname, char* erm);
EXTERN_CALODB int ECALCellsAtPOW(char *type, int *x, int *y, int *dac, int  cwpow, int *adc, char* PMname, int PMname_len, char* erm);

EXTERN_CALODB int HCALConnByCell (char  type, int  x, int  y, int &mod, int &mch, int &dac, int &cwpow, int &adc, char* PMname, char* erm);
EXTERN_CALODB int HCALConnByModCh(char &type, int &x, int &y, int  mod, int  mch, int &dac, int &cwpow, int &adc, char* PMname, char* erm);
EXTERN_CALODB int HCALConnByPM   (char &type, int &x, int &y, int &mod, int &mch, int &dac, int &cwpow, int &adc, char* PMname, char* erm);
EXTERN_CALODB int HCALConnByADC  (char &type, int &x, int &y, int &mod, int &mch, int &dac, int &cwpow, int  adc, char* PMname, char* erm);
EXTERN_CALODB int HCALConnByDAC  (char &type, int &x, int &y, int &mod, int &mch, int  dac, int &cwpow, int &adc, char* PMname, char* erm);
EXTERN_CALODB int HCALGetCell(int Search, char &type, int &x, int &y, int &mod, int &mch, int &dac, int &cwpow, int &adc, char* PMname, char* erm);
EXTERN_CALODB int HCALCellsAtPOW (char *type, int *x, int *y, int *mod, int *mch, int *dac, int  cwpow, int *adc, char* PMname, int PMname_len, char* erm);
EXTERN_CALODB int HCALCellsAtPM_date (const char* thedate, char *type, int *x, int *y, const char* PMname, char* erm);

EXTERN_CALODB int ECALCheckUniqPM(char* PMname, int PMname_len, char *type1, int *x1, int *y1, char *type2, int *x2, int *y2, char* erm);
EXTERN_CALODB int HCALCheckUniqPM(char* PMname, int PMname_len, char *type1, int *x1, int *y1, char *type2, int *x2, int *y2, char* erm);

EXTERN_CALODB bool HCALCellExists(char typ, int x, int y);
EXTERN_CALODB bool ECALCellExists(char typ, int x, int y);
EXTERN_CALODB bool ECALCellActive(char typ, int x, int y);

// HCAL integrators
EXTERN_CALODB int HCALGetINTcalib(char  type, int  x, int  y, int* date, double* peds, double* slops, double* peds_cal, double* slops_cal, char* erm);
EXTERN_CALODB int HCALAddINTcalib(char  type, int  x, int  y,            double* peds, double* slops, double* peds_cal, double* slops_cal, char* erm);
EXTERN_CALODB int HCALSetINTcalib(char  type, int  x, int  y,            double* peds, double* slops, double* peds_cal, double* slops_cal, char* erm);

// HCAL Cs user identification
EXTERN_CALODB int HCALCsLogin(const char* unam, const char* pass, int& level, char* erm);

// LED/PIN part
EXTERN_CALODB int ECALGetLEDDAC(int opt, char* PINname, int PINname_len, int* LEDnum, int* DAC, double* uLED, char* erm);
EXTERN_CALODB int ECALGetLEDDAC_date(const char* thedate, int opt, char* PINname, int PINname_len, int* LEDnum, int* DAC, double* uLED, char* erm);
EXTERN_CALODB int ECALStoreLEDDAC(       char* PINname, int PINname_len, int* LEDnum,           double* uLED, char* erm);
EXTERN_CALODB int ECALClearLEDDACUpds(char* erm);
EXTERN_CALODB int ECALSetLEDDACApplied(char* erm);
EXTERN_CALODB int ECALSetLEDDACUnapplied(int* d1, int* d2, char* erm);

EXTERN_CALODB int ECALGetPINADC(int opt, char* PINname, int PINname_len, int* ADC, double* delay, char* erm);
EXTERN_CALODB int ECALGetPINADC_date(const char* thedate, int opt, char* PINname, int PINname_len, int* ADC, double* delay, char* erm);
EXTERN_CALODB int ECALStorePINADC(       char* PINname, int PINname_len,           double* delay, char* erm);
EXTERN_CALODB int ECALClearPINADCUpds(char* erm);
EXTERN_CALODB int ECALSetPINADCApplied(char* erm);
EXTERN_CALODB int ECALSetPINADCUnapplied(int* d1, int* d2, char* erm);

EXTERN_CALODB int ECALGetLEDTSB(int opt, char* PINname, int PINname_len, int* LEDnum, int* TSB, int* delay, char* tune, char* erm);
EXTERN_CALODB int ECALGetLEDTSB_date(const char* thedate, int opt, char* PINname, int PINname_len, int* LEDnum, int* TSB, int* delay, char* tune, char* erm);
EXTERN_CALODB int ECALStoreLEDTSB(       char* PINname, int PINname_len, int* LEDnum,           int* delay, char* tune, char* erm);
EXTERN_CALODB int ECALClearLEDTSBUpds(char* erm);
EXTERN_CALODB int ECALSetLEDTSBApplied(char* erm);
EXTERN_CALODB int ECALSetLEDTSBUnapplied(int* d1, int* d2, char* erm);

EXTERN_CALODB int ECALGetLEDChans(char* type, int* x, int* y, const char* PINname, const int  LEDnum, char* erm);
EXTERN_CALODB int ECALGetChanLED( char  type, int  x, int  y, char* PINname, int& LEDnum, char* erm);
EXTERN_CALODB int ECALGetChanLEDs(char* type, int* x, int* y, char* PINname, int PINname_len, int* LEDnum, char* erm);
EXTERN_CALODB int ECALGetLEDPIN(int Search, char* PINname, int& Iled, int& iDAC, int& iTSB, int& iADC, char* erm);

EXTERN_CALODB int ECALGetLEDFiberLengths(char* PINname, int PINname_len, int* LEDnum, double* LED_len, double* PIN_len, char* erm);
EXTERN_CALODB int ECALGetLEDFiberLength (char* PINname,                  int  LEDnum, double& LED_len, double& PIN_len, char* erm);
EXTERN_CALODB int ECALSetLEDFiberLength (char* PINname,                  int  LEDnum, double  LED_len, double  PIN_len, char* erm);


EXTERN_CALODB int HCALGetLEDDAC(int opt, int* modx, int* mody, int* DAC1, int* DAC2, double* uLED1, double* uLED2, char* erm);
EXTERN_CALODB int HCALGetLEDDAC_date(const char* thedate, int opt, int* modx, int* mody, int* DAC1, int* DAC2, double* uLED1, double* uLED2, char* erm);
EXTERN_CALODB int HCALStoreLEDDAC(       int* modx, int* mody,                       double* uLED1, double* uLED2, char* erm); // uLED<1 : do not change
EXTERN_CALODB int HCALStoreLED1DAC(      int* modx, int* mody,                       double* uLED1,                char* erm); 
EXTERN_CALODB int HCALStoreLED2DAC(      int* modx, int* mody,                                      double* uLED2, char* erm); 
EXTERN_CALODB int HCALClearLEDDACUpds(char* erm);
EXTERN_CALODB int HCALSetLEDDACApplied(char* erm);
EXTERN_CALODB int HCALSetLEDDACUnapplied(int* d1, int* d2, char* erm);

EXTERN_CALODB int HCALGetPINADC(int opt, int* modx, int* mody, int* ADC1, int* ADC2, double* delay1, double* delay2, char* erm);
EXTERN_CALODB int HCALGetPINADC_date(const char* thedate, int opt, int* modx, int* mody, int* ADC1, int* ADC2, double* delay1, double* delay2, char* erm);
EXTERN_CALODB int HCALStorePINADC(       int* modx, int* mody,                       double* delay1, double* delay2, char* erm); // delay<-90 : do not change
EXTERN_CALODB int HCALStorePIN1ADC(      int* modx, int* mody,                       double* delay1,                 char* erm); 
EXTERN_CALODB int HCALStorePIN2ADC(      int* modx, int* mody,                                       double* delay2, char* erm); 
EXTERN_CALODB int HCALClearPINADCUpds(char* erm);
EXTERN_CALODB int HCALSetPINADCApplied(char* erm);
EXTERN_CALODB int HCALSetPINADCUnapplied(int* d1, int* d2, char* erm);

EXTERN_CALODB int HCALGetLEDTSB(int opt, int* modx, int* mody, int* TSB1, int* TSB2, int* delay1, int* delay2, char* tune1, char* tune2, char* erm);
EXTERN_CALODB int HCALGetLEDTSB_date(const char* thedate, int opt, int* modx, int* mody, int* TSB1, int* TSB2, int* delay1, int* delay2, char* tune1, char* tune2, char* erm);
EXTERN_CALODB int HCALStoreLEDTSB(       int* modx, int* mody,                       int* delay1, int* delay2, char* tune1, char* tune2, char* erm);// delay<0 : do not change; tune[k] other than 0 and 1
EXTERN_CALODB int HCALStoreLED1TSB(      int* modx, int* mody,                       int* delay1,              char* tune1,              char* erm);
EXTERN_CALODB int HCALStoreLED2TSB(      int* modx, int* mody,                                    int* delay2,              char* tune2, char* erm);
EXTERN_CALODB int HCALClearLEDTSBUpds(char* erm);
EXTERN_CALODB int HCALSetLEDTSBApplied(char* erm);
EXTERN_CALODB int HCALSetLEDTSBUnapplied(int* d1, int* d2, char* erm);

EXTERN_CALODB int HCALGetModChans(char* type, int* x, int* y, int modx, int mody, char* erm);
EXTERN_CALODB int HCALGetLEDID(int modx, int mody, char* LED1_ID, char* LED2_ID, char* erm);
EXTERN_CALODB int HCALGetLEDIDs(int* modx, int* mody, char* LED1_ID, char* LED2_ID, int LEDnam_len, char* erm);

EXTERN_CALODB bool HCALdecodeLEDname(int& Modx, int& Mody, int& Iled, const char* LEDnam);
EXTERN_CALODB bool HCALmakeLEDname(int Modx, int Mody, int Iled, char* LEDnam);

EXTERN_CALODB int HCALGetLEDConn(const char* LEDname, int& iDAC, int& iTSB, int& iADC, char* erm);
EXTERN_CALODB int HCALGetLEDPIN(int Search, int& Modx, int& Mody, int& Iled, char* LEDnam, char* LEDID, int& idac, int& itsb, int& iadc, char* erm);

// editing connectivity
EXTERN_CALODB int ECALConnDefault(const char* act, const char* what, char* erm);
EXTERN_CALODB int ECALSetChanConn(char type, int x, int y, const char* what, const void* val, char* erm);
EXTERN_CALODB int ECALSetLEDConn(const char* pinnam, int lednum, const char* what, int val, char* erm);
EXTERN_CALODB int ECALSetPINConn(const char* pinnam, const char* what, int val, char* erm);

EXTERN_CALODB int HCALConnDefault(const char* act, const char* what, char* erm);
EXTERN_CALODB int HCALSetChanConn(char type, int x, int y, const char* what, int val, char* erm);
EXTERN_CALODB int HCALSetModuleConn(int modx, int mody, const char* what, int val, char* erm);

// HARDWARE part
EXTERN_CALODB int HW_hvboard_sn   (const int    ser, const char* act, const char* what, void* val, char* erm);
EXTERN_CALODB int HW_hvboard_role (const char* name, const char* act, const char* what, void* val, char* erm);
EXTERN_CALODB int HW_hvchan (const int sn, const  int chan, const char* act, double& a0, double& a1, char* erm);
EXTERN_CALODB int HW_hvchans(const int sn, const char* act, double* a0, double* a1, char* erm);

EXTERN_CALODB int HW_allfeb(int* crate, int* slot, int* sn, int* slave, char* erm);

EXTERN_CALODB int PM_getCW(const char* PMname, int& CW, char* erm);
EXTERN_CALODB int PM_getQeff(char* PMname, double& qeff, char* erm);

EXTERN_CALODB int PM_getRefCalib(const char* PMname, const char* calibsrc, double& g0, double& alpha, char* erm);
EXTERN_CALODB int PM_getRefCalibPnts(const char* PMname, const char* calibsrc, char* dess, int width, int nvalmax, double* vals, char* erm);
EXTERN_CALODB int PM_setRefCalib(const char* PMname, const char* calibSrc, double g0, double glpha, char* erm);
EXTERN_CALODB int PM_addRefCalibPnt(const char* PMname, const char* calibSrc, char* des, double val, char* erm);
EXTERN_CALODB int PM_listRefCalibs(const char* PMname, char* listsrc, int srcname_len, char* erm);

EXTERN_CALODB int HCALSetPMCalib(char tip, int x, int y, double g, double alpha, char* erm);
EXTERN_CALODB int HCALAddPMCalib(char tip, int x, int y, char* PMnam, double g, double alpha, char* erm);
EXTERN_CALODB int ECALSetPMCalib(char tip, int x, int y, double g, double alpha, char* erm);
EXTERN_CALODB int ECALAddPMCalib(char tip, int x, int y, char* PMnam, double g, double alpha, char* erm);

EXTERN_CALODB int ECALgetDACforGain(double gain, char* type, int* x, int* y, int* DAC, double* hv, char* erm);
EXTERN_CALODB int HCALgetDACforGain(double gain, char* type, int* x, int* y, int* DAC, double* hv, char* erm);

EXTERN_CALODB int HCALCsSetCalib  (char* date, char* comment, char* erm);
EXTERN_CALODB int HCALCsDeleteCalib  (char* date, char* erm);
EXTERN_CALODB int HCALCsListCalibs  (char* dat1, char* dat2, char* dates, char* comms, char* erm);
EXTERN_CALODB int HCALCsSetChan   (char* date, char type, int x, int y, double hv, double curr, double ped, double coef, double rms, char* erm);
EXTERN_CALODB int HCALCsDeleteChan(char* date, char type, int x, int y, char* erm);
EXTERN_CALODB int HCALCsSetRow(char* date, char type, int x, int y, int ir, int nt, double gam, double sig, double lam, double x0, double del, double chi, double v, double t1, double t2, char* erm);
EXTERN_CALODB int HCALCsSetTile(char* date, char type, int x, int y, int ir, int it, double curr, char* erm);
EXTERN_CALODB int HCALCsGetChan(char* date, char type, int x, int y, double& hv, double& curr, double& ped, double& coef, double& rms, char* erm);
EXTERN_CALODB int HCALCsGetRow(char* date, char type, int x, int y, int ir, int& nt, double& gam, double& sig, double& lam, double& xx0, double& del, double& chi, double& v, double& t1, double& t2, char* erm);
EXTERN_CALODB int HCALCsGetRows(char* date, char type, int x, int y, int* nt, double* gam, double* sig, double* lam, double* xx0, double* del, double* chi, double* v, double* t1, double* t2, char* erm);
EXTERN_CALODB int HCALCsGetTile(char* date, char type, int x, int y, int ir, int it, double& cur, char* erm);
EXTERN_CALODB int HCALCsGetTiles(char* date, char type, int x, int y, double cur[6][13], char* erm);
