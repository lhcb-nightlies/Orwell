// $Id: CaloEFlowAnalysis.cpp,v 1.6 2009/11/30 18:20:34 odescham Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 
#include "GaudiUtils/Aida2ROOT.h"

// from CaloDet
#include "Kernel/CaloCellID.h"
// local
#include "CaloEFlowAnalysis.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloEFlowAnalysis
//
// 2009-04-11 : Aurelien Martens
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( CaloEFlowAnalysis )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloEFlowAnalysis::CaloEFlowAnalysis( const std::string& name,
                                      ISvcLocator* pSvcLocator)
  : AnalysisTask ( name , pSvcLocator ), 
    m_htemporary ( 0 )
{
  declareProperty("Energy"          , m_energy       = false); //default is transverse E
  declareProperty("Normalized"      , m_normalized   = false); //default is not normalized EFlow (i.e. normalized by Nevents)
  declareProperty("Methods"         , m_methods ); //Symmetric and Neighbours and Mean
  declareProperty("KernelType"      , m_kernelType = "Flat"); //Kernel for the symmetric method
  declareProperty("KernelDimension" , m_kernelDimension = 3 ); //Kernel dimension for the symmetric method
  declareProperty("DiagonalExtrapolation" , m_diagonal = false ); //flag for diagonal extrapolation
  declareProperty("TopBottom" , m_topbottom = true ); //assume top/bottom symmetry for symmetric method
  declareProperty("LeftRight" , m_leftright = true ); //assume left/right symmetry for symmetric method
  declareProperty("DetectorName"    , m_detectorName );
  declareProperty("Bin1D"           , m_bin1d        = 12288); // 1D histo binning (default no pin : 12288 | full 14bits)
  declareProperty("splitAreas"      , m_split=true);
  declareProperty("oneDimension"    , m_1D = false);
  declareProperty("listOfAreas"     , m_areas); // list of areas to be split
  declareProperty("doubtfulThresholdUp", m_doubtfulThresholdUp); //threshold for doubtful channels detections
  declareProperty("doubtfulThresholdDown", m_doubtfulThresholdDown); //threshold for doubtful channels detections

  debug() << " properties declared " << endmsg;
  
  // set default detectorName
  debug() << " setting default detector name " << endmsg;
  int index = name.find_last_of(".") +1 ; // return 0 if '.' not found --> OK !!
  m_detectorName = name.substr( index, 4 ); 
  if ( name.substr(index,3) == "Prs" ) m_detectorName = "Prs";
  if ( name.substr(index,3) == "Spd" ) m_detectorName = "Spd";
  m_name = name;
  m_caloID = CaloCellCode::CaloNumFromName(m_detectorName);

  // set default path names
  m_hhitsName = "CaloMoniDst/" + m_detectorName + "EFlowAlgo/1";
  m_heName = "CaloMoniDst/" + m_detectorName + "EFlowAlgo/2";
  m_hetName = "CaloMoniDst/" + m_detectorName + "EFlowAlgo/3";
  m_hnbdigitsName = "CaloMoniDst/" + m_detectorName + "EFlowAlgo/4";

  // Areas
  m_nAreas = 1 << (CaloCellCode::BitsArea +1);
  m_areas.push_back("Outer");
  m_areas.push_back("Middle");
  m_areas.push_back("Inner");

  // Default number of cells in each area
  if (m_detectorName=="Ecal" || m_detectorName=="Prs" || m_detectorName=="Spd") {
    m_Ncells[0] = 1536;
    m_Ncells[1] = 1792;
    m_Ncells[2] = 2688;
    m_FirstCellInX[0] = 0;
    m_FirstCellInX[1] = 0;
    m_FirstCellInX[2] = 8;
    m_FirstCellInY[0] = 6;
    m_FirstCellInY[1] = 12;
    m_FirstCellInY[2] = 14;
    m_DeadZoneMinInX[0] = 16;
    m_DeadZoneMinInX[1] = 16;
    m_DeadZoneMinInX[2] = 24;
    m_DeadZoneMinInY[0] = 22;
    m_DeadZoneMinInY[1] = 20;
    m_DeadZoneMinInY[2] = 26;
  }
  else if (m_detectorName=="Hcal") {
    m_Ncells[0] = 880;
    m_Ncells[1] = 608;
    m_FirstCellInX[0] = 0;
    m_FirstCellInX[1] = 0;
    m_FirstCellInY[0] = 3;
    m_FirstCellInY[1] = 2;
    m_DeadZoneMinInX[0] = 8;
    m_DeadZoneMinInX[1] = 14;
    m_DeadZoneMinInY[0] = 6;
    m_DeadZoneMinInY[1] = 12;
  }

  debug() << "Service Locator " << pSvcLocator << endmsg;
  

}
//=============================================================================
// Destructor
//=============================================================================
CaloEFlowAnalysis::~CaloEFlowAnalysis() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode CaloEFlowAnalysis::initialize() {
 
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  //get detector element
  if ( "Ecal" == m_detectorName ) {
    m_detector = DeCalorimeterLocation::Ecal ;
  } else if ( "Hcal" == m_detectorName ) {
    m_detector = DeCalorimeterLocation::Hcal ;
  } else if ( "Prs" == m_detectorName ) {
    m_detector = DeCalorimeterLocation::Prs ;
  } else{
    warning() <<" Unable to load the DetectorElement for " << m_detectorName << endmsg;
    return StatusCode::SUCCESS;
  }
  info() << "Detector element is set to : " << m_detectorName << endmsg;
  m_calo = getDet<DeCalorimeter>( m_detector );
  
  //default methods vector if empty
  debug() << " method size is " << m_methods.size() << endmsg;
  if (m_methods.size()==0) {
    warning() << " Method vector size is null, set a default procedure : Neighbours calibration " << endmsg;
    m_methods.push_back( "Neighbours" );
  }

  debug() << "Kernel dim "<< m_kernelDimension << " kernel type " << m_kernelType << endmsg;

  //set default Kernel
  if (m_kernelType=="Flat" && m_kernelDimension == 3) {
    for (int ix=0; ix<m_kernelDimension; ix++) {
      for (int iy=0; iy<m_kernelDimension; iy++) {
        int x = ix-(m_kernelDimension-1)/2;
        int y = iy-(m_kernelDimension-1)/2;
        m_kernel[x*m_kernelDimension + y] = 1;
      }
    }
  }
  else if (m_kernelType=="FirstOrder" && m_kernelDimension == 3) {
    for (int ix=0; ix<m_kernelDimension; ix++) {
      for (int iy=0; iy<m_kernelDimension; iy++) {
        int x = ix-(m_kernelDimension-1)/2;
        int y = iy-(m_kernelDimension-1)/2;
        if (x*y!=0)
          m_kernel[x*m_kernelDimension + y] = 0;
        else if ((x+y)!=0)
          m_kernel[x*m_kernelDimension + y] = 1;
        else if (x==0 && y==0)
          m_kernel[x*m_kernelDimension + y] = 2;
        else
          error() << "False statement in Kernel" << endmsg;
      }
    }
  }
  else if (m_kernelType=="SecondOrder" && m_kernelDimension == 3) {
    for (int ix=0; ix<m_kernelDimension; ix++) {
      for (int iy=0; iy<m_kernelDimension; iy++) {
        int x = ix-(m_kernelDimension-1)/2;
        int y = iy-(m_kernelDimension-1)/2;
        if (x*y!=0)
          m_kernel[x*m_kernelDimension + y] = 1/sqrt(2);
        else if ((x+y)!=0)
          m_kernel[x*m_kernelDimension + y] = 1;
        else if (x==0 && y==0)
          m_kernel[x*m_kernelDimension + y] = 2*(1+1/sqrt(2));
        else
          error() << "False statement in Kernel" << endmsg;
      }
    }
  }
 else if (m_kernelType=="Flat" && m_kernelDimension == 5) {
    for (int ix=0; ix<m_kernelDimension; ix++) {
      for (int iy=0; iy<m_kernelDimension; iy++) {
        int x = ix-(m_kernelDimension-1)/2;
        int y = iy-(m_kernelDimension-1)/2;
        m_kernel[x*m_kernelDimension + y] = 1;
      }
    }
  }
  else if (m_kernelType=="FirstOrder" && m_kernelDimension == 5) {
    for (int ix=0; ix<m_kernelDimension; ix++) {
      for (int iy=0; iy<m_kernelDimension; iy++) {
        int x = ix-(m_kernelDimension-1)/2;
        int y = iy-(m_kernelDimension-1)/2;
        if (abs(x*y)>1)
          m_kernel[x*m_kernelDimension + y] = 0;
        else if (x*y==0 && abs(x+y)>1)
          m_kernel[x*m_kernelDimension + y] = 1;
        else if (abs(x+y)>0)
          m_kernel[x*m_kernelDimension + y] = 2;
        else if (x==0 && y==0)
          m_kernel[x*m_kernelDimension + y] = 5;
        else
          error() << "False statement in Kernel" << endmsg;
      }
    }
  }
  else if (m_kernelType=="SecondOrder" && m_kernelDimension == 5) {
    for (int ix=0; ix<m_kernelDimension; ix++) {
      for (int iy=0; iy<m_kernelDimension; iy++) {
        int x = ix-(m_kernelDimension-1)/2;
        int y = iy-(m_kernelDimension-1)/2;
        if (abs(x*y)>2)
          m_kernel[x*m_kernelDimension + y] = 0;
        else if (abs(x*y)>1)
          m_kernel[x*m_kernelDimension + y] = 1;
        else if ((abs(x)+abs(y))>1)
          m_kernel[x*m_kernelDimension + y] = 2;
        else if (abs(x+y)==1)
          m_kernel[x*m_kernelDimension + y] = 4;
        else if (x==0 && y==0)
          m_kernel[x*m_kernelDimension + y] = 8;
        else
          error() << "False statement in Kernel" << endmsg;
      }
    }
  }
  else {
    error() << "False Kernel requirement" << endmsg;
  }
  
  //initialize 2Dviews
  std::string etype = m_energy ? "E" : "Et";
  m_h2Dview = new Calo2Dview( m_name, serviceLocator());
  if ( m_h2Dview == 0 ) return StatusCode::FAILURE;
  m_h2Dview->setOneDimension( m_1D );
  m_h2Dview->setSplit( m_split );
  if( (m_h2Dview->initialize()).isFailure() ){
    error() << "Unable to initialize Calo2Dview" << endmsg;
    return StatusCode::FAILURE;
  }
  std::string hname_norm = "Normalized " + etype  + "-weighted digit position";
  m_h2Dview->bookCalo2D("0",hname_norm,m_detectorName);
  for (unsigned int index = 0; index<m_methods.size() ; index++) {
    GaudiAlg::HistoID hid( Gaudi::Utils::toString(index+1) );
    //define name
    std::string hname_calib = "Calibrated (";
    hname_calib += m_methods[0];
    for (unsigned int ind=0; ind<index; ind++) { hname_calib += "+" + m_methods[ind+1]; }
    hname_calib += ") " + etype  + "-weighted digit position";
    m_h2Dview->bookCalo2D(hid,hname_calib,m_detectorName);
  }
  unsigned int i = m_methods.size() +1;
  debug() << m_methods.size() << " i " << i << endmsg;
  GaudiAlg::HistoID hid_coeff( Gaudi::Utils::toString(i) );
  debug() << hid_coeff << endmsg;
  m_h2Dview->bookCalo2D(hid_coeff, "Calibration coefficients-weighted digit position", m_detectorName);
 
  //initialize histograms
  m_hnormalized = new TH1D("","Normalized energy flow",m_bin1d, 0, (double) m_bin1d);
  m_hcalibrated = new TH1D("","Calibrated energy flow",m_bin1d, 0, (double) m_bin1d);
  m_hcoeff = new Calo2Dview(m_name , serviceLocator()) ;
  if ( m_hcoeff == 0 ) {
    error() << "Unable to create Coeff 1D view" << endmsg;
    return StatusCode::FAILURE;
  }
  m_hcoeff->setOneDimension( true ); 
  m_hcoeff->setSplit( false ); 
  if( (m_hcoeff->initialize()).isFailure() ){
    error() << "Unable to initialize Coeff 1D view" << endmsg;
    return StatusCode::FAILURE;
  }

  //initialize nb of events processed
  m_Nevents = 0;
  
  // Calo parameters :
  // Hcal
  m_centreMap[3] = 16;
  // Ecal
  m_centreMap[2] = 32;
  // Prs
  m_centreMap[1] = 32;
  // Spd
  m_centreMap[0] = 32;
  
  // must be executed last, error printed already by AnalysisTask
  if ( (AnalysisTask::initialize()).isFailure() ) return StatusCode::FAILURE;
  
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode CaloEFlowAnalysis::analyze(std::string& SaveSet, std::string Task) {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Analyze" << endmsg;

  // mandatory: call default analyze method
  StatusCode sc = AnalysisTask::analyze(SaveSet, Task);
  if ( sc.isFailure() ) return sc; 
   
  info() << "Opening root file " << SaveSet << endmsg;
  m_finput = new TFile(SaveSet.c_str(),"READ");  
  if ( ! m_finput->IsOpen()){
    error() << " Unable to open input file : " << SaveSet  << endmsg;
    return StatusCode::SUCCESS;
  }

  //Looking for histos
  debug() <<"Looking for histo : " << m_hhitsName << endmsg;    
  m_hhits =(TH1D*) m_finput->Get( m_hhitsName.c_str() );
  if( 0 == m_hhits ){ error() << "HISTO NOT FOUND" <<endmsg; return StatusCode::FAILURE; } 
  
  if ( m_energy ) {
    debug() <<"Looking for histo : " << m_heName << endmsg;    
    m_he =(TH1D*) m_finput->Get( m_heName.c_str() );
  }
  else {
    debug() <<"Looking for histo : " << m_hetName << endmsg;    
    m_he =(TH1D*) m_finput->Get( m_hetName.c_str() );
  }
  if( 0 == m_he ){ error() << "HISTO NOT FOUND" <<endmsg; return StatusCode::FAILURE; } 
  
  debug() <<"Looking for histo : " << m_hnbdigitsName << endmsg;    
  m_hnbdigits =(TH1D*) m_finput->Get( m_hnbdigitsName.c_str() );
  if( 0 == m_hnbdigits ){ error() << "HISTO NOT FOUND" <<endmsg; return StatusCode::FAILURE; } 
  m_Nevents = (long int) m_hnbdigits->Integral();

  info() << "It seems there is : " << m_Nevents 
         << " that have been processed by CaloEFlowAlg." << endmsg;
  
 

  sc = normalize();//normalization of histograms
  if ( sc.isFailure() ) return sc;
  sc = doubtful();//doubtful channels detection
  if ( sc.isFailure() ) return sc;
  sc = calibrate();//calibration process
  if ( sc.isFailure() ) return sc;
  sc = coefficients();//coefficients computation and filling corresponding histogram fro further analysis
  if ( sc.isFailure() ) return sc;

  //delete transients histo
  delete m_hcalibrated;
  delete m_hnormalized;
  delete m_htemporary;

  //close input file
  debug() << "closing input file " << m_finput << endmsg;
  m_finput->Close();
  delete m_finput;
  
  debug() << "<===== END of Analyze" << endmsg;
  return StatusCode::SUCCESS;
}
//=============================================================================
// Finalization
//=============================================================================
StatusCode CaloEFlowAnalysis::finalize() {
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  
  return AnalysisTask::finalize();
}
//=============================================================================
// getCellID
//=============================================================================
LHCb::CaloCellID CaloEFlowAnalysis::getCellID(int bin, int calo) 
{
  LHCb::CaloCellID::ContentType cellindex = (bin << CaloCellCode::ShiftIndex) & CaloCellCode::MaskIndex;
  LHCb::CaloCellID::ContentType cellcalo = (calo << CaloCellCode::ShiftCalo) & CaloCellCode::MaskCalo;  
  LHCb::CaloCellID::ContentType all = cellindex | cellcalo;
  LHCb::CaloCellID id = LHCb::CaloCellID(all);
  verbose() << " BIN " << bin  << " CELLID " << id << " RETRIEVED BIN FROM CELLID " << id.all() << endmsg;
  return id;
}
//=============================================================================
// Normalization
//=============================================================================
StatusCode CaloEFlowAnalysis::normalize() {
  debug() << "Normalizing Energy FLow " << endmsg;
  
  //loop over bins
  debug() << " looping on cellIDs " << endmsg;
  for( int bin = 0 ; bin < m_hhits->GetNbinsX() ; bin++ ) {
    //get cellID
    LHCb::CaloCellID id = getCellID(bin,m_caloID);

    //cell validity
    bool isvalid = m_calo->valid(id) && !m_calo->isPinId( id ); //PinID ??? LEDId ?
    if( isvalid == false ) continue ;
    if (m_calo->hasQuality(id,CaloCellQuality::Shifted) == true ) continue ; //skip shifted cells
    if (m_calo->hasQuality(id,CaloCellQuality::VeryShifted) == true ) continue ; //skip shifted cells
    if (m_calo->hasQuality(id,CaloCellQuality::StuckADC) == true ) continue ; //skip shifted cells
    if (m_calo->hasQuality(id,CaloCellQuality::OfflineMask) == true ) continue ; //skip shifted cells	

    double enorm = 0;
    if (m_hhits->GetBinContent( bin+1 ) !=0) { //skip empty cells (should not appear here if dead prescription is applied before)
      enorm = m_he->GetBinContent( bin+1 );
      if ( m_normalized )
        enorm /= m_hhits->GetBinContent( bin+1 );
      else {
        verbose() << " Energy " << enorm << " Events " << m_Nevents 
               << " Energy Normalized by Events " << enorm/(double) m_Nevents << endmsg;
        enorm /= (double) m_Nevents;
      }
    }
    else
      warning() << "No hit in cell " << id << endmsg;

    verbose() << "cellID : " << id << " normalized energy  : " << enorm << endmsg;
    GaudiAlg::HistoID hid_coeff( Gaudi::Utils::toString(0) );
    m_h2Dview->fillCalo2D(hid_coeff, id, enorm );//note that not calibrated cells are not filled here
    m_hnormalized->SetBinContent(bin+1, enorm);
   
  }
  return StatusCode::SUCCESS;
}
//=============================================================================
// Doubtful cell search Method
//=============================================================================
StatusCode CaloEFlowAnalysis::doubtful() 
{
  debug() << "==> Find doubtful cells using the first calibration method set in the pipeline" << m_methods.size() << endmsg;
  //Apply a first pass of the first calibration procedure to mask problematic cells and estimate a calibration constant for such cells
  //NB : this could create pbs in the case of a multi-procedure calibration has been chosen
  StatusCode sc = StatusCode::SUCCESS;
     
  m_htemporary = new TH1D( *m_hnormalized );

  //call the proper method with respect to the procedure set in the pipeline
  //define a fake index as argument for the method in order not to write the estimated energy flow
  // this will create a hcalibrated histogram containing the estimated calibrated energy flow
  if (m_methods[0]=="Neighbours") sc = neighbours(-1);
  else if (m_methods[0]=="NeighboursBorders") sc = neighbours(-1);
  else if (m_methods[0]=="NeighboursExtrapolation") sc = neighbours(-1);
  else if (m_methods[0]=="Symmetric") sc = symmetric(-1);
  else error() << "Calibration method " << m_methods[0] << " is not defined. Please check option file. " << endmsg;

  //loop over the cells in order to mask problematic cells
  for( int bin = 0 ; bin < m_hnormalized->GetNbinsX() ; bin++ ) {
    LHCb::CaloCellID id = getCellID(bin,m_caloID);
    //ignore dead or invalid cells
    bool isvalid = m_calo->valid(id) && !m_calo->isPinId( id ); //PinID ??? LEDId ?
    if( isvalid == false ) continue ;
    //if (m_calo->isDead(id) == true ) continue ;
    if (m_calo->hasQuality(id,CaloCellQuality::Shifted) == true ) continue ; //skip shifted cells                                                                                 
    if (m_calo->hasQuality(id,CaloCellQuality::VeryShifted) == true ) continue ; //skip shifted cells                                                                             
    if (m_calo->hasQuality(id,CaloCellQuality::StuckADC) == true ) continue ; //skip shifted cells                                                                                
    if (m_calo->hasQuality(id,CaloCellQuality::OfflineMask) == true ) continue ; //skip shifted cells    
    //estimate calibration constant
    double coeff = 1.;
    if (m_hnormalized->GetBinContent( bin+1 )!=0) {
      coeff = m_hcalibrated->GetBinContent(bin+1)/m_hnormalized->GetBinContent(bin+1);
    }
    else if (m_hhits->GetBinContent( bin+1 )==0) {
      coeff = 0;//previously unflagged dead (no hit above threshold) cell
    }
    //flag problematic cell (dead cells included by definition since doubtful thresholds are >0)
    if (coeff>m_doubtfulThresholdUp || coeff<m_doubtfulThresholdDown) {
      info() << " DOUBTFUL CELL, id " << id << " with coeff " << coeff << endmsg;
      //mask the cell in order to properly calibrate afterwards
      m_hnormalized->SetBinContent(bin+1, 0);
      //store the estimatated calibration constant for this problematic cell
      // multplied by a factor -1 in order to keep this information afterwards
      //if coeff>100 assign -99
      //if coeff==0 (dead cell) assign -100
      if (coeff>100) coeff = 99.;
      if (coeff==0) coeff = 100.;
      m_hcoeff->fillCalo2D("0", id, -coeff, m_detectorName + " 1D Calibration coefficients-weighted digit position");
    }
  }

  //reset temporary and calibration histograms
  if (m_htemporary!=0) {
    delete m_htemporary;
    m_htemporary = 0;
  }
  if (m_hcalibrated!=0) {
    delete m_hcalibrated;
    m_hcalibrated = 0;
  }

  debug() << "<=== END doubtuful() " << endmsg;
  return sc;
}
//=============================================================================
// Calibration
//=============================================================================
StatusCode CaloEFlowAnalysis::calibrate() 
{
  debug() << "=> Calibrating EnergyFlow, method size is : " << m_methods.size() << endmsg;
  StatusCode sc = StatusCode::SUCCESS;
  //loop over the methods that have been set in the pipeline
  for (unsigned int index=0; index<m_methods.size(); index++) {
    //temporary histogram definition
    //temporary is set for the first calibration method in the pipeline to the energy flow before calibration
    //for the other calibration methods in the pipeline, the output of the preceding pass is used
    //NB : the calibration procedure should be blind to problematic cells, a dedicated procedure is thus applied before this procedure (see analyze for more details)
    if (index==0) { 
      m_htemporary = new TH1D( *m_hnormalized );
      if (m_hcalibrated==0) 
        m_hcalibrated = new TH1D("","Calibrated energy flow",m_bin1d, 0, (double) m_bin1d);
    }
    else 
      m_htemporary = new TH1D( *m_hcalibrated );
    //call the proper method with respect to the procedure set in the pipeline
    if (m_methods[index]=="Neighbours") { 
      sc = neighbours(index); 
    } 
    else if (m_methods[index]=="NeighboursBorders") { 
      sc = neighboursBorders(index); 
    } 
    else if (m_methods[index]=="NeighboursExtrapolation") { 
      sc = neighboursExtrapolation(index); 
    }
    else if (m_methods[index]=="Symmetric") {
      sc = symmetric(index); 
    }
    else
      error() << "Calibration method " << m_methods[index] << " is not defined. Please check option file. " << endmsg;
    
    //is it really useful ?
    if ( sc.isFailure() ) return sc;
    
    //reset temporary histogram
    debug() << " temp histo " << m_htemporary << endmsg;
    if (m_htemporary!=0) {
      delete m_htemporary;
      m_htemporary = 0;
    }

  }//end loop over methods
 
  debug() << "<=== END calibrate() " << endmsg;
  return sc;
}
//=============================================================================
// Coefficients
//============================================================================= 
StatusCode CaloEFlowAnalysis::coefficients() 
{
  std::string etype = m_energy ? "E" : "Et";
  //  int calo =  CaloCellCode::CaloNumFromName(m_detectorName);
  //-------- GetCoefficients
  debug() << "Getting calibration coefficients " << endmsg;
  //loop over bins
  unsigned int i = m_methods.size() +1;
  GaudiAlg::HistoID hid_coeff( Gaudi::Utils::toString(i) );
  GaudiAlg::HistoID hid_coeff1D( Gaudi::Utils::toString(i+1) );
  debug() << hid_coeff << " " << hid_coeff1D << endmsg;
  verbose() << "norm histo address " << m_hnormalized << " calib histo address " << m_hcalibrated << endmsg;
  //loop on cellIDs
  for( int bin = 0 ; bin < m_hnormalized->GetNbinsX() ; bin++ ) {
    LHCb::CaloCellID id = getCellID(bin,m_caloID);
    //remove unvalid or dead cells according to DB
    bool isvalid = m_calo->valid(id) && !m_calo->isPinId( id ); //PinID ??? LEDId ?
    if( isvalid == false ) continue ;
    //if (m_calo->isDead(id) == true ) continue ;
    if (m_calo->hasQuality(id,CaloCellQuality::Shifted) == true ) continue ; //skip shifted cells                                                                                 
    if (m_calo->hasQuality(id,CaloCellQuality::VeryShifted) == true ) continue ; //skip shifted cells                                                                             
    if (m_calo->hasQuality(id,CaloCellQuality::StuckADC) == true ) continue ; //skip shifted cells                                                                                
    if (m_calo->hasQuality(id,CaloCellQuality::OfflineMask) == true ) continue ; //skip shifted cells    
    //get coeff for cells that are effectively calibrated by the procedure
    double coeff = 1.;
    if (m_hnormalized->GetBinContent( bin+1 ) != 0 ) {
      coeff = m_hcalibrated->GetBinContent( bin+1 )/m_hnormalized->GetBinContent( bin+1 );
      m_h2Dview->fillCalo2D(hid_coeff, id, coeff );//note that not calibrated cells are not filled here
      m_hcoeff->fillCalo2D("0", id, coeff , m_detectorName + " 1D Calibration coefficients-weighted digit position");
    }
    
    verbose() << " BIN " << bin << " CELLID " << id << " RETRIEVED BIN FROM CELLID " << id.all() 
              << " COEFF " << coeff << endmsg;
    
   
  }
  return StatusCode::SUCCESS;
}
//=============================================================================
// Neighbours Calibration Method
//=============================================================================
StatusCode CaloEFlowAnalysis::neighbours(int index) 
{
  debug() << "==> Try to calibrate using Local mean (Neighbours), index is " << index << endmsg;
  GaudiAlg::HistoID hid_calib( Gaudi::Utils::toString((unsigned int)(index)+1) );
  //loop over bins
  debug() << " looping on cellIDs " << endmsg;
  for( int bin = 0 ; bin < m_hnormalized->GetNbinsX() ; bin++ ) {
    LHCb::CaloCellID id = getCellID(bin,m_caloID);
    //skip invalid or dead cells
    bool isvalid = m_calo->valid(id) && !m_calo->isPinId( id ); //PinID ??? LEDId ?
    if( isvalid == false ) continue ;
//    if (m_calo->isDead(id) == true ) continue ;        
    if (m_calo->hasQuality(id,CaloCellQuality::Shifted) == true ) continue ; //skip shifted cells                                                                                 
    if (m_calo->hasQuality(id,CaloCellQuality::VeryShifted) == true ) continue ; //skip shifted cells                                                                             
    if (m_calo->hasQuality(id,CaloCellQuality::StuckADC) == true ) continue ; //skip shifted cells                                                                                
    if (m_calo->hasQuality(id,CaloCellQuality::OfflineMask) == true ) continue ; //skip shifted cells    
    double ecalib = m_htemporary->GetBinContent( bin+1 );
    if (ecalib==0) continue; //skip empty cell (dead one !!! but not yet flaged as)
    ecalib *= m_kernel[0];
    
    double norm = m_kernel[0];
    //looping on neighbors in the same area (zsupNeighborCells)
    for( CaloNeighbors::const_iterator ineighbor =  m_calo->zsupNeighborCells( id ).begin() ;
         ineighbor != m_calo->zsupNeighborCells( id ).end() ; ineighbor++ ) {
      verbose() << " looping on neighbors " << endmsg;
      //if (m_calo->isDead(*ineighbor) == true ) continue ;
      if (m_calo->hasQuality(*ineighbor,CaloCellQuality::Shifted) == true ) continue ; //skip shifted cells                                                                                 
    if (m_calo->hasQuality(*ineighbor,CaloCellQuality::VeryShifted) == true ) continue ; //skip shifted cells                                                                             
    if (m_calo->hasQuality(*ineighbor,CaloCellQuality::StuckADC) == true ) continue ; //skip shifted cells                                                                                
    if (m_calo->hasQuality(*ineighbor,CaloCellQuality::OfflineMask) == true ) continue ; //skip shifted cells    
      if (m_htemporary->GetBinContent( ineighbor->index()+1)==0) continue;
      int ix = id.col()-ineighbor->col();
      int iy = id.row()-ineighbor->row();
      ecalib += m_kernel[ix*m_kernelDimension+iy]*m_htemporary->GetBinContent( ineighbor->index()+1) ;
      norm += m_kernel[ix*m_kernelDimension + iy];

      debug() << "Ecalib " << ecalib << " Kernel " << m_kernel[ix*m_kernelDimension + iy]
              << " norm " << norm << endmsg;
    }//end loop on neighbors
    if (m_kernelDimension==5) {
      for (int kr=-2;kr<3;kr++) {
        for (int kc=-2;kc<3;kc++) {
          if (abs(kr)!=2 && abs(kc)!=2) { continue ; } //skip cell (already in neighbours)
          LHCb::CaloCellID idnew = LHCb::CaloCellID( id.calo(), id.area(), id.row()+kr,id.col()+kc);
          
          if (m_calo->valid(idnew) && !m_calo->isPinId( idnew ) ) {
	    if (m_calo->hasQuality(idnew,CaloCellQuality::Shifted) == true ) continue ; //skip shifted cells                                                                                 
    if (m_calo->hasQuality(idnew,CaloCellQuality::VeryShifted) == true ) continue ; //skip shifted cells    	                                                                         
    if (m_calo->hasQuality(idnew,CaloCellQuality::StuckADC) == true ) continue ; //skip shifted cells                                                                                
    if (m_calo->hasQuality(idnew,CaloCellQuality::OfflineMask) == true ) continue ; //skip shifted cells    
            if (m_htemporary->GetBinContent( idnew.index() +1 )!=0) {
              ecalib += m_kernel[kc*m_kernelDimension+kr]*m_htemporary->GetBinContent( idnew.index()+1) ;
              norm += m_kernel[kc*m_kernelDimension + kr];

              debug() << "Ecalib " << ecalib << " Kernel " << m_kernel[kc*m_kernelDimension + kr]
                      << " norm " << norm << endmsg;
            }
          }
        }
      }//end loop kr
    }//end if kernle dimension
    
    ecalib /= norm;
    
    debug() << " cellID " << id << " ecalib " << ecalib << endmsg;
    
    if( index!=-1) {
      debug() << " filling m_h2Dview"  << endmsg;
      m_h2Dview->fillCalo2D(hid_calib, id, ecalib);
    }
    debug() << " filling calibrated"  << endmsg;
    m_hcalibrated->SetBinContent(bin+1, ecalib);
    
  }//end loop on bins
  return StatusCode::SUCCESS;
}
//=============================================================================
// Neighbours with special processing at the Borders Calibration Method
//=============================================================================
StatusCode CaloEFlowAnalysis::neighboursBorders(int index) 
{
  debug() << "==> Try to calibrate using local mean  (neighbours) with special processing at the borders, index is " 
          << index << endmsg;
  GaudiAlg::HistoID hid_calib( Gaudi::Utils::toString((unsigned int)(index)+1) );
  //set temp histo
  StatusCode sc = neighbours(index); 
  if ( sc.isFailure() ) return sc;
  //temp histo for classical neighbours process
  TH1D* htemp = new TH1D( *m_hcalibrated );

  std::string etype = m_energy ? "E" : "Et";
  
  debug() << " will loop over bins, index is " << index << endmsg;
  //loop over bins
  for( int bin = 0 ; bin < m_hnormalized->GetNbinsX() ; bin++ ) {
    LHCb::CaloCellID id = getCellID(bin,m_caloID);
    //skip invalid cells
    bool isvalid = m_calo->valid(id) && !m_calo->isPinId( id ); //PinID ??? LEDId ?
    if( isvalid == false ) continue ;
    //skip dead cells
//    if (m_calo->isDead(id) == true ) continue ;        
    if (m_calo->hasQuality(id,CaloCellQuality::Shifted) == true ) continue ; //skip shifted cells                                                                                 
    if (m_calo->hasQuality(id,CaloCellQuality::VeryShifted) == true ) continue ; //skip shifted cells                                                                             
    if (m_calo->hasQuality(id,CaloCellQuality::StuckADC) == true ) continue ; //skip shifted cells                                                                                
    if (m_calo->hasQuality(id,CaloCellQuality::OfflineMask) == true ) continue ; //skip shifted cells    
    double ecalib = m_htemporary->GetBinContent( bin+1 );
    if (ecalib==0) continue; //skip empty cell (dead one !!! but not yet flaged as)

    if (m_calo->zsupNeighborCells( id ).size()==8) {//neighours and neighboursBorders are equivalent
      ecalib = htemp->GetBinContent(bin+1);
    }
    else {
      unsigned int count = 1;
      //loop on neighbours cellIDs
      for (int ir=-1;ir<2;ir++) {
        for (int ic=-1;ic<2;ic++) {
          if (ir==0 && ic==0) { continue ; } //skip current cell : already done
          
          int epsilon = 0;
          int isrow = 0;
          if (((int)id.row()+ir)<0) {
            epsilon = +1;
            isrow = +1;
          }
          else if (((int)id.row()+ir)>63) {
            epsilon = -1;
            isrow = +1;
          }
          else if  (((int)id.col()+ic)<0) {
            epsilon = +1;
            isrow = -1;
          }
          else if(((int)id.col()+ic)>63) {
            epsilon = -1;
            isrow = -1;
          }
          
          //Do ill defined outer borders
          if (epsilon!=0 && isrow!=0) {
            double eextrapol = 0;
            double counter = 0;
            for (int k=-1;k<2;k++) {//in 3 directions
              //int ind = 0;
              int r1 = 0;
              int r2 = 0;
              int c1 = 0;
              int c2 = 0;
              if (isrow == +1) {
                //ind = id.col();
                r1 = id.row();
                r2 = id.row() + epsilon;
                c1 = id.col() + k;
                c2 = id.col() + 2*k;
              }
              else if (isrow == -1){
                //ind = id.row();
                r1 = id.row() + k;
              r2 = id.row() + 2*k;
              c1 = id.col();
              c2 = id.col() + epsilon;
              }
              if (r1<0 || r2<0 || c1<0 || c2<0) { continue ; } //skip undefined cell
              if (r1>63 || r2>63 || c1>63 || c2>63) { continue ; } //skip undefined cell
              LHCb::CaloCellID id1 = LHCb::CaloCellID( id.calo(), id.area(), r1,c1);
              LHCb::CaloCellID id2 = LHCb::CaloCellID( id.calo(), id.area(), r2,c2);
              
              if (m_calo->valid(id1) && !m_calo->isPinId( id1 ) ) {
                if (m_calo->valid(id2) && !m_calo->isPinId( id2 ) ) {
                  if (/*m_calo->isDead(id1) == false  &&*/ htemp->GetBinContent( id1.index() +1 )!=0   
        && m_calo->hasQuality(id1,CaloCellQuality::Shifted) == false                                                                                                        
      
        && m_calo->hasQuality(id1,CaloCellQuality::VeryShifted) == false                                                                                                    
      
        && m_calo->hasQuality(id1,CaloCellQuality::StuckADC) == false                                                                                                       
      
        && m_calo->hasQuality(id1,CaloCellQuality::OfflineMask) == false) {
                    if (/*m_calo->isDead(id2) == false  &&*/ htemp->GetBinContent( id2.index() +1 )!=0   
        && m_calo->hasQuality(id2,CaloCellQuality::Shifted) == false                                                                                                        
      
        && m_calo->hasQuality(id2,CaloCellQuality::VeryShifted) == false                                                                                                    
      
        && m_calo->hasQuality(id2,CaloCellQuality::StuckADC) == false                                                                                                       
      
        && m_calo->hasQuality(id2,CaloCellQuality::OfflineMask) == false) {  
                      double etemp = 0;
                      etemp = 3/*2*/*htemp->GetBinContent( id1.index() +1 );
                      etemp -= 2/*1*/*htemp->GetBinContent( id2.index() +1 );
                      eextrapol += etemp;
                      counter ++;
                      verbose() << "CellID " << id 
                                << " Eextrapolate " << eextrapol << " counter " << counter << std::endl;
                    }
                  }
                }
              }
            }//end loop k
            //take care of the fact that all neighbouring cell could be dead ones !
            if (counter!=0) {
              ecalib += eextrapol/counter;
              count++;
              debug() << "CellID " << id 
                      << " Ecalib " << ecalib << " count " << count << std::endl;
            }
          }
          else {
            LHCb::CaloCellID idnew = LHCb::CaloCellID( id.calo(), id.area(), id.row()+ir, id.col()+ic);
            if (m_calo->valid(idnew) && !m_calo->isPinId( idnew ) ) {
              //fill for non dead cells
              if (/*m_calo->isDead(idnew) == false  &&*/ m_htemporary->GetBinContent( idnew.index() +1 )!=0   
        && m_calo->hasQuality(idnew,CaloCellQuality::Shifted) == false                                                                                                        
      
        && m_calo->hasQuality(idnew,CaloCellQuality::VeryShifted) == false                                                                                                    
      
        && m_calo->hasQuality(idnew,CaloCellQuality::StuckADC) == false                                                                                                       
      
        && m_calo->hasQuality(idnew,CaloCellQuality::OfflineMask) == false) { 
                ecalib += m_htemporary->GetBinContent( idnew.index() +1 );
                count++; 
                debug() << "CellID " << id << " defined neighbour " << idnew 
                        << " Ecalib " << ecalib << " count " << count << std::endl;
              }
            }
            else {
              double eextrapol = 0;
              double counter = 0;
            
               for (int kr=-1;kr<2;kr++) {
                 for (int kc=-1;kc<2;kc++) {
                   if (kr==0 && kc==0) { continue ; } //skip current undefined cell :|
                   LHCb::CaloCellID id1 = LHCb::CaloCellID( id.calo(), id.area(), idnew.row()+kr,idnew.col()+kc);
                   LHCb::CaloCellID id2 = LHCb::CaloCellID( id.calo(), id.area(), idnew.row()+2*kr,idnew.col()+2*kc);

                   if (abs(kc)==1 && abs(kr)==1){
                     if (!m_diagonal) {
                       continue;
                     }
                   }
                   
                   if (m_calo->valid(id1) && !m_calo->isPinId( id1 ) ) {
                     if (m_calo->valid(id2) && !m_calo->isPinId( id2 ) ) {
                       if (/*m_calo->isDead(id1) == false  &&*/ htemp->GetBinContent( id1.index() +1 )!=0   
        && m_calo->hasQuality(id1,CaloCellQuality::Shifted) == false                                                                                                        
      
        && m_calo->hasQuality(id1,CaloCellQuality::VeryShifted) == false                                                                                                    
      
        && m_calo->hasQuality(id1,CaloCellQuality::StuckADC) == false                                                                                                       
      
        && m_calo->hasQuality(id1,CaloCellQuality::OfflineMask) == false) {
                         if (/*m_calo->isDead(id2) == false  &&*/ htemp->GetBinContent( id2.index() +1 )!=0   
        && m_calo->hasQuality(id2,CaloCellQuality::Shifted) == false                                                                                                        
      
        && m_calo->hasQuality(id2,CaloCellQuality::VeryShifted) == false                                                                                                    
      
        && m_calo->hasQuality(id2,CaloCellQuality::StuckADC) == false                                                                                                       
      
        && m_calo->hasQuality(id2,CaloCellQuality::OfflineMask) == false) {  
                           double etemp = 0;
                           etemp = 3/*2*/*htemp->GetBinContent( id1.index() +1 );
                           etemp -= 2/*1*/*htemp->GetBinContent( id2.index() +1 );
                           eextrapol += etemp;
                           counter ++;
                           verbose() << "CellID " << id << " defined neighbour " << idnew 
                                     << " Eextrapolate " << eextrapol << " counter " << counter << std::endl;
                         }
                       }
                     }
                   }
                 }
               }//end loop kr
               //take care of the fact that all neighbouring cell could be dead ones !
               if (counter!=0) {
                 ecalib += eextrapol/counter;
                 count++;
                 debug() << "CellID " << id << " undefined neighbour " << idnew 
                         << " Ecalib " << ecalib << " count " << count << std::endl;
               }     
            }
          }//end else and if epsilon and isrow
        }//end loop ic
      }//end loop ir
    
      ecalib /= (double) count;
      debug() << "CellID " << id << " ecalib " << ecalib << " (count) " << count << std::endl;
    }
    
    verbose() << " cellID " << id << " ecalib " << ecalib << endmsg;
    
    //fill histos
    if (index!=-1) {
     
      m_h2Dview->reset(hid_calib);
      m_h2Dview->fillCalo2D(hid_calib, id, ecalib);
    }
    
    m_hcalibrated->SetBinContent(bin+1, ecalib);
    
  }//end loop on bins
  

  return StatusCode::SUCCESS;
}//=============================================================================
// Neighbours with extrapolation at the Border Belts Calibration Method
//=============================================================================
StatusCode CaloEFlowAnalysis::neighboursExtrapolation(int index) 
{
  debug() << "==> Try to calibrate using local mean  (neighbours) with special processing at the borders, index is " 
          << index << endmsg;
  GaudiAlg::HistoID hid_calib( Gaudi::Utils::toString((unsigned int)(index)+1) );
  //set temp histo
  StatusCode sc = neighbours(index); 
  if ( sc.isFailure() ) return sc;
  //temp histo for classical neighbours process
  TH1D* htemp = new TH1D( *m_hcalibrated );

  std::string etype = m_energy ? "E" : "Et";
  
  debug() << " will loop over bins, index is " << index << endmsg;
  //loop over bins
  for( int bin = 0 ; bin < m_hnormalized->GetNbinsX() ; bin++ ) {
    LHCb::CaloCellID id = getCellID(bin,m_caloID);
    //skip invalid cells
    bool isvalid = m_calo->valid(id) && !m_calo->isPinId( id ); //PinID ??? LEDId ?
    if( isvalid == false ) continue ;
    //skip dead cells
//    if (m_calo->isDead(id) == true ) continue ;        
    if (m_calo->hasQuality(id,CaloCellQuality::Shifted) == true ) continue ; //skip shifted cells                                                                                 
    if (m_calo->hasQuality(id,CaloCellQuality::VeryShifted) == true ) continue ; //skip shifted cells                                                                             
    if (m_calo->hasQuality(id,CaloCellQuality::StuckADC) == true ) continue ; //skip shifted cells                                                                                
    if (m_calo->hasQuality(id,CaloCellQuality::OfflineMask) == true ) continue ; //skip shifted cells    
    double ecalib = m_htemporary->GetBinContent( bin+1 );
    if (ecalib==0) continue; //skip empty cell (dead one !!! but not yet flaged as)
    // info() << "area " << id.area() <<  " row " << id.row() << " col " << id.col() << endmsg;
 
   
     //number of valid cells
    unsigned int nvalid = 0;
    double sumIndices = 0;
    double sumIndices2 = 0;
    double sumFunc = 0;
    double sumIndicesFunc = 0;
    //flags to parametrize the extrapolation (vertical/hortizontal positive/negative)
    int col = -99;//stupid default values
    int row = -99;//stupid default values
    if (id.area()==0) {
      if ( id.row()>(unsigned int)(m_FirstCellInY[id.area()]+1) && id.row()<(unsigned int) (63-m_FirstCellInY[id.area()]-1)) {
	if (id.col()==(unsigned int) m_FirstCellInX[id.area()] || id.col()==(unsigned int) (m_FirstCellInX[id.area()]+1) ) {
	  //left outer belts
	  col = +1;
	}
	else if (id.col()==(unsigned int) (63-m_FirstCellInX[id.area()]) || id.col()==(unsigned int) (63-m_FirstCellInX[id.area()]-1) ) {
	  //right outer belts
	  col = -1;
	}
      }
      else if (id.col()>(unsigned int) (m_FirstCellInX[id.area()]+1) && id.col()<(unsigned int) (63-m_FirstCellInX[id.area()]-1)) {
	if (id.row()==(unsigned int) m_FirstCellInY[id.area()] || id.row()==(unsigned int) (m_FirstCellInY[id.area()]+1) ) {
	  //bottom outer belts
	  row = +1;
	}
	else if (id.row()==(unsigned int) (63-m_FirstCellInY[id.area()]) || id.row()==(unsigned int) (63-m_FirstCellInY[id.area()]-1) ) {
	  //top outer belts
	  row = -1;
	}
      }
    }
    if (id.area()==2) {
      if (id.row()>=(unsigned int) (m_DeadZoneMinInY[id.area()]-2) && id.row()<=(unsigned int) (63-m_DeadZoneMinInY[id.area()]+2)) {
	if ( id.col()==(unsigned int) (m_DeadZoneMinInX[id.area()]-2) || id.col()==(unsigned int) (m_DeadZoneMinInX[id.area()]-1)) {
	  //left inner belts
	  col = -1;
	}
	else if (id.col()==(unsigned int) (63-m_DeadZoneMinInX[id.area()]+2) ||id.col()==(unsigned int) (63-m_DeadZoneMinInX[id.area()]+1)) {
	  //right inner belts
	  col = +1;
	}
      }
      if (id.col()>=(unsigned int) (m_DeadZoneMinInX[id.area()]) && id.col()<=(unsigned int) (63-m_DeadZoneMinInX[id.area()])) {
	if (id.row()==(unsigned int) (m_DeadZoneMinInY[id.area()]-2) || id.row()==(unsigned int) (m_DeadZoneMinInY[id.area()]-1)) {
	  //bottom inner belts
	  row = -1;
	}
	else if (id.row()==(unsigned int) (63-m_DeadZoneMinInY[id.area()]+2) || id.row()==(unsigned int) (63-m_DeadZoneMinInY[id.area()]+1)) {
	  //top inner belts
	  row = +1;
	}
      }
    }
    if (row==-99 && col==-99) {
      //not at the (next to) outer/inner-most belts
      ecalib = htemp->GetBinContent(bin+1);
    }
    else {
      unsigned int epscol = 1;
      unsigned int epsrow = 1;
      if (row==-99) epsrow = 0;
      else epsrow = row;
      if (col==-99) epscol = 0;
      else epscol = col;
      if (epscol==epsrow)
	debug() << "error in calculation" << endmsg;
      for (unsigned int k=1;k<5;k++) {
	LHCb::CaloCellID idk = LHCb::CaloCellID( id.calo(), id.area(), id.row()+k*epsrow,id.col()+k*epscol);
	if ( !(m_calo->valid(idk) && !m_calo->isPinId(idk)) ) continue;//dead cell
	double e = htemp->GetBinContent( idk.index()+1 );
	if (e<=0) continue; //empty cell
	if (idk.col()==(unsigned int) (m_FirstCellInX[id.area()]+1)) continue; //skip cell next to the border since biased
	else if (idk.col()==(unsigned int) (63-m_FirstCellInX[id.area()]-1)) continue; //skip cell next to the border since biased
	else if (idk.row()==(unsigned int) (m_FirstCellInY[id.area()]+1)) continue; //skip cell next to the border since biased
	else if (idk.row()==(unsigned int) (63-m_FirstCellInY[id.area()]-1)) continue; //skip cell next to the border since biased
	else if (idk.col()==(unsigned int) (m_DeadZoneMinInX[id.area()]-2)) continue; //skip cell next to the border since biased
	else if (idk.col()==(unsigned int) (63-m_DeadZoneMinInX[id.area()]+2)) continue; //skip cell next to the border since biased
	else if (idk.row()==(unsigned int) (m_DeadZoneMinInY[id.area()]-2)) continue; //skip cell next to the border since biased
	else if (idk.row()==(unsigned int) (63-m_DeadZoneMinInY[id.area()]+2)) continue; //skip cell next to the border since biased
	
	nvalid++;
	double dI = epscol*(idk.col()-id.col())+epsrow*(idk.row()-id.row());
	sumIndices += dI;
	sumIndices2 += dI*dI;
	sumFunc += e;
	sumIndicesFunc += dI*e;
      }
      if (nvalid<2) 
	debug() << "not engouh cells to extrapolate" << endmsg; //cannot give meaningfull information
      else {
	//use simple regression model
	ecalib = sumIndices2*sumFunc - sumIndices*sumIndicesFunc;
	ecalib /= nvalid*sumIndices2 - sumIndices*sumIndices;
	// use flat approximation
	// ecalib = sumFunc;
	// ecalib /= nvalid;
	debug() << "calib cell" << id << " with " << nvalid << " measurements "
	       << " e old " << htemp->GetBinContent(bin+1) 
	       << " e new " << ecalib 
	       << " calib " << ecalib/htemp->GetBinContent(bin+1) << endmsg;
      }
    }
    verbose() << " cellID " << id << " ecalib " << ecalib << endmsg;
    //fill histos
    if (index!=-1) {
      m_h2Dview->reset(hid_calib);
      m_h2Dview->fillCalo2D(hid_calib, id, ecalib);
    }
    info() << " id " << id << " old ecalib " << m_hcalibrated->GetBinContent(bin+1) << " new " << ecalib << endmsg;
    m_hcalibrated->SetBinContent(bin+1, ecalib);
    
  }//end loop on bins
  
  return StatusCode::SUCCESS;
}
//=============================================================================
// Symmetric Calibration Method
//=============================================================================
StatusCode CaloEFlowAnalysis::symmetric(int index) 
{
   debug() << "==> Try to calibrate using Symmetric mean, index is " << index << endmsg;
   GaudiAlg::HistoID hid_calib( Gaudi::Utils::toString(index+1) );
   //loop over bins
   for( int bin = 0 ; bin < m_hnormalized->GetNbinsX() ; bin++ ) {
     LHCb::CaloCellID id = getCellID(bin,m_caloID);
     //skip invalid and dead cells
     bool isvalid = m_calo->valid(id) && !m_calo->isPinId( id ); //PinID ??? LEDId ?
     if( isvalid == false ) continue ;
//     if (m_calo->isDead(id) == true ) continue ;
if (m_calo->hasQuality(id,CaloCellQuality::Shifted) == true ) continue ; //skip shifted cells                                                                                 
    if (m_calo->hasQuality(id,CaloCellQuality::VeryShifted) == true ) continue ; //skip shifted cells                                                                             
    if (m_calo->hasQuality(id,CaloCellQuality::StuckADC) == true ) continue ; //skip shifted cells                                                                                
    if (m_calo->hasQuality(id,CaloCellQuality::OfflineMask) == true ) continue ; //skip shifted cells            
     //get present energy estimate for this cell
     double ecalib = m_htemporary->GetBinContent( bin+1 );
     if (ecalib==0) continue; //skip empty cell (dead one !!! but not yet flaged as)
     
     unsigned int count = 1;
     //define symmetric cellIDs
     unsigned int r = 2*m_centreMap[id.calo()]- id.row() - 1;
     unsigned int c = 2*m_centreMap[id.calo()]- id.col() - 1;
     LHCb::CaloCellID idx = LHCb::CaloCellID( id.calo(), id.area(), id.row(), c);
     LHCb::CaloCellID idy = LHCb::CaloCellID( id.calo(), id.area(), r, id.col());
     LHCb::CaloCellID idxy = LHCb::CaloCellID( id.calo(), id.area(), r, c);
     
     //compute ecalib
     if( /*m_calo->isDead(idx) == false &&*/ m_htemporary->GetBinContent( idx.index() +1 )!=0
         && m_leftright==true
	&& m_calo->hasQuality(idx,CaloCellQuality::Shifted) == false
	&& m_calo->hasQuality(idx,CaloCellQuality::VeryShifted) == false
	&& m_calo->hasQuality(idx,CaloCellQuality::StuckADC) == false
	&& m_calo->hasQuality(idx,CaloCellQuality::OfflineMask) == false) { 
       ecalib += m_htemporary->GetBinContent( idx.index() +1 ); count++; 
     }

     if( /*m_calo->isDead(idy) == false &&*/ m_htemporary->GetBinContent( idy.index() +1 )!=0
         && m_topbottom==true   
        && m_calo->hasQuality(idy,CaloCellQuality::Shifted) == false                                                                                                        
      
        && m_calo->hasQuality(idy,CaloCellQuality::VeryShifted) == false                                                                                                    
      
        && m_calo->hasQuality(idy,CaloCellQuality::StuckADC) == false                                                                                                       
      
        && m_calo->hasQuality(idy,CaloCellQuality::OfflineMask) == false) {
      ecalib += m_htemporary->GetBinContent( idy.index() +1 ); count++;
     }
     if( /*m_calo->isDead(idxy) == false &&*/ m_htemporary->GetBinContent( idxy.index() +1 )!=0
         && m_leftright==true && m_topbottom==true   
        && m_calo->hasQuality(idxy,CaloCellQuality::Shifted) == false                                                                                                        
      
        && m_calo->hasQuality(idxy,CaloCellQuality::VeryShifted) == false                                                                                                    
      
        && m_calo->hasQuality(idxy,CaloCellQuality::StuckADC) == false                                                                                                       
      
        && m_calo->hasQuality(idxy,CaloCellQuality::OfflineMask) == false) { 
       
ecalib += m_htemporary->GetBinContent( idxy.index() +1 ); count++;
     }
     ecalib /= (double) count;
     
     verbose() << " cellID " << id  << " & " << idx << " & " << idy << " & " << idxy << " ecalib " << ecalib << endmsg;
     
    //fill histos
     if (index!=-1) {
      
       m_h2Dview->fillCalo2D(hid_calib, id, ecalib);
     }
     m_hcalibrated->SetBinContent(bin+1, ecalib);
    
   }//end loop on bins
   return StatusCode::SUCCESS;
}
//=============================================================================

