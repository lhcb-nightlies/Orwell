// $Id: CaloEFlowAnalysis.h,v 1.4 2009/11/03 12:37:13 odescham Exp $
#ifndef CALOEFLOWANALYSIS_H
#define CALOEFLOWANALYSIS_H 1

// Include files

// from OMALib
#include "OMAlib/AnalysisTask.h"
// from CaloDet
#include "CaloDet/DeCalorimeter.h"
// from CaloUtils
#include "CaloUtils/Calo2Dview.h"
// from AIDA
#include "AIDA/IHistogram1D.h"
// from ROOT
#include <TFile.h>

/** @class CaloEFlowAnalysis CaloEFlowAnalysis.h
 *
 *
 *  @author Aurelien Martens
 *  @date   2009-04-11
 */
class CaloEFlowAnalysis : public AnalysisTask {
public:

   /// Standard constructor
  CaloEFlowAnalysis( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~CaloEFlowAnalysis( ); ///< Destructor
  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode analyze(std::string& SaveSet,
                     std::string Task) override; ///< Algorithm analyze
  StatusCode finalize() override;    ///< Algorithm finalization

  StatusCode normalize(); // normalization of histograms
  StatusCode calibrate(); // calibration process
  StatusCode coefficients(); // coefficients computation

  LHCb::CaloCellID getCellID(int bin, int calo); //give the cellID knowing the bin number and the calo number

  StatusCode doubtful(); // detection of doubtful cells

  //  StatusCode mean(unsigned int index); // calibration process with global mean
  StatusCode symmetric(int index); // calibration process with symmetric mean
  StatusCode neighbours(int index); // calibration process with mean over neighbours
  StatusCode neighboursBorders(int index); // calibration process with mean over neighbours with treatment at borders
  StatusCode neighboursExtrapolation(int index); // calibration process with mean over neighbours with treatment at borders



protected:

private:

  DeCalorimeter* m_calo;

  //names
  std::string m_hhitsName;
  std::string m_heName;
  std::string m_hetName;
  std::string m_hnbdigitsName;
  std::string m_detectorName;
  std::string m_detector;
  std::string m_name;

  int m_caloID;
  double m_doubtfulThresholdUp;
  double m_doubtfulThresholdDown;


  TFile* m_finput;

  TH1D* m_hhits;
  TH1D* m_he;
  TH1D* m_hnbdigits;
  TH1D* m_hcalibrated;
  TH1D* m_hnormalized;
  TH1D* m_htemporary;

  std::vector<TH1D*> m_refHistos;
  std::vector<TH1D*> m_trendHistos;

  long int m_Nevents;

  std::map< unsigned int, Calo2Dview* > m_h2Dviews;
  Calo2Dview *m_hcoeff;
  Calo2Dview* m_h2Dview;

  int m_nAreas;
  std::vector<std::string> m_areas;
  std::vector< std::string > m_methods;

  int m_kernelDimension;
  std::string m_kernelType;
  std::map< int, double > m_kernel;

  int m_bin1d;

  bool m_split;
  bool m_energy;
  bool m_normalized;
  bool m_1D;
  bool m_diagonal;
  bool m_leftright;
  bool m_topbottom;

  bool m_mctruth;


  std::map<int,unsigned int> m_centreMap;
  std::map<int,int> m_Ncells;
  std::map<int,int> m_FirstCellInX;
  std::map<int,int> m_FirstCellInY;
  std::map<int,int> m_DeadZoneMinInX;
  std::map<int,int> m_DeadZoneMaxInX;
  std::map<int,int> m_DeadZoneMinInY;
  std::map<int,int> m_DeadZoneMaxInY;
};
#endif // CALOEFLOWANALYSIS_H
