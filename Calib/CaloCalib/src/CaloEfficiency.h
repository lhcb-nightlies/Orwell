 // $Id: CaloEfficiency.h,v 1.3 2010-10-11 10:54:03 odescham Exp $
#ifndef CALOEFFICIENCY_H
#define CALOEFFICIENCY_H 1

// Include files
// from Gaudi
#include "Kernel/CaloCellID.h"
#include "CaloUtils/CaloCellIDAsProperty.h" // MUST BE FIRST
//
#include "GaudiAlg/GaudiTupleAlg.h"
#include "CaloDAQ/ICaloDataProvider.h"
#include "CaloDAQ/ICaloL0DataProvider.h"
#include "GaudiKernel/IEventTimeDecoder.h"
#include "Event/L0DUReport.h"
#include "AIDA/IHistogram1D.h"
#include "AIDA/IProfile1D.h"
#include "AIDA/IProfile2D.h"
#include "AIDA/IHistogram2D.h"
#include "CaloUtils/Calo2Dview.h"
#include "CaloKernel/CaloVector.h"
// from Calo
#include "CaloDet/DeCalorimeter.h"

#include <string>


/** @class CaloEfficiency CaloEfficiency.h
 *
 *
 *  @author Alessandro Camboni  ////  acamboni@ecm.ub.es
 *  @date   2009-05-20
 */

class CaloEfficiency : public Calo2Dview {
 public:

  /// Standard constructor
  CaloEfficiency( const std::string& name, ISvcLocator* pSvcLocator );


  virtual ~CaloEfficiency( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization


 protected:

 private:

  int findPattern(const CaloVector<LHCb::CaloAdc>& adcs);

  std::string m_readoutTool;
  std::map<std::string,int> m_slots;

  DeCalorimeter* m_det;

  IEventTimeDecoder* m_odin;

  ICaloDataProvider*  m_daq;


  int m_currPat;
  unsigned int m_cellCheck;
  bool m_statusOnTES;
  bool m_splitParity;
  bool m_ok;
  int m_nPattern;
  bool m_activeCounter;


  double m_thresh;
  std::string m_detectorName;

  CaloVector<int> m_cellPattern;
  //
  std::map<std::string  , std::vector<int>  > m_sequence;
  std::string m_timeSlot;

  // predefined histograms to speed-up filling
  AIDA::IHistogram1D* h0 ;
  AIDA::IHistogram1D* h02 ;
  AIDA::IHistogram1D* h1 ;
  AIDA::IHistogram1D* h2 ;
  AIDA::IHistogram1D* h3 ;
  AIDA::IHistogram1D* h4 ;
  AIDA::IProfile1D* p1 ;
  AIDA::IProfile1D* p2 ;
  unsigned int m_nChannels;
};
#endif
