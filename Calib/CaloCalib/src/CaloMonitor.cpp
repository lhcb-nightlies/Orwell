// $Id: CaloMonitor.cpp,v 1.19 2010-10-11 10:54:51 odescham Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/ToolFactory.h" 
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
// local
#include "CaloUtils/CaloAlgUtils.h"
#include "CaloMonitor.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloChannelStatus
//
// 2009-03-10 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( CaloMonitor )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloMonitor::CaloMonitor( const std::string& type,
                        const std::string& name,
                        const IInterface* parent )
  : GaudiHistoTool ( type, name , parent ),
    m_all(0),
    m_sum(0.),
    m_sum2(0.),
    m_allPin(0),
    m_sumPin(0.),
    m_sumPin2(0.),
    m_channels(),
    m_status(true),
    m_confStatus(true),
    m_book2D(false),
    m_auto(false),
    m_levels(),
    m_iName(""),
    m_id(-1),
    m_hasRange(true){
  declareInterface<CaloMonitor>(this);
  declareProperty("Description"   , m_desc = "");     // optional : monitor text description (default built from setting)
  declareProperty("HistoName"     , m_histo="NONE");     // optional : input histo name (default from CaloCalib histo tree)
  declareProperty("Data"          , m_data     );     // dataName,dataType
  declareProperty("ExpectedRange" , m_range    );      // Expected range for data for PMT (and PIN if not defined specifically)
  declareProperty("ExpectedPINRange", m_pinRange);   // Expected range for the PIN data if different from PMT
  declareProperty("InvertRange"     , m_invert = false  );   // invert range
  declareProperty("DBQualityMask" , m_flag = 0x0 );   // associated quality DB flag 
  declareProperty("AlarmLevels"   , m_levels );       // map of alarm levels
  declareProperty("PINMonitor"    , m_pin = false );  // are pin monitored ?
  declareProperty("PMTMonitor"    , m_pmt = true );   // are pmt monitored ?
  declareProperty("DataView2D"    , m_2dData=true );  // 2D view for data 
  declareProperty("LocationView2D", m_2dTypes     );  // list of location 2Dviews to be produced
  declareProperty("MonitorChannelsAverage", m_ave = false );  // monitor the global average over all calo channels
  declareProperty("MonitorChannelsSpread" , m_aveSpread = false );  // ... or the spread 
  declareProperty("MonitorRelativeChannelSpread" , m_relAveSpread = false );  // ... or the relative spread 
  declareProperty("MonitorChannelsMin2Max" , m_min2max = false );  // ... or the min-to-max range
  declareProperty("GeometricalView" , m_geo = false );  // 2D view type
  declareProperty("Bin1D"           , m_bin = 70 );  // 1D binning
  declareProperty("HistoTop"        , m_dir  );  // top dir
  declareProperty("FitFunction"     ,  m_fitfunc = "gaus");  
  declareProperty("FitParameters"   ,  m_fitParameters);  
  declareProperty("FitBounds"       ,  m_fitBounds);  
  declareProperty("DefaultLED",m_led="LED0");  // default monitoring led in case SplitLeds is active in CaloCalib (expert usage)
  declareProperty("HistDBPage",m_dbPage="");
  declareProperty("PublishAlarm", m_publish=false);
  declareProperty("OnlineTrend" , m_trend=false);
  

  // default
  m_fitBounds = std::make_pair(0.,0.);
  m_parity = "";
  m_histoType="Profile";
  

  // get Calo2DView (cannot inherit)
  m_view = new Calo2Dview( name , serviceLocator() );


  // Do not delegate the DeclareInfo for histos booked into private tools (owner naming)
  StatusCode sc = setProperty("MonitorHistograms","false");
  if( sc.isFailure() )Warning("Unable to set Property",StatusCode::SUCCESS).ignore();
  m_view->setProperty("MonitorHistograms","false");
  
  // Histo (top) dir
  std::string top = name;
  std::string dir = name;
  int index = name.find_last_of(".")+1;
  if(index !=0){
    top =  name.substr(0,index-1)+"/";
    dir =  name.substr(index,std::string::npos);
  }
  m_iName = dir;
  setHistoTopDir( top );
  setHistoDir( dir );
  m_view->setHistoTopDir( top );
  m_view->setHistoDir( dir );

  // set monitorName
  m_monName = dir;

  // set default detectorName
  m_detectorName = LHCb::CaloAlgUtils::CaloNameFromAlg( top );
  if( "Prs" == m_detectorName ){
    m_fitfunc = "PrsGausBin";
  }

  // default input directory (CaloCalib analysis)
  m_dir = m_detectorName + "Calib";

  // allowed data types
  using namespace CaloMonitorHistoType;
  using namespace CaloMonitorDataType;
  m_typeMap[PROFILE].push_back(ENTRIES);
  m_typeMap[PROFILE].push_back(MEAN);
  m_typeMap[PROFILE].push_back(RMS);
  m_typeMap[PROFILE].push_back(RMSoMEAN);
  m_typeMap[PROFILE].push_back(MEANoRMS);
  m_typeMap[PROFILE].push_back(GAINVARIATION);
  m_typeMap[FIT].push_back(ENTRIES);
  m_typeMap[FIT].push_back(MEAN);
  m_typeMap[FIT].push_back(SIGMA);
  m_typeMap[FIT].push_back(SIGMAoMEAN);
  m_typeMap[FIT].push_back(CHI2);
  m_typeMap[FIT].push_back(GAINVARIATION);
  m_typeMap[MOMENTS].push_back(ENTRIES);
  m_typeMap[MOMENTS].push_back(MEAN);
  m_typeMap[MOMENTS].push_back(RMS);
  m_typeMap[MOMENTS].push_back(RMSoMEAN);
  m_typeMap[MOMENTS].push_back(MEANoRMS);
  m_typeMap[MOMENTS].push_back(GAINVARIATION);  
  m_typeMap[ANALYSIS].push_back(OFFSET);
  m_typeMap[ANALYSIS].push_back(OFFSETSHIFT);
  // default 2D histo to be produced
  using namespace CaloMonitorType;
  m_2dTypes.push_back(CaloMonitorType::Name[NEW]);
  m_2dTypes.push_back(CaloMonitorType::Name[KNOWN]);
  m_2dTypes.push_back(CaloMonitorType::Name[RESURRECTED]);
  if(m_2dData)m_2dTypes.push_back(CaloMonitorType::Name[ALL]);

  // Auto-created CaloMonitor
  if( name.find("Auto") != std::string::npos || name.find("AUTO") != std::string::npos)m_auto=true;
  }

StatusCode CaloMonitor::finalize(){
  if( trend() )trendTool()->closeFile();
  StatusCode sc = m_view->initialize();
  sc.ignore();
  return GaudiHistoTool::finalize();
}

StatusCode CaloMonitor::initialize(){
  StatusCode sc = GaudiHistoTool::initialize();
  debug() << "Initialize" << endmsg;

  using namespace CaloMonitorType;
  using namespace CaloMonitorHistoType;
  using namespace CaloMonitorLevel;

  // initialisation
  reset();

  // get DeCalorimeter
  std::string caloLoc = LHCb::CaloAlgUtils::DeCaloLocation( m_detectorName );
  m_calo = getDet<DeCalorimeter>( caloLoc );
  
  // Check setting end set defaults 
  m_status = true;     // processing status
  m_confStatus = true;// configuration status

  m_dataName = CaloMonitorDataName::Name[CaloMonitorDataName::NONE];
  m_dataType = CaloMonitorDataType::Name[CaloMonitorDataType::NONE];
  m_histoType= CaloMonitorHistoType::Name[CaloMonitorHistoType::NONE];
  m_parity = "";
  if( m_data.size() == 2  ){
    m_dataName = (*(m_data.begin()));
    m_dataType = (*(m_data.begin()+1));
    m_histoType= "Profile"; // default
  } else if( m_data.size() == 3){
    m_dataName = (*(m_data.begin()));
    m_dataType = (*(m_data.begin()+1));
    m_histoType = (*(m_data.begin()+2));
  } else if( m_data.size() == 4){
    m_dataName = (*(m_data.begin()));
    m_dataType = (*(m_data.begin()+1));
    m_histoType = (*(m_data.begin()+2));
    m_parity = (*(m_data.begin()+3));
  }else if( !m_auto){
    Warning("The 'Data' property  must have 2, 3 or 4 entries :").ignore();
    Warning(" Data = { DataName , DataType [,DataSource='Profile' , BXParity = ''] };").ignore();
    m_confStatus = false;
  }
  if( !m_auto &&  dataByName( m_dataName ) < 0){
    Warning("The Data Name " + m_dataName + " is unknown ").ignore();
    m_confStatus = false;
  }
  m_min = 0;
  m_max = 0;
  if( m_range.size() == 2 ){
    m_min = (*(m_range.begin()));
    m_max = (*(m_range.begin()+1));
  } else if( !m_auto) {
    m_hasRange = false;
    m_min = -1.E15;
    m_max = +1.E15;    
    info() << "No 'Range' specified for CaloMonitor " << name() << " - assumed all data are OK" << endmsg;
    //Warning("The 'Range' property must have 2 : Data = { min , max }").ignore();
    //m_confStatus=false;
  }
  m_pinMin = 0;
  m_pinMax = 0;
  if( !m_pinRange.empty() ){
    if( m_pinRange.size() == 2 ){
      m_pinMin = (*(m_pinRange.begin()));
      m_pinMax = (*(m_pinRange.begin()+1));
      m_pin = true;
    } else if( !m_auto) {
      Warning("The 'PINRange' property must have 2 : Data = { min , max }").ignore();
      m_confStatus=false;
    }
  }
  if(m_pin && m_pinRange.empty()){
    m_pinMin = m_min;
    m_pinMax = m_max;
  }

  if( !m_auto && "NONE"== m_histo){
    if( parity() != "") m_dir += parity();
    m_histo = "/" + m_dir + "/";
    std::string dir  = ( m_dataName == "PMT/PIN" ) ? "Ratio" : m_dataName;
    if(  histoTypeByName(m_histoType) == PROFILE)
      m_histo += "Profile/"+dir+"/1";
    else if(  histoTypeByName(m_histoType) == MOMENTS)
      m_histo += "Summary/"+dir;
    else if(  histoTypeByName(m_histoType) == FIT)
      m_histo +=  dir + "/[CaloCellID]"; 
  }

  // convert dataType to upper case
  std::string dataType(m_dataType);
  std::string udataType(dataType);
  std::transform( dataType.begin() , dataType.end() , udataType.begin () , ::toupper ) ;
  m_dataType = udataType;

  // Check the configuration
  if ( !m_pin && !m_pmt ){
    Warning("The CaloMonitor '"+name()+"' will monitor neither the PMTs nor the PIN-diodes", StatusCode::SUCCESS).ignore();
    m_confStatus = false;
  }

  if( !m_auto){
    CaloMonitorHistoType::Type histoType = (CaloMonitorHistoType::Type) histoTypeByName(m_histoType);
    std::vector<CaloMonitorDataType::Type> dataTypes = m_typeMap[histoType];
    bool ok = false;
    std::ostringstream styp("");
    styp << "[";
    for(std::vector<CaloMonitorDataType::Type>::iterator typ = dataTypes.begin();typ!=dataTypes.end();++typ){
      styp << CaloMonitorDataType::Name[ *typ ] << " , " ;
      if( dataTypeByName(m_dataType) == *typ){
        ok=true;
        break;
      }  
    }
    if( !ok ){
      m_confStatus = false;
      warning() << "Unknown DataType '"<<m_dataType<<"'" << endmsg;
      warning() << " The allowed data types for " << m_histoType << " histos are : " << styp.str() << "]" << endmsg;
      return StatusCode::SUCCESS;
    } 

    // load long trending tool
    m_trendTool =  tool<ITrendingTool>("TrendingTool","TrendingTool",this);
    // Book 2Dviews
    m_book2D= book2D();
  }
  
  return StatusCode::SUCCESS;
}

void CaloMonitor::openTrendFile(){
  if( m_trend ){
    std::string tfile = "LHCb_"+m_detectorName + iName();
    std::vector<std::string> tags;
    tags.push_back("mean");
    tags.push_back("rms");
    tags.push_back("min2max");
    if( trendTool()->openWrite( tfile, tags) )info() << "opening trending file " << tfile << endmsg;
    else{
      warning() << "cannot open trending file " << tfile << endmsg;
      m_trend = false;
    }      
  }
}



bool CaloMonitor::book2D(){
  using namespace CaloMonitorType;
  StatusCode sc = m_view->initialize();
  m_view->setGeometricalView(m_geo);
  if(sc.isFailure() ){
    Warning("unable to initialiwe Calo2Dviews").ignore();
    m_2dTypes.clear();
    return false;
  }

  for(std::vector<std::string>::iterator it = m_2dTypes.begin();it!=m_2dTypes.end();++it){
    int ityp = typeByName( *it );
    if( ityp < 0 )continue;
    CaloMonitorType::Type type = (CaloMonitorType::Type) ityp;
    if( !hasDBQualityMask() && type != NEW && type != ALL ){m_2d[type]=false;continue;}
    m_2d[type] = true;
    std::ostringstream loc("");
    std::string dname = CaloMonitorDataName::Name[ dataByName( m_dataName ) ];
    std::string dtype = CaloMonitorDataType::Name[ dataTypeByName( m_dataType ) ];
    if( type == ALL){
      m_view->bookCalo2D( dname + dtype +"2dView", 
                          m_dataType + " " + m_dataName + " 2D view", m_detectorName );
      m_h2D[type]= m_view->histo2D( HistoID(dname + dtype +"2dView") );
      m_h2DName[type] = std::string( m_monName +"/"+dname + dtype +"2dView");
    }
    else {
      m_view->bookCalo2D( CaloMonitorType::Name[ityp] + "Locations", 
                          " Location of "+ CaloMonitorType::Name[ityp] + " problematic channels  detected by " 
                          + name() + " monitor" , m_detectorName );
      m_h2D[type]= m_view->histo2D( HistoID(CaloMonitorType::Name[ityp] + "Locations"));
      m_h2DName[type] = std::string( std::string( m_monName +"/"+CaloMonitorType::Name[ityp] + "Locations") );
    }
  }
  return true;
}



void CaloMonitor::reset(){
  // reset monitors
  m_vMin = 999999.;
  m_vMinPin = 999999.;
  m_vMax = -999999.;
  m_vMaxPin = -999999.;
  m_evMin = 0;
  m_evMinPin = 0;
  m_evMax = 0;
  m_evMaxPin = 0;
  m_all  = 0;
  m_sum    = 0.;
  m_sum2    = 0.;
  m_allPin  = 0;
  m_sumPin    = 0.;
  m_sumPin2    = 0.; 
  m_channels.clear();
  m_val.clear();
  for( int i = 0 ; i < CaloMonitorType::Number ; ++i ){
    CaloMonitorType::Type type =(CaloMonitorType::Type) i;
    m_alarmLevel[ type ] = CaloMonitorLevel::OK;
    m_channels[ type].clear();
  }

  // reset histo2D
  for(std::map<CaloMonitorType::Type, AIDA::IHistogram2D*>::iterator h2Map = m_h2D.begin() ; h2Map != m_h2D.end() ; ++h2Map){
    AIDA::IHistogram2D* h2D = (*h2Map).second;
    CaloMonitorType::Type type = (*h2Map).first;
    if( NULL != h2D ){
      debug() << "Reseting Monitor " << name() << " histo " << m_h2DName[type] << endmsg;
      h2D->reset();
    }
  }
}

std::string CaloMonitor::description(){
  if( "" == m_desc){
    std::ostringstream desc("");
    desc <<  m_dataName << " "<< m_dataType ;
    if(m_ave)desc << " AVERAGE ";
    else if(m_aveSpread)desc << " SPREAD ";
    else if(m_relAveSpread)desc << " SPREAD/AVERAGE ";
    else if(m_min2max)desc << " Min-to-Max ";
    else desc << "" ;
    desc << " - Expected range : ";    
    m_invert ? desc << " NOT in " : desc << " in ";
    if( m_hasRange )
      desc << "[" << m_min << " , " << m_max << "] ";
    else
      desc << "[ full range ]";
    if( m_pmt && !m_pin )desc<< " for PMTs " ;
    if( m_pin && !m_pmt)desc << " for PINs";
    if(m_pin && m_pmt && m_min == m_pinMin && m_max == m_pinMax)desc  << " for PMts & PINs ";
    if(m_pin && m_pmt && (m_min != m_pinMin || m_max != m_pinMax)){
      desc << " for PMTs ( ["<< m_pinMin << " , " << m_pinMax << "] for PINs)";
    }
    m_desc = desc.str();
  }  
  return m_desc;
}


void CaloMonitor::addToStatus( LHCb::CaloCellID id, double value, double error ){  
  using namespace CaloMonitorType;
  using namespace CaloMonitorLevel;
  if( !m_status)return;
  if( !m_calo->valid(id) ) return;
  if( !pmtMonitor() && !id.isPin()  )return;
  if( !pinMonitor() && id.isPin()   )return;


  if( !m_book2D)book2D();
  // book all values
  m_val.addEntry( value, id );
  m_valError.addEntry( error, id );
  m_channels[ALL].push_back( id );
  std::string dname = CaloMonitorDataName::Name[ dataByName( m_dataName ) ];
  std::string dtype = CaloMonitorDataType::Name[ dataTypeByName( m_dataType ) ];
  if(m_2d[ALL])m_view->fillCalo2D( dname + dtype +"2dView" , id , value );

  // counters
  if( !id.isPin() ){
    m_all++;
    m_sum += value;  
    m_sum2 += value*value;
    if(value < m_vMin){m_vMin = value; m_evMin = error;}
    if(value > m_vMax){m_vMax = value; m_evMax = error;}
  }else{
    m_allPin++;
    m_sumPin += value;  
    m_sumPin2 += value*value;
    if(value < m_vMinPin){m_vMinPin = value; m_evMinPin = error;}
    if(value > m_vMaxPin){m_vMaxPin = value; m_evMaxPin = error;}
  }
  
  if( outsideRange(id, value) ){
    m_channels[NEWANDKNOWN].push_back(id);
    if(m_2d[NEWANDKNOWN])m_view->fillCalo2D( "NEWANDKNOWNLocations" , id , 1 );
    
    if( hasDBQualityMask() ){
      if( m_calo->hasQuality( id , (CaloCellQuality::Flag) m_flag ) ) {
        m_channels[KNOWN].push_back(id);
        if(m_2d[KNOWN])m_view->fillCalo2D( "KNOWNLocations" , id , 1 );
      } else {
        m_channels[NEW].push_back(id);
        if(m_2d[NEW])m_view->fillCalo2D( "NEWLocations" , id , 1 );
      }     
    } else {
      m_channels[NEW].push_back(id);
      if(m_2d[NEW])m_view->fillCalo2D( "NEWLocations" , id , 1 );
    }
  } else {
    if( hasDBQualityMask() ){
      if(m_calo->hasQuality( id , (CaloCellQuality::Flag) m_flag )){
        m_channels[RESURRECTED].push_back(id);
        if(m_2d[RESURRECTED])m_view->fillCalo2D( "RESURRECTEDLocations" , id , 1 );
      }
    }
  } 


  // Check alarms
  for( int i = 0 ; i < CaloMonitorType::Number ; ++i ){
    CaloMonitorType::Type type = (CaloMonitorType::Type) i;
    m_alarmLevel[ type ] = CaloMonitorLevel::OK;
    int lmax = 0;
    int tmax = -1;
    for( std::map<int,std::string>::iterator il = m_levels.begin() ; il != m_levels.end(); ++il){
      int threshold =  (*il).first ;
      int level = levelByName( (*il).second );
      //      if( (int) counter(type) >= threshold && level > lmax)lmax = level;
      if( (int) counter(type) >= threshold && (int) counter(type) > tmax){
        lmax = level;
        tmax = threshold;
      }


      if( m_ave && !outsideRange( id , average() ) )lmax = CaloMonitorLevel::OK;
      else if( m_aveSpread && !outsideRange( id , spread()  ) )lmax = CaloMonitorLevel::OK;
      else if( m_relAveSpread && !outsideRange( id , relativeSpread() ) )lmax = CaloMonitorLevel::OK;
      else if( m_min2max && !outsideRange( id , min2max() ) )lmax = CaloMonitorLevel::OK;

      // @ToDo : declare global quality in CaloDet and check the average is resurrected or not
      if( overall() && (type == CaloMonitorType::KNOWN || type == CaloMonitorType::RESURRECTED ))lmax  = CaloMonitorLevel::OK;
    }
    m_alarmLevel[(CaloMonitorType::Type) i] = (CaloMonitorLevel::Level) lmax;
  }
}




int CaloMonitor::levelByName(std::string level ){ 
  std::string ulevel( level );
  std::transform( level.begin() , level.end() , ulevel.begin () , ::toupper ) ;
  if( ulevel == "WARNING")return CaloMonitorLevel::Warning;
  else if( ulevel == "ALARM")return CaloMonitorLevel::Alarm;
  else if( ulevel == "FATAL")return CaloMonitorLevel::Fatal;
  else if( ulevel == "OK")return CaloMonitorLevel::OK;
  else{
    Warning("Unknown CaloMonitorLevel::Level = '" + level +"'").ignore();
    m_status = false;
  }
  return -1;
}

int CaloMonitor::typeByName(std::string type){
  std::string utype( type );
  std::transform( type.begin() , type.end() , utype.begin () , ::toupper ) ;
  if( utype == "NEW")return CaloMonitorType::NEW;
  else if( utype == "KNOWN")return CaloMonitorType::KNOWN;
  else if( utype == "RESURRECTED")return CaloMonitorType::RESURRECTED;
  else if( utype == "ALL")return CaloMonitorType::ALL;
  else if( utype == "NEWANDKNOWN")return CaloMonitorType::NEWANDKNOWN;
  else{
    Warning("Unknown CaloMonitorType::Type= '" + type +"'").ignore();
  }
  return -1;
}

int CaloMonitor::histoTypeByName(std::string type){
  std::string utype( type );
  std::transform( type.begin() , type.end() , utype.begin () , ::toupper ) ;
  if( utype == "PROFILE")return CaloMonitorHistoType::PROFILE;
  else if( utype == "MOMENTS")return CaloMonitorHistoType::MOMENTS;
  else if( utype == "FIT")return CaloMonitorHistoType::FIT;
  else if( utype == "ANALYSIS")return CaloMonitorHistoType::ANALYSIS;
  else{
    Warning("Unknown CaloMonitorHistoType::Type= '" + type +"'").ignore();
  }
  return -1;
}

int CaloMonitor::dataTypeByName(std::string type){
  std::string utype( type );
  std::transform( type.begin() , type.end() , utype.begin () , ::toupper ) ;
  if( utype == "ENTRIES")return CaloMonitorDataType::ENTRIES;
  else if( utype == "MEAN")return CaloMonitorDataType::MEAN;
  else if( utype == "RMS")return CaloMonitorDataType::RMS;
  else if( utype == "SIGMA")return CaloMonitorDataType::SIGMA;
  else if( utype == "RMS/MEAN")return CaloMonitorDataType::RMSoMEAN;
  else if( utype == "MEAN/RMS")return CaloMonitorDataType::MEANoRMS;
  else if( utype == "SIGMA/MEAN")return CaloMonitorDataType::SIGMAoMEAN;
  else if( utype == "GAINVARIATION")return CaloMonitorDataType::GAINVARIATION;
  else if( utype == "CHI2")return CaloMonitorDataType::CHI2;
  else if( utype == "OFFSET")return CaloMonitorDataType::OFFSET;
  else if( utype == "OFFSETSHIFT")return CaloMonitorDataType::OFFSETSHIFT;
  else{
    Warning(iName() + " : Unknown CaloMonitorDataType::Type= '" + type +"'").ignore();
  }
  return -1;
}

int CaloMonitor::dataByName(std::string type){
  std::string utype( type );
  std::transform( type.begin() , type.end() , utype.begin () , ::toupper ) ;
  if( utype == "PEDESTAL")return CaloMonitorDataName::PEDESTAL;
  else if( utype == "LED")return CaloMonitorDataName::SIGNAL;
  else if( utype == "SIGNAL")return CaloMonitorDataName::SIGNAL;
  else if( utype == "RATIO")return CaloMonitorDataName::RATIO;
  else if( utype == "PMT/PIN")return CaloMonitorDataName::RATIO;
  else if( utype == "DESERT")return CaloMonitorDataName::DESERT;
  else{
    Warning("Unknown CaloMonitorDataName::Name= '" + type +"'").ignore();
  }
  return -1;
}


std::string CaloMonitor::alarmLogger(bool skipKnown,bool skipOK,bool printout){
  using namespace  CaloMonitorType;
  using namespace  CaloMonitorLevel;
  std::ostringstream olog("");
  if( skipKnown && hasAlarm(KNOWN) && !hasAlarm(NEW) && !hasAlarm(RESURRECTED) )return olog.str();
  if( skipOK && !hasAlarm())return olog.str();
  

  std::ostringstream channel("");
  if( pinMonitor() && pmtMonitor() ){
    channel << " PMT or PIN " ;
  } else if( pinMonitor() ){
    channel << " PIN ";
  } else {
    channel << " PMT ";
  }

  olog  << "+-------------------------------------------------------------------------------------------+"  << std::endl;
  olog  << "|" << format("%62s", std::string(" monitor : " + name() ).c_str() ) << format("%30s","|") << std::endl;
  olog  << "|" << format("%91s", std::string( description()  ).c_str() ) << format("%1s","|") << std::endl;
  olog  << "|" << format("%62s", std::string( alarmSetting()  ).c_str() ) << format("%30s","|") << std::endl;
  std::string type;
  if( dbQualityMask() != 0x0){
    std::string sflag ="( ";
    int kk=0;
    for(int iflag=0 ; iflag < CaloCellQuality::Number-1; ++iflag){
      if( (dbQualityMask() & (1 << iflag) ) == 0 )continue;
      if(kk>0) sflag += " || ";
      sflag += CaloCellQuality::Name[iflag+1];
      kk++;
    }    
    sflag += " )";
    olog  << "|" << format("%30s", "Associated condDB Quality mask : ") 
          << format("0x%04X", dbQualityMask()) <<  " -> " <<format("%40s", sflag.c_str() )
          << format("%9s","|") << std::endl;
  }
  else 
    olog  << "|" << format("%80s", " NO Associated condDB Quality mask - report all (new and known) problems ") 
        << format("%12s","|") << std::endl;

  std::string hFit = "";
  if(  histoTypeByName(m_histoType) == CaloMonitorHistoType::FIT)hFit += " (" + m_fitfunc + ")";
  olog << "|" 
      << format("%80s" , std::string("Analyzing the " + m_histoType+ " histo [" + histoName() + "] "  + hFit).c_str()) 
      <<  format("%12s","|") << std::endl;
  if( !m_confStatus)olog << "|" << format("%72s" , " *** badly configured *** ")  <<  format("%20s","|") << std::endl;
  else if( !m_status)olog << "|" << format("%72s" , " *** Status Unknown *** ")  <<  format("%20s","|") << std::endl;
  olog  << "+--------+----------------------------------------------------------------------------------+"  << std::endl;

  if( !m_status){
    if(printout)info() << olog.str() << endmsg;
    return olog.str();
  }  
  std::string typeName ="";
  for( int i = 0 ; i < CaloMonitorType::ALL ; ++ i){
    CaloMonitorType::Type type = (CaloMonitorType::Type) i;
    std::string where =  " outside";
    if( type == RESURRECTED)where=" whithin";
    if( dbQualityMask() == 0x0 && type != NEW)continue;
    if( dbQualityMask() != 0x0) typeName = std::string(CaloMonitorType::Name[type]);
    if( !skipOK || hasAlarm( type ) )
      olog  << "|" << format("%7s",std::string( alarmLevelName( type ) ).c_str() ) << " | " 
          << format("%5i" ,  counter(type)) << " / " << format("%5i",  counter(ALL))
          << format("%13s",  typeName.c_str())
          << format("%5s" ,   m_detectorName.c_str())
          << format("%13s",  channel.str().c_str()) ;
    if(m_ave) 
      olog << format( "%9s","average");
    else if(m_aveSpread) 
      olog << format( "%9s","spread");
    else if(m_aveSpread) 
      olog << format( "%9s","spread/average");
    else if(m_min2max) 
      olog << format( "%9s","max-min");
    else 
      olog << format( "%9s","channels");
    olog << format("%10s", std::string( where + " the expected range").c_str()) << " |"
        <<std::endl;    
  }
  std::ostringstream aLevel("");
  std::ostringstream sLevel("");
  std::ostringstream rsLevel("");
  std::ostringstream mmLevel("");
  if(m_ave){
    if( hasAlarm(NEW) )aLevel << alarmLevelName( NEW ) ;
    else if( hasAlarm(KNOWN))aLevel << alarmLevelName( KNOWN ) ;
    else if( hasAlarm(RESURRECTED) )aLevel << alarmLevelName( RESURRECTED ) ;
    else aLevel <<  CaloMonitorLevel::Name[OK];
  }
  else if(m_aveSpread){
    if( hasAlarm(NEW) )sLevel << alarmLevelName( NEW ) ;
    else if( hasAlarm(KNOWN))sLevel << alarmLevelName( KNOWN ) ;
    else if( hasAlarm(RESURRECTED) )sLevel << alarmLevelName( RESURRECTED ) ;
    else sLevel <<  CaloMonitorLevel::Name[OK] ;
  }
  else if(m_relAveSpread){
    if( hasAlarm(NEW) )rsLevel << alarmLevelName( NEW ) ;
    else if( hasAlarm(KNOWN))rsLevel << alarmLevelName( KNOWN ) ;
    else if( hasAlarm(RESURRECTED) )rsLevel << alarmLevelName( RESURRECTED ) ;
    else rsLevel << CaloMonitorLevel::Name[OK] ;
  }
  else if(m_min2max){
    if( hasAlarm(NEW) )mmLevel << alarmLevelName( NEW ) ;
    else if( hasAlarm(KNOWN))mmLevel << alarmLevelName( KNOWN ) ;
    else if( hasAlarm(RESURRECTED) )mmLevel << alarmLevelName( RESURRECTED ) ;
    else mmLevel << CaloMonitorLevel::Name[OK] ;
  }
  std::ostringstream aPin("");
  std::ostringstream sPin("");
  std::ostringstream rsPin("");
  std::ostringstream mmPin("");
  if(m_pin){
    aPin << "   ("
           << format("%4i",  m_allPin )  
           << format("%7s",  " PIN : " )
           <<  format("%10.2g", average(true)) 
           << "  )"
           << format("%6s","|");
    sPin << "   ("
           << format("%4i",  m_allPin )  
           << format("%7s",  " PIN : " )
           <<  format("%10.2g", spread(true)) 
           << "  )"
           << format("%6s","|");
    rsPin << " ("
           << format("%4i",  m_allPin )  
           << format("%7s",  " PIN : " )
           <<  format("%10.2g", relativeSpread(true)*100) 
           << " %)"
           << format("%6s","|");
    
    mmPin << "   ("
          << format("%4i",  m_allPin )  
          << format("%7s",  " PIN : " )
          <<  format("%10.2g", min2max(true)) 
          << "  )"
          << format("%6s","|");
    
  }else{
    aPin << format("%34s","|");
    sPin << format("%34s","|");
    rsPin << format("%32s","|");
    mmPin << format("%34s","|");
  }  
  
  olog  << "|" << format("%7s", aLevel.str().c_str() ) << " | "  
      << format("%25s"," average over ")
      << format("%5i",  m_all )
      << format("%8s",  " PMT : " )
      << format("%10.3g", average()) 
      << aPin.str()
      << std::endl;
  olog  << "|" << format("%7s", sLevel.str().c_str() ) << " | "  
      << format("%25s"," spread over ")
      << format("%5i",  m_all )
      << format("%8s",  " PMT : " )
      << format("%10.3g", spread()) 
      << sPin.str() 
      << std::endl;
  olog  << "|" << format("%7s", rsLevel.str().c_str() ) << " | "  
      << format("%25s"," spread/average over ")
      << format("%5i",  m_all)
      << format("%8s",  " PMT : " )
      << format("%10.3g", relativeSpread()*100.) << " %" 
      << rsPin.str()
      << std::endl;
  olog  << "|" << format("%7s", mmLevel.str().c_str() ) << " | "  
      << format("%25s"," max-min over ")
      << format("%5i",  m_all)
      << format("%8s",  " PMT : " )
      << format("%10.2g", min2max())  
      << mmPin.str()
      << std::endl;
  olog  << "+--------+----------------------------------------------------------------------------------+"  << std::endl;

  // verbose output
  if( msgLevel(MSG::VERBOSE) ){
    std::vector<LHCb::CaloCellID>& chan = channels(NEW);
    for( std::vector<LHCb::CaloCellID>::iterator it = chan.begin();it!=chan.end();++it){
      LHCb::CaloCellID cell = *it;
      int feb   = m_calo->cardNumber( cell );
      int crate = m_calo->cardCrate( feb );
      int slot  = m_calo->cardSlot( feb );
      verbose() << cell  << " (id.index = " << cell.index() << ")"  
                << " NEW PROBLEMATIC CHANNELS => (crate/slot/index) = (" << crate << "/" << slot <<"/"<<cell.index() 
                <<") | " <<  value(cell)  << " |" << endmsg;  
    }
  }

  if(printout)info() << olog.str() << endmsg;
  return olog.str();
}
bool CaloMonitor::outsideRange(LHCb::CaloCellID id , double value){  
  if( !m_hasRange ) return false;
  if( !id.isPin() ){
    if( value < m_min || value > m_max )return m_invert ? false : true;
  } else {
    if( value < m_pinMin || value > m_pinMax )return m_invert ? false : true;
  }
  return m_invert ? true : false;
}

AIDA::IHistogram1D* CaloMonitor::calo1D(std::string& name,bool pin){
  std::string dname = CaloMonitorDataName::Name[ dataByName( m_dataName ) ];
  std::string dtype = CaloMonitorDataType::Name[ dataTypeByName( m_dataType ) ];
  double xmin = pin ? m_vMinPin : m_vMin;
  double xmax = pin ? m_vMaxPin : m_vMax;
  double delta = (xmax - xmin)*0.15;  
  double min = ( xmin >= 0 && (xmin - delta) < 0) ? 0. : xmin-delta;
  double max = ( xmax <= 0 && (xmax + delta) > 0) ? 0. : xmax+delta;
  int bin = pin ? m_bin/2 : m_bin;
  std::string chan = pin ? "PINs" : "PMTs";
  AIDA::IHistogram1D* h1 = NULL;
  HistoID lun = HistoID(dname + dtype + chan + "1d");
  if( histoExists ( lun ) )histo1D(lun)->reset();
  std::vector<LHCb::CaloCellID> channels = m_channels[CaloMonitorType::ALL];
  debug() << "producing histo " << lun << endmsg;
  for(std::vector<LHCb::CaloCellID>::iterator it = channels.begin();channels.end()!=it;++it){
    LHCb::CaloCellID id = *it;
    if( pin && !id.isPin() )continue;
    if( !pin && id.isPin() )continue;
    double value = m_val[id];
    h1 = plot1D ( value , lun, 
                  m_dataName + " " + m_dataType + " for " + m_detectorName + " " +chan + " channels" , min, max, bin);
  }
  name = name + std::string( m_monName +"/"+ std::string(lun) );
  if(h1!=NULL){
    TH1D* th = Gaudi::Utils::Aida2ROOT::aida2root( h1 );
    th->SetName(name.c_str());
    th->SetCanExtend(TH1::kXaxis);
  }
  
  return h1;
}

//=============================================================================
// Destructor
//=============================================================================
CaloMonitor::~CaloMonitor() {}
//=============================================================================




