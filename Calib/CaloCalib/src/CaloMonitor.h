// $Id: CaloMonitor.h,v 1.16 2010-10-11 10:54:51 odescham Exp $
#ifndef CALOMONITOR_H
#define CALOMONITOR_H 1
// Include files
#include "GaudiAlg/GaudiHistoTool.h"
#include "GaudiAlg/Fill.h"
#include "Kernel/CaloCellID.h"
#include "CaloUtils/Calo2Dview.h"
#include "CaloDet/DeCalorimeter.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "Trending/ITrendingTool.h"

static const InterfaceID IID_CaloMonitor ( "CaloMonitor", 1, 0 );

/** @class CaloMonitor CaloMonitor.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2009-03-10
 */
namespace CaloMonitorLevel{
  enum Level {
    OK      =  0,
    Warning =  1,
    Alarm   =  2,
    Fatal   =  3
  };
  static const int Number = 4;
  static const std::string Name[Number] = {"OK", "Warning","Alarm","Fatal"};
}
namespace CaloMonitorType{
  enum Type {
    NEW= 0 ,
    KNOWN ,
    RESURRECTED   ,
    ALL,
    NEWANDKNOWN
  };
  static const int Number = 5;
  static const std::string Name[Number] = {"NEW","KNOWN","RESURRECTED","ALL","NEWANDKNOWN"};
}
namespace CaloMonitorHistoType{
  enum Type {
    PROFILE= 0 ,
    MOMENTS ,
    FIT     ,
    ANALYSIS,
    NONE
  };
  static const int Number = 6;
  static const std::string Name[Number] = {"PROFILE","MOMENTS","FIT","ANALYSIS","NONE"};
}

namespace CaloMonitorDataType{
  enum Type {
    ENTRIES= 0 ,
    MEAN ,
    RMS  ,
    RMSoMEAN,
    MEANoRMS,
    SIGMA,
    SIGMAoMEAN,
    GAINVARIATION,
    CHI2,
    OFFSET,
    OFFSETSHIFT,
    NONE
  };
  static const int Number = 12;
  static const std::string Name[Number] = {"ENTRIES","MEAN","RMS","RMSoMEAN","MEANoRMS",
                                           "SIGMA","SIGMAoMEAN","GAINVARIATION","CHI2","OFFSET","OFFSETSHIFT","NONE"};
}


namespace CaloMonitorDataName{
  enum Name {
    PEDESTAL= 0 ,
    SIGNAL ,
    RATIO  ,
    DESERT,
    NONE
  };
  static const int Number = 5;
  static const std::string Name[Number] = {"PEDESTAL","SIGNAL","RATIO","DESERT","NONE"};
}




class CaloMonitor : public GaudiHistoTool {
public:


  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_CaloMonitor; }

  /// Standard constructor
  CaloMonitor( const std::string& type,
              const std::string& name,
              const IInterface* parent);

  virtual ~CaloMonitor( ); ///< Destructor
  StatusCode initialize() override;
  StatusCode finalize() override;


  //
  // getters
  std::string alarmLevelName(CaloMonitorType::Type type){ return CaloMonitorLevel::Name[m_alarmLevel[type]]; }
  int alarmLevel(CaloMonitorType::Type type){ return m_alarmLevel[type]; }
  std::map<int, std::string > alarmThresholds(){return m_levels;}
  std::string alarmSetting(){
    std::string def ="";
    if( m_levels.size() == 0 ){
      def = " [ no alarm setting ]";
    }
    else{
      def = " [ | ";
      for( std::map<int,std::string>::const_iterator i = m_levels.begin(); i != m_levels.end() ; ++i){
        def += i->second + " : " + Gaudi::Utils::toString( i->first ) +" | ";
      }
      def+=" ]";
    }
    return def;
  }


  bool hasAlarm(CaloMonitorType::Type type )   {return m_alarmLevel[ type ] > 0 ;}
  bool hasAlarm(){
    for(int i = 0 ; i < CaloMonitorType::RESURRECTED ; ++i ){
      if(hasAlarm((CaloMonitorType::Type) i))return true;
    }
    return false;
  }
  unsigned int counter(CaloMonitorType::Type type ){
    if ( overall() )return ( hasAlarm(type) || type == CaloMonitorType::ALL ) ? 1 : 0;
    return channels(type).size();
  }


  const std::vector<double>  fitParameters(){return m_fitParameters;}
  std::pair<double,double>  fitBounds(){return m_fitBounds;}
  std::string fitFunction(){return m_fitfunc;}
  std::vector<LHCb::CaloCellID>& channels(CaloMonitorType::Type type ){return m_channels[type];}
  bool has2Dview(CaloMonitorType::Type type ){return m_2d[type];}
  CaloVector<double>& values(){return m_val;}
  double value( LHCb::CaloCellID id ){return m_val[id];}
  double valueError( LHCb::CaloCellID id ){return m_valError[id];}

  AIDA::IHistogram2D* calo2D(std::string& name,CaloMonitorType::Type type ){
    name +=  m_h2DName[type];
    AIDA::IHistogram2D* h2D = m_h2D[type];
    Gaudi::Utils::Aida2ROOT::aida2root( h2D  )->SetName(name.c_str());
    return h2D;
  }
  AIDA::IHistogram1D* calo1D(std::string& name, bool pin = false );

  std::pair<double,double> range(){return std::make_pair( m_min,m_max);}
  std::string histoName(){return m_histo;}
  int dbQualityMask(){return m_flag;}
  bool hasDBQualityMask(){return (m_flag != 0x0);}
  bool pinMonitor(){return m_pin;}
  bool pmtMonitor(){return m_pmt;}
  unsigned int nChannels(bool pin = false){
    return (pin) ? m_allPin :  m_all;
  }

  double min2max(bool pin = false){
    if(m_val.size()==0)return 0.;
    if( !pin )return m_vMax - m_vMin;
    else return m_vMaxPin - m_vMinPin;
  }
  double min2maxError(bool pin = false){
    if(m_valError.size() == 0)return 0.;
    if( !pin )return sqrt(m_evMax*m_evMax + m_evMin*m_evMin);
    else return sqrt(m_evMaxPin*m_evMaxPin + m_evMinPin*m_evMinPin);
  }
  double average(bool pin = false){
    if( !pin )return ( m_all > 0) ? m_sum/(double) m_all : 0.;
    return ( m_allPin > 0) ? m_sumPin/(double) m_allPin : 0.;
  }
  double spread(bool pin = false){
    if( !pin && m_all == 0 )return 0;
    if(  pin && m_allPin == 0 )return 0;
    double ave2 = average(pin)*average(pin);
    double rms = (pin) ? (m_sumPin2/(double)m_allPin) - ave2 : (m_sum2/(double)m_all) - ave2;
    rms = (rms>0.) ? sqrt(rms) : 0.;
    return rms ;
  }
  double relativeSpread(bool pin = false){
    double ave = average(pin);
    return (ave>0) ? spread(pin)/ave : 0.;
  }
  std::string iName(){ return m_iName; }
  bool autoConf(){ return m_auto; }
  bool status(){ return m_status && m_confStatus; }
  bool confStatus(){ return m_confStatus; }
  int histoType(){ return histoTypeByName( m_histoType ); }
  int dataType(){return dataTypeByName(m_dataType);}
  int dataName(){return dataByName(m_dataName);}
  int levelByName(std::string level);
  int typeByName(std::string type);
  int histoTypeByName(std::string type);
  int dataTypeByName(std::string type);
  int dataByName(std::string type);
  std::string alarmLogger(bool skipKnown = false, bool skipOK = false,bool printout=false);
  std::string defaultLED(){return m_led ;}
  std::string histDBPage(){ return m_dbPage;}



  // setters
  void setDataName(std::string dName, bool force = true){
    if(force  || m_dataName ==CaloMonitorDataName::Name[CaloMonitorDataName::NONE] )m_dataName=dName;}
  void setDataType(std::string dType, bool force = true){
    if(force  || m_dataType == CaloMonitorDataType::Name[CaloMonitorDataType::NONE] )m_dataType=dType;}
  void setHistoName(std::string hName, bool force = true){if(force || m_histo == "NONE")m_histo=hName;}
  void setHistoType(std::string hType, bool force = true){
    if(force || m_histoType== CaloMonitorHistoType::Name[CaloMonitorHistoType::NONE] )m_histoType=hType;}
  void setRange(double min, double max, bool force = true){
    if(force|| m_range.empty() ){
      m_range.push_back(min);
      m_range.push_back(max);
      m_min=min;
      m_max=max;
    }
  }
  void reset();
  void addToStatus(LHCb::CaloCellID id, double value, double error=0. );
  void setStatus(bool status){ m_status=status; }
  void setConfStatus(bool status){ m_confStatus=status; }
  bool overall(){return (m_ave||m_aveSpread||m_relAveSpread||m_min2max);};
  std::string parity(){return m_parity;}
  int parityBit(){
    if(m_parity=="OddBX")return 1;
    else if(m_parity=="EvenBX")return 2;
    return 0;
  }
  bool publishAlarm(){return m_publish;}
  bool book2D();
  std::string description();

  void setID(int id){m_id = id ;}
  int  ID(){return m_id;}
  bool outsideRange(LHCb::CaloCellID id , double value);
  bool hasRange(){ return m_hasRange;}
  bool trend(){return m_trend;}
  void openTrendFile();
  ITrendingTool* trendTool(){return m_trendTool;}



protected:

private:
  unsigned int m_all;
  double m_sum;
  double m_sum2;
  unsigned int m_allPin;
  double m_sumPin;
  double m_sumPin2;
  std::map<CaloMonitorType::Type,std::vector<LHCb::CaloCellID> >  m_channels;
  bool m_status;
  bool m_confStatus;
  // properties
  std::string m_desc;
  std::string m_histo;
  std::string m_histoType;
  std::vector<std::string> m_data;
  std::vector<double> m_range;
  std::vector<double> m_pinRange;
  bool m_invert;
  int m_flag;
  bool m_pin;
  bool m_pmt;
  std::vector<std::string> m_2dTypes;
  bool m_2dData;
  bool m_ave;
  bool m_aveSpread;
  bool m_relAveSpread;
  bool m_min2max;
  bool m_geo;
  int  m_bin;
  std::string m_dir;
  std::string m_led;
  //
  double m_min;
  double m_max;
  double m_pinMin;
  double m_pinMax;
  std::string m_dataType;
  std::string m_dataName;
  Calo2Dview* m_view;
  std::map<CaloMonitorType::Type, CaloMonitorLevel::Level> m_alarmLevel;
  std::map<CaloMonitorType::Type, AIDA::IHistogram2D*> m_h2D;
  std::map<CaloMonitorType::Type, std::string > m_h2DName;
  std::map<CaloMonitorType::Type,bool> m_2d;
  CaloVector<double>  m_val;
  CaloVector<double>  m_valError;
  std::string m_detectorName;
  std::map<CaloMonitorHistoType::Type,std::vector<CaloMonitorDataType::Type> > m_typeMap;
  DeCalorimeter* m_calo ;
  double m_vMin,m_vMax;
  double m_vMinPin,m_vMaxPin;
  double m_evMin,m_evMax;
  double m_evMinPin,m_evMaxPin;
  std::string m_fitfunc;
  std::pair<double,double> m_fitBounds;
  std::vector<double> m_fitParameters;
  std::string m_monName;
  std::string m_dbPage;
  std::string m_parity;
  bool m_publish;
  bool m_book2D;
  bool m_auto;
  std::map<int, std::string > m_levels;
  std::string m_iName;
  int m_id;
  bool m_hasRange;
  bool m_trend;
  ITrendingTool* m_trendTool;
};
#endif // CALOSMONITOR_H



