#ifndef CALOPMSIGNAL_H
#define CALOPMSIGNAL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/GaudiAlgorithm.h"
// from LHCb
#include "CaloDAQ/ICaloDataProvider.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloKernel/CaloVector.h"

/** @class CaloPMSignal CaloPMSignal.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2009-01-13
 */
class CaloPMSignal : public GaudiHistoAlg {

public:
  /// Standard constructor
  CaloPMSignal( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~CaloPMSignal( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization
  StatusCode bookHistos(std::string slot);

protected:
  //

private:


  // set-up
  DeCalorimeter* m_calo;
  std::string m_detectorName;
  std::string m_readoutTool;
  //
  std::map<std::string,bool> m_ok;
  std::map<std::string,ICaloDataProvider*>  m_daqs;
  std::vector<std::string> m_slots;
  // Counters
  long m_k[20];
  unsigned int m_cells;
  unsigned int m_cards;
  int m_refresh;
  int m_evt;
  bool m_average;
  std::map<int, std::pair<int,int> > m_driver;
  bool m_split;
};
#endif // CALOPMSIGNAL_H


