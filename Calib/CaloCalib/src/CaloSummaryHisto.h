// $Id: CaloSummaryHisto.h,v 1.1 2009-10-27 16:49:40 odescham Exp $
#ifndef CALOSUMMARYHISTO_H 
#define CALOSUMMARYHISTO_H 1

// Include files
#include "CaloKernel/CaloVector.h"
#include "AIDA/IHistogram1D.h"
#include "AIDA/IProfile1D.h"

/** @class CaloSummaryHisto CaloSummaryHisto.h
 *  
 *
 *  @author Olivier Deschamps
 *  @date   2009-10-12
 */
class CaloSummaryHisto {
public: 
  /// constructor
  CaloSummaryHisto(bool booked = false){
    m_booked=booked;
    m_histos.clear();
    m_profiles.clear();
  }
  // destructor
  ~CaloSummaryHisto() {} ;

  enum Type{
    Pedestal = 0,
    Signal   = 1,
    Ratio    = 2,
    All      = 3
  };

  void setBooked(bool booked){    m_booked = booked; }
  
  bool                        booked(){return m_booked; };
  inline AIDA::IHistogram1D*  histo(Type type, const std::string& slot="T0",  int order = 0, unsigned int index=0) ;
  inline std::vector<AIDA::IHistogram1D* >&  histos(Type type, const std::string& slot="T0", int order=0) ;
  inline void addHisto(AIDA::IHistogram1D* histo, Type type, int order,const std::string& slot) ;

  inline AIDA::IProfile1D*  profile(Type type, const std::string& slot="T0",  unsigned int index=0) ;
  inline std::vector<AIDA::IProfile1D* >&  profiles(Type type, const std::string& slot="T0") ;
  inline void addProfile(AIDA::IProfile1D* histo, Type type ,const std::string& slot) ;


protected:
  typedef std::vector< AIDA::IHistogram1D*>       Vec;
  typedef std::map<int, Vec>                      MapV;
  typedef std::map<const std::string, MapV>       MapMapV;
  typedef std::map<int, MapMapV >                 MapMapMapV;

  typedef std::vector< AIDA::IProfile1D*>    VecP;
  typedef std::map<const std::string, VecP>  MapVP;
  typedef std::map<int, MapVP>               MapMapVP;

private:
  MapMapMapV m_histos;
  MapMapVP   m_profiles;
  bool       m_booked;
  Vec        m_dummyVec;
  VecP       m_dummyVecP;
};
#endif // CALOSUMMARYHISTO_H


inline  AIDA::IHistogram1D* CaloSummaryHisto::histo(CaloSummaryHisto::Type type,const std::string& slot,
                                                    int order,unsigned int index) {
  Vec& hs = histos(type,slot,order);
  if( index < hs.size() )return hs[index];
  return NULL;
}
inline  AIDA::IProfile1D* CaloSummaryHisto::profile(CaloSummaryHisto::Type type,const std::string& slot, unsigned int index) {
  VecP& ps = profiles(type,slot);
  if( index < ps.size() )return ps[index];
  return NULL;
}

// --
inline std::vector<AIDA::IHistogram1D*>& CaloSummaryHisto::histos(CaloSummaryHisto::Type type,const std::string& slot, int order){
  MapMapMapV::iterator it = m_histos.find( type );
  if(it == m_histos.end())return m_dummyVec;
  MapMapV& t = (*it).second;  
  MapMapV::iterator itt = t.find( slot );
  if( itt == t.end() )return m_dummyVec;
  MapV& tt = (*itt).second;
  MapV::iterator ittt = tt.find( order );
  if( ittt == tt.end())return m_dummyVec;
  return (*ittt).second;
}

inline std::vector<AIDA::IProfile1D*>& CaloSummaryHisto::profiles(CaloSummaryHisto::Type type,const std::string& slot) {
  MapMapVP::iterator it = m_profiles.find( type );
  if(it == m_profiles.end())return m_dummyVecP;
  MapVP& t = (*it).second;  
  CaloSummaryHisto::MapVP::iterator itt = t.find( slot );
  if( itt == t.end() )return m_dummyVecP;
  return (*itt).second;
}

//-----
inline void CaloSummaryHisto::addHisto(AIDA::IHistogram1D* histo,CaloSummaryHisto::Type type, int order, const std::string& slot){
  m_histos[type][slot][order].push_back(histo);
}
inline void CaloSummaryHisto::addProfile(AIDA::IProfile1D* profile,CaloSummaryHisto::Type type, const std::string& slot) {
  m_profiles[type][slot].push_back(profile);
}

