// $Id: CaloTiming.h,v 1.4 2009-09-11 22:12:59 odescham Exp $
#ifndef CALOTIMING_H
#define CALOTIMING_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "CaloDAQ/ICaloDataProvider.h"
#include "CaloDet/DeCalorimeter.h"


/** @class CaloTiming CaloTiming.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2008-08-19
 */
class CaloTiming : public GaudiHistoAlg {
public:
  /// Standard constructor
  CaloTiming( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~CaloTiming( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:
  std::string m_detectorName;
  std::string m_readoutTool;
  int m_threshold;
  int m_bin;
  ICaloDataProvider* m_t0;
  ICaloDataProvider* m_next;
  ICaloDataProvider* m_prev;
  DeCalorimeter* m_calo;
  double m_sat;
  bool m_crate;
};
#endif // CALOTIMING_H
