// $Id: DumpCalibCoeff.cpp,v 1.6 2010-11-03 14:53:43 odescham Exp $
#include "GaudiKernel/AlgFactory.h" 
#include "OnlineHistDB/OnlineHistDB.h"
#include "OnlineHistDB/OnlineHistogram.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "DumpCalibCoeff.h"

DECLARE_ALGORITHM_FACTORY( DumpCalibCoeff )


// Constructor
DumpCalibCoeff::DumpCalibCoeff(const std::string& name,ISvcLocator* pSvcLocator ) :
  AnalysisTask( name, pSvcLocator ){

  declareProperty("Histo", m_histo = "" );
  declareProperty("Detector", m_detectorName );
  declareProperty("HistoMin", m_min = 0.5 );
  declareProperty("HistoMax", m_max = 1.5 );
  declareProperty("HistoBin", m_bin = 100 );
  declareProperty("OneDimension", m_1d = false);
  declareProperty("GeometricalView", m_geo = true);
  declareProperty("OutputMask" , m_outMask = 0x0 );
  declareProperty("OutputRange" , m_range  );
  declareProperty("Coefficients", m_coef);
  
  m_range = std::make_pair(0.5,1.5);
  
  // set default detectorName
  m_detectorName = LHCb::CaloAlgUtils::CaloNameFromAlg( name  ) ;
  m_view = new Calo2Dview( name , serviceLocator() );
  setHistoDir( name );
} 

// Destructor
DumpCalibCoeff::~DumpCalibCoeff(){}

//--------------------------------------------------
StatusCode DumpCalibCoeff::finalize(){
  return AnalysisTask::finalize();
}
StatusCode DumpCalibCoeff::initialize(){
  // get the detector element
  if ( "Ecal" == m_detectorName ) {
    m_calo = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  } else if ( "Hcal" == m_detectorName ) {
    m_calo = getDet<DeCalorimeter>(  DeCalorimeterLocation::Hcal);    
  } else if ( "Prs" == m_detectorName ) {
    m_calo = getDet<DeCalorimeter>(  DeCalorimeterLocation::Prs );
  } else if ( "Spd" == m_detectorName ) {
    m_calo = getDet<DeCalorimeter>(  DeCalorimeterLocation::Spd );
  } else{
    warning() <<" Unable to load the DetectorElement for " << m_detectorName << endmsg;
    return StatusCode::SUCCESS;
  }  


  if( m_histo == "" ) m_histo = m_detectorName;

  StatusCode sc ;
  m_view->setGeometricalView(m_geo);
  m_view->setOneDimension(m_1d);
  sc  = m_view->initialize();

  // WARNING : MUST BE AT THE END
  sc = AnalysisTask::initialize();
  return sc;
}

//--------------------------------------------------
StatusCode DumpCalibCoeff::analyze(std::string& SaveSet, std::string Task){

  // Init
  m_set  = SaveSet;
  m_task = Task;


  TH1* th = NULL;
  if( SaveSet == "OptionsTable" && !m_coef.empty() ){
    // create histo
    info() << "Filling " << m_histo << " profile1D with coefficient table " << endmsg;
    for(std::map<LHCb::CaloCellID,double>::iterator imap = m_coef.begin() ; m_coef.end() != imap ; ++imap){
      double x = imap->first.index();
      double y = imap->second;
      AIDA::IHistogram1D* h1 = plot1D( x, m_histo, "Coeff profile 1D", 0., 16384., 16384 ,y );
      th = Gaudi::Utils::Aida2ROOT::aida2root( h1 ); 
    }
  } 
  else if (SaveSet != "" && m_coef.empty() ){
    info() << "Opening root file " << SaveSet << endmsg;
    m_tf = new TFile(SaveSet.c_str(),"READ");
    if ( m_tf->IsZombie())
      Warning( "Root file is a zombie").ignore();
    else
      th =  (TH1*) m_tf->Get( m_histo.c_str() );
    
    if( 0 == th ){ 
      warning() << "histo '" << m_histo << "' NOT FOUND : analyze DB content only" << endmsg;
    }    
  }
  else if (SaveSet != "" && !m_coef.empty() ){
    error() << "Both Saveset and coefficient table are defined  : analyze DB content only" << endmsg;
  }else{
    error() << "Both Saveset and coefficient table are undefined : analyze DB content only" << endmsg;
  }

  StatusCode sc = histoAnalysis( th );
  if( NULL != m_tf){
    m_tf->Close();
    m_tf->Delete();
    delete m_tf;
  }
  
  // close
  return StatusCode::SUCCESS;
}


StatusCode DumpCalibCoeff::histoAnalysis(TH1* h0){

  bool ok = ( NULL == h0) ? false : true;


  if(ok)debug() << " #bins " << h0->GetNbinsX() << endmsg;
  double value = 0.;
  std::string calo = m_detectorName;


  int nCells  = m_calo->numberOfCells();
  std::stringstream dead("");
  std::stringstream db("");
  for(int ic = 0 ; ic < nCells ; ++ic){
    LHCb::CaloCellID id = m_calo->cellIdByIndex(ic);
    int bin = id.index();
    if( !m_calo->valid( id ) || m_calo->isPinId( id ) )continue;
    if(ok && bin >=  h0->GetNbinsX() )break;
    // for( int bin = 0 ; bin < h0->GetNbinsX() ; bin++ ) { 
    // LHCb::CaloCellID id = LHCb::CaloCellID(bin);
    //id.setCalo( CaloCellCode::CaloNumFromName( m_detectorName ));
    std::string area = id.areaName();
    if(ok)value = h0->GetBinContent( bin + 1) ;

    if(ok){
      m_view->fillCalo2D( calo + "Coeff2D" , id , value , calo + " Calibration Coefficients" );
      plot1D( value, calo + "Coeff" , calo + " Calibration Coefficients", m_min , m_max, m_bin );
      plot1D( value, area + "/" + calo + "Coeff" , area + calo + " Calibration Coefficients", m_min , m_max, m_bin );
    }

    double nominal = m_calo->cellParam( id ).nominalGain();

    // condDB value
    double dbValue = m_calo->cellParam( id ).calibration();
    m_view->fillCalo2D( calo + "DBCoeff2D" , id , dbValue , calo + " Calibration Coefficient in condDB");
    plot1D( dbValue, calo + "DBCoeff" , calo + " Calibration Coefficient in condDB", m_min , m_max, m_bin );
    plot1D( dbValue, area + "/" + calo + "DBCoeff" , area + calo + " Calibration Coefficient in condDB", m_min , m_max, m_bin );

    // new calib constant 
    if(ok)m_view->fillCalo2D( calo + "Calib2D" , id , value*dbValue, calo + " NEW Calibration ( DBCoeff * Coeff)"); 
    if(ok)plot1D( value*dbValue, calo + "Calib" ,  calo + " NEW Calibration ( DBCoeff * Coeff)" , m_min , m_max, m_bin );
    if(ok)plot1D( value*dbValue, area + "/" + calo + "Calib" ,  area+calo + " NEW Calibration ( DBCoeff * Coeff)" 
                  ,m_min , m_max,m_bin);
    
    // value/dbValue
    double ratio = ( dbValue != 0 ) ? value/dbValue : 0. ; 
    if(ok){
      m_view->fillCalo2D( calo + "Ratio2D" , id , ratio , calo + " Coeff / DBCoeff" ); 
      plot1D( ratio , calo + "Ratio" , calo + " Coeff / DBCoeff", m_min , m_max, m_bin );
      plot1D( ratio , area + "/" + calo + "Ratio" , area + calo + " Coeff / DBCoeff", m_min , m_max, m_bin );
      plot2D( value , dbValue , calo + "Correlations", "Coeff vs DBCoeff" , m_min, m_max, m_min,m_max,m_bin,m_bin);
      plot2D( value , dbValue , area + "/" + calo + "Correlations", "Coeff vs DBCoeff" , m_min, m_max, m_min,m_max,m_bin,m_bin );
    }

    db  << "        " << id.all() ;

    if( value < m_range.first || value > m_range.second )value = 1.;

    if( m_outMask & 0x1) db  << "   " << Gaudi::Utils::toString(value*dbValue );
    if( m_outMask & 0x2) db  << "   " << Gaudi::Utils::toString( value );
    if( m_outMask & 0x4) db  << "   " << Gaudi::Utils::toString( dbValue) ;
    if( m_outMask & 0x8) db  << "   " << Gaudi::Utils::toString( ( dbValue != 0 ) ? value/dbValue : 0.) ;
    if( m_outMask & 0x10) db  << "   " << id;
    if( m_outMask & 0x20) db  << "   " << Gaudi::Utils::toString( nominal)  ;
    db << std::endl;
    
    if( value == 0 )dead << "        " << id.all() << "    1" << std::endl;
  } 

  std::stringstream header("cellID") ;
  header  << "  " << "cellid" << " : ";
  if( m_outMask & 0x1) header  << "  " << "newGain*dbGain";
  if( m_outMask & 0x2) header  << "  " << "newGain";
  if( m_outMask & 0x4) header  << "  " << "dbGain";
  if( m_outMask & 0x8) header  << "  " << "newGain/dbGain";
  if( m_outMask & 0x10) header  << "  " << "cellID";
  if( m_outMask & 0x20) header  << "  " << "NominalGain";
  header <<  " , " ;
  if( m_outMask != 0 ) {
    info() << "------------------ EFlow parameters for " << m_detectorName << " ----------------"<< std::endl 
           << "        " << header.str() << std::endl                    
           << db.str() << std::endl
           << "___________________________"<< std::endl
           << "  ------------- Dead channel(s) list : " << std::endl
           << dead.str() << std::endl
           << "-----------------------------------------------------------------------------------" << endmsg;
  info() << "Coefficients value is bounded in the [" << m_range.first << "," << m_range.second << "]" << endmsg;
  }
  return StatusCode::SUCCESS; 
}
