// $Id: DumpGainShiftCoeff.cpp,v 1.6 2010/11/03 14:53:43 odescham Exp $
#include "GaudiKernel/AlgFactory.h" 
#include "OnlineHistDB/OnlineHistDB.h"
#include "OnlineHistDB/OnlineHistogram.h"
#include "Kernel/CaloCellID.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "DumpGainShiftCoeff.h"

DECLARE_ALGORITHM_FACTORY( DumpGainShiftCoeff )

// Constructor
DumpGainShiftCoeff::DumpGainShiftCoeff(const std::string& name,ISvcLocator* pSvcLocator ) :
  AnalysisTask( name, pSvcLocator ){

  declareProperty("Histo", m_histo = "" );
  declareProperty("Detector", m_detectorName );
  declareProperty("HistoMin", m_min = 0. );
  declareProperty("HistoMax", m_max = 5. );
  declareProperty("HistoBin", m_bin = 100 );
  declareProperty("OneDimension", m_1d = false);
  declareProperty("GeometricalView", m_geo = true);
  declareProperty("OutputMask" , m_outMask = 0x0 );
  declareProperty("OutputRange" , m_range  );
  declareProperty("Quality"     , m_data);
  declareProperty("QualityRef"  , m_dataDB);
  
  m_range = std::make_pair(0.,5.);
  
  // set default detectorName
  m_detectorName = LHCb::CaloAlgUtils::CaloNameFromAlg( name  ) ;
  m_view = new Calo2Dview( name , serviceLocator() );
  setHistoDir( name );
} 

// Destructor
DumpGainShiftCoeff::~DumpGainShiftCoeff(){}

//--------------------------------------------------
StatusCode DumpGainShiftCoeff::finalize(){
  return AnalysisTask::finalize();
}
StatusCode DumpGainShiftCoeff::initialize(){
  // get the detector element
  if ( "Ecal" == m_detectorName ) {
    m_calo = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  } else if ( "Hcal" == m_detectorName ) {
    m_calo = getDet<DeCalorimeter>(  DeCalorimeterLocation::Hcal);    
  } else if ( "Prs" == m_detectorName ) {
    m_calo = getDet<DeCalorimeter>(  DeCalorimeterLocation::Prs );
  } else if ( "Spd" == m_detectorName ) {
    m_calo = getDet<DeCalorimeter>(  DeCalorimeterLocation::Spd );
  } else{
    warning() <<" Unable to load the DetectorElement for " << m_detectorName << endmsg;
    return StatusCode::SUCCESS;
  }  


  // ==== convert quality tables when exist
  int bad=0;
  int badDB=0;
  int size = 6;
  for ( unsigned int kk = 0; m_data.size()/size > kk  ; ++kk ) {
    int ll = size*kk;
    double cell       = m_data[ll];
    double qFlag      = m_data[ll+1];
    double ledMoni    = m_data[ll+4];
    LHCb::CaloCellID id = LHCb::CaloCellID( (int) cell );
    m_qMoni[id]= ledMoni;
    if(qFlag != 0)bad++;
  }

  for ( unsigned int kk = 0; m_dataDB.size()/size > kk  ; ++kk ) {
    int ll = size*kk;
    double cell       = m_dataDB[ll];
    double qFlag      = m_dataDB[ll+1];
    double ledMoni    = m_dataDB[ll+4];
    LHCb::CaloCellID id = LHCb::CaloCellID( (int) cell );
    m_qCond[id]=ledMoni;
    if(qFlag != 0)badDB++;
  }
  
  if( !m_data.empty())info() << "Found " << bad << " problematic channels in Quality table" << endmsg;
  if( !m_dataDB.empty())info() << "Found " << badDB << " problematic channels in reference Quality table" << endmsg;

  if( m_histo == "" ) m_histo = m_detectorName;
  StatusCode sc ;
  m_view->setGeometricalView(m_geo);
  m_view->setOneDimension(m_1d);
  sc  = m_view->initialize();

  // WARNING : MUST BE AT THE END
  sc = AnalysisTask::initialize();
  return sc;
}

//--------------------------------------------------
StatusCode DumpGainShiftCoeff::analyze(std::string& SaveSet, std::string Task){

  // Init
  m_set  = SaveSet;
  m_task = Task;
  m_tf = NULL;

  TH1* th = NULL;
  if( SaveSet == "OptionsTable" && !m_qMoni.empty() ){
    // create histo
    info() << "Filling " << m_histo << " profile1D with coefficient table " << endmsg;
    for(std::map<LHCb::CaloCellID,double>::iterator imap = m_qMoni.begin() ; m_qMoni.end() != imap ; ++imap){
      double x = imap->first.index();
      double y = imap->second;
      AIDA::IHistogram1D* h1 = plot1D( x, m_histo, "Coeff profile 1D", 0., 16384., 16384 ,y );
      th = Gaudi::Utils::Aida2ROOT::aida2root( h1 ); 
    }
  } 
  else if (SaveSet != "" && m_qMoni.empty() ){
    info() << "Opening root file " << SaveSet << endmsg;
    m_tf = new TFile(SaveSet.c_str(),"READ");
    if ( m_tf->IsZombie())
      Warning( "Root file is a zombie").ignore();
    else
      th =  (TH1*) m_tf->Get( m_histo.c_str() );
    
    if( 0 == th ){ 
      warning() << "histo '" << m_histo << "' NOT FOUND : analyze DB content only" << endmsg;
    }    
  }
  else if (SaveSet != "" && !m_qMoni.empty() ){
    error() << "Both Saveset and coefficient table are defined  : analyze DB content only" << endmsg;
  }else{
    error() << "Both Saveset and coefficient table are undefined : analyze DB content only" << endmsg;
  }

  StatusCode sc = histoAnalysis( th );
  info() << "End of analysis" << endmsg;

  if( NULL != m_tf){
    m_tf->Close();
    m_tf->Delete();
    delete m_tf;
  }
  info() << "End of processing" << endmsg;
  
  // close
  return StatusCode::SUCCESS;
}


StatusCode DumpGainShiftCoeff::histoAnalysis(TH1* h0){

  bool ok = ( NULL == h0) ? false : true;


  if(ok)debug() << " #bins " << h0->GetNbinsX() << endmsg;
  double value = 0.;
  std::string calo = m_detectorName;

    if( m_qCond.empty() )
      info() << "get DB reference <PMT/PIN> from conditionDB" << endmsg;
    else
      info() << "get DB reference <PMT/PIN> from options table" << endmsg;

  int badDB = 0;
  int nCells  = m_calo->numberOfCells();
  std::stringstream db("");
  for(int ic = 0 ; ic < nCells ; ++ic){
    LHCb::CaloCellID id = m_calo->cellIdByIndex(ic);
    int bin = id.index();
    if( !m_calo->valid( id ) || m_calo->isPinId( id ) )continue;
    if(ok && bin >=  h0->GetNbinsX() )break;
    std::string area = id.areaName();
    if(ok)value = h0->GetBinContent( bin + 1) ;

    if(ok){
      m_view->fillCalo2D( calo + "Coeff2D" , id , value , calo + " Calibration Coefficients" );
      plot1D( value, calo + "Coeff" , calo + " Calibration Coefficients", m_min , m_max, m_bin );
      plot1D( value, area + "/" + calo + "Coeff" , area + calo + " Calibration Coefficients", m_min , m_max, m_bin );
    }


    // condDB value
    double dbMoni = 0;
    if( m_qCond.empty() ){
      dbMoni = m_calo->cellParam( id ).ledMoniRef();
      if( m_calo->cellParam( id ).quality() != 0 ) badDB++;
    }else{
      dbMoni = m_qCond[id];
    }
    
    
    
    m_view->fillCalo2D( calo + "DBCoeff2D" , id , dbMoni , calo + " reference <PMT/PIN> in condDB");
    plot1D( dbMoni, calo + "DBCoeff" , calo + " reference <PMT/PIN> in condDB", m_min , m_max, m_bin );
    plot1D( dbMoni, area + "/" + calo + "DBCoeff" , area + calo + " reference <PMT/PIN> in condDB", m_min , m_max, m_bin );
    
    // value/ref : gain shift
    double ratio = ( dbMoni != 0 && value !=0 ) ? value/dbMoni : 1. ; 
    if(ok){
      m_view->fillCalo2D( calo + "GainShift2D" , id , ratio , calo + " GainShift = <PMT/PIN> / <PMT/PIN>_ref" ); 
      plot1D( ratio , calo + "GainShift" , calo + " GainShift = <PMT/PIN> / <PMT/PIN>_ref", m_min , m_max, m_bin );
      plot1D( ratio , area + "/" + calo + "GainShift" , area + calo + " GainShift = <PMT/PIN> / <PMT/PIN>_ref",m_min,m_max,m_bin);
      plot2D( value , dbMoni , calo + "Correlations", " <PMT/PIN> versus <PMT/PIN>_ref" , m_min, m_max, m_min,m_max,m_bin,m_bin);
      plot2D( value , dbMoni , area + "/" + calo + "Correlations", " GainShift = <PMT/PIN> / <PMT/PIN>_ref" 
              , m_min, m_max, m_min,m_max,m_bin,m_bin );
    }
    
    db  << "        " << id.all() ;
    
    if( value < m_range.first || value > m_range.second )value = 1.;
    
    if( m_outMask & 0x1) db  << "   " << Gaudi::Utils::toString( value );
    if( m_outMask & 0x2) db  << "   " << Gaudi::Utils::toString( dbMoni) ;
    if( m_outMask & 0x4) db  << "   " << Gaudi::Utils::toString( ( dbMoni != 0 ) ? value/dbMoni : 1.) ;
    if( m_outMask & 0x8) db  << "   " << id;
    db << std::endl;
    
  } 
  std::stringstream header("cellID") ;
  header  << "  " << "cellid" << " : ";
  if( m_outMask & 0x1) header  << "  " << "<PMT/PIN>";
  if( m_outMask & 0x2) header  << "  " << "<PMT/PIN>_ref";
  if( m_outMask & 0x4) header  << "  " << "gainShift";
  if( m_outMask & 0x8) header  << "  " << "cellID";
  header <<  " , " ;
  if( m_outMask != 0 ) {
    info() << "------------------ GainShift parameters for " << m_detectorName << " ----------------"<< std::endl 
           << "        " << header.str() << std::endl                    
           << db.str() << std::endl
           << "___________________________"<< std::endl;
    info() << "<PMT/PIN> value is bounded in the [" << m_range.first << "," << m_range.second << "]" << endmsg;
    if( m_qCond.empty() )info() << "Found " << badDB << " problematic channels in reference Quality from condDB" << endmsg;
  }
  
  return StatusCode::SUCCESS; 
} 



