// $Id: DumpGainShiftCoeff.h,v 1.5 2010/11/03 14:53:43 odescham Exp $
#ifndef CALOCALIB_DUMPGAINSHIFTCOEFF_H
#define CALOCALIB_DUMPGAINSHIFTCOEFF_H 1

class StatusCode;
#include "GaudiKernel/DeclareFactoryEntries.h"
#include "Kernel/CaloCellID.h"
#include "CaloUtils/CaloCellIDAsProperty.h"
#include "OMAlib/AnalysisTask.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloKernel/CaloVector.h"
#include "CaloUtils/Calo2Dview.h"
#include <string>
#include <vector>
// ROOT includes
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TProfile.h>
#include <TF1.h>
#include <TMath.h>

class DumpGainShiftCoeff :  public AnalysisTask{
 public:

  DumpGainShiftCoeff(const std::string& name,ISvcLocator* pSvcLocator );
  virtual ~DumpGainShiftCoeff ();
  StatusCode initialize() override;
  StatusCode finalize() override;
  StatusCode analyze(std::string& SaveSet, std::string Task) override;

private :
  StatusCode histoAnalysis( TH1* h0 );
  std::string m_detector;
  DeCalorimeter* m_calo;
  TFile* m_tf;
  std::string m_set;
  std::string m_task;
  std::string m_detectorName;
  std::string m_histo;
  Calo2Dview* m_view;
  double m_min;
  double m_max;
  int m_bin;
  bool m_geo;
  bool m_1d;
  int m_outMask ;
  std::pair<double,double> m_range;
  std::map<LHCb::CaloCellID,double> m_qMoni;
  std::map<LHCb::CaloCellID,double> m_qCond;
  std::vector<double> m_data;
  std::vector<double> m_dataDB;
};

#endif // CALOCALIB_DUMPCALIBCOEFF_H
