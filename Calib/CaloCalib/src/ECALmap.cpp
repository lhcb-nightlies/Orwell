#include "TROOT.h"
#include "TFile.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TF1.h"
#include "TGraphErrors.h"
#include "TMinuit.h"
#include "TMatrixD.h"

#include <map>
#include <math.h>

#include "ECALmap.h"

double stepX_ECAL[4]={0,121.2,60.6,40.4}; // mm
double stepY_ECAL[4]={0,121.2,60.6,40.4}; // mm
double    LY[4]={0,2600,3500,3100}; // photo electrons per GeV

double Zc=12490.0;

double Xc(int z, int x, int y){// X coordinate of a cell, in mm
  if(!isECALcell(z,x,y))return -100000;
  else return ( (double)x-31.5 ) * stepX_ECAL[z];
}

double Yc(int z, int x, int y){// Y coordinate of a cell, in mm
  if(!isECALcell(z,x,y))return -100000;
  else return ( (double)y-31.5 ) * stepY_ECAL[z];
}

double nominal_gain(int z, int x, int y){
  if(!isECALcell(z,x,y))return -100000;
  
  double xc=Xc(z,x,y);
  double yc=Yc(z,x,y);
  double zc=Zc;
  double sthe=sqrt( (xc*xc+yc*yc) / (xc*xc+yc*yc+ zc*zc) );
  double emax=7+10/sthe;
  
  double nphmax=emax*LY[z];
  double nemax=80*6000000; // 80 pC * 6000000 electrons per pC
  
  double clip=3.2;
  double gain=nemax/nphmax * clip;
  return gain;
}

char pinside[124]={
  'C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C'
  ,'C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C'
  ,'A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A'
  ,'A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A'};
int pinarea[124]={
  1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2
  ,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3
  ,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2
  ,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3};
int pinnum[124]={
  1, 2, 3, 4, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,25,26,27,28, 1, 2, 3, 4, 5
  , 6, 7, 8, 9,10,11,12,13,14, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22
  , 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27, 1, 2, 3, 4, 5
  , 6, 7, 8, 9,10,11,12,13,14, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22};
int pincid[124]={
  45068,45189,45125,45061,45132,45252,45124,45060,45196,45188,45251,45123,45187,45059,45250,45122
  ,45249,45121,45186,45058,45185,45057,45184,45120,45056,45248,45260,45069,45255,45191,45127,45063
  ,45254,45190,45126,45062,45256,45192,45128,45064,45070,45261,45197,45133,45262,45198,45134,45071
  ,45263,45199,45135,45195,45131,45067,45258,45194,45130,45066,45257,45193,45129,45065,45501,45437
  ,45373,45360,45424,45564,45436,45372,45488,45500,45563,45499,45435,45371,45562,45498,45561,45497
  ,45434,45370,45433,45369,45560,45496,45432,45368,45552,45361,45567,45503,45439,45375,45566,45502
  ,45438,45374,45556,45492,45428,45364,45362,45553,45489,45425,45554,45490,45426,45363,45555,45491
  ,45427,45495,45431,45367,45558,45494,45430,45366,45557,45493,45429,45365};
int pxmin[124]={
  24,16, 8, 0,24,16, 8, 0,24,16, 8, 0, 8, 0, 8, 0,24,16, 8, 0,24,16,16, 8, 0,24,24,16, 8, 0, 8
  , 0, 8, 0, 8, 0,24,16, 8, 0,26,20,14, 8,26,20,14, 8,20,14, 8,20,14, 8,26,20,14, 8,26,20,14, 8
  ,40,48,56,32,32,40,48,56,32,40,48,56,48,56,48,56,32,40,48,56,32,40,32,40,48,56,32,40,48,56,48
  ,56,48,56,48,56,32,40,48,56,32,38,44,50,32,38,44,50,40,44,50,40,44,50,32,38,44,50,32,38,44,50};
int pxmax[124]={
  31,23,15, 7,31,23,15, 7,31,23,15, 7,15, 7,15, 7,31,23,15, 7,31,23,23,15, 7,31,31,23,15, 7,15
  , 7,15, 7,15, 7,31,23,15, 7,31,25,19,13,31,25,19,13,23,19,13,23,19,13,31,25,19,13,31,25,19,13
  ,47,55,63,39,39,47,55,63,39,47,55,63,55,63,55,63,39,47,55,63,39,47,39,47,55,63,39,47,55,63,55
  ,63,55,63,55,63,39,47,55,63,37,43,49,55,37,43,49,55,43,49,55,43,49,55,37,43,49,55,37,43,49,55};
int pymin[124]={
  50,50,50,50,46,46,42,42,42,42,34,34,30,30,22,22,18,18,14,14,14,14, 6, 6, 6, 6,44,44,44,44,36
  ,36,28,28,20,20,12,12,12,12,44,41,44,44,38,38,38,38,32,32,32,26,26,26,20,20,20,20,14,14,14,14
  ,50,50,50,50,46,46,42,42,42,42,34,34,30,30,22,22,18,18,14,14,14,14, 6, 6, 6, 6,44,44,44,44,36
  ,36,28,28,20,20,12,12,12,12,44,44,44,44,38,38,38,38,32,32,32,26,26,26,20,20,20,20,14,14,14,14};
int pymax[124]={
  57,57,57,57,49,49,49,49,45,45,41,41,33,33,29,29,21,21,21,21,17,17,13,13,13,13,51,51,51,51,43
  ,43,35,35,27,27,19,19,19,19,49,49,49,49,43,46,43,43,37,37,37,31,31,31,25,25,25,25,19,19,19,19
  ,57,57,57,57,49,49,49,49,45,45,41,41,33,33,29,29,21,21,21,21,17,17,13,13,13,13,51,51,51,51,43
  ,43,35,35,27,27,19,19,19,19,49,49,49,49,43,43,43,43,37,37,37,31,31,31,25,25,25,25,19,19,19,19};
int l1xmin[124]={
  28,20,12, 4,28,16,12, 4,28,16,12, 4, 8, 0,12, 4,24,16,12, 4,24,16,20,12, 4,24,28,20,12, 4,12
  , 4,12, 4,12, 4,28,20,12, 4,29,23,17,11,29,20,17,11,20,17,11,23,17,11,29,23,17,11,29,23,17,11
  ,40,48,56,36,36,40,48,56,36,40,48,56,48,56,48,56,32,40,48,56,32,40,32,40,48,56,32,40,48,56,48
  ,56,48,56,48,56,32,44,48,56,32,38,44,50,32,38,44,50,40,44,50,40,44,50,32,38,44,50,32,38,44,50};
int l1xmax[124]={
  31,23,15, 7,31,19,15, 7,31,19,15, 7,11, 3,15, 7,27,19,15, 7,27,19,23,15, 7,27,31,23,15, 7,15
  , 7,15, 7,15, 7,31,23,15, 7,31,25,19,13,31,22,19,13,22,19,13,23,19,13,31,25,19,13,31,25,19,13
  ,43,51,59,39,39,43,51,59,39,43,51,59,51,59,51,59,35,43,51,59,35,43,35,43,51,59,35,43,51,59,51
  ,59,51,59,51,59,35,47,51,59,34,40,46,52,34,40,46,52,40,46,52,40,46,52,34,40,46,52,34,40,46,52};
int l1ymin[124]={
  54,50,50,50,46,46,42,42,42,42,34,34,30,30,22,22,18,18,14,14,14,14, 6, 6, 6,10,44,44,44,44,36
  ,36,28,28,20,20,12,12,12,12,44,41,44,44,38,38,38,38,32,32,32,26,26,26,20,20,20,20,14,14,14,14
  ,54,54,54,54,46,46,46,46,42,42,38,38,30,30,26,26,18,18,18,18,14,14,10,10,10,10,48,48,48,48,40
  ,40,32,32,24,24,16,16,16,16,47,47,47,47,41,41,41,41,35,35,35,29,29,29,23,23,23,23,14,17,17,17};
int l1ymax[124]={
  57,53,53,53,49,49,45,45,45,45,37,37,33,33,25,25,21,21,17,17,17,17, 9, 9, 9,13,47,47,47,47,39
  ,39,31,31,23,23,15,15,15,15,46,43,46,46,40,40,40,40,34,34,34,28,28,28,22,22,22,22,16,16,16,16
  ,57,57,57,57,49,49,49,49,45,45,41,41,33,33,29,29,21,21,21,21,17,17,13,13,13,13,51,51,51,51,43
  ,43,35,35,27,27,19,19,19,19,49,49,49,49,43,43,43,43,37,37,37,31,31,31,25,25,25,25,16,19,19,19};
int l2xmin[124]={
  24,16, 8, 0,24,20, 8, 0,24,20, 8, 0,12, 4, 8, 0,28,20, 8, 0,28,20,16, 8, 0,28,24,16, 8, 0, 8
  , 0, 8, 0, 8, 0,24,16, 8, 0,26,23,14, 8,26,20,14, 8,20,14, 8,20,14, 8,26,20,14, 8,26,20,14, 8
  ,44,52,60,32,32,44,52,60,32,44,52,60,52,60,52,60,36,44,52,60,36,44,36,44,52,60,36,44,52,60,52
  ,60,52,60,52,60,36,40,52,60,35,41,47,53,35,41,47,53,41,47,53,41,47,53,35,41,47,53,35,41,47,53};
int l2xmax[124]={
  27,19,11, 3,27,23,11, 3,27,23,11, 3,15, 7,11, 3,31,23,11, 3,31,23,19,11, 3,31,27,19,11, 3,11
  , 3,11, 3,11, 3,27,19,11, 3,28,25,16,10,28,22,16,10,22,16,10,22,16,10,28,22,16,10,28,22,16,10
  ,47,55,63,35,35,47,55,63,35,47,55,63,55,63,55,63,39,47,55,63,39,47,39,47,55,63,39,47,55,63,55
  ,63,55,63,55,63,39,43,55,63,37,43,49,55,37,43,49,55,43,49,55,43,49,55,37,43,49,55,37,43,49,55};
int l2ymin[124]={
  54,50,50,50,46,46,42,42,42,42,34,34,30,30,22,22,18,18,14,14,14,14, 6, 6, 6,10,44,44,44,44,36
  ,36,28,28,20,20,12,12,12,12,44,44,44,44,38,44,38,38,35,32,32,26,26,26,20,20,20,20,14,14,14,14
  ,54,54,54,54,46,46,46,46,42,42,38,38,30,30,26,26,18,18,18,18,14,14,10,10,10,10,48,48,48,48,40
  ,40,32,32,24,24,16,16,16,16,47,47,47,47,41,41,41,41,35,35,35,29,29,29,23,23,23,23,17,17,17,17};
int l2ymax[124]={
  57,53,53,53,49,49,45,45,45,45,37,37,33,33,25,25,21,21,17,17,17,17, 9, 9, 9,13,47,47,47,47,39
  ,39,31,31,23,23,15,15,15,15,46,46,46,46,40,46,40,40,37,34,34,28,28,28,22,22,22,22,16,16,16,16
  ,57,57,57,57,49,49,49,49,45,45,41,41,33,33,29,29,21,21,21,21,17,17,13,13,13,13,51,51,51,51,43
  ,43,35,35,27,27,19,19,19,19,49,49,49,49,43,43,43,43,37,37,37,31,31,31,25,25,25,25,19,19,19,19};
int l3xmin[124]={
  28,20,12, 4,-9,-9,12, 4,-9,-9,12, 4,-9,-9,12, 4,-9,-9,12, 4,-9,-9,20,12, 4,24,28,20,12, 4,12
  , 4,12, 4,12, 4,28,20,12, 4,29,20,17,11,29,20,17,11,23,17,11,23,17,11,29,23,17,11,29,23,17,11
  ,40,48,56,36,-9,-9,48,56,-9,-9,48,56,-9,-9,48,56,-9,-9,48,56,-9,-9,32,40,48,56,32,40,48,56,48
  ,56,48,56,48,56,32,44,48,56,32,38,44,50,32,38,44,50,40,44,50,40,44,50,32,38,44,50,32,38,44,50};
int l3xmax[124]={
  31,23,15, 7,-9,-9,15, 7,-9,-9,15, 7,-9,-9,15, 7,-9,-9,15, 7,-9,-9,23,15, 7,27,31,23,15, 7,15
  , 7,15, 7,15, 7,31,23,15, 7,31,22,19,13,31,22,19,13,23,19,13,23,19,13,31,25,19,13,31,25,19,13
  ,43,51,59,39,-9,-9,51,59,-9,-9,51,59,-9,-9,51,59,-9,-9,51,59,-9,-9,35,43,51,59,35,43,51,59,51
  ,59,51,59,51,59,35,47,51,59,34,40,46,52,34,40,46,52,40,46,52,40,46,52,34,40,46,52,34,40,46,52};
int l3ymin[124]={
  50,54,54,54,-9,-9,46,46,-9,-9,38,38,-9,-9,26,26,-9,-9,18,18,-9,-9,10,10,10, 6,48,48,48,48,40
  ,40,32,32,24,24,16,16,16,16,47,47,47,47,41,41,41,41,32,35,35,29,29,29,23,23,23,23,17,17,17,17
  ,50,50,50,50,-9,-9,42,42,-9,-9,34,34,-9,-9,22,22,-9,-9,14,14,-9,-9, 6, 6, 6, 6,44,44,44,44,36
  ,36,28,28,20,20,12,12,12,12,44,44,44,44,38,38,38,38,32,32,32,26,26,26,20,20,20,20,17,14,14,14};
int l3ymax[124]={
  53,57,57,57,-9,-9,49,49,-9,-9,41,41,-9,-9,29,29,-9,-9,21,21,-9,-9,13,13,13, 9,51,51,51,51,43
  ,43,35,35,27,27,19,19,19,19,49,49,49,49,43,43,43,43,34,37,37,31,31,31,25,25,25,25,19,19,19,19
  ,53,53,53,53,-9,-9,45,45,-9,-9,37,37,-9,-9,25,25,-9,-9,17,17,-9,-9, 9, 9, 9, 9,47,47,47,47,39
  ,39,31,31,23,23,15,15,15,15,46,46,46,46,40,40,40,40,34,34,34,28,28,28,22,22,22,22,19,16,16,16};
int l4xmin[124]={
  24,16, 8, 0,-9,-9, 8, 0,-9,-9, 8, 0,-9,-9, 8, 0,-9,-9, 8, 0,-9,-9,16, 8, 0,28,24,16, 8, 0, 8
  , 0, 8, 0, 8, 0,24,16, 8, 0,26,23,14, 8,26,23,14, 8,23,14, 8,20,14, 8,26,20,14, 8,26,20,14, 8
  ,44,52,60,32,-9,-9,52,60,-9,-9,52,60,-9,-9,52,60,-9,-9,52,60,-9,-9,36,44,52,60,36,44,52,60,52
  ,60,52,60,52,60,36,40,52,60,35,41,47,53,35,41,47,53,41,47,53,41,47,53,35,41,47,53,35,41,47,53};
int l4xmax[124]={
  27,19,11, 3,-9,-9,11, 3,-9,-9,11, 3,-9,-9,11, 3,-9,-9,11, 3,-9,-9,19,11, 3,31,27,19,11, 3,11
  , 3,11, 3,11, 3,27,19,11, 3,28,25,16,10,28,25,16,10,23,16,10,22,16,10,28,22,16,10,28,22,16,10
  ,47,55,63,35,-9,-9,55,63,-9,-9,55,63,-9,-9,55,63,-9,-9,55,63,-9,-9,39,47,55,63,39,47,55,63,55
  ,63,55,63,55,63,39,43,55,63,37,43,49,55,37,43,49,55,43,49,55,43,49,55,37,43,49,55,37,43,49,55};
int l4ymin[124]={
  50,54,54,54,-9,-9,46,46,-9,-9,38,38,-9,-9,26,26,-9,-9,18,18,-9,-9,10,10,10, 6,48,48,48,48,40
  ,40,32,32,24,24,16,16,16,16,47,47,47,47,41,38,41,41,35,35,35,29,29,29,23,23,23,23,17,17,17,17
  ,50,50,50,50,-9,-9,42,42,-9,-9,34,34,-9,-9,22,22,-9,-9,14,14,-9,-9, 6, 6, 6, 6,44,44,44,44,36
  ,36,28,28,20,20,12,12,12,12,44,44,44,44,38,38,38,38,32,32,32,26,26,26,20,20,20,20,14,14,14,14};
int l4ymax[124]={
  53,57,57,57,-9,-9,49,49,-9,-9,41,41,-9,-9,29,29,-9,-9,21,21,-9,-9,13,13,13, 9,51,51,51,51,43
  ,43,35,35,27,27,19,19,19,19,49,49,49,49,43,40,43,43,37,37,37,31,31,31,25,25,25,25,19,19,19,19
  ,53,53,53,53,-9,-9,45,45,-9,-9,37,37,-9,-9,25,25,-9,-9,17,17,-9,-9, 9, 9, 9, 9,47,47,47,47,39
  ,39,31,31,23,23,15,15,15,15,46,46,46,46,40,40,40,40,34,34,34,28,28,28,22,22,22,22,16,16,16,16};

int iPINs[NECELLS]={-1};
int iLEDs[NECELLS]={-1};

void initPINs(){
  for(int ip=0; ip<124; ++ip){
    int z=pinarea[ip];
    for(int x=l1xmin[ip]; x<=l1xmax[ip]; ++x){
      for(int y=l1ymin[ip]; y<=l1ymax[ip]; ++y){
	if(isECALcell(z,x,y)){
	  int icell=cellid_E(z,x,y);
	  int iECAL=cellID2iECAL(icell);
	  iPINs[iECAL]=ip;
	  iLEDs[iECAL]=0;
	}
      }
    }
    for(int x=l2xmin[ip]; x<=l2xmax[ip]; ++x){
      for(int y=l2ymin[ip]; y<=l2ymax[ip]; ++y){
	if(isECALcell(z,x,y)){
	  int icell=cellid_E(z,x,y);
	  int iECAL=cellID2iECAL(icell);
	  iPINs[iECAL]=ip;
	  iLEDs[iECAL]=1;
	}
      }
    }
    for(int x=l3xmin[ip]; x<=l3xmax[ip]; ++x){
      for(int y=l3ymin[ip]; y<=l3ymax[ip]; ++y){
	if(isECALcell(z,x,y)){
	  int icell=cellid_E(z,x,y);
	  int iECAL=cellID2iECAL(icell);
	  iPINs[iECAL]=ip;
	  iLEDs[iECAL]=2;
	}
      }
    }
    for(int x=l4xmin[ip]; x<=l4xmax[ip]; ++x){
      for(int y=l4ymin[ip]; y<=l4ymax[ip]; ++y){
	if(isECALcell(z,x,y)){
	  int icell=cellid_E(z,x,y);
	  int iECAL=cellID2iECAL(icell);
	  iPINs[iECAL]=ip;
	  iLEDs[iECAL]=3;
	}
      }
    }
  }
}

int iECAL2iPIN(int iECAL){
  if(iPINs[0]<0)initPINs();
  return iPINs[iECAL];
}

char* PINname(int iPIN){
  if(iPINs[0]<0)initPINs();
  
  static char nm[8];
  sprintf(nm,"%c%d%2.2d",pinside[iPIN],pinarea[iPIN],pinnum[iPIN]);
  return &nm[0];
}

int iECAL2iLED(int iECAL){
  if(iPINs[0]<0)initPINs();
  return iLEDs[iECAL];
}

bool isECALLED(int z, int p, int l){
  if(z!=4)return false;
  if(p<0 || p>=124)return false;
  if(0==l)return l1xmax[p]>0;
  else if(1==l)return l2xmax[p]>0;
  else if(2==l)return l3xmax[p]>0;
  else if(3==l)return l4xmax[p]>0;
  else return false;
}

bool iPIN2zxy(int iPIN, int &area, int &xmin, int &xmax, int &ymin, int &ymax){
  if(iPINs[0]<0)initPINs();
  
  xmin=xmax=ymin=ymax=0;
  if(iPIN<0 || iPIN>=124)return false;
  
  area=pinarea[iPIN];
  xmin=pxmin[iPIN];
  xmax=pxmax[iPIN];
  ymin=pymin[iPIN];
  ymax=pymax[iPIN];
  
  return xmax>0;
}

bool iLED2zxy(int iPIN, int iLED, int &area, int &xmin, int &xmax, int &ymin, int &ymax){
  if(iPINs[0]<0)initPINs();
  
  xmin=xmax=ymin=ymax=0;
  if(iPIN<0 || iPIN>=124)return false;
  if(iLED<0 || iLED>=4)return false;
  
  area=pinarea[iPIN];
  
  if(0==iLED){
    xmin=l1xmin[iPIN];
    xmax=l1xmax[iPIN];
    ymin=l1ymin[iPIN];
    ymax=l1ymax[iPIN];
  }else if(1==iLED){
    xmin=l2xmin[iPIN];
    xmax=l2xmax[iPIN];
    ymin=l2ymin[iPIN];
    ymax=l2ymax[iPIN];
  }else if(2==iLED){
    xmin=l3xmin[iPIN];
    xmax=l3xmax[iPIN];
    ymin=l3ymin[iPIN];
    ymax=l3ymax[iPIN];
  }else if(3==iLED){
    xmin=l4xmin[iPIN];
    xmax=l4xmax[iPIN];
    ymin=l4ymin[iPIN];
    ymax=l4ymax[iPIN];
  }
  
  return xmax>0;
}


bool isECALcell(int z, int x, int y){
  if(z!=1 && z!=2 && z!=3)return isECALLED(z,x,y);
  if(1==z){
    if(x<0 || x>63 || y<6 || y>57)return false;
    if(x>=16 && x<=47 && y>=22 && y<=41)return false;
  }else if(2==z){
    if(x<0 || x>63 || y<12 || y>51)return false;
    if(x>=16 && x<=47 && y>=20 && y<=43)return false;
  }else if(3==z){
    if(x<8 || x>55 || y<14 || y>49)return false;
    if(x>=24 && x<=39 && y>=26 && y<=37)return false;
  }
  return true;
}

bool ECALcell2xy(int z, int x, int y, double& xc, double& yc){ // coordinates in mm; inner==(z==3), middle==(z==2), outer==(z==1)
  if(!isECALcell(z,x,y)){
    return false;
  }else{
    xc=((double)x-31.5)*stepX_ECAL[z];
    yc=((double)y-31.5)*stepY_ECAL[z];
    return true;
  }
}

bool ECALxy2cell(int& z, int& x, int& y, double xc, double yc){ // coordinates in mm; inner==(z==3), middle==(z==2), outer==(z==1)
  for(z=1; z<=3; ++z){
    x=(int)floor(xc/stepX_ECAL[z])+32;
    y=(int)floor(yc/stepY_ECAL[z])+32;
    if(isECALcell(z,x,y))	return true;
  }
  return false;
}

int cellid_E(int z, int x, int y){
  //int c=0x8000;
  int c=0;
  c += (x & 0x003F);
  c += (y & 0x003F) <<6;
  c += ((z-1) & 0x0003) <<12;
  return c;
}

bool zxy_E(int cellid, int& z, int& x, int& y){
  int idet=(cellid&0xC000)>>14;
  z=(cellid&0x3000)>>12;
  z+=1;
  y=(cellid&0x0FC0)>>6;
  x= cellid&0x003F  ;
  return (idet==2);
}

int ECALcellIDs[NECELLS]={0};
std::map<int,int> cellIDECAL;

int iECAL2cellID(int iECAL){
  if(0==ECALcellIDs[0]){
    int necal=0;
    for(int iz=1; iz<NEZ; ++iz){
      for(int ix=0; ix<NEX; ++ix){
	for(int iy=0; iy<NEY; ++iy){
	  if(isECALcell(iz,ix,iy)){
	    ECALcellIDs[necal]=cellid_E(iz,ix,iy);
	    ++necal;
	  }
	}
      }
    }
    if(necal!=NECELLS){
      printf("impossible: necal!=NECELLS!\n");
      return 0;
    }
  }
  
  if(iECAL>=0 && iECAL<NECELLS)return ECALcellIDs[iECAL];
  else return 0;
}

int cellID2iECAL(int cellID){
  if(0==cellIDECAL.size()){
    for(int i=0; i<NECELLS; ++i){
      cellIDECAL.insert(std::pair<int,int>(iECAL2cellID(i),i));
    }
  }
  return cellIDECAL[cellID];
}

int zxy2iECAL(int iz, int ix, int iy){
  if(iz<1 || iz>3)return -1;
  if(!isECALcell(iz,ix,iy)) return -1;
  int cid=cellid_E(iz,ix,iy);
  int iECAL=cellID2iECAL(cid);
  return iECAL;
}

TH2D* creECALmap(const char* name, const char* title){
  TH2D* emap=(TH2D*)gROOT->FindObject(name);
  if(emap){
    printf("creECALmap: %s already exists, deleting and re-creating...\n",name);
    emap->Delete();
  }
  
  emap=new TH2D(name,title,384,-3878.4,3878.4, 312,-3151.2,3151.2);
  emap->SetOption("colz");
  
  return emap;
}

TH2F* creECALmapF(const char* name, const char* title){
  TH2F* map=new TH2F(name,title,384,-3878.4,3878.4, 312,-3151.2,3151.2);
  return map;
}

bool isECALmapH(TH2* emap){
  if(!emap)return false;
  
  int nx=0, ny=0;
  double x1=0,x2=0,y1=0,y2=0;
  nx=emap->GetXaxis()->GetNbins();
  x1=emap->GetXaxis()->GetXmin();
  x2=emap->GetXaxis()->GetXmax();
  ny=emap->GetYaxis()->GetNbins();
  y1=emap->GetYaxis()->GetXmin();
  y2=emap->GetYaxis()->GetXmax();
  if(384==nx && -3878.4==x1 && 3878.4==x2 
     && 312==ny && -3151.2==y1 && 3151.2==y2)return true;
  return false;
}

bool isECALmapO(TObject* ob){
  if(!ob)return false;
  
  const char *classnam;//, *objnam;
  classnam=ob->IsA()->GetName();
  //objnam=ob->GetName();
  
  int cmp=strncmp("TH2",classnam,3);
  //printf("classnam=%s, objnam=%s, cmp=%d\n", classnam, objnam, cmp);
  if(0!=cmp)return false;
  
  TH2* emap=(TH2*)ob;
  return isECALmapH(emap);
}

int FEBwd2in_E(int wd){
  static int pre[32]={ 0, 4, 8,12,16,20,24,28, 1, 5, 9,13,17,21,25,29, 2, 6,10,14,18,22,26,30, 3, 7,11,15,19,23,27,31};
  if(wd>=0 && wd<32)return pre[wd];
  else return -1;
}

int FEBin2wd_E(int in){
  static int pre[32]={ 0, 8,16,24, 1, 9,17,25, 2,10,18,26, 3,11,19,27, 4,12,20,28, 5,13,21,29, 6,14,22,30, 7,15,23,31};
  if(in>=0 && in<32)return pre[in];
  else return -1;
}

void saveECALallHd(const char* dnam, const char* fnam, ...){
  TH1D* h1[100];
  TH2D* h2[100];
  for(int i=0; i<100;++i){h1[i]=NULL; h2[i]=NULL;}
  
  va_list vl;
  va_start(vl, fnam);
  
  gDirectory->cd("App:/");
  gDirectory->cd(dnam);
  
  int n=0;
  const char* p=0;
  while(true){
    try{
      h2[n]=va_arg(vl,TH2D*);
      if(NULL==h2[n])break;
      p=h2[n]->GetName();
      if(!p) break;
    }
    catch (...){
      break;
    }
    if(isECALmapH(h2[n])){
      h2[n]->SetOption("colz");
      h1[n]=makeECALmap_1H(h2[n]);
    }
    n++;
  }
  
  va_end(vl);
  
  TFile* wfil=new TFile(fnam,"update");
  
  if(!gDirectory->cd(dnam)){
    gDirectory->mkdir(dnam);
    gDirectory->cd(dnam);
  }
  
  for(int i=0; i<n;++i){
    if(h2[i])h2[i]->Write();
    if(h1[i])h1[i]->Write();
  }
  wfil->Close();
  gDirectory->cd("App:/");
}

void saveECALallH(char* fnam, ...){
  TH1D* h1[1000];
  TH2D* h2[1000];
  for(int i=0; i<1000;++i){h1[i]=NULL; h2[i]=NULL;}
  
  va_list vl;
  va_start(vl, fnam);
  
  int n=0;
  const char* p=0;
  while(true){
    try{
      h2[n]=va_arg(vl,TH2D*);
      if(NULL==h2[n])break;
      p=h2[n]->GetName();
      if(!p) break;
    }
    catch (...){
      break;
    }
    if(isECALmapH(h2[n])){
      h2[n]->SetOption("colz");
      h1[n]=makeECALmap_1H(h2[n]);
    }
    n++;
  }
  
  va_end(vl);
  
  char clast=fnam[strlen(fnam)-1];
  TFile* wfil=0;
  if('+'==clast){
    fnam[strlen(fnam)-1]='\0';
    wfil=new TFile(fnam,"update");
  }else wfil=new TFile(fnam,"recreate");
  for(int i=0; i<n;++i){
    if(h2[i])h2[i]->Write();
    if(h1[i])h1[i]->Write();
  }
  wfil->Close();
}

