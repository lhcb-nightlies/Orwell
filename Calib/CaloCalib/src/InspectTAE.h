// $Id: $
#ifndef INSPECTTAE_H
#define INSPECTTAE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/IEventTimeDecoder.h"


/** @class InspectTAE InspectTAE.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2011-03-17
 */
class InspectTAE : public GaudiAlgorithm {
public:
  /// Standard constructor
  InspectTAE( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~InspectTAE( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:
  std::map<std::string,IEventTimeDecoder*> m_odins;
  std::vector<std::string> m_slots;

};
#endif // INSPECTTAE_H
