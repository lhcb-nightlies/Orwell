// $Id: L0CaloGainMonitor.cpp,v 1.31 2009-06-04 07:53:00 robbep Exp $

// local
#include "L0CaloGainMonitor.h"

// Gaudi
#include "GaudiKernel/AlgFactory.h"

// Online
#include "Trending/ITrendingTool.h"

// AIDA
#include "AIDA/IHistogram1D.h"

// Calo
#include "CaloDet/DeCalorimeter.h"

// Event
#include "Event/L0CaloCandidate.h"
#include "Event/L0DUBase.h"
#include "Event/ODIN.h"

// Declare Algorithm
DECLARE_ALGORITHM_FACTORY( L0CaloGainMonitor )

//-----------------------------------------------------------------------------
// Implementation file for class : L0CaloGainMonitor
//
// 11/08/2011 : Patrick Robbe
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor
//=============================================================================
L0CaloGainMonitor::L0CaloGainMonitor( const std::string& name, 
				      ISvcLocator* pSvcLocator ) : 
  GaudiHistoAlg( name , pSvcLocator ) ,
  m_reset( true ) ,
  m_outerCounter( 0 ) ,
  m_innerCellCounter( 0 ) ,
  m_startTime( 0 ) { 
  declareProperty( "InputData" , 
		   m_inputData = LHCb::L0CaloCandidateLocation::Default ) ;  
  declareProperty( "EtThreshold" , m_etThreshold = 180 ) ;
  declareProperty( "Partition" , m_partition = "LHCb" ) ;
  declareProperty( "AnalysisTime" , m_analysisTime= 60 * 5 ) ; // 5 minutes
}

//=============================================================================
// Standard destructor
//=============================================================================
L0CaloGainMonitor::~L0CaloGainMonitor() { }

//=============================================================================
// Initialisation. 
//=============================================================================
StatusCode L0CaloGainMonitor::initialize() {

  StatusCode sc = GaudiHistoAlg::initialize();

  if ( sc.isFailure() ) return sc;  // error printed already by Calo2Dview

  debug() << "==> Initialize" << endmsg;

  // Retrieve the HCAL detector element, build cards
  m_hcal = getDet<DeCalorimeter>( DeCalorimeterLocation::Hcal );  

  // Book all histograms created by monitoring algorithm
  debug() << "==> Default Monitoring histograms booking " << endmsg;  

  m_innerCellHistogram = GaudiHistoAlg::book( "InnerCellHistogram" , 
					      "Inner Cell Histogram" , 
					      1312976804 , 
					      1312976804 + 
					      20 * 60 * 60 , 
					      120 ) ;

  m_outerHistogram = GaudiHistoAlg::book( "OuterHistogram" , 
					  "Outer Histogram" , 
					  1312976804 , 
					  1312976804 + 
					  20 * 60 * 60 , 
					  120 ) ;
 
  // Trending plot initialization
  m_trend = tool< ITrendingTool >( "TrendingTool" , 
				   "L0CaloGainTrends" , this ) ;
  std::string name = m_partition + "_L0CaloGainMonitor" ;
  std::vector< std::string > tags ;
  tags.push_back( std::string( "InnerToOuterFrequencyRatioTest" ) ) ;
  bool status = m_trend -> openWrite( name , tags ) ;
  if ( ! status ) 
    Error( "Cannot open trending file" , StatusCode::SUCCESS ).ignore() ;

  // Initialize counters
  m_reset = true ;
  m_outerCounter = 0 ;
  m_innerCellCounter = 0 ;
  m_startTime = 0 ;

  return StatusCode::SUCCESS; 
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode L0CaloGainMonitor::execute() {
  if ( ! exist< LHCb::L0CaloCandidates >( m_inputData ) ) {
    Warning( "No data at " + m_inputData ).ignore() ;
    return StatusCode::SUCCESS ;
  }
  
  LHCb::L0CaloCandidates * candidates = 
    get<LHCb::L0CaloCandidates>( m_inputData );
  LHCb::L0CaloCandidates::iterator cand ;
  
  if ( 0 == candidates ) return StatusCode::SUCCESS ;

  // Read ODIN bank to obtain time
  unsigned int event_time( 0 ) ;

  if ( exist< LHCb::ODIN >( LHCb::ODINLocation::Default ) ) {
    LHCb::ODIN * odin = get< LHCb::ODIN >( LHCb::ODINLocation::Default ) ;
    if ( 0 != odin ) {
      event_time = ( odin -> gpsTime() / 1000000 ) ;  // in sec
    }
  }

  // If reset requested:
  if ( m_reset ) {
    m_reset = false ;
    m_outerCounter = 0 ;
    m_innerCellCounter = 0 ;
    m_startTime = event_time ; 
  }

  LHCb::CaloCellID caloCell ;
  int row( 0 ) , col( 0 ) ;

  // Loop over the candidates
  for ( cand = candidates -> begin() ; candidates -> end() != cand ; 
        ++cand ) { 
    LHCb::L0CaloCandidate * theCand = (*cand) ;

    // Use only hadron candidates
    if ( theCand -> type() != L0DUBase::CaloType::Hadron ) 
      continue ;
    
    if ( theCand -> etCode() < m_etThreshold ) 
      continue ;
    
    caloCell = theCand -> id() ;

    if ( 0 == caloCell.area() ) { // Outer HCAL
      m_outerHistogram -> fill( event_time ) ;
      m_outerCounter++ ;
    } else if ( 1 == caloCell.area() ) { // Inner HCAL
      // keep cells around the beam pipe (1st layer ) 
      row = caloCell.row() ;
      col = caloCell.col() ;
      if ( ( ( row == 13 ) && ( ( col >= 13 ) && ( col <= 18 ) ) ) ||
	   ( ( row == 18 ) && ( ( col >= 13 ) && ( col <= 18 ) ) ) || 
	   ( ( col == 13 ) && ( ( row >= 13 ) && ( col <= 18 ) ) ) || 
	   ( ( col == 18 ) && ( ( row >= 13 ) && ( col <= 18 ) ) ) ) {
	m_innerCellHistogram -> fill( event_time ) ;
	m_innerCellCounter++ ;
      }
    }
  }

  // If analysis time is enough, request request and write in trend file
  if ( ( event_time - m_startTime ) > m_analysisTime ) {
    m_reset = true ;
    std::vector< float > values ;
    if ( m_outerCounter == 0 ) 
      values.push_back( 0. ) ;
    else
      values.push_back( (float) m_innerCellCounter / (float) m_outerCounter ) ;
    bool status = m_trend -> write( values ) ;
    if ( ! status ) 
      Error( "Cannot write values in trending file" , StatusCode::SUCCESS , 10 ).ignore() ;
  }

  return StatusCode::SUCCESS; 
}

