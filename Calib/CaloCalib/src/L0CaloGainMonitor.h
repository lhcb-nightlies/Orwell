// $Id: L0CaloGainMonitor.h,v 1.6 2009-06-03 18:04:18 robbep Exp $
#ifndef L0CALOGAINMONITOR_H
#define L0CALOGAINMONITOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"

// Forward declarations
class IHistogram1D ;
class DeCalorimeter ;
class ITrendingTool ;

/** @class L0CaloGainMonitor L0CaloGainMonitor.h
 *  Monitor the gain for the L0 HCAL trigger
 *
 *  @author Patrick Robbe
 *  @date   11/08/2011
 */

class L0CaloGainMonitor: public GaudiHistoAlg {

 public:
  /// Standard constructor
  L0CaloGainMonitor( const std::string& name , ISvcLocator* pSvcLocator ) ;

  /// Standard destructor
  virtual ~L0CaloGainMonitor( ) ;

  /// Initialization: book histograms
  StatusCode initialize() override;

  /// Main execution rouxtine: fill histograms and find hot cells
  StatusCode execute   () override;

 private:

  DeCalorimeter * m_hcal ; ///< Pointer to Hcal detector element

  std::string m_inputData ; ///< name of input data

  int         m_etThreshold ; ///< Et Threshold (in L0ADC) for the analysis

  std::string m_partition ; ///< Name of partition

  unsigned int m_analysisTime ; ///< Duration of analysis before storing in trend file

  /// Inner cell histogram
  IHistogram1D * m_innerCellHistogram ;

  /// Outer area histogram
  IHistogram1D * m_outerHistogram ;

  /// Trending tool pointer
  ITrendingTool * m_trend ;

  /// Request to reset counters at next event
  bool m_reset ;

  /// Counter of candidates in outer HCAL
  unsigned int m_outerCounter ;

  /// Counter of candidates in inner cells of HCAL
  unsigned int m_innerCellCounter ;

  /// Time of start of analysis
  unsigned int m_startTime ;

};
#endif // L0CALOGAINMONITOR_H
