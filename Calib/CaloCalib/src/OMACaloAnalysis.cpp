#include "GaudiKernel/AlgFactory.h" 
#include <string>
#include "GaudiAlg/Fill.h"
#include "OnlineHistDB/OnlineHistDB.h"
#include "OnlineHistDB/OnlineHistogram.h"
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "OMACaloAnalysis.h"
#include "CaloUtils/CaloAlgUtils.h"
#include <fstream>

DECLARE_ALGORITHM_FACTORY( OMACaloAnalysis )



double fg2(double*x,double*par){
  double xx=x[0];
  double i = 0.5*(1+TMath::Erf((0-par[1])/(par[2]*1.41421356237309515)));

  if(xx<=0) return par[0]*i+par[0]*0.398942280401*TMath::Exp(-0.5*TMath::Power((0-par[1])/par[2],2))/par[2]; 

  return par[0]*0.398942280401*TMath::Exp(-0.5*TMath::Power((xx-par[1])/par[2],2))/par[2];
  
}

double fg3(double*x,double*par){
  Double_t xx=TMath::Nint(x[0]); // #### +0.5 (ask Valentin)

  if(xx<1) return par[0]*0.5*(1+TMath::Erf((0.5-par[1])/(par[2]*1.41421356237309515))); 
  
  return par[0]*0.5*
    (TMath::Erf((xx+0.5-par[1])/(par[2]*1.41421356237309515))-TMath::Erf((xx-0.5-par[1])/(par[2]*1.41421356237309515)));
}

// Constructor
OMACaloAnalysis::OMACaloAnalysis(const std::string& name,ISvcLocator* pSvcLocator ) :
  AnalysisTask( name, pSvcLocator )
  ,m_first(true)
  ,m_mon(false)
  ,m_count(0)
  ,m_sDirPath(""){    
  declareProperty("HistoSummary"  , m_summary);            // optional (default = [Calo]MonitorSummary 
  declareProperty("Monitors"      , m_monitors  );         // List of CaloMonitors 
  declareProperty("xLabelsOptions" , m_lab ="" );           // Summary Histo label options (""/"v") 
  declareProperty("LogAlarms"     , m_log = true );       // Alarm report in log output  
  declareProperty("FullReport"    , m_full  = false);      // Produce a complete report in the log
  declareProperty("skipKnownAlarms",m_skipKnown = true);// Produce Alarm report only for New or Resurrected  problematic channels 
  declareProperty("ProduceSaveSet"    , m_storeInSaveSet = false ); // Store Output histos in a dedicated SaveSet 
  declareProperty("SaveSetDir"    , m_sDir = "/hist/Savesets" ); // Store Output histos in a dedicated SaveSet 
  declareProperty("SaveInInput", m_copyInInput = false );       // Make a copy in the input SaveSet 
  declareProperty("SaveFitInInput", m_storeFit = false );    // Store fit in the SaveSet (for test only) 
  declareProperty("InputTopDir"   , m_source=""  );         // input top directory
  declareProperty("CounterHisto"  , m_hisCount  );     // optional : input counter histo in SaveSet 
  declareProperty("MinStatistics" , m_stat = 100 );        // Statistic threshold 
  declareProperty("TELL1s"        , m_tell1    );        // expert usage : list of Tell1s to be monitored (default all) 
  declareProperty("FEBs"          , m_feb      );        // expert usage : list of crates to be monitored (default all) 
  declareProperty("Crates"        , m_crate    );        // expert usage : list of febs to be monitored (default all) 
  declareProperty("RevertSelection", m_revert=false  );  // Revert Tell1/FEB/Crate selection 
  declareProperty("LocationInPath",  m_loc  = true); // expert usage : to be synchronized with CaloCalib histo production 
  declareProperty("FitOptions"    ,  m_fit  = "LQ");  // expert usage : Fit options 
  declareProperty("Chi2CutOff"    ,  m_chi2CutOff = 250.);  // expert usage : Chi2 cut-off for display 
  declareProperty("TrendPeriod"   ,  m_trending = 48); // # consecutive Savesets (N * 15 mn) : default 12 hours in continuous run 
  declareProperty("ControlChannels", m_controls); 
  declareProperty("ControlChannelsShow", m_controlsDir); 
  declareProperty("HistDBPagePrefix", m_prefPage="");
  declareProperty("OddNumericGains"   , m_prsGainsO); // temporary (will be in condDB)
  declareProperty("EvenNumericGains"  , m_prsGainsE); // temporary (will be in condDB)
  declareProperty("SubChannel"    , m_subChannel  = 1 );  // PRS subchannel for updating conf files (temp)
  declareProperty("SplitAnalysis" , m_splitAna);
  declareProperty("UpdatedConfFiler"    , m_uConfFiler="");
  declareProperty("ReferenceConfFiler"  , m_rConfFiler="Reference/");
  declareProperty("PrsUpdateThreshold"  , m_upd = 1);
  declareProperty("FitQualityThreshold"      , m_uRange = 20);
  declareProperty("HistDBBuildPage"     , m_build = false); // expert usage
  declareProperty("HistDBPageComment"   , m_comment = true); // expert usage
  declareProperty("UpdatedCondDBTable"   , m_dbTable = ""); // path to the condDB table printout
  declareProperty("UpdateIncompleteCondDB",m_updIncomplete = false); // update the condDB table when incomplete
  declareProperty("CreateNewReference",m_newref=false);
  declareProperty("CondDBTableBackUp",m_nBackUp = 10); // number of previous backup (< 0 --> timestamp ALL tables)
  declareProperty("ForceProcessing"  ,m_force = false); // force processing when no counter available (expert usage)
  declareProperty("ShortSummary"     ,m_short);

  m_splitAna.clear();
  m_hisCount.clear();
  m_controlsDir.clear();
  // 
  m_prsOut = false;
  m_prsUpd["EvenBX"] = true;
  m_prsUpd["OddBX"] = true;
  
	// check MonitorSvc is at work
 	m_mon  = pSvcLocator->existsService("MonitorSvc");
  if(m_mon)info() << "MonitoringSvc is at work" << endmsg;

  // set default analysis task name (Saveset directory)
  m_anaTask = "CaloCalibAnalysis";
  m_trend = "CF"; // calibration farm analysis
  if( "" != context() ){
    m_anaTask = context();  // set it via context
    m_trend = "MF"; // monitoring farm analysis
  }
  //m_anaTaskname = m_anaTask; // AnalysisTask doesn't allow multiple DIM access to alarms with the same naming !!
  
  // set default detectorName
  m_detectorName = LHCb::CaloAlgUtils::CaloNameFromAlg( name );


  // Summary histo dir :
  m_summary = m_detectorName+"MonitorSummary";

  // set default counter histo name (CaloCalib analysis)
  m_source = m_detectorName+"Calib";



  // special case for Hcal
  if ( m_detectorName == "Hcal" ){
    m_feb.push_back(51); // DO NOT MONITOR UNFIRED PIN
    m_feb.push_back(53);
    m_revert = true;
  }
  
} 

// Destructor
OMACaloAnalysis::~OMACaloAnalysis(){}

//--------------------------------------------------
StatusCode OMACaloAnalysis::finalize(){
  int nbad = 0;
  for( MonMap::iterator im = m_monMap.begin() ; m_monMap.end() != im ; ++im){
    if( im->second->status() ){
      if (msgLevel( MSG::DEBUG))debug() << " CaloMonitor '" << im->first 
              << "' is badly configured or  have too low (or missing) data to be processed" << endmsg;
      nbad++;
    }    
  }
  if( nbad > 0 )warning() << " --- Found " << nbad << " unprocessed CaloMonitor(s) " 
                          << "(missing data, low statistics or bad configuration)" << endmsg;
  
  return AnalysisTask::finalize();
}
StatusCode OMACaloAnalysis::initialize(){
  return  AnalysisTask::initialize();
}


// first pass initialization ========
StatusCode OMACaloAnalysis::init(){

  m_first = false;
  m_count = 0;
  m_cloned.clear(); // List of cloned input histos
  m_trendMap.clear();
  m_trendValMap.clear();
  m_monMap.clear();

  // detector dependent setting
  if( m_detectorName == "Prs" && context() == ""){
    if(m_splitAna.empty()){
      m_splitAna.push_back( "EvenBX" );
      m_splitAna.push_back( "OddBX" );
    }
    if(m_hisCount.empty()){
      m_hisCount.push_back( m_source+ "EvenBX" + "/Counters/1" );
      m_hisCount.push_back( m_source+ "OddBX" + "/Counters/1" );
    }
    if(m_controlsDir.empty()){
      m_controlsDir.push_back( "OddBx/Pedestal" );
      m_controlsDir.push_back( "EvenBx/Pedestal" );
    }
  }else if(m_detectorName == "Spd"  && context() == ""){
    if(m_hisCount.empty()){
      m_hisCount.push_back( m_source+ "/Counters/2" );
      m_hisCount.push_back( m_source+ "/Counters/1" );
    }
    if(m_splitAna.empty()){
      m_splitAna.push_back( "EvenBX" );
      m_splitAna.push_back( "OddBX" );
    }
  }else if(m_detectorName == "Ecal" || m_detectorName == "Hcal" ){
    if(m_splitAna.empty())m_splitAna.push_back( "" );
    if(m_hisCount.empty())m_hisCount.push_back( m_source+ "/Counters/1" );
    if(m_controlsDir.empty()){
      m_controlsDir.push_back( "/Pedestal" );
      m_controlsDir.push_back( "/All" );
    }
  }else{
    if(m_splitAna.empty())m_splitAna.push_back("");
    if(m_hisCount.empty())m_hisCount.push_back( m_source+ "/Counters/1" );
  }

  if( m_splitAna.size() != m_hisCount.size() ){
    Error("Split.size != HistoCounter.size" , StatusCode::FAILURE );
    return StatusCode::FAILURE;
  }



  // Check MonitorSvc is available 
  m_pGauchoMonitorSvc = NULL;
  if( m_mon )
    if( serviceLocator()->service("MonitorSvc", m_pGauchoMonitorSvc, false).isFailure())
      return Error("Cannot load MonitorSvc",StatusCode::FAILURE);
  

  // Saveset path
  if ( m_storeInSaveSet && "" == m_sDir )m_sDir=".";

  // get DeCalorimeter
  m_calo = getDet<DeCalorimeter>( LHCb::CaloAlgUtils::DeCaloLocation( m_detectorName ) );


  // Get Prs numeric Gains from condDB (if not set via options)
  if( m_detectorName == "Prs" && m_prsGainsO.empty() && m_prsGainsE.empty() ){
    debug() << "Get Prs Numeric Gains from condDB" << endmsg;
    const CaloVector<CellParam>& cells = m_calo->cellParams();
    for( CaloVector<CellParam>::const_iterator it = cells.begin(); it != cells.end() ; ++it){
      LHCb::CaloCellID id = it->cellID();
      id.setCalo("Prs");
      //      m_prsGainsO[id] = it->numericGain(0);
      //      m_prsGainsE[id] = it->numericGain(1);
      //if (msgLevel( MSG::DEBUG))debug() << "Numeric Gain from DB " << id << " : " << it->numericGain() << endmsg;
    }
  }
  else if( m_detectorName == "Prs" && !m_prsGainsO.empty() && !m_prsGainsE.empty() ){
    info() << "Get Prs Numeric Gains from options setting" << endmsg;
  }else if( m_detectorName == "Prs")
    info() << "No Prs Numeric Gains defined" <<endmsg;
  

  // post-processing :

  loadCaloMonitors();            // create all CaloMonitors (external + automatic) 
  trendLogHeader();              // trends logout header 
  iniBook();                     // book & register (global summaries, trending, ...)
  if( m_build )buildHistDB();    // Re-build histDB pages
  info() << "initialization successful" << endmsg;
  return StatusCode::SUCCESS;}
  

//============================
void OMACaloAnalysis::loadCaloMonitors(){
  // get CaloMonitor tools =====================
  m_bad = 0;  // count badly configured monitors
  std::vector<int> flags;

  int lastID = 0;
  for( std::vector<std::string>::iterator mon = m_monitors.begin() ; m_monitors.end() != mon ; ++mon){
    CaloMonitor* monitor = tool<CaloMonitor>( "CaloMonitor" , *mon , this );
    m_monMap[ *mon ]= monitor;
    monitor->setID(lastID);
    if(m_mon)monitor->openTrendFile(); // Online trending
    lastID++;
    // automatic creation of associated CaloMonitor
    if( prsAutoUpdate(monitor)  ){
      getAutoCaloMonitor( monitor->iName() + "_AutoOffset")->setID(lastID);
      getAutoCaloMonitor( monitor->iName() + "_AutoOffsetShift",-3.,+3.,"PEDESTAL","OFFSETSHIFT")->setID(lastID+1);
      lastID += 2;
    }
    bool ok = true;
    if(monitor->hasDBQualityMask()){
      for(std::vector<int>::iterator iflag = flags.begin();flags.end()!=iflag;++iflag){
        if( ( monitor->dbQualityMask() & *iflag) != 0){
          ok=false;
          Warning("Two CaloMonitors cannot share the same DB Quality flag ('"+*mon+" removed from the list)").ignore();
          break;
        }
      }  
      ok ?  flags.push_back(monitor->dbQualityMask()) : monitor->setConfStatus(false);
    }
    if( !monitor->confStatus() )m_bad++;
  }
  info() << "Loaded " << m_monMap.size()  << " monitoring tools (" << m_bad << " badly configured)"<< endmsg;
  std::ostringstream n;
  n<<m_bad;
  if( m_bad > 0 )Warning("Found " + n.str() + " badly configured CaloMonitors - please check your options").ignore();


  // update m_monitors with auto-configured CaloMonitors
  for( MonMap::iterator i = m_monMap.begin() ; m_monMap.end() != i ; ++i){
    if( i->second->autoConf())m_monitors.push_back(i->first);
  }
}

//============================
void OMACaloAnalysis::trendLogHeader(){
  // trend log
  m_mylog.str("");
  std::ostringstream trend("");
  trend << std::string("@@@" + m_detectorName + m_trend + "Trend |").c_str();
  trend << format("%6s", std::string(" # |").c_str()) ;
  trend << format("%8s", std::string(" stat  |").c_str()) ;
  trend << format("%28s", std::string(" TimeStamp |").c_str()) ;  
  int im = 0;

  for( std::vector<std::string>::iterator imon = m_monitors.begin() ; m_monitors.end() != imon ; ++imon){
    CaloMonitor* monitor = m_monMap[ *imon ];
    std::string mon = *imon;
    //  for( MonMap::iterator i = m_monMap.begin() ; m_monMap.end() != i ; ++i){
    //CaloMonitor* monitor =  i->second;
    //std::string mon = i->first;
    trend << format("%4i", im ) << "|";
    std::ostringstream lname("");
    lname << format("%40s",std::string(mon).c_str());
    std::string map = "@@@" + m_detectorName + m_trend + "Trend ::  "
      + Gaudi::Utils::toString(format("%3i",im)) + " =>" + lname.str() + " " +  monitor->alarmSetting();
    logOut( map ); 
    im++;
  }
  logOut(std::string("@@@" + m_detectorName + m_trend + "Trend ::  " +
                     "Number of problematic channels per CaloMonitor as a function of the SaveSet : "));
  logOut( trend.str() );
  info() << std::endl << m_mylog.str() << endmsg;
}



//----------------------------
bool OMACaloAnalysis::iniBook(){


  if (msgLevel( MSG::DEBUG))debug() << "book summary histo" << endmsg;
  //----- pre-book summary histos
  int nLevels = CaloMonitorLevel::Number;
  if ( "" != m_summary ){



    //-- Short summary (aka DataManager plot)
    int ssize = m_short.size();
    if( ssize > 0 ){
      AIDA::IHistogram2D* h0 = 
        book2D( m_summary +"/ShortSummary" 
                , m_detectorName + " Alarm *short* Summary : NEW problematic channels "
                ,-1. , (double) nLevels , nLevels+1
                , 0. , (double) ssize, ssize ); 
      Gaudi::Utils::Aida2ROOT::aida2root( h0 )->SetName( std::string(name()+"/"+m_summary + "/ShortSummary").c_str() );
      shortBinLabels( h0 );
    }
    
    //-- Detailled summary
    int size = m_monMap.size();
    AIDA::IHistogram2D* h1 = 
      book2D( m_summary +"/New" 
              , m_detectorName + " Alarm Summary : NEW problematic channels "
              ,-1. , (double) nLevels , nLevels+1
              , 0. , (double) size, size ); 
    Gaudi::Utils::Aida2ROOT::aida2root( h1 )->SetName( std::string(name()+"/"+m_summary + "/New").c_str() );
    
    AIDA::IHistogram2D* h2 = 
      book2D( m_summary +"/Resurrected" 
              , m_detectorName + " Alarm Summary : RESURRECTED channels "
              ,-1. , (double) nLevels , nLevels+1
              , 0. , (double) size, size);
    Gaudi::Utils::Aida2ROOT::aida2root( h2 )->SetName( std::string(name()+"/"+m_summary + "/Resurrected").c_str() );
    
    AIDA::IHistogram2D* h3 = 
      book2D( m_summary +"/Known"
              , m_detectorName + " Alarm Summary : KNOWN problematic channels)"
              ,-1. , (double) nLevels , nLevels+1
              , 0. , (double) size, size);
    Gaudi::Utils::Aida2ROOT::aida2root( h3 )->SetName( std::string(name()+"/"+m_summary + "/Known").c_str() );
    setBinLabels( h1 );
    setBinLabels( h2 );
    setBinLabels( h3 );
  }


  if( m_trending<0)return true;
  // trends booking
  if (msgLevel( MSG::DEBUG))debug() << "book trending histo" << endmsg;

  // statistic counter trending
  AIDA::IHistogram1D* sh1 = book1D( "Counters/EventTrending",
                                    "Number of  data events  (trending)",
                                    0., (double) m_trending, m_trending); 
  Gaudi::Utils::Aida2ROOT::aida2root( sh1 )->SetName( std::string(name()+"/Counters/EventTrending").c_str()) ;
  m_statMap["EventTrending"]= sh1 ;
  AIDA::IHistogram1D* sh2 = book1D( "Counters/SIGTrending",
                                    "Average number of  signal flash/channel  (trending)",
                                    0., (double) m_trending, m_trending);  
  Gaudi::Utils::Aida2ROOT::aida2root( sh2 )->SetName( std::string(name()+"/Counters/SIGTrending").c_str() );
  m_statMap["LEDTrending"]= sh2 ;
  AIDA::IHistogram1D* sh3 = book1D( "Counters/PEDTrending",
                                    "Average number of  pedestal entry/channel  (trending)",
                                    0., (double) m_trending, m_trending);  
  Gaudi::Utils::Aida2ROOT::aida2root( sh3 )->SetName( std::string(name()+"/Counters/PEDTrending").c_str() );
  m_statMap["PEDTrending"]= sh3 ;
  
  
  // 2D problem counter
  AIDA::IHistogram2D* h4 = book2D( "Trending", "number of problematic channels (trending)",
                                   0, (double) m_trending, m_trending,
                                   0, m_monMap.size(), m_monMap.size());
  Gaudi::Utils::Aida2ROOT::aida2root( h4 )->SetName( std::string(name()+"/Trending").c_str() );
  
  // 1D problems counter
  for( MonMap::iterator im = m_monMap.begin() ; m_monMap.end() != im ; ++im){
    CaloMonitor* monitor =  im->second;
    std::string mon = im->first;
    if( !monitor->confStatus() )continue;
    
    AIDA::IHistogram1D* h = book1D( mon + "/Trending",
                                    mon + " : number of problematic channels (trending)",
                                    0., (double) m_trending, m_trending);
    Gaudi::Utils::Aida2ROOT::aida2root( h )->SetName( std::string(name()+"/"+mon + "/Trending").c_str() );
    m_trendMap[mon]= h ;

    
    // average value
    const std::string dName = CaloMonitorDataName::Name[ monitor->dataName()  ];
    const std::string dType = CaloMonitorDataType::Name[ monitor->dataType() ];
    AIDA::IHistogram1D* hh = book1D( mon + "/"+dName+dType+"Trending",
                                       dName + " " + dType + " average over " + m_detectorName+ " PMTs (trending)",
                                     0., (double) m_trending, m_trending);
    Gaudi::Utils::Aida2ROOT::aida2root( hh )->SetName( std::string(name()+"/"+mon + "/"+dName+dType+"Trending").c_str() );
    m_trendValMap[mon + "/average"]=hh;    
    AIDA::IHistogram1D* hhs = book1D( mon + "/"+dName+dType+"TrendingSpread",
                                       dName + " " + dType + " spread over " + m_detectorName+ " PMTs (trending)",
                                     0., (double) m_trending, m_trending);
    Gaudi::Utils::Aida2ROOT::aida2root( hhs )->SetName( std::string(name()+"/"+mon + "/"+dName+dType+"TrendingSpread").c_str() );
    m_trendValMap[mon + "/spread"]=hhs;    
    AIDA::IHistogram1D* hhm = book1D( mon + "/"+dName+dType+"TrendingMin2Max",
                                       dName + " " + dType + " min-to-max over " + m_detectorName+ " PMTs (trending)",
                                     0., (double) m_trending, m_trending);
    Gaudi::Utils::Aida2ROOT::aida2root( hhm )->SetName( std::string(name()+"/"+mon + "/"+dName+dType+"TrendingMin2Max").c_str() );
    m_trendValMap[mon + "/min2max"]=hhm;    

    /*
      AIDA::IHistogram1D* hhh = book1D( *mon + "/"+dName+dType+"SpreadTrending",
      dName + " " + dType + " spread over " + m_detectorName+ " PMTs (trending)",
      0., (double) m_trending, m_trending);
      Gaudi::Utils::Aida2ROOT::aida2root( hhh )->SetName(std::string(name()+"/"+*mon + "/"+dName+dType+"SpreadTrending").c_str());
        m_trendSpreadMap[mon]=hhh;    
    */
    
    if( monitor->overall() )continue;
    // Trending control channels    
    int k = 0;
    for(std::map<int,std::string>::iterator ic = m_controls.begin();ic!=m_controls.end();++ic){
      k++;
      std::ostringstream loc;
      const LHCb::CaloCellID id = getID(ic->first);
      if( LHCb::CaloCellID() == id)continue;
      int feb = m_calo->cardNumber( id );
      loc << "[crate=" << format("%02i", m_calo->cardCrate( feb ) ) <<  ","
          << "feb="   << format( "%02i" , m_calo->cardSlot( feb  ) )<< ","
          << "readout=" <<Gaudi::Utils::toString( m_calo->cardColumn( id ) + nColCaloCard * m_calo->cardRow( id ) )
          << "] => " << id << " : index="<< Gaudi::Utils::toString( id.index() )  ;
      std::string unit =  "ControlChannels/" +  Gaudi::Utils::toString( k ) + "/" + dName+dType + "Trending" ;
      if( histo1D(HistoID( unit )) == NULL){
        AIDA::IHistogram1D* cth = book1D( unit,
                                          dName+ " " + dType + " trending for control channel " + loc.str(), 
                                          0, (double) m_trending, m_trending );
        Gaudi::Utils::Aida2ROOT::aida2root( cth )->SetName( std::string(name()+"/"+unit ).c_str() );
      } 
    }
  }
  // end trends booking
  return true;
}
  
  

//--------------------------------------------------
void OMACaloAnalysis::resetMonitors(){
  // reset properly configured monitors
  for( MonMap::iterator im = m_monMap.begin() ; m_monMap.end() != im ; ++im){
    CaloMonitor* monitor = im->second;
    std::string nam = name() +"/";
    
    if( monitor->pmtMonitor() ){
      AIDA::IHistogram1D* h1 = monitor->calo1D(nam, false ) ;
      if( 0 != h1 )h1->reset();
    }
    if( monitor->pinMonitor() ){
      AIDA::IHistogram1D* h1 = monitor->calo1D(nam, true ) ;
      if( 0 != h1 )h1->reset();
    }
    monitor->reset();
    if( monitor->confStatus() )monitor->setStatus( true );
  }
}


void OMACaloAnalysis::reset(){
  // clean maps and vectors
  for(std::vector<TH1D*>::iterator it = m_inter.begin();m_inter.end() != it ; ++it){
    if( NULL != *it) delete *it;
  }
  m_inter.clear();
  m_fitMap.clear();
  m_prsUpd["EvenBX"] = true;
  m_prsUpd["OddBX"] = true;

  // reset monitors & histos
  m_mylog.str("");
  if( histo2D (HistoID(m_summary + "/ShortSummary") ) != NULL ){
    if (msgLevel( MSG::DEBUG))debug() << "Reseting 2D histo '" << m_summary+"/ShortSummary" << "'" << endmsg;
    histo2D(HistoID(m_summary +"/ShortSummary"))->reset();
  }  
  if( histo2D (HistoID(m_summary + "/New") ) != NULL ){
    if (msgLevel( MSG::DEBUG))debug() << "Reseting 2D histo '" << m_summary+"/New" << "'" << endmsg;
    histo2D(HistoID(m_summary +"/New"))->reset();
  }  
  if( histo2D(HistoID( m_summary + "/Known" )) != NULL ){
    if (msgLevel( MSG::DEBUG))debug() << "Reseting 2D histo '" << m_summary+"/Known" << "'" << endmsg;
    histo2D(HistoID(m_summary +"/Known"))->reset();
  }
  if( histo2D(HistoID( m_summary + "/Resurrected" )) != NULL ){
    histo2D(HistoID(m_summary +"/Resurrected"))->reset();
    if (msgLevel( MSG::DEBUG))debug() << "Reseting 2D histo '" << m_summary+"/Resurrected" << "'" <<endmsg;
  }
  // reseting clone h1D
  for(std::vector<AIDA::IHistogram1D*>::iterator ic = m_cloned.begin() ; m_cloned.end() != ic ; ++ic){
    (*ic)->reset();
  }
  if (msgLevel( MSG::DEBUG))debug() << "Reseting CaloMonitors " << endmsg;
  resetMonitors();
}
  


//--------------------------------------------------
StatusCode OMACaloAnalysis::execute(){
  // update gaucho at each pass (each saveset)
  if ( m_mon ){
    if (msgLevel( MSG::DEBUG))debug() << "Update All" << endmsg;
    m_pGauchoMonitorSvc->updateAll(false); 
  }  
  return StatusCode::SUCCESS;
}

//--------------------------------------------------
StatusCode OMACaloAnalysis::analyze(std::string& SaveSet, std::string Task){

  // first pass init
  if(m_first)
    if(init().isFailure() )return Error("Analysis failed",StatusCode::SUCCESS);

  // pass counter
  m_count++;
  
  // reset
  if (msgLevel( MSG::DEBUG))debug() << "reseting" << endmsg;

  reset();

  //
  AnalysisTask::analyze( SaveSet ,Task); // init Saveset & Task naming
  if( m_savesetName == "" && m_taskname == "")return Warning("No saveset nor task to analyze",StatusCode::SUCCESS);

  // output saveset path :
  char year[5];
  char month[3];
  char day[3]; 
  time_t rawTime=time(NULL); 
  struct tm* timeInfo = localtime(&rawTime); 
  ::strftime(year, sizeof(year),"%Y", timeInfo); 
  ::strftime(month, sizeof(month),"%m", timeInfo);   
  ::strftime(day, sizeof(day),"%d", timeInfo); 
  m_sDirPath = m_sDir  + "/" + year + "/" + m_partition + "/" + m_anaTask + "/" + month + "/" + day;
  
  // derive the timestamp from the input saveset
  int i1 = m_savesetName.find_last_of("/") +1 ; 
  std::string file = m_savesetName.substr(i1,std::string::npos);
  int i2 = file.find_first_of("-") ; 
  int i3 = file.find_last_of(".");
  if( i2 == i3 || i2 == (int) std::string::npos || i3 == (int) std::string::npos)m_timeStamp = "00000000T000000";
  else m_timeStamp = file.substr(i2,i3-i2);
  // execution time
  m_time =  Gaudi::Time::current () ;  

  // open SaveSet (a ROOT file)
  if (msgLevel( MSG::DEBUG))debug() << "Opening root file " << SaveSet << endmsg;
  if(m_copyInInput || m_storeFit)m_tf = new TFile(SaveSet.c_str(),"UPDATE");  
  else m_tf = new TFile(SaveSet.c_str(),"READ");
  if ( m_tf->IsZombie()){ 
    m_tf->Close("R");
    //m_tf->Delete("T*;*");
    delete m_tf ;
    m_tf = NULL;
    Warning( "Root file is a zombie",StatusCode::SUCCESS).ignore(); 
    return StatusCode::SUCCESS; 
  };

  // get counter histo when exists
  m_refCount = 0. ;
  m_pedCount = 0. ;
  m_ledCount = 0. ;
  int minStat = m_stat;
  int k=1;
  bool ok=false;
  for( std::vector<std::string>::iterator ih = m_hisCount.begin();m_hisCount.end()!=ih;++ih){
    std::string hist = *ih;
    TH1* hisCount = (TH1*) m_tf->Get( std::string("/"+hist).c_str() );
    if( NULL == hisCount){
      Warning("Counter histo "+hist+" not found",StatusCode::SUCCESS).ignore();
    }else {
      if( m_splitAna.size() == 2){
        std::string tit = hisCount->GetTitle();    
        std::string nt  = tit + " (" + m_splitAna[k-1] + ")";
        hisCount->SetTitle( tit.c_str() );
      }
      ok=true;
      m_refCount += hisCount->GetBinContent(1) ;
      m_ledCount += hisCount->GetBinContent(2) ;
      m_pedCount += hisCount->GetBinContent(3) ;
      clone1D(hist,"Counters/"+Gaudi::Utils::toString(k) );
    }
    k++;
  }
  if(!ok){
    if( m_stat > 0)Warning( "COUNTER HISTO(S) NOT FOUND - CANNOT APPLY STATISTICS THRESHOLD",StatusCode::SUCCESS).ignore();
    if( m_force ){
      Warning(" --> Force processing (no threshold applied)",StatusCode::SUCCESS).ignore();
      minStat = -1;
    }
    else Warning("  --> Abort processing",StatusCode::SUCCESS).ignore();
  }
  

  if (msgLevel( MSG::DEBUG))debug() << "trending counters" << endmsg;
  // trending
  trending( "EventTrending", (int) m_refCount );
  trending( "LEDTrending", (int) m_ledCount );
  trending( "PEDTrending", (int) m_pedCount );
  
  
  // clone cald07 histo -> mona08 via DimBridge in CaloCalibAna !!!
  if (msgLevel( MSG::DEBUG))debug() << "clone histos" << endmsg;
  clone1D("TriggerTypeCounter/BXTypesCounter/1");
  clone1D("TriggerTypeCounter/TriggerTypesCounter/1");
  clone1D("TriggerTypeCounter/CalibrationTypesCounter/1");
  clone1D("CaloReadoutStatus/"+m_detectorName+"Packed/1");
  // clone spectra for Control Channels  
  for(std::vector<std::string>::iterator is = m_controlsDir.begin();is != m_controlsDir.end();++is){
    std::string dir =*is;
    int k = 0;
    for(std::map<int,std::string>::iterator ic = m_controls.begin();ic!=m_controls.end();++ic){    
      k++;
      const LHCb::CaloCellID id = getID(ic->first);
      if( LHCb::CaloCellID() == id)continue;
      std::string lunIn  = m_source + dir+"/"+getUnit( id );
      std::string lunOut = "ControlChannels/" + Gaudi::Utils::toString(k) + "/" + "Histo" + dir;
      clone1D( lunIn , lunOut);
    }
  }  
  if (msgLevel( MSG::DEBUG))debug() << "process the monitors" << m_monMap << endmsg;


  //===== Prepare PrsAutoUpdate before monitor processing
  inPrsAutoUpdate();

  // Process the monitors ===========================================================================
  for(std::vector<std::string>::iterator mon = m_monitors.begin() ; mon != m_monitors.end() ; ++mon){
    if (msgLevel( MSG::DEBUG))debug() << "processing " << *mon << endmsg;
    CaloMonitor* monitor = m_monMap[ *mon ];
    if( monitor->dataName() == CaloMonitorDataName::PEDESTAL && m_pedCount >= 0 && m_pedCount <  minStat ){
      info() << "Not enough pedestal statistics (<" << minStat << ") to process the '" << *mon << "' monitor" << endmsg;
      monitor->setStatus( false );
      if(prsAutoUpdate(monitor) ){
        getAutoCaloMonitor( monitor->iName() + "_AutoOffset")->setStatus( false );
        getAutoCaloMonitor( monitor->iName() + "_AutoOffsetShift")->setStatus( false );
      }
    }
    if( monitor->dataName() != CaloMonitorDataName::PEDESTAL && m_ledCount >= 0 && m_ledCount <  minStat ){
      info() << "Not enough signal statistics (<" << minStat << ") to process the '" << *mon << "' monitor" << endmsg;
      monitor->setStatus( false );
    }
    if( !monitor->status() )continue;
    std::string histo = monitor->histoName(); // histo to be analyzed
    std::string  nam    = *mon;
    verbose() <<"Monitoring '" << nam << "' -> looking for histo : " << histo << endmsg;
    StatusCode sc = StatusCode::SUCCESS;
    if (msgLevel( MSG::DEBUG))debug() << "histo analysis " << *mon << endmsg;
    if( monitor->histoType() == CaloMonitorHistoType::PROFILE ){
      sc = profileAnalysis( monitor );
    }else if( monitor->histoType() == CaloMonitorHistoType::FIT ){
      sc = histoAnalysis( monitor );
    }else  if( monitor->histoType() == CaloMonitorHistoType::MOMENTS ){
      sc = momentAnalysis( monitor );
    }
    if( sc.isFailure() ){
      monitor->setStatus(false); 
      continue;
    }
  }
  if (msgLevel( MSG::DEBUG))debug() << "All monitor processed " <<m_monMap << endmsg;
  

  //======== Register histo
  for( MonMap::iterator im = m_monMap.begin() ; m_monMap.end() != im ; ++im){
    CaloMonitor* monitor = im->second;
    std::string mon = im->first;
    if (msgLevel( MSG::DEBUG))debug() << "Registering histo for CaloMonitor = " << mon << endmsg;
    // register 1D/2D histos
    if( monitor->pmtMonitor() ){
      std::string namPMT = name() +"/";
      AIDA::IHistogram1D* h1 = monitor->calo1D(namPMT, false ) ;
      if(h1)declareHisto(namPMT, h1 ); // register histo to MonitorSvc with this owner
    }
    if( monitor->pinMonitor() ){
      std::string namPIN = name() +"/";
      AIDA::IHistogram1D* h1 = monitor->calo1D(namPIN, true) ;
      if(h1)declareHisto(namPIN, h1 );
    }
    for(int i = 0 ; i < CaloMonitorType::Number;++i){
      CaloMonitorType::Type type = (CaloMonitorType::Type) i;      
      if( monitor->has2Dview(type) ){
        std::string nam2D = name() +"/";
        AIDA::IHistogram2D* h2 = monitor->calo2D(nam2D,type) ;
        if(h2)declareHisto(nam2D, h2 );
      }
    }
  }
  if (msgLevel( MSG::DEBUG))debug() << "CaloMonitor histo registered" << endmsg;

  //======== Alarm Report 
  alarmReport();
  AnalysisTask::analyze( SaveSet ,Task); // re-init messaging
  if (msgLevel( MSG::DEBUG))debug() << "Alarm reported" << endmsg;


  //=========== Produce PrsAutoUpdate table
  outPrsAutoUpdate();
 
 //======== Produc CondDB table
  if("" != m_dbTable)
    if( !FillCondDBTable() )Warning("Updated DB table has not been written",StatusCode::SUCCESS).ignore(); 


  // ------------------- post-analysis -----------------------------------------------------
  // Store output histos into the input SaveSet when requested
  if( "" != m_summary && m_copyInInput){
    info() << "Store  the analysis histos in the input SaveSet '" << m_savesetName << "'" << endmsg;
    storeAll(m_tf);
    if(m_storeFit){
      if (msgLevel( MSG::DEBUG))debug() << "Store  the fits in the input SaveSet '" << m_savesetName << "'" << endmsg;
      m_tf->Write( NULL , TObject::kOverwrite );
    }
  }
  // close input SaveSet
  m_tf->Close("R");
  delete m_tf;
  m_tf  = NULL;
  

  // --------------------------------------------------------
  // Produce a dedicated SaveSet  when requested
  if( "" != m_summary &&  m_storeInSaveSet ){
    if (msgLevel( MSG::DEBUG))debug() << "Produce a dedicated Saveset in " << m_sDirPath << endmsg;   
    // create dir path
    void *dir = gSystem->OpenDirectory(m_sDirPath.c_str());
    if (dir == 0)gSystem->mkdir(m_sDirPath.c_str(),true);
    std::string set = m_sDirPath + "/" + m_anaTask + m_timeStamp + ".root";
    m_tf2 = new TFile( set.c_str() , "UPDATE");   
    info() << "Store the analysis histos in a dedicated SaveSet '" << set << "'" <<endmsg;
    storeAll(m_tf2,true);
    m_tf2->Close("R");
    delete m_tf2;
    gSystem->FreeDirectory(dir);
    m_tf2 = NULL;
  }
  
  info() << "Analyzing '" << SaveSet << "' is completed " << endmsg ;
  // analyzing a Saveset should be seen as an 'event' for MonitorSvc
  execute().ignore();
  return StatusCode::SUCCESS;
}

//-------------------------------------------- PrsAutoUpdate in/out
void OMACaloAnalysis::outPrsAutoUpdate(){
  if( !m_prsOut ){
    info() << "Configuration file in '"+m_uConfFiler+"' WON'T be updated" << endmsg;
    return;
  }  

  for(int k=0;k<8;k++){
    if (msgLevel( MSG::DEBUG))debug() << "Updating configuration file '"+m_uConfFiler+"/m_Commissioning_PRS" 
                                      <<  _utos( k ) << ".conf'" << endmsg; 
    std::string sfile=(m_uConfFiler+"/Commissioning_PRS" +  _utos( k ) + ".conf").c_str(); 
    if( !WriteFile(m_prsVect[k],sfile))Error("Conf files cannot be written - check permissions",StatusCode::SUCCESS ).ignore(); 
  }
}

void OMACaloAnalysis::inPrsAutoUpdate(){
  m_prsOut = false;
  if("Prs" != m_detectorName || m_uConfFiler == "")return;
  info() << "Accessing reference configuration files at '" << m_uConfFiler+"/"+m_rConfFiler << endmsg;
  m_prsComment.clear();
  for(int k=0;k<8;k++){
    m_prsVect[k].clear();
    if (msgLevel( MSG::DEBUG))debug() << "Reading configuration file 'Commissioning_PRS" <<_utos( k ) << ".conf" << "'" << endmsg;
      std::string sfile=(m_uConfFiler+"/"+m_rConfFiler+"/Commissioning_PRS" +  _utos( k ) + ".conf").c_str();
      m_prsVect[k]=getFromFile( sfile.c_str() );
  }
  info() << "All files correctly read" << endmsg;
}
//--------------------------------------------
void OMACaloAnalysis::storeAll(TFile* tf, bool clone){
  store( tf, histo2D(HistoID(  m_summary +"/New" ) ) );
  store( tf, histo2D(HistoID(  m_summary +"/Resurrected" ) ) );
  store( tf, histo2D(HistoID(  m_summary +"/Known" ) ) );
  store( tf, histo2D(HistoID(  m_summary +"/ShortSummary" ) ) );
  store( tf, histo2D(HistoID(  "Trending" ) ) );
  if( clone){
    for(std::vector<AIDA::IHistogram1D*>::iterator ic = m_cloned.begin() ; m_cloned.end() != ic ; ++ic){
      if( NULL == *ic )continue;
      store(tf, *ic);
    }
  }
  for(std::map<std::string,AIDA::IHistogram1D*>::iterator it = m_trendMap.begin() ; m_trendMap.end() != it ; ++it){
    AIDA::IHistogram1D* h = (*it).second;
    if( NULL == h)continue;
    store(tf, h);
  }

  using namespace CaloMonitorType;
  for( MonMap::iterator im = m_monMap.begin() ; m_monMap.end() != im ; ++im){
    CaloMonitor* monitor = im->second;
    std::string mon = im->first;
    if (msgLevel( MSG::DEBUG))debug() << "Store " << mon << " histos (status = " << monitor->status() << ")" <<endmsg;
    if(  !monitor->status() )continue;
    if (msgLevel( MSG::DEBUG))debug() << "Store " << mon << " hist1D " << endmsg;
    std::string nam = name() +"/";
    if( monitor->pmtMonitor() )store( tf, monitor->calo1D(nam,false ));
    nam = name() +"/";
    if( monitor->pinMonitor() )store( tf, monitor->calo1D(nam,true  ));
    if (msgLevel( MSG::DEBUG))debug() << "Store " << mon << " hist2D " << endmsg;
    for(int i = 0 ; i < CaloMonitorType::Number;++i){
      CaloMonitorType::Type type = (CaloMonitorType::Type) i;      
      nam = name() +"/";
      if( monitor->has2Dview(type) )store( tf, monitor->calo2D(nam,type) );
    }
    //trending value 
    AIDA::IHistogram1D* hh = m_trendValMap[mon+"/average"];
    AIDA::IHistogram1D* hhs = m_trendValMap[mon+"/spread"];
    AIDA::IHistogram1D* hhm = m_trendValMap[mon+"/min2max"];
    if( NULL != hh)store(tf,hh);
    if( NULL != hhs)store(tf,hhs);
    if( NULL != hhm)store(tf,hhm);
  }
}

//--------------------------------------------
void OMACaloAnalysis::store( TFile* tf, AIDA::IHistogram2D* h2 ){
  if( NULL == h2 )return;
  
  const TH2D* th2 = Gaudi::Utils::Aida2ROOT::aida2root( h2 );
  std::string nam = "/" + std::string( th2->GetName());
  int index = nam.find_last_of(".")+1;
  if(index !=0)nam =  nam.substr(0,index-1)+"/"+nam.substr(index,std::string::npos);
  //nam = nam.substr(0,index-1)+"/" + nam;
  // check the histo does not yet exists
  if( NULL != tf){
    TH2D* check = NULL;
    check = (TH2D*) tf->Get( nam.c_str() );
    if(  NULL !=  check ){
      if (msgLevel( MSG::DEBUG))debug() << "Histograms '" <<nam
                <<"' already saved in " << tf << " - do not overwrite"<< endmsg;
      return;
    }
  }  
  if (msgLevel( MSG::DEBUG))debug() << "storing histo '" << nam << "' in " << tf << endmsg;
  std::string newName = tree(nam); 
  TH2D* out = new TH2D( *th2 );
  out->SetName( newName.c_str() );
  out->Write( NULL , TObject::kOverwrite );
  delete out;
  }

//--------------------------------------------
void OMACaloAnalysis::store( TFile* tf, AIDA::IHistogram1D* h1 ){
  if( NULL == h1 )return;
  const TH1D* th1 = Gaudi::Utils::Aida2ROOT::aida2root( h1 );
  std::string nam = "/" + std::string(th1->GetName());
  //
  int index = nam.find_last_of(".")+1;
  if(index !=0)nam =  nam.substr(0,index-1)+"/"+nam.substr(index,std::string::npos);
  // check the histo does not yet exists
  if( NULL != tf){
    TH1D* check = NULL;
    check = (TH1D*) tf->Get( nam.c_str() );
    if(  NULL !=  check ){
      if (msgLevel( MSG::DEBUG))debug() << "Histograms '" << nam
                <<"' already saved  in " <<tf << " - do not overwrite"<< endmsg;
      return;
    }  
  }
  if (msgLevel( MSG::DEBUG))debug() << "storing histo '" << nam << "' in " << tf << endmsg;
  std::string newName = tree(nam);
  TH1D* out = new TH1D( *th1 );
  out->SetName( newName.c_str() );
  out->Write( NULL , TObject::kOverwrite );
  delete out;
}
  
  
// ----------------------------------------------
std::string OMACaloAnalysis::tree( std::string name){
  unsigned int index = name.find_last_of("/") +1 ;
  std::string full = name.substr(0,index-1);
  std::string newName = name.substr(index,std::string::npos);
  
  int p,i;
  std::string fil,cur,s;
  TDirectory *gDir = gDirectory;

  std::list<std::string> lpath;
  i = 1;
  if ( (p=full.find(":",0)) != -1 ) {
    fil = full.substr(0,p);
    i = p+1;
    fil += ":/";
    gDirectory->cd(fil.c_str());
  }
  while ( (p = full.find("/",i)) != -1) {
    s = full.substr(i,p-i);
    lpath.push_back(s);
    i = p+1;
  }
  lpath.push_back( full.substr(i,full.length()-i) );
  if ( full.substr(0,1) == "/") {
    gDirectory->cd("/");
  }

  std::list<std::string>::const_iterator litr;
  for(litr=lpath.begin(); litr!=lpath.end(); ++litr) {
    cur = *litr;
    if (! gDirectory->GetKey(litr->c_str()) ) {
      gDirectory->mkdir(litr->c_str());
    }
    gDirectory->cd(litr->c_str());
  }
  gDirectory = gDir;
  gDirectory->cd(full.c_str());
  return newName;
}

// ------------------------------------------ logOut
void OMACaloAnalysis::logOut(std::string log ){
  if(m_log)m_mylog <<  log << std::endl;
}

//------------------------------------------- AlarmReport
void OMACaloAnalysis::alarmReport(){
  using namespace  CaloMonitorType;

  // prepare log out
  std::string timer = getTime();  
  std::string lline = "==========================================================================" ;

  logOut( lline );
  logOut("==== Automatic " + m_detectorName + " histo analysis (" + timer +") =================");
  logOut( lline );
  logOut(" -- Analyzing the SaveSet '" + m_savesetName + "' (#"+ Gaudi::Utils::toString(m_count) + ") filled with " 
         + Gaudi::Utils::toString( m_refCount)+ " data events");
  logOut(" -- Based on <" + Gaudi::Utils::toString((int)m_pedCount) + "> pedestal entries and <" 
         + Gaudi::Utils::toString((int)m_ledCount)+ "> signal entries per channel");

  std::string pref = m_revert ? "NOT " : " " ;
  if(!m_crate.empty() )
    logOut("  -- " + pref + "Monitored crates : " + Gaudi::Utils::toString(m_crate) );
  if(!m_feb.empty())
    logOut("  -- " + pref + "Monitored FEBs : "   + Gaudi::Utils::toString(m_feb) );
  if(!m_tell1.empty())
    logOut("  -- " + pref + "Monitored TELL1s : " + Gaudi::Utils::toString(m_tell1) );
  if(m_crate.empty() && m_feb.empty() && m_tell1.empty())
    logOut("  -- Monitoring  ALL " + m_detectorName + " Channels" );
  int alarm = 0;


  std::ostringstream trend("");
  trend << std::string("@@@" + m_detectorName + m_trend + "Trend |").c_str();
  trend << format("%4i |" , m_count );
  trend << format("%6i |" , (int) m_refCount );
  trend << format("%28s",std::string( m_timeStamp+" |").c_str() );

  // loop over CaloMonitors
  for( std::vector<std::string>::iterator imon = m_monitors.begin() ; m_monitors.end() != imon ; ++imon){
    CaloMonitor* monitor = m_monMap[ *imon ];
    std::string mon = *imon;
    // Unknown status :
    if( !monitor->status()){
      //double nCells = (double) m_calo->numberOfCells();
      double nCells = 0.49;
      fill( histo2D(HistoID( m_summary +"/Known" )), -1, (double) monitor->ID() , nCells);
      fill( histo2D(HistoID( m_summary +"/Resurrected" )), -1 , (double) monitor->ID() , nCells );
      fill( histo2D(HistoID( m_summary +"/New" )),(double) -1, monitor->ID() , nCells );
      std::string line =  "+-------------------------------------------------------------------------------------------+";
      std::string mline =  "+--------+----------------------------------------------------------------------------------+";
      logOut( monitor->alarmLogger( m_skipKnown , !m_full ));
      std::string sCells = Gaudi::Utils::toString((int) nCells);
      std::string aCells = Gaudi::Utils::toString(m_calo->numberOfCells());
      if (msgLevel( MSG::DEBUG))debug() << "Trending update" << endmsg;
      trending(mon,monitor->ID());
      trend << format("%4s","-") << "|";
      continue;
    };


    // known status 
    if( monitor->hasAlarm())alarm++;
    logOut( monitor->alarmLogger(m_skipKnown , !m_full ) );
    

    // fill 2D summary histo
    double eps = 0.45;
    double k = (monitor->counter(KNOWN) == 0) ? eps : (double) monitor->counter(KNOWN);
    double r = (monitor->counter(RESURRECTED) == 0) ? eps : (double) monitor->counter(RESURRECTED);
    double n = (monitor->counter(NEW) == 0) ? eps : (double) monitor->counter(NEW);
    fill( histo2D(HistoID( m_summary +"/Known" )), monitor->alarmLevel(KNOWN),(double) monitor->ID(), k  );
    fill( histo2D(HistoID( m_summary +"/Resurrected" )), monitor->alarmLevel(RESURRECTED),(double) monitor->ID(),r);
    fill( histo2D(HistoID( m_summary +"/New" )), monitor->alarmLevel(NEW),(double) monitor->ID(), n); 
    // fill trending
    std::string t = trending(mon,monitor->ID());
    trend << t.c_str() << "|";

    // DB alarm logger

    if( monitor->publishAlarm() ){
      
      CaloMonitorType::Type typ = CaloMonitorType::NEW;
      // publish new alarms

      std::string msg =    name() + "." + mon + " monitor status changed to  **" 
        + monitor->alarmLevelName( typ ) + "**  - timeStamp = [" + m_timeStamp + "]";
      
      int ind = name().find_last_of("/") +1 ; 
      std::string iName = name().substr(ind,std::string::npos);
      std::string his = iName +"/" +m_summary + "/New";
      
      // const std::string dName = CaloMonitorDataName::Name[ monitor->dataName()  ];
      // const std::string dType = CaloMonitorDataType::Name[ monitor->dataType() ];
      // std::string his = name() +"/" + m_detectorName +"Monitors/" + mon +"/"+ dName+dType+"2dView";
      OMAMessage::OMAMsgLevel oLevel ;
      if( monitor->alarmLevel( typ ) == CaloMonitorLevel::OK) oLevel = OMAMessage::INFO;        
      else if( monitor->alarmLevel( typ ) == CaloMonitorLevel::Warning) oLevel = OMAMessage::WARNING;        
      else oLevel = OMAMessage::ALARM; // error + alarm state
      
      // message :
      std::string outSS = m_sDirPath+"/"+m_anaTask + m_timeStamp + ".root";
      AnalysisTask::analyze( outSS ,m_anaTask);
      resetMessages(m_anaTask);
      m_anaName =  name() + "::" + mon ;
      if( oLevel == OMAMessage::ALARM || oLevel == OMAMessage::WARNING)raiseMessage(  oLevel , msg , his);
      refreshMessageList( m_anaTask );
      if (msgLevel( MSG::DEBUG))debug() << "message published " << msg << endmsg;
    }
  } // end loop over CaloMonitors


  // Build short summary
  int sid=0;
  for(std::map<std::string,std::vector<std::string> >::iterator is = m_short.begin(); m_short.end() != is ; is++){
    std::string sname=is->first;
    std::vector<std::string> list=is->second;
    if( list.empty() )list.push_back(sname);
    int slevel=-1;
    unsigned int scount=0;
    for( std::vector<std::string>::const_iterator il=list.begin();list.end()!=il;++il){
      std::string monName = *il;
      MonMap::iterator it = m_monMap.find( monName );
      if( m_monMap.end() == it){
        warning()<< "CaloMonitor '"<< monName <<"' not found for short summary" << endmsg;
        continue;
      }
      CaloMonitor* mon = it->second;
      if( mon->status() && mon->alarmLevel( CaloMonitorType::NEW ) >= slevel && mon->counter( CaloMonitorType::NEW ) >= scount){
          slevel =  mon->alarmLevel( CaloMonitorType::NEW ) ;
          scount =  mon->counter( CaloMonitorType::NEW );
      }
    }
    double count = (scount==0) ? 0.45 : (double) scount;
    fill( histo2D(HistoID( m_summary +"/ShortSummary" )), (double) slevel ,(double) sid, count  );
    sid++;
  }


  logOut("======> The " + Gaudi::Utils::toString(m_monMap.size()) + " "+ m_detectorName
       + " CaloMonitors have reported " + Gaudi::Utils::toString( alarm ) + " problem(s)");
  logOut( trend.str() );
  logOut(lline);

  info() << "@@@@@ " << m_detectorName << " Automated Analysis ( " << timer << ")"  << std::endl 
         << m_mylog.str()  << endmsg;
  


  // ===============================
  // update presenter page content :
  // ===============================
  if( m_useDB && "" != m_prefPage ){    
    
    //--------------
    // Page Comment
    //--------------
    if( m_comment){
      // global header
      std::ostringstream head("");
      head << "Calorimeter Calibration Analysis   (" << timer << ")"<<std::endl
           << "Latest saveset (#"<<m_count<<") : " << m_savesetName << std::endl
           << "Analysis based on " <<  Gaudi::Utils::toString( (int)m_refCount) << " calibration triggers" << std::endl;

      // fill global header
      histDBPage(m_prefPage + "/AnalysisSummary",head.str());
      histDBPage(m_prefPage + "/AnalysisShortSummary",head.str());
      histDBPage(m_prefPage + "/ReadoutStatus",head.str());
      histDBPage(m_prefPage + "/TriggerTypeCounters",head.str());
      
      // detector dependent header
      head << "Statistics : " <<  Gaudi::Utils::toString((int) m_refCount) 
           <<  " events (<" << Gaudi::Utils::toString((int)m_ledCount) << "> signal entries/channel +  <" 
           << Gaudi::Utils::toString((int)m_pedCount)+ "> pedestal entries/channel)";
      histDBPage(m_prefPage + "/"+ m_detectorName+"/Counters",head.str());
      histDBPage(m_prefPage + "/"+ m_detectorName+"/"+m_detectorName+"AnalysisSummary",head.str());
      
      // monitor pages
      for( MonMap::iterator im = m_monMap.begin() ; m_monMap.end() != im ; ++im){
        CaloMonitor* monitor = im->second;
        std::string mon = im->first;
        if( monitor->overall() )continue;
        std::string page = mon;
        if( "" != monitor->histDBPage() )page=monitor->histDBPage();
        std::ostringstream text("");
        text << head.str() << std::endl;
        text <<  "<pre>" << monitor->alarmLogger(m_skipKnown , !m_full ) << "</pre>";
        histDBPage(m_prefPage+"/"+m_detectorName+"/Monitors/"+page+"/Data", text.str() );
        histDBPage(m_prefPage+"/"+m_detectorName+"/Monitors/"+page+"/Trending", text.str() );
      }
      // controlChannels pages
      int ich = 0;
      for(std::map<int,std::string>::iterator ic = m_controls.begin();ic!=m_controls.end();++ic){    
        ich++;
        const LHCb::CaloCellID idd = getID(ic->first);
        if( LHCb::CaloCellID() == idd)continue;
      int feb = m_calo->cardNumber( idd );
      std::ostringstream loc("");
      loc << "Control Channel : [crate=" << format("%02i", m_calo->cardCrate( feb ) ) <<  ","
          << "feb="   << format( "%02i" , m_calo->cardSlot( feb  ) )<< ","
          << "readout=" <<Gaudi::Utils::toString( m_calo->cardColumn( idd ) + nColCaloCard * m_calo->cardRow( idd) )
          << "] => (" 
          << idd.caloName() << "," 
          << idd.areaName() << ","
          << idd.row() << ","
          << idd.col() << ") : index="<< Gaudi::Utils::toString( idd.index() ) << std::endl  << ic->second;
      histDBPage(m_prefPage+"/"+m_detectorName+"/ControlChannels/Channel"+Gaudi::Utils::toString(ich),loc.str());
      }    
    } 
  }
} 


//-----------------------------------------------------------------------
// monitored channel 
bool OMACaloAnalysis::monitorChannel(LHCb::CaloCellID id){
  if( m_feb.empty() && m_tell1.empty() && m_crate.empty() )return true;
  int feb   = m_calo->cardNumber( id );
  int tell1 = m_calo->cardToTell1( feb ) ;
  int crate = m_calo->cardCrate( feb );
  bool ok = false;
  for(std::vector<int>::iterator i = m_tell1.begin() ; i != m_tell1.end() ; ++i){
    if ( *i == tell1 ) { ok = true; break;}
  }
  for(std::vector<int>::iterator i = m_feb.begin() ; i != m_feb.end() ; ++i){
    if ( *i == feb )  { ok = true; break;}
  }
  for(std::vector<int>::iterator i = m_crate.begin() ; i != m_crate.end() ; ++i){
    if ( *i == crate ) { ok = true; break;}
  }
  return m_revert ? !ok : ok;
}


// monitoring profile histos
StatusCode OMACaloAnalysis::profileAnalysis( CaloMonitor* monitor){
  using namespace CaloMonitorDataType;
  std::string histo    =  monitor->histoName();  
  // Detect 'SplitLeds' configuration
  int index = histo.find_last_of("/") +1 ;
  std::string pref = histo.substr(0,index);
  std::string def = monitor->defaultLED();
  if( NULL != m_tf->GetDirectory( std::string(pref + def).c_str()) ){
    std::string his= pref + def + "/" + histo.substr(index,histo.length());
    histo = his;
    m_tf->GetDirectory("/");
  }  
  //

  TProfile* rooth =(TProfile*) m_tf->Get( histo.c_str() ); 
  if( 0 == rooth ){ 
    warning() << "Profile histo '" << histo << "' NOT FOUND" << endmsg;
    counter("Profile histo not found") += 1;
    monitor->setStatus(false);
    return StatusCode::FAILURE; 
  }


  // loop over bins
  double mean  = 0.;
  double rms   = 0.;
  double emean  = 0.;
  double erms   = 0.;
  double binEntries=0.;
  for( int bin = 0 ; bin < rooth->GetNbinsX() ; bin++ ) {
    LHCb::CaloCellID id = LHCb::CaloCellID(bin);
    id.setCalo( CaloCellCode::CaloNumFromName( m_detectorName ));
    if( !monitorChannel( id ) )continue;
    binEntries=rooth->GetBinEntries(bin+1) ;
    mean  = rooth->GetBinContent(bin+1) ;
    rms   = rooth->GetBinError(bin+1)   ;
    if( TString(rooth->GetErrorOption()) != "s" )rms *= sqrt(binEntries);
    emean  = (0<binEntries) ? rms/sqrt(binEntries) : 0. ;
    erms   = (0<binEntries) ? rms/sqrt(2.*binEntries) : 0. ;
    double value = 0;
    double error = 0;
    if (monitor->dataType() == ENTRIES){
      value= binEntries;
    }else if( monitor->dataType() == MEAN ){
      value = mean;
      error = emean;
    }else if( monitor->dataType() == RMS){
      value = rms;
      error = erms;
    }else if( monitor->dataType() == RMSoMEAN){
      value = (mean != 0 ) ? rms/mean : 0.;
      error = (mean*rms !=0) ? value * sqrt( emean*emean/mean/mean + erms*erms/rms/rms) : 0;
    }else if( monitor->dataType() == MEANoRMS){
      value = (rms != 0 ) ? mean/rms : 0.;
      error = (mean*rms !=0) ? value * sqrt( emean*emean/mean/mean + erms*erms/rms/rms) : 0;
    }else if( monitor->dataType() == GAINVARIATION){
      value = deltaDB( id , mean );
    }else {
      Error("Unknown data type " ).ignore();
      value =0;
    } 
    monitor->addToStatus( id , value , error );
  }
  return StatusCode::SUCCESS;
}


// monitoring moment histos
StatusCode OMACaloAnalysis::momentAnalysis( CaloMonitor* monitor){
  using namespace CaloMonitorDataType;
  std::string histo    =  monitor->histoName();
  // Detect 'SplitLeds' configuration
  int index = histo.find_last_of("/") +1 ;
  std::string pref = histo.substr(0,index);
  std::string def = monitor->defaultLED();
  if( NULL != m_tf->GetDirectory( std::string(pref + def).c_str()) ){
    std::string his= pref + def +"/" + histo.substr(index,histo.length());
    histo = his;
    m_tf->GetDirectory("/");
  }  
  //
  std::string o0 = histo + "/1";
  TH1* h0 =(TH1*) m_tf->Get( o0.c_str() ); 
  if( 0 == h0 ){ 
    warning() << "Moment histo '" << o0 << "' NOT FOUND" << endmsg;
    counter("Moment histo not found") += 1;  
    return StatusCode::FAILURE;     
  }
  TH1* h1 = NULL;
  if( monitor->dataType() == MEAN || 
      monitor->dataType() == RMS || 
      monitor->dataType() == RMSoMEAN ||
      monitor->dataType() == MEANoRMS ||
      monitor->dataType() == GAINVARIATION ){
    std::string o1 = histo + "/2";
    h1 =(TH1*) m_tf->Get( o1.c_str() ); 
    if( 0 == h1 ){ 
      warning() << "Moment histo '" << o1 << "' NOT FOUND" << endmsg;
      counter("Moment histo not found") += 1;
      return StatusCode::FAILURE;    
    }
  }
  TH1* h2 = NULL;
  if( monitor->dataType() == RMS || monitor->dataType() == RMSoMEAN || monitor->dataType() == MEANoRMS ){
    std::string o2 = histo + "/3";
    h2 =(TH1*) m_tf->Get( o2.c_str() ); 
    if( 0 == h2 ){ 
      warning() << "Moment histo '" << o2 << "' NOT FOUND" << endmsg;
      counter("Moment histo not found") += 1;
      monitor->setStatus(false);
      return StatusCode::FAILURE;    
    }
  }
  
  double value = 0.;
  double mean  = 0.;
  double rms   = 0.;
  //double emean  = 0.;
  //double error = 0.;
  //double erms   = 0.;
  double m0   = 0.;
  double m1   = 0.;
  double m2   = 0.;
  for( int bin = 0 ; bin < h0->GetNbinsX() ; bin++ ) {
    LHCb::CaloCellID id = LHCb::CaloCellID(bin);
    id.setCalo( CaloCellCode::CaloNumFromName( m_detectorName ));
    if( !monitorChannel( id ) )continue;
    m0  = h0->GetBinContent(bin+1) ;
    if(NULL != h1)m1 = h1->GetBinContent(bin+1) ;
    mean = (m0 > 0) ? m1/m0 : 0.;
    if(NULL != h2)m2 = h2->GetBinContent(bin+1) ;
    rms = (m0>0) ? m2/m0 - mean*mean :0.;
    rms = (rms>0) ? sqrt(rms) : 0.;    
    //emean = (m0 >0) ? rms / sqrt(m0) : 0;
    //erms  = emean/sqrt(2.);
    if (monitor->dataType() == ENTRIES)value = m0;
    else if (monitor->dataType() == MEAN){
      value = mean;
      //error = emean;
    }    
    else if( monitor->dataType() == RMS )value = rms;
    else if( monitor->dataType() == RMSoMEAN)value = (mean != 0 ) ? rms/mean : 0.;
    else if( monitor->dataType() == MEANoRMS)value = (rms != 0 ) ? mean/rms : 0.;
    else if( monitor->dataType() == GAINVARIATION)value = deltaDB( id , mean );
    else {
      Error("Unknown data type " ).ignore();
      value =0;
    } 
    monitor->addToStatus( id, value );
  }
    return StatusCode::SUCCESS;
}


// monitoring channel histo =====================================================
StatusCode OMACaloAnalysis::histoAnalysis( CaloMonitor* monitor){
  using namespace CaloMonitorDataType;
  std::string histo    =  monitor->histoName();
  std::string fitfunc  =  monitor->fitFunction();
  int index = histo.find_last_of("/") +1 ; // return 0 if '.' not found --> OK !!
  std::string path = histo.substr( 0, index );
  const CaloVector<CellParam>& cells = m_calo->cellParams();
  std::string def = monitor->defaultLED();

  bool prsUpdate = prsAutoUpdate( monitor );

  // prepare the fit
  std::string oFit = m_fit;
  if( "Prs" == m_detectorName && m_storeFit  ){
    std::string dir = m_source + monitor->parity()+ "FittedHistograms";
    m_tf->mkdir(dir.c_str());
    m_tf->cd(dir.c_str());
  }

  unsigned int kFail = 0;
  int nUpd = 0;
  double mUpd = 0.;
  double rUpd = 0.;
  int ifail=0;

  // loop over histos
  for( CaloVector<CellParam>::const_iterator icel = cells.begin() ; cells.end() != icel ; ++icel){
    LHCb::CaloCellID id = (*icel).cellID();
    
    id.setCalo( CaloCellCode::CaloNumFromName( m_detectorName ));
    if( !monitorChannel( id ) )continue;
    std::string lun = getUnit( id );
    std::string his = path+lun;
    if( NULL != m_tf->GetDirectory( std::string(path + def).c_str()) ){
      his = path + def + "/" + lun;
      m_tf->GetDirectory("/");
    }

    // init
    int ndf = 0;
    double mean   = 0.;
    double sigma  = 0.;
    double emean  = 0.;
    double esigma = 0.;
    double chi2   = m_chi2CutOff;
    double value  = 0.;
    double error  = 0.;

    // get histo
    TH1D* roothh =(TH1D*) m_tf->Get( his.c_str() );
    if( 0 == roothh ){ 
      if (msgLevel( MSG::DEBUG))debug() << "Histo '" << his << "' NOT FOUND" << endmsg;
      counter("Detailed histo not found") += 1;
      return StatusCode::FAILURE;
    }    
    double entries= (double) roothh->GetEntries();
    TH1D* rooth = roothh;
      
      
    // set fit  
    TF1* fit = NULL;
    bool newfit = true;
    std::map<std::string,TF1>::iterator ifit = m_fitMap.find( his + "_"+fitfunc );
    if( ifit != m_fitMap.end() ){ 
      // Prs fit :  already performed
      verbose() << monitor->name() << " : fit already processed "<< id << endmsg;
      fit = &(ifit->second);
      newfit = false;
    }else {
      // Prs fit :  create intermediate 'de-gained' histo 
      int bin = roothh->GetXaxis()->GetNbins();
      double min = roothh->GetXaxis()->GetXmin();
      double max = roothh->GetXaxis()->GetXmax();
      TH1D* temp=new TH1D( his.c_str() ,"rooth",bin ,min ,max);
      if( m_detectorName == "Prs" && !m_prsGainsO.empty() && !m_prsGainsE.empty()){
        for(int i=1;i<=bin;i++){
          int gain =0;
          if( monitor->parityBit() == 0 )gain= (int) m_prsGainsO[id];
          else if( monitor->parityBit() == 1 )gain= (int) m_prsGainsE[id];
          int newbin=prsBinCor(i-1, gain );
          int binc=(int) roothh->GetBinContent(i);
          for(int j=0;j<binc;j++){         
            temp->Fill(newbin);
          }
        }
        rooth = temp;
      }else{
        delete temp;
        temp = NULL;
      }
      
      
      // use binned parameters as default when fit fails
      mean  =  rooth->GetMean();
      sigma =  rooth->GetRMS();
      
      if(fitfunc == "PrsGausBin" && m_detectorName == "Prs" ){
        fit = new TF1("fit",fg3,min,max,3);
      }else if(fitfunc == "PrsGausCont" && m_detectorName == "Prs" ){
        fit = new TF1("fit",fg2,min,max,3);
      }else{
        if( monitor->fitBounds().first < monitor->fitBounds().second ){
          fit = new TF1("fit", fitfunc.c_str(), monitor->fitBounds().first , monitor->fitBounds().second );
          oFit += "r";
        }else{        
          fit = new TF1("fit", fitfunc.c_str() );
        }
      }
      

      // inline test
      if( fitfunc == "gaus" || fitfunc == "PrsGausBin" || fitfunc == "PrsGausCont"){
        double sum0 = rooth->GetEntries();
        double mu0  = rooth->GetMean();
        double sig0  = rooth->GetRMS();
        double h0 = sum0/sqrt(2.*3.1415*sig0);
        fit->SetParameters( h0, mu0,sig0 );
      }else{
        const std::vector<double>& params = monitor->fitParameters();
        int k = 0;
        for(std::vector<double>::const_iterator  i = params.begin() ; i != params.end() ; ++i ){
          fit->SetParameter( k , *i );
          k++;
        }
      }

      // sanity checks
      if(entries == 0 ){
        if( fit != NULL )delete fit;
        fit = NULL;      
        counter( monitor->name() + " : statistics too low for fit") += 1;
        //        monitor->setStatus(false);
      }          
      else{        
        rooth->Fit("fit", oFit.c_str());
        double cc =   fit->GetNDF() > 0 ? fit->GetChisquare()/ fit->GetNDF() :  fit->GetChisquare();
        bool bad = false;
        if( cc > m_chi2CutOff)bad=true;
        else if( fit && fabs(fit->GetParameter(1) - mean) > m_uRange) bad = true;  
        else if( fit && fabs(fit->GetParameter(2) - sigma) > m_uRange)  bad = true;  
        if( bad ) {
          delete fit;
          fit = NULL;
        }
      }
      
      if( fit != NULL){ 
        m_fitMap[his+"_"+fitfunc] = *fit; // store fit locally
      }else{
        counter(monitor->name() + " : fit failed")+=1;
        kFail += 1;
      }
      if( !m_storeFit && NULL != temp )delete temp; 
      else m_inter.push_back(temp);
    }

    if ( fit != NULL ) {
      ndf   = fit->GetNDF();
      chi2  = fit->GetChisquare();
      if(chi2 > m_chi2CutOff){
        chi2=m_chi2CutOff;
      }else{
        mean  = fit->GetParameter(1);
        sigma = fit->GetParameter(2);
        emean  = fit->GetParError(1);
        esigma = fit->GetParError(2);
      }      
    }
    
    // update Prs 
    if( prsUpdate ){
      int subChannel = monitor->parityBit();
      if(subChannel <= 0){
        Warning("wrong parity bit - cannot update Prs configuration files",StatusCode::SUCCESS).ignore();
        m_prsUpd[monitor->parity()]=false;
      }else{
        int feb   = m_calo->cardNumber( id );
        int crate = m_calo->cardCrate( feb );
        int chan  = m_calo->cardColumn( id ) + nColCaloCard * m_calo->cardRow( id ) ; //
        int calind= indmap(crate,feb,chan,subChannel);       
        //info() << " ==> " << calind << " " << id << endmsg;

        int idef = -3; // **** default Prs offset shift when fit fails (negative pedestal) *****
        int meanint = idef;
        if( fit != NULL ){
          if (mean>=0) meanint = int(mean+0.5);
          if (mean<0 ) meanint = int(mean-0.5);
        }
        for(int k=0;k<8;k++) {
          if(crate!=k)continue;
          if( calind >= (int) m_prsVect[k].size() || calind < 0){
            //warning() << k << " calind >= size " << calind << " / " << m_prsVect[k].size() << endmsg;
            ifail++;
          }
          else{
            int oVal = m_prsVect[k].at(calind);
            int nVal = oVal+meanint;
            CaloMonitor* monAU = getAutoCaloMonitor( monitor->iName() + "_AutoOffset");                
            CaloMonitor* monAUShift =getAutoCaloMonitor(monitor->iName()+"_AutoOffsetShift",-5.,+5.,"PEDESTAL","OFFSETSHIFT");
            if(monAU)monAU->addToStatus( id , (double) nVal );
            if(monAUShift)monAUShift->addToStatus( id , (double) meanint );     
            if( nVal > 255){
              debug() << id << " : new offset (" + monitor->parity() +")= " 
                      << nVal <<" > 255 (old = " << oVal << ")"<<   endmsg;
              nVal = 255;
            }else if( nVal<0){ 
              debug() << id << " : new offset = " << nVal <<" < 0  (old = " << oVal << ")" <<  endmsg;
              nVal = 0;
            }
            m_prsVect[k].at(calind)=nVal;
            int shift = nVal - oVal;
            if( meanint != 0)nUpd++;
            mUpd += (double) shift;
            rUpd += (double) shift * (double) shift;
          }
        }
      }
    }
    if( newfit && NULL != fit )delete fit;
    
    
    // update CaloMonitors
    if( monitor->dataType() == ENTRIES ){
      value = entries;
    }else if( monitor->dataType() == MEAN ){
      value = mean;
      error = emean;
    }else if( monitor->dataType() == SIGMA ){
      value = sigma;
      error = esigma;
    }else if( monitor->dataType() == CHI2 ){
      value = (ndf>0) ? chi2/(double) ndf : chi2;
    }else if( monitor->dataType() == SIGMAoMEAN ){
        value = (mean != 0 ) ? sigma/mean : 0.;
        error = (mean*sigma !=0) ? value * ( emean*emean/mean/mean + esigma*esigma/sigma/sigma) : 0;
    }else if( monitor->dataType() == GAINVARIATION ){
      value = deltaDB( id , mean );   
    }else {
      Error("Unknown data type " ).ignore();
      value =0;
    }
    monitor->addToStatus( id, value, error );
  }


  if( kFail > 0){
    warning() << monitor->name() << " " << kFail << " fits have failed" << endmsg;
    if( m_calo->numberOfCells() == kFail) monitor->setStatus(false);
  }  
  if( ifail != 0)warning() << " ----> Prs AutoUpdate fails for " << ifail << " channels (" << monitor->name() << ") "
                           << " - #fits registered = " << m_fitMap.size() << endmsg;

  // add criteria
  using namespace  CaloMonitorType;
  if( prsUpdate && monitor->counter(NEW) >= m_upd ){
    info() << "Found "<< monitor->counter(NEW)<<" channels outside the expected range -> update configuration file"<<endmsg;
    info() << "Accessing reference configuration files at '" << m_uConfFiler+"/"+m_rConfFiler << endmsg;
    if( nUpd !=0){
      mUpd /= nUpd;
      rUpd  =  sqrt(rUpd/nUpd - mUpd*mUpd) ;
    }

    std::string flag =  "@@@" + m_detectorName + m_trend ;
    logOut( flag + "    Updating the configuration file with new pedestal offsets derived from " + monitor->name() );
    logOut( flag + "         - Alarm status :" + monitor->alarmLevelName(NEW) );
    logOut( flag + "         - TimeStamp : " + m_timeStamp  ); 
    logOut( flag + "         - # updated channels : " + Gaudi::Utils::toString(nUpd) 
            + " - average shift : " + Gaudi::Utils::toString(mUpd) 
            + " - rms  : " + Gaudi::Utils::toString(rUpd));
    std::string txt = "Pedestal auto-update from : "+m_anaTask+m_timeStamp
      +" | "+ monitor->name() + " Alarm status : " + monitor->alarmLevelName(NEW);
    m_prsComment.push_back(txt);
    m_prsUpd[monitor->parity()] = false;
    m_prsOut = true;
  }  
  return StatusCode::SUCCESS;
} 


// DB comparaison
double OMACaloAnalysis::deltaDB( LHCb::CaloCellID id , double value){
  double ref = m_calo->cellParams()[id].ledMoni();
  return ( ref > 0 ) ? value/ref : 1. ;
}


void OMACaloAnalysis::setBinLabels( AIDA::IHistogram2D* h2 ){
  TH2D* th2 = Gaudi::Utils::Aida2ROOT::aida2root( h2 );
  for( MonMap::iterator im = m_monMap.begin() ; m_monMap.end() != im ; ++im){
    CaloMonitor* moni=im->second;
    std::string mon = im->first;
    th2->GetYaxis()->SetBinLabel( moni->ID() +1 , mon.c_str() );
  } 
  th2->GetXaxis()->SetBinLabel( 1  , "Unknown" );
  for( int k = 1 ; k <= CaloMonitorLevel::Number;++k){
    th2->GetXaxis()->SetBinLabel( k+1  , CaloMonitorLevel::Name[k-1].c_str() );
  }
  th2->GetYaxis()->LabelsOption( m_lab.c_str() );  
}

void OMACaloAnalysis::shortBinLabels( AIDA::IHistogram2D* h2 ){
  TH2D* th2 = Gaudi::Utils::Aida2ROOT::aida2root( h2 );
  int n = 1;
  for( std::map<std::string,std::vector<std::string> >::iterator im = m_short.begin() ; m_short.end() != im ; ++im){
    std::string mon = im->first;
    int group = (im->second).size();
    if( group == 0 )group=1;
    std::string label = mon + " (#"+Gaudi::Utils::toString(group)+")";
    th2->GetYaxis()->SetBinLabel( n , label.c_str() );
    n++;
  } 
  th2->GetXaxis()->SetBinLabel( 1  , "Unknown" );
  for( int k = 1 ; k <= CaloMonitorLevel::Number;++k){
    th2->GetXaxis()->SetBinLabel( k+1  , CaloMonitorLevel::Name[k-1].c_str() );
  }
  th2->GetYaxis()->LabelsOption( m_lab.c_str() );  
}



// histDB page update
void OMACaloAnalysis::histDBPage( std::string page , std::string text, bool reset){
  if( !m_useDB )return;
  OnlineHistDB* histDB = dbSession() ;
  OnlineHistPage* pg=histDB->getPage( page );
  if( NULL == pg ){
    warning() << "histDB page '" << page << "' cannot be loaded " << endmsg;
    return;
  }
  std::string doc =   reset ? text : pg->doc() + " " + text;
  pg->setDoc( doc );
}

void OMACaloAnalysis::clone1D(std::string lunIn , std::string lunOut){
  if( m_tf == NULL)return;
  TH1* th1 = (TH1*) m_tf->Get( lunIn.c_str() );
  if( th1 == NULL)return;

  if("" == lunOut)lunOut =  lunIn;

  double min = th1->GetXaxis()->GetXmin();
  double max = th1->GetXaxis()->GetXmax();
  int   bin = th1->GetXaxis()->GetNbins();
  std::string title = std::string( th1->GetTitle() );


  AIDA::IHistogram1D* h =  histo1D( HistoID( lunOut ) );
  if( NULL == h ){
    h = book1D( lunOut, title, min, max , bin );
    Gaudi::Utils::Aida2ROOT::aida2root( h )->SetName( std::string(name() +"/"+lunOut).c_str() );
  }
  
  if( NULL == h)return;
  TH1D* th = Gaudi::Utils::Aida2ROOT::aida2root( h );
  double entries = th1->GetEntries(); 
  for( int i = 1  ; i <= bin ; i++){ 
    double value = th1->GetBinContent( i ); 
    double error = th1->GetBinError( i );
    std::string lab = std::string( th1->GetXaxis()->GetBinLabel( i ) ); 
    if( 0. != value){
      th->SetBinContent( i, value ); 
      th->SetBinError( i, error );
    }
    if( "" != lab)th ->GetXaxis()->SetBinLabel( i , lab.c_str() ); 
  } 
  th->SetEntries( entries );
  
  if(NULL != h)m_cloned.push_back( h ); 
  return ; 
}  

// monitor trending :  
std::string OMACaloAnalysis::trending( std::string mon,int imon){
  using namespace  CaloMonitorType;

  // statistics trending
  std::map<std::string,AIDA::IHistogram1D*>::iterator istat = m_statMap.find( mon );
  if( m_statMap.end() != istat && m_trending > 0){
    AIDA::IHistogram1D* sh = istat->second;
    addToTrending(sh, m_timeStamp, (double) imon);
    return "";
  }

  // CaloMonitor trending
  CaloMonitor* monitor = m_monMap[ mon ];
  if( NULL == monitor )return "";

  // === Problematic channels counter ===
  double count = monitor->counter(NEW);
  if( 0 == count && monitor->status() )count = 0.45;

  // output message
  std::ostringstream trend("");
  trend << format("%4.0f", count ) ;    
  if(m_trending<=0)return trend.str();

  // get the monitor trending histograms
  AIDA::IHistogram1D* h = m_trendMap[mon];
  AIDA::IHistogram2D* h2 = histo2D(HistoID( "Trending"));  
  addToTrending( h  , m_timeStamp, round(count));
  addToTrending( h2 , m_timeStamp, imon+1 , count)->GetYaxis()->SetBinLabel( imon+1 , mon.c_str() );


  //=== Average value ===
  AIDA::IHistogram1D* hh = m_trendValMap[mon+"/average"];
  AIDA::IHistogram1D* hhs = m_trendValMap[mon+"/spread"];
  AIDA::IHistogram1D* hhm = m_trendValMap[mon+"/min2max"];
  double v = monitor->average();
  double n = (double)monitor->nChannels();
  double e = (n>0) ? monitor->spread()/sqrt(n) : 0.;
  double s = monitor->spread();
  double es = (n>0) ? monitor->spread()/sqrt(2.*n) : 0.;
  double mm = monitor->min2max();
  double emm= monitor->min2maxError();
  // check trending tool
  if( monitor->trend() && m_mon && monitor->status() ){
    std::vector<float> values;
    values.push_back((float) v);
    values.push_back((float) s);
    values.push_back((float) mm);
    info() << "updating trending file for " << monitor->iName() << " monitor " << endmsg;
    monitor->trendTool()->write( values );
  }  
  //
  addToTrending( hh , m_timeStamp, v , e);
  addToTrending( hhs , m_timeStamp, s , es);
  addToTrending( hhm , m_timeStamp, mm , emm);
  
  // Trending for Control channels   ===============
  if( monitor->overall() ) return trend.str();
  int k = 0;
  for(std::map<int,std::string>::iterator ic = m_controls.begin();ic!=m_controls.end();++ic){    
    // get the control channel trending plot
    k++;
    const LHCb::CaloCellID id = getID(ic->first);
    if( LHCb::CaloCellID() == id)continue;
    const std::string dName = CaloMonitorDataName::Name[ monitor->dataName()  ];
    const std::string dType = CaloMonitorDataType::Name[ monitor->dataType() ];
    std::string unit =  "ControlChannels/" +  Gaudi::Utils::toString( k ) + "/" + dName+dType + "Trending" ;
    AIDA::IHistogram1D* ch = histo1D(HistoID( unit ));
    // get the new value
    double val = 0;
    double err = 0;
    if( monitor->status() ) {
      val = monitor->value( id );  
      err = monitor->valueError( id );
    }
    addToTrending( ch , m_timeStamp , val, err);
  }  
  return trend.str();
}

TH2D* OMACaloAnalysis::addToTrending(AIDA::IHistogram2D* h, std::string stamp, int bin, double val, double err){
  if( NULL == h )return NULL;
  TH2D* th = Gaudi::Utils::Aida2ROOT::aida2root( h );
  if( NULL == th)return NULL;

  if( stamp != th->GetXaxis()->GetBinLabel( m_trending) ) {
    for(int binx = 1 ; binx < m_trending ; binx++){
      std::string s = th->GetXaxis()->GetBinLabel( binx+1);
      if( "" == s)continue;
      for(int biny = 1 ; biny <= th->GetNbinsY() ; biny++){
        double      v = th->GetBinContent( binx+1, biny );
        double      e = th->GetBinError( binx +1, biny );
        th->SetBinContent( binx , biny, v );
        th->SetBinError(   binx , biny, e );
      }
      th->GetXaxis()->SetBinLabel(binx, s.c_str() );
    }  
  }
  th->SetBinContent( m_trending , bin, val);
  th->SetBinError  ( m_trending , bin, err);
  th->GetXaxis()->SetBinLabel(m_trending, stamp.c_str());
  th->GetXaxis()->LabelsOption( m_lab.c_str() );
  return th;
}

TH1D* OMACaloAnalysis::addToTrending(AIDA::IHistogram1D* h, std::string stamp,double val, double err){
  if( NULL == h )return NULL;
  TH1D* th = Gaudi::Utils::Aida2ROOT::aida2root( h );
  if( NULL == th)return NULL;

  if( stamp != th->GetXaxis()->GetBinLabel( m_trending) ) {
    for(int bin = 1 ; bin < m_trending ; bin++){
      std::string s = th->GetXaxis()->GetBinLabel( bin+1);
      if( "" == s)continue;
      double      v = th->GetBinContent( bin+1 );
      double      e = th->GetBinError( bin +1  );
      th->SetBinContent( bin , v );
      th->SetBinError(   bin , e );
      th->GetXaxis()->SetBinLabel(bin, s.c_str() );
    }
  }  
  th->SetBinContent( m_trending , val);
  th->SetBinError  ( m_trending , err);
  th->GetXaxis()->SetBinLabel(m_trending, stamp.c_str());
  th->GetXaxis()->LabelsOption( m_lab.c_str() ); 
  return th;
}




const LHCb::CaloCellID OMACaloAnalysis::getID(int index){
  LHCb::CaloCellID id(index);
  id.setCalo( m_detectorName );
  if( !m_calo->valid( id ) )return LHCb::CaloCellID();
  return id;  
}




// ----------------- Prs pedestal fit
int OMACaloAnalysis::prsBinCor(int bin, int G){  
  int ADC=0;int multiplied_gain=0;
  while(multiplied_gain<=bin){    
    multiplied_gain = (int)(ADC + floor( G*floor( ADC/2 )/128 )); 
    ADC++;    
  }
  return ADC-2; 
}





//reading offsets from config file
std::vector<int> OMACaloAnalysis::getFromFile( std::string m_OffsetFile ){
  std::vector<int> VFile;  
  for ( unsigned int nfeb = 0; nfeb < 20; nfeb++ ) {
    FILE *fid = fopen( m_OffsetFile.c_str(), "r" );
    if( fid == NULL){
      warning() << "Cannot access file " << m_OffsetFile <<endmsg;
      return VFile;
    }
    std::string brdheader = "[FEBOARD_SLOT" + _utos(nfeb) + "]";
    
    if ( _fjump( fid, brdheader ) ) {
      unsigned int u;
      
      // FEPGA OFFSETS  
      for ( unsigned int i = 0; i < 8; i++ ) {
        std::string header = "FEPGA"  + _utos( i ) + "_o255sets";
        if ( _fjump( fid, header ) ) {
          for ( unsigned int j = 0; j < 16; j++ ) {
            fscanf( fid, "%u", &u );
            VFile.push_back((unsigned char)u);
          }
        }        
      }      
    } 
    fclose( fid );
  }
  return VFile;  
}

//updating new config-file
bool OMACaloAnalysis::WriteFile(std::vector<int> vfile,std::string m_OffsetFile){
  FILE *myfile_out = fopen( "/tmp/ttt", "w" );  
  if( NULL == myfile_out )return false;
  std::ifstream myfile_in ( m_OffsetFile.c_str() );
  std::string line;  
  int vind=0;
  while (! myfile_in.eof() ){
    getline (myfile_in,line);
    std::string ltemp=line;
    ltemp.erase (ltemp.begin()+17, ltemp.end());
    std::string header1 = "  FEPGA0_o255sets";
    if ( ltemp == header1 ) {
      for ( int i = 0; i < 8; i++ ) {          
        fprintf( myfile_out, "  FEPGA%1d_o255sets", i );
        for ( int j = 0; j < 16; j++ ) {
          fprintf(  myfile_out, " %3d", vfile.at(vind) );
          vind++;
        }
        fprintf( myfile_out, "\n" );
        getline (myfile_in,line);
      }
      fprintf( myfile_out, "\n" );
    } else {
      fprintf( myfile_out, line.c_str() );
      fprintf( myfile_out, "\n" );
    }    
  }  
  fclose(myfile_out);
  std::string command1="cp " + m_OffsetFile + " " + m_OffsetFile + "_prev";
  std::string command3="mv /tmp/ttt " + m_OffsetFile;
  std::string command4="rm -f /tmp/ttt";
  // apply system commands
  system( command1.c_str());
  
  int k=0;
  for( std::vector<std::string>::iterator ic = m_prsComment.begin() ; m_prsComment.end() != ic; ++ic){ 
    std::string txt = *ic; 
    std::string command2= 
      "sed '"+ Gaudi::Utils::toString( 9+ k ) 
      + " c " + txt + "' /tmp/ttt > /tmp/ttt0 ; mv /tmp/ttt0 /tmp/ttt"; 
    system( command2.c_str());
    k++; 
  }
  system( command3.c_str());
  system( command4.c_str());
  return kTRUE; 
}

std::string OMACaloAnalysis::_utos( unsigned int u ){
  char buffer[16];
  for ( unsigned int i = 0; i < 16; i++ ) buffer[ i ] = 0;
  sprintf( buffer, "%u", u );  
  return std::string( buffer );
}

bool OMACaloAnalysis::_fjump( FILE* fid, std::string pattern ){
  char c;
  while( !feof( fid ) ) {
    unsigned int cnt = 0;
    for ( unsigned int i = 0; i < pattern.size(); i++ ) {
      fscanf( fid, "%c", &c );
      if ( pattern[ i ] != c ) break;
      else cnt++; 
    }
    if ( cnt == pattern.size() ) {
      return true;
    }
  }
  
  return false;  
}

int OMACaloAnalysis::indmap(int crate, int feb, int channel, int subchannel)
{
  int indcal=0;

  int febslot=m_calo->cardSlot( feb  );

  //
  int offset = 1;
  if( crate == 1 || crate == 6)offset = 4; // hardcoded
  int slot = febslot-offset ;
  indcal += 128 * slot ;

  std::pair<int,int> map = prsMap(feb,channel);
  indcal += map.first*16;
  indcal += map.second*2;


  // Even first - Odd second (when L0_LATENCY is ODD - else switch)

  int par1 = 1;
  int par2 = 0;
  // exceptions (hardcoded)
  if( (crate == 2 && febslot == 3 ) ||
      (crate == 5 && febslot == 7 ) ||
      (crate == 5 && febslot == 12 ) ){
    par1 = 0;
    par2 = 1;
  }




  if(subchannel==1) return indcal+par1; // ODD BCID
  if(subchannel==2) return indcal+par2; // EVEN BCID

  Error("Unknown SubChannel " ).ignore();
  return -1;
}


//bool OMACaloAnalysis::doFit(CaloMonitor* monitor, TH1* rooth)



// Prs mapping for config file update = return pair<pga,channel> for a given (feb,local-channel) [mapping from Valentin]
std::pair<int,int> OMACaloAnalysis::prsMap(int feb, int local){
  
  std::pair<int,int> dummy = std::make_pair(0,0);
  CardParam::Mapping map = m_calo->cardParam( feb ).mapping();
  
  // sanity checks
  bool ok = true;
  if( map == CardParam::None)ok = false;
  if( local < 0 )ok=false;
  if( (map == CardParam::Prs32Top || map == CardParam::Prs32Bottom) && local > 31)ok = false;
  if( (map == CardParam::Prs64Top || map == CardParam::Prs64Bottom) && local > 63)ok = false;
  if( local > 63 || local < 0)ok=false;
    
  if( !ok ){
    warning() << "undefined prs mapping " << map << " "  << feb << " " << local << endmsg;
    return dummy;
  }
  
  int pga = -1;
  int chn = -1;

  if( map == CardParam::Prs64Bottom){
    pga =  PrsMaps::fepga_B[local];
    chn =  PrsMaps::fechn_B[local];
  }
  else if( map == CardParam::Prs64Top){
    pga =  PrsMaps::fepga_T[local];
    chn =  PrsMaps::fechn_T[local];
  }else if( map == CardParam::Prs32Bottom){
    pga =  PrsMaps::fepga_HB[local];
    chn =  PrsMaps::fechn_HB[local];
  }else if( map == CardParam::Prs32Top){
    pga =  PrsMaps::fepga_HT[local];
    chn =  PrsMaps::fechn_HT[local];
  }
  //info() << " * ==>(feb,local) = (" << feb << "," << local << ") -> (pga,chn)=("<< pga << "," << chn << ")"<<endmsg;




  return std::make_pair( pga , chn );
}



void OMACaloAnalysis::buildHistDB(){

  std::string gpath = "/Calorimeters/Analysis/";
  if( context() == "CaloMoniAnalysis" )gpath += "MonitoringData/"; 
  else gpath += "CalibrationData/";
  std::string path = gpath + m_detectorName +"/";

  // ---- build detector summary histos ---- //
  std::string spname = path +  m_detectorName +"Analysis";
  std::string sprefix = m_anaTask +"/"+ name() + "/";
  std::string shName = sprefix+m_detectorName+"MonitorSummary/";
  OnlineHistPage* spage= dbSession()->getPage(spname + "Summary") ;
  info() << "Rebuilding histDB page : '" << spname + "Summary" << "'"<<endmsg;
  spage->removeAllHistograms();
  OnlineHistogramOnPage( spage , shName + "Resurrected" , "summary"  , 0.00, 0.00 , 0.49 , 0.49 );  
  OnlineHistogramOnPage( spage , shName + "Known"       , "summary"  , 0.51, 0.00 , 1.00 , 0.49 );  
  OnlineHistogramOnPage( spage , shName + "New"         , "summary",0.00,0.51,1.00,1.00,spname +"Trending");
  
  // ---- build global summary 
  int icrow = CaloCellCode::CaloNumFromName(m_detectorName)/2;
  int iccol = 1-CaloCellCode::CaloNumFromName(m_detectorName)%2;
  info() << "Rebuilding histDB page : '" << gpath+ "AnalysisSummary"  << "'"<<endmsg;
  std::vector<OnlineHistoOnPage*> hlist;
  OnlineHistPage* gpage=dbSession()->getPage(gpath+ "AnalysisSummary");
  if(gpage)gpage->getHistogramList( &hlist );
  if( !hlist.empty() ){
    for( std::vector<OnlineHistoOnPage*>::iterator ih=hlist.begin();hlist.end()!=ih;++ih){
      OnlineHistoOnPage* ohop=*ih;
      if(ohop ==NULL)continue;
      OnlineHistogram*   ohi =ohop->histo;
      if( ohi == NULL)continue;
      std::string hName= ohi->hname();
      if( hName.find( m_detectorName) != std::string::npos && ohi != NULL)gpage->removeHistogram( ohi );
    }
  }
  OnlineHistogramOnPage( dbSession()->getPage(gpath+ "AnalysisSummary"), shName+"New", "summary", 
                         iccol*0.5, icrow*0.5 , iccol*0.5+0.5, icrow*0.5+0.5,
                         spname + "Summary"); 

  //
  info() << "Rebuilding histDB page : '" << gpath+ "AnalysisShortSummary"  << "'"<<endmsg;
  std::vector<OnlineHistoOnPage*> shlist;
  OnlineHistPage* sgpage=dbSession()->getPage(gpath+ "AnalysisShortSummary");
  if(sgpage)sgpage->getHistogramList(&shlist);
  if( !shlist.empty() ){
    for( std::vector<OnlineHistoOnPage*>::iterator ih=shlist.begin();shlist.end()!=ih;++ih){
      OnlineHistoOnPage* ohop=*ih;
      if( ohop == NULL)continue;
      OnlineHistogram*   ohi =ohop->histo;
      if( ohi == NULL)continue;
      std::string hName= ohi->hname();
      if( hName.find( m_detectorName) != std::string::npos && ohi != NULL)sgpage->removeHistogram( ohi );
    }
  }
  OnlineHistogramOnPage( dbSession()->getPage(gpath+ "AnalysisShortSummary"), shName+"ShortSummary", "summary", 
                         iccol*0.5, icrow*0.5 , iccol*0.5+0.5, icrow*0.5+0.5,
                         gpath + "AnalysisSummary");


  // trending summary
  spage= dbSession()->getPage(spname+"Trending") ;
  info() << "Rebuilding histDB page : '" << spname + "Trending" << "'"<<endmsg;
  spage->removeAllHistograms();
  OnlineHistogramOnPage( spage , sprefix+"Trending", "tsummary"  , 0.00, 0.00 , 1.00 , 1.00 , spname + "Summary" );  
  
  // counters
  spage= dbSession()->getPage(path+"Counters") ;
  
  info() << "Rebuilding histDB page : '" << path + "Counters" << "'"<<endmsg;
  spage->removeAllHistograms();
  if(m_splitAna.size() == 2){
    OnlineHistogramOnPage( spage , sprefix+"Counters/1", "1dc"  , 0.00 , 0.51 , 0.245 , 1.00 );  
    OnlineHistogramOnPage( spage , sprefix+"Counters/2", "1dc"  , 0.245, 0.51 , 0.49 , 1.00 );  
  }
  else{
    OnlineHistogramOnPage( spage , sprefix+"Counters/1", "1dc"  , 0.00, 0.51 , 0.49 , 1.00 );  
  }
  OnlineHistogramOnPage( spage , sprefix+"Counters/EventTrending", "trend1"  , 0.51, 0.51 , 1.00 , 1.00 );  
  OnlineHistogramOnPage( spage , sprefix+"Counters/PEDTrending"  , "trend1"  , 0.00, 0.00 , 0.49 , 0.49 );  
  OnlineHistogramOnPage( spage , sprefix+"Counters/SIGTrending"  , "trend1"  , 0.51, 0.00 , 1.00 , 0.49 );  
  
  // ---- built monitor pages ---- //
  
  
  //  re-create the pages
  for( MonMap::iterator im = m_monMap.begin() ; m_monMap.end() != im ; ++im){
    CaloMonitor* monitor = im->second;
    std::string mon = im->first;
    if( monitor->overall() )continue;



    // first remove all pages :
    std::vector<std::string> mlist;
    dbSession()->getPageNamesByFolder( path + "Monitors/" + mon ,mlist);
    for(std::vector<std::string>::iterator i = mlist.begin() ; mlist.end() != i ; ++i){
      info()<<" removing page : '" <<  *i << "'"<<endmsg;
      dbSession()->getPage( *i )->remove();
    }


    std::string pname = path + "Monitors/" + mon + "/"; 

    OnlineHistPage* page= dbSession()->getPage(pname + "Data") ;
    info() << "Rebuilding histDB page : '" << pname + "Data" << "'"<<endmsg;
    page->removeAllHistograms();
    
    const std::string dName = CaloMonitorDataName::Name[ monitor->dataName()  ];
    const std::string dType = CaloMonitorDataType::Name[ monitor->dataType() ];
    
    // 1D histos
    std::string prefix = sprefix+ mon +"/";
    std::string hName  = prefix + dName + dType;
    if( !monitor->pinMonitor() )
      OnlineHistogramOnPage( page , hName + "PMTs1d" , "1d"  , 0.00, 0.51 , 1.00 , 1.00 , pname + "Trending"); 
    else{  
      OnlineHistogramOnPage( page , hName + "PMTs1d" , "1d"  , 0.00, 0.51 , 0.49 , 1.0  , pname + "Trending");  
      OnlineHistogramOnPage( page , hName + "PINs1d" , "1d"  , 0.51, 0.51 , 1.00 , 1.00 , pname + "Trending");  
    }        
    
    // 2D histos
    OnlineHistogramOnPage( page, hName + "2dView" , "2dView" , 0.00 ,0.00,0.49,0.49,  pname + "Trending"); 
    OnlineHistogramOnPage( page, prefix + "NEWLocations", "2dLoc", 0.51 ,0.00,1.00 , 0.49);           
    
    // Trending pages
    OnlineHistPage* tpage= dbSession()->getPage(pname + "Trending") ;
    info() << "Rebuilding histDB page : '" << pname + "Trending" << "'"<<endmsg;
    tpage->removeAllHistograms();
    OnlineHistogramOnPage( tpage , prefix + "Trending" , "trend1"  , 0.00, 0.51 , 0.49 , 1.00 , pname); 
    OnlineHistogramOnPage( tpage , hName  + "Trending" , "trend2"  , 0.51, 0.51 , 1.00 , 1.00 , pname); 
    OnlineHistogramOnPage( tpage , hName  + "TrendingSpread"  , "trend2"  , 0.00, 0.00 , 0.49 , 0.49 , pname); 
    OnlineHistogramOnPage( tpage , hName  + "TrendingMin2Max" , "trend2"  , 0.51, 0.00 , 1.00 , 0.49 , pname); 
  }
  
  // Control channels
  // first remove all pages :
  std::vector<std::string> clist;
  dbSession()->getPageNamesByFolder( path + "ControlChannels",clist);
  for(std::vector<std::string>::iterator i = clist.begin() ; clist.end() != i ; ++i){
    dbSession()->getPage( path+"Monitors/" + *i)->remove();
  }
      
  // then re-create the pages
  int ich = 0;
  for(std::map<int,std::string>::iterator ic = m_controls.begin();ic!=m_controls.end();++ic){    
    ich++;
    std::string cpname = path+ "/ControlChannels/Channel" +Gaudi::Utils::toString(ich) + "/";
    info() << "Rebuilding histDB page : '" << cpname  << "'"<<endmsg;
    std::string chname = sprefix + "ControlChannels/"+Gaudi::Utils::toString(ich)+"/";
    OnlineHistPage* cpage = dbSession()->getPage( cpname ) ;
    cpage->removeAllHistograms();
    OnlineHistogramOnPage( cpage , chname + "Histo/All"         , "1d"  , 0.00, 0.40 , 0.49 , 1.00 );
    OnlineHistogramOnPage( cpage , chname + "Histo/Pedestal"    , "1d"  , 0.51, 0.40 , 1.00 , 1.00 );
    // push trending
    std::map<std::string,std::string> ccMap;
    for( MonMap::iterator im = m_monMap.begin() ; m_monMap.end() != im ; ++im){
      CaloMonitor* monitor = im->second;
      std::string mon = im->first;
      if( monitor->overall() )continue;
      const std::string dName = CaloMonitorDataName::Name[ monitor->dataName()  ];
      const std::string dType = CaloMonitorDataType::Name[ monitor->dataType() ];
      ccMap[dName + dType]= chname + dName + dType +"Trending";
    }
    int nrow = 1;
    if(ccMap.size() > 4 ) nrow=2;
    int ncol = ccMap.size()/nrow + ccMap.size()%2;
    int icm = 0;
    for( std::map<std::string,std::string>::iterator it = ccMap.begin() ; ccMap.end() != it ; ++it){
      int itrow = icm/ncol;
      int itcol = icm%ncol;
      double d  = 1./(double) ncol;
      OnlineHistogramOnPage( cpage , (*it).second   , "trend2NG"  , 
                             d*itcol     , 0.2*itrow , 
                             d*(itcol+1) , 0.2*(itrow+1) );
      icm+=1;
    }
  }
  dbSession()->refresh();
}

// ---------- Display options :

OnlineHistogram* OMACaloAnalysis::OnlineHistogramOnPage(OnlineHistPage* page,
                                                        std::string hName,std::string style, 
                                                        double x0,double y0,double x1,double y1,
                                                        std::string page2display){

  if( !page ) return NULL;
  //std::string pName = page->name();
  //page->remove();
  //page = dbSession()->getPage(pName) ; // page from scratch

  OnlineHistogram* oh  = dbSession()->getHistogram(hName);
  if( !oh ){
    warning() << "Cannot load OnlineHistogram " + hName << endmsg;
    return NULL;
  }
  // set style
  int stat = 1100;
  int un = 1;   
  int blue = 4;
  int red  = 2;
  std::string barhist    = "BARHIST";
  std::string coltextz = "COLTEXTZ";
  std::string colz    = "COLZ";
  std::string colscat = "COLBOX";
  std::string label   = "ADC";
  if( "Spd" == m_detectorName )label = "Rate";
  std::string labelc   = "Number of channels";
  float titleSize    = 0.04;
  float titleOffs    = 0.85;
  std::string marker = "P";
  int markerStyle    = 29;
  float markerSize   = 0.4;
  float labelSize    = 0.02 ;
  float labelASize    = 0.08*(x1-x0) ;
  float labelAYSize    = 0.05 ;
  float zmin = 0.1;
  float zmax = 6500;
  if("Hcal" == m_detectorName)zmax = 1700;
  float marginL   = 0.16/(x1-x0);  // for 2D summary  (y-label on the left)
  float marginR   = 0.15; // for 2D view (colZ palette on the right)
  int width = 2;
  
  oh->unsetAllDisplayOptions();
  oh->unsetPage();
  //dbSession()->declareHistogram(name(), name(), hName, oh->type() );
  oh->getTask()->setSubDetectors("CALO",toUpper(m_detectorName));
  //  info() << "Get task  : " << oh->getTask()->name() << endmsg;
  

  if( style == "1d" || style == "1dc" ){
    if (msgLevel( MSG::DEBUG))debug() << "setting style '1d' for histo " << hName << endmsg;
    oh->setDisplayOption("DRAWOPTS", (void*)& barhist);
    oh->setDisplayOption("FILLCOLOR", (void*)& blue);
    oh->setDisplayOption("STATS",(void*)& stat);
    oh->setDisplayOption("LOGY", (void*)& un );
    if( style == "1d"){
      oh->setDisplayOption("LABEL_X", (void*)& label );
      oh->setDisplayOption("TIT_X_SIZE", (void*)& titleSize );
      oh->setDisplayOption("TIT_X_OFFS", (void*)& titleOffs );
    }
  }else if( style == "2dView" ){
    if (msgLevel( MSG::DEBUG))debug() << "setting style '2dView' for histo " << hName << endmsg;
    oh->setDisplayOption("DRAWOPTS", (void*)& colz);
    //    oh->setDisplayOption("LOGZ", (void*)& un);
    oh->setDisplayOption("GRIDX", (void*)& un);
    oh->setDisplayOption("GRIDY", (void*)& un);
    oh->setDisplayOption("LABEL_Z", (void*)& label );
    oh->setDisplayOption("TIT_Z_SIZE", (void*)& titleSize );
    oh->setDisplayOption("MARGIN_RIGHT", (void*)& marginR );
  }else if( style == "2dLoc" ){
    if (msgLevel( MSG::DEBUG))debug() << "setting style '2dLoc' for histo " << hName << endmsg;
    oh->setDisplayOption("DRAWOPTS", (void*)& colscat);
    oh->setDisplayOption("GRIDX", (void*)& un);
    oh->setDisplayOption("GRIDY", (void*)& un);
    oh->setDisplayOption("LOGZ", (void*)& un);
    oh->setDisplayOption("LINECOLOR", (void*)& red);
  }else if( style == "trend1" ){
    oh->setDisplayOption("DRAWOPTS", (void*)& marker);
    oh->setDisplayOption("MARKERSTYLE", (void*)& markerStyle);
    oh->setDisplayOption("GRIDX", (void*)& un);
    oh->setDisplayOption("GRIDY", (void*)& un);
    oh->setDisplayOption("LAB_X_SIZE", (void*)& labelSize);
    oh->setDisplayOption("MARKERCOLOR", (void*)& red);
  }else if( style == "trend2" || style == "trend2NG"){
    if( style != "trend2NG")oh->setDisplayOption("GRIDX", (void*)& un);
    oh->setDisplayOption("ZMIN", (void*)& zmin);
    oh->setDisplayOption("GRIDY", (void*)& un);  
    oh->setDisplayOption("LAB_X_SIZE", (void*)& labelSize);
    oh->setDisplayOption("LINECOLOR", (void*)& red);
    oh->setDisplayOption("LINEWIDTH", (void*)& width);
  }else if ( style == "summary"){  
    oh->setDisplayOption("LABEL_Z", (void*)& labelc );
    oh->setDisplayOption("LAB_Z_SIZE", (void*)& labelSize);
    oh->setDisplayOption("DRAWOPTS", (void*)& coltextz);
    oh->setDisplayOption("LOGZ", (void*)& un);
    oh->setDisplayOption("GRIDX", (void*)& un);
    oh->setDisplayOption("GRIDY", (void*)& un);
    oh->setDisplayOption("MARGIN_LEFT", (void*)& marginL );
    oh->setDisplayOption("MARGIN_RIGHT", (void*)& marginR );
    oh->setDisplayOption("TIT_Z_OFFS", (void*)& titleOffs );
    oh->setDisplayOption("LAB_X_SIZE", (void*)& labelASize);
    oh->setDisplayOption("LAB_Y_SIZE", (void*)& labelAYSize);
    oh->setDisplayOption("ZMIN", (void*)& zmin);
    oh->setDisplayOption("ZMAX", (void*)& zmax);
  }else if ( style == "tsummary"){  
    oh->setDisplayOption("LABEL_Z", (void*)& labelc );
    oh->setDisplayOption("LAB_Z_SIZE", (void*)& labelSize);
    oh->setDisplayOption("DRAWOPTS", (void*)& coltextz);
    oh->setDisplayOption("LOGZ", (void*)& un);
    oh->setDisplayOption("GRIDX", (void*)& un);
    oh->setDisplayOption("GRIDY", (void*)& un);
    oh->setDisplayOption("ZMIN", (void*)& zmin);
    oh->setDisplayOption("ZMAX", (void*)& zmax);
    marginL*=1.5;
    oh->setDisplayOption("MARGIN_LEFT", (void*)& marginL  );
    oh->setDisplayOption("MARGIN_RIGHT", (void*)& marginR );
    oh->setDisplayOption("LAB_X_SIZE", (void*)& labelSize);
    labelAYSize=0.03;
    oh->setDisplayOption("LAB_Y_SIZE", (void*)& labelAYSize);
    oh->setDisplayOption("MARKERSIZE", (void*)& markerSize);
    oh->setDisplayOption("TIT_Z_OFFS", (void*)& titleOffs );
  }else if ( style == "none"){  
  }else
    warning() << "Unknown style " + style << endmsg;  

  if( page2display != "" )oh->setPage2display( page2display );

  oh->saveDisplayOptions();
  oh->saveHistDisplayOptions();
  if( x0>=0 && y0 >= 0 && x1 >=0 && y1 >= 0)page->declareHistogram(oh  ,x0,y0,x1,y1);
  page->save();
  return oh;
}



CaloMonitor* OMACaloAnalysis::getAutoCaloMonitor(std::string monName,
                                             double lR, double uR,
                                             std::string dName,std::string dType,
                                             std::string hName,std::string hType){
  MonMap::iterator it = m_monMap.find( monName );
  if( m_monMap.end() != it){
    //    if (msgLevel( MSG::DEBUG))debug() << "Accessing CaloMonitor : '"<<monName<<"'"<<endmsg;
    return it->second;
  } else { // create an 'empty' monitor
    info() << "Automatic creation of CaloMonitor : '"<<monName<<"' ("<<dName<<","<<dType<<")"<<endmsg;
    CaloMonitor* monitor = tool<CaloMonitor>( "CaloMonitor" , monName , this );
    bool force = false;
    monitor->setDataName(dName,force);
    monitor->setDataType(dType,force);
    monitor->setHistoName(hName,force);
    monitor->setHistoType(hType,force);
    monitor->setRange(lR,uR,force);    
    monitor->setConfStatus(true);
    monitor->setStatus(true);
    m_monMap[ monName ]= monitor;
    return  monitor;
  }
  return NULL;
}




// Fill Table for CondDB
bool OMACaloAnalysis::FillCondDBTable(){

  CaloVector<int> qualityFlag;   // quality flag vector (must be 'double' in condDB)
  CaloVector<double>   aveLED;   // <LED>
  CaloVector<double>   rmsLED;   // RMS(LED)
  CaloVector<double>   aveRatio; // <LED/PIN>
  CaloVector<double>   rmsRatio; // RMS(LED/PIN)




  //Loop over active cells
  const CaloVector<CellParam>& cells = m_calo->cellParams();  
  for( CaloVector<CellParam>::const_iterator icel = cells.begin() ; cells.end() != icel ; ++icel){
    
    const LHCb::CaloCellID id = icel->cellID();    
    if( !m_calo->valid(id) ) continue;
    if( !monitorChannel( id ) )continue;

    int quality = 0;

    // loop over CaloMonitors    
    for( std::vector<std::string>::iterator mon = m_monitors.begin() ; m_monitors.end() != mon ; ++mon){
      
      CaloMonitor* monitor = m_monMap[ *mon ];
      if( !monitor->status())continue;

      //Quantities of interest
      if( monitor->dataType() == CaloMonitorDataType::MEAN && monitor->dataName() == CaloMonitorDataName::SIGNAL){  
        if( 0 > aveLED.index(id))aveLED.addEntry( monitor->value(id) , id );
      }
      if( monitor->dataType() == CaloMonitorDataType::MEAN && monitor->dataName() == CaloMonitorDataName::RATIO){  
        if( 0 > aveRatio.index(id))aveRatio.addEntry( monitor->value( id ), id);
      }
      if( monitor->dataType() == CaloMonitorDataType::RMS   && monitor->dataName() == CaloMonitorDataName::SIGNAL){  
        if( 0 > rmsLED.index(id))rmsLED.addEntry( monitor->value(id) , id ); 
      }
      if( monitor->dataType() == CaloMonitorDataType::RMS   && monitor->dataName() == CaloMonitorDataName::RATIO){
        if( 0 > rmsRatio.index(id))rmsRatio.addEntry(  monitor->value( id ), id);
      }
      
      // update quality flag
      if( monitor->hasDBQualityMask() ){        
        double value  = monitor->value( id );

        //Check first whether the value for this monitor and this cell is in range.
        if( monitor->outsideRange( id , value ) ) {

          //First deal with dead led or dead pmt
          if(   0 != ( monitor->dbQualityMask() & CaloCellQuality::Dead) 
                || 0 != ( monitor->dbQualityMask() & CaloCellQuality::DeadLED) ){

            if( m_detectorName == "Prs" || m_detectorName == "Spd" ){//Special case for Spd and Prs
              quality |= CaloCellQuality::DeadLED;// to be confirmed with real data as there is no pin for validation              
            }else{// For Ecal and Hcal
              
              if( !id.isPin() ){// If we're dealing with a pmt channel
                
                const LHCb::CaloCellID pinID    = m_calo->cellParam( id ).pins()[0]  ;
                double pinvalue = monitor->value( pinID );
                if( monitor->outsideRange( pinID, pinvalue ) ) {
                  quality |= CaloCellQuality::DeadLED ;
                }else{
                  // quality |= (CaloCellQuality::Dead | CaloCellQuality::BadLEDOpticBundle)  ;//Dead pmt channel or bundle
                  quality |= (CaloCellQuality::BadLEDOpticBundle)  ;//Dead bundle (most likely)
                }
                
              }else{// If we're dealing with a pin channel
                
                // Find the PMTs corresponding to this pin. 
                // They don't all have the same value: test at least one has signal
                CaloPin thePin = m_calo->caloPin(id);          
                const std::vector<LHCb::CaloCellID>& theCells = thePin.cells() ;
                bool onepmtOK = false ;
                int npmtcells= theCells.size();
                
                for(int icellcor = 0 ; npmtcells != icellcor ; ++icellcor){
                  const LHCb::CaloCellID pmtID = theCells[ icellcor];            
                  double pmtval = monitor->value(pmtID);      
                  if( !monitor->outsideRange( pmtID, pmtval)){
                    onepmtOK = true;
                    break; 
                  }
                }
                
                if( onepmtOK ) {
                  quality |= ( CaloCellQuality::Dead | CaloCellQuality::BadLEDOpticBundle)  ; //Dead Pin or bundle
                }
                else{ 
                  quality |= CaloCellQuality::DeadLED ;
                }
                
              }
              
            }//Xcal or Spd/Prs
            
            
          }else{//All the other monitors (ie not those checking DEAD or DEADLED)
            quality |= monitor->dbQualityMask();
          }
          
        }// Is this cell out of range: if( monitor->outsideRange( id , value ) )
        
        
      }//monitor->hasQualityMask ??
      
      qualityFlag.addEntry(  quality, id); // always filled
    
  
    }//Loop on monitors  
    
  }//Loop on cells

  int ncol = 6;
  if( m_detectorName == "Prs")ncol   = 4;
  else if( m_detectorName == "Spd")ncol   = 2;  
  std::string rtyp = Gaudi::Utils::toString(ncol/2);
  std::string typ = Gaudi::Utils::toString(ncol);
  
  
  // check whether we have a complete condDB table
  if( ncol > 3 && aveLED.size() == 0 ) info() << "CondDB table update : missing  <LED> data"<< endmsg;
  if( ncol > 3 && rmsLED.size() == 0 ) info() << "CondDB table update : missing  RMS(LED) data"<< endmsg;
  if( ncol > 5 && aveRatio.size() == 0 )info()<< "CondDB table update : missing  <LED/PIN> data"<< endmsg;
  if( ncol > 5 && rmsRatio.size() == 0 )info()<< "CondDB table update : missing  RMS(LED/PIN) data"<< endmsg;

  // handle the partial update
  int pcol = ncol;
  if( ncol == 6 && aveRatio.size()*rmsRatio.size() == 0 ) pcol -= 2;
  if( ncol > 3 && aveLED.size()*rmsLED.size() == 0) pcol -= 2;

  if( !m_updIncomplete && pcol != ncol )return false;
  
  std::string loc = (m_mon) ?  "on" : "off";
  loc += m_detectorName;  

  // Print the table
  FILE* out = fopen(std::string("/tmp/"+loc+"dbUpd").c_str(), "w");
  FILE* db  = fopen(std::string("/tmp/"+loc+"dbCur").c_str(), "w");
  FILE* ref = fopen(std::string("/tmp/"+loc+"dbRef").c_str(), "w");
  FILE* newref = fopen(std::string("/tmp/"+loc+"dbNewRef").c_str(), "w");
  if( NULL == out )return false;
  //

  for( CaloVector<CellParam>::const_iterator icel = cells.begin() ; cells.end() != icel ; ++icel){
    const LHCb::CaloCellID id = icel->cellID(); 
    if( !m_calo->valid(id) ) continue;
    if( !monitorChannel( id ) )continue;

    double printflag=(double) qualityFlag[id];

    // updated db table (partial or complete)

    if( typ == "6" ){
      fprintf( out , "%10.0f %10.0f %10.4f %10.4f %10.4f %10.4f\n",
               double(id.index()),printflag,aveLED[id],rmsLED[id],aveRatio[id],rmsRatio[id]);
      fprintf( db , "%10.0f %10.0f %10.4f %10.4f %10.4f %10.4f\n",
               double(id.index()),double(m_calo->cellParam(id).quality()),
               m_calo->cellParam(id).ledData(),m_calo->cellParam(id).ledDataRMS(),
               m_calo->cellParam(id).ledMoni(),m_calo->cellParam(id).ledMoniRMS());
      if ( !id.isPin() ){
        fprintf( ref , "%10.0f %10.4f %10.4f\n",
                 double(id.index()),m_calo->cellParam(id).ledDataRef(),m_calo->cellParam(id).ledMoniRef());
        fprintf( newref , "%10.0f %10.4f %10.4f\n",
                 double(id.index()),aveLED[id],aveRatio[id]);
      }
    }   
    else if( typ == "4"){
      fprintf( out , "%10.0f %10.0f %10.4f %10.4f\n",
               double(id.index()),printflag,aveLED[id],rmsLED[id]);
      fprintf( db , "%10.0f %10.0f %10.4f %10.4f\n",
               double(id.index()),double(m_calo->cellParam(id).quality()),
               m_calo->cellParam(id).ledData(),m_calo->cellParam(id).ledDataRMS());
      if ( !id.isPin() ){
        fprintf( ref , "%10.0f %10.4f\n",
                 double(id.index()),m_calo->cellParam(id).ledDataRef());
        fprintf( newref , "%10.0f %10.4f\n",
                 double(id.index()),aveLED[id]);
      }
    }
    else if( typ == "2"){
      fprintf( out , "%10.0f %10.0f\n", double(id.index()),printflag);
      fprintf( db  , "%10.0f %10.0f\n", double(id.index()),double(m_calo->cellParam(id).quality()));
    }
  }
  fclose( out );
  fclose( db );
  fclose( ref );
  fclose( newref );
  std::string det = m_detectorName;
  std::vector<std::string> c;
  std::string q=std::string("\"");
  // backup

  if(m_nBackUp>0){
    for(int k = m_nBackUp-1 ; k != 0 ; --k){
      std::string sk=Gaudi::Utils::toString( k );
      std::string skp=Gaudi::Utils::toString( k+1 );
      c.push_back(std::string("cp "+m_dbTable+"/backup/Quality_"+det+"Upd_prev"+sk+".xml "
                              +m_dbTable+"/backup/Quality_"+det+"Upd_prev"+skp+".xml >& /dev/null "));
    }
  }
  
  c.push_back(std::string("cp "+m_dbTable+"Quality_"+det+"Upd.xml "
                          +m_dbTable+"/backup/Quality_"+det+"Upd_prev1.xml >& /dev/null"));
  
  // disclaimers
  c.push_back(std::string("echo   '<!--- Quality auto-update from "+m_anaTask+m_timeStamp+" saveset -->' >> /tmp/"+loc+"dUpd")); 
  if( pcol != ncol )c.push_back(
         std::string("echo   '<!--- Warning : condDB table is incomplete due to missing information  -->' >> /tmp/"+loc+"dUpd"));
  c.push_back(std::string("echo   '<!--- Analysis time : "+getTime()+" -->' >> /tmp/"+loc+"dUpd"));
  c.push_back(std::string("echo   '<!--- Quality condDB table for "+det+" -->' >> /tmp/"+loc+"dCur"));
  c.push_back(std::string("echo   '<!--- Extraction time : "+getTime()+" -->' >> /tmp/"+loc+"dCur"));
  c.push_back(std::string("echo   '<!--- LEDReference condDB table for "+det+" -->' >> /tmp/"+loc+"dRef"));
  c.push_back(std::string("echo   '<!--- Extraction time : "+getTime()+" -->' >> /tmp/"+loc+"dRef"));

  // prefixes
  c.push_back(std::string("echo  '<DDDB>' > /tmp/"+loc+"pQual"));
  c.push_back(std::string("echo   '<condition name = "+q+"Quality"+q+">' >> /tmp/"+loc+"pQual"));
  c.push_back(std::string("echo    '<param name ="+q+"size"+q+"  type = "+q+"int"+q+"> "+typ+" </param>' >> /tmp/"+loc+"pQual"));
  c.push_back(std::string("echo    '<paramVector name ="+q+"data"+q+"  type = "+q+"double"+q+">'   >> /tmp/"+loc+"pQual"));

  c.push_back(std::string("echo  '<DDDB>' > /tmp/"+loc+"pRef"));
  c.push_back(std::string("echo   '<condition name = "+q+"LEDReference"+q+">' >> /tmp/"+loc+"pRef"));
  c.push_back(std::string("echo    '<param name ="+q+"size"+q+"  type = "+q+"int"+q+"> "+rtyp+" </param>' >> /tmp/"+loc+"pRef"));
  c.push_back(std::string("echo    '<paramVector name ="+q+"data"+q+"  type = "+q+"double"+q+">'   >> /tmp/"+loc+"pRef"));
  std::string col;
  col = std::string("channel   quality ");
  if( "4"  == typ)col += "  <LED>  RMS(LED) ";
  else if( "6"  == typ)col += "  <LED>  RMS(LED)   <LED/PIN>   RMS(LED/PIN)";
  c.push_back(std::string("echo      '<!---  "+col+"  -->' >> /tmp/"+loc+"pQual"));
  c.push_back(std::string("echo      '<!--- channel   <LED>_Ref    <LED/PIN>_Ref   -->' >> /tmp/"+loc+"pRef"));
  // suffix
  c.push_back(std::string("echo      '</paramVector>' > /tmp/"+loc+"suff"));   
  c.push_back(std::string("echo   '</condition>' >> /tmp/"+loc+"suff"));   
  c.push_back(std::string("echo  '</DDDB>' >> /tmp/"+loc+"suff"));  

  std::string stamp = (m_nBackUp < 0) ? m_timeStamp : "";
  // build output
  c.push_back(std::string("cat /tmp/"+loc+"dUpd /tmp/"+loc+"pQual /tmp/"+loc+"dbUpd /tmp/"+loc+"suff > /tmp/"+loc+"upd"));  
  c.push_back(std::string("mv /tmp/"+loc+"upd " + m_dbTable+"/Quality_"+det+"Upd.xml"+stamp)); 

  c.push_back(std::string("cat /tmp/"+loc+"dCur /tmp/"+loc+"pQual /tmp/"+loc+"dbCur /tmp/"+loc+"suff > /tmp/"+loc+"cur")); 
  c.push_back(std::string("mv /tmp/"+loc+"cur " + m_dbTable+"/Quality_"+det+"DB.xml"+stamp)); 
  
  if( "Spd" != det){
    c.push_back(std::string("cat /tmp/"+loc+"dRef /tmp/"+loc+"pRef  /tmp/"+loc+"dbRef /tmp/"+loc+"suff > /tmp/"+loc+"ref")); 
    c.push_back(std::string("cat /tmp/"+loc+"dUpd /tmp/"+loc+"pRef  /tmp/"+loc+"dbNewRef /tmp/"+loc+"suff > /tmp/"+loc+"newref"));
    c.push_back(std::string("mv /tmp/"+loc+"ref " + m_dbTable+"/LEDReference_"+det+"DB.xml"+stamp));  
    if(m_newref)c.push_back(std::string("mv /tmp/"+loc+"newref " + m_dbTable+"/LEDReference_"+det+"NEW.xml"+stamp));  
  }
  
  // clean /tmp
  c.push_back(std::string("rm -f /tmp/"+loc+"dUpd /tmp/"+loc+"dCur /tmp/"+loc+"dRef /tmp/"+loc+"suff /tmp/"+loc+"pQual"));
  c.push_back(std::string("rm -f /tmp/"+loc+"pRef /tmp/"+loc+"dbUpd /tmp/"+loc+"dbRef /tmp/"+loc+"dbCur /tmp/"+loc+"dbNewRef")); 

  // execute the system commands
  for(std::vector<std::string>::iterator ic = c.begin() ; c.end() != ic ; ++ ic){
    system( ic->c_str() );
   }
   

  //info()<< db.str() << std::endl;
  return kTRUE;
}
