// $Id: OMACaloAnalysis.h,v 1.14 2010-10-16 13:32:35 odescham Exp $
#ifndef CALOCALIB_OMACALOANALYSIS_H
#define CALOCALIB_OMACALOANALYSIS_H 1

class StatusCode;
#include "Kernel/CaloCellID.h"
#include "CaloUtils/CaloCellIDAsProperty.h"
#include "GaudiKernel/DeclareFactoryEntries.h"
#include "OMAlib/AnalysisTask.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloKernel/CaloVector.h"
#include "Gaucho/IGauchoMonitorSvc.h"
#include "CaloMonitor.h"
#include <string>
#include <vector>
// ROOT includes
#include "TROOT.h"
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TProfile.h>
#include <TF1.h>
#include <TMath.h>
#include <TSystem.h>
#include <iostream>


namespace PrsMaps{
  // Prs (readout order)->(pga,channel) mapping
  static const int fepga_B[64] = { 7, 7, 5, 5, 3, 3, 1, 1, 5, 5, 7, 7, 1, 1, 3, 3,
                                     7, 7, 5, 5, 3, 3, 1, 1, 5, 5, 7, 7, 1, 1, 3, 3,
                                     2, 2, 0, 0, 6, 6, 4, 4, 0, 0, 2, 2, 4, 4, 6, 6,
                                     2, 2, 0, 0, 6, 6, 4, 4, 0, 0, 2, 2, 4, 4, 6, 6 };
  static const int fechn_B[64] = { 2, 6, 2, 6, 2, 6, 2, 6, 1, 5, 1, 5, 1, 5, 1, 5,
                                     3, 7, 3, 7, 3, 7, 3, 7, 0, 4, 0, 4, 0, 4, 0, 4,
                                     2, 6, 2, 6, 2, 6, 2, 6, 1, 5, 1, 5, 1, 5, 1, 5,
                                     3, 7, 3, 7, 3, 7, 3, 7, 0, 4, 0, 4, 0, 4, 0, 4 };
  static const int fepga_T[64] = { 6, 6, 4, 4, 2, 2, 0, 0, 4, 4, 6, 6, 0, 0, 2, 2,
                                     6, 6, 4, 4, 2, 2, 0, 0, 4, 4, 6, 6, 0, 0, 2, 2,
                                     3, 3, 1, 1, 7, 7, 5, 5, 1, 1, 3, 3, 5, 5, 7, 7,
                                     3, 3, 1, 1, 7, 7, 5, 5, 1, 1, 3, 3, 5, 5, 7, 7 };
  static const int fechn_T[64] = { 4, 0, 4, 0, 4, 0, 4, 0, 7, 3, 7, 3, 7, 3, 7, 3,
                                     5, 1, 5, 1, 5, 1, 5, 1, 6, 2, 6, 2, 6, 2, 6, 2,
                                     4, 0, 4, 0, 4, 0, 4, 0, 7, 3, 7, 3, 7, 3, 7, 3,
                                     5, 1, 5, 1, 5, 1, 5, 1, 6, 2, 6, 2, 6, 2, 6, 2 };
  static const int fepga_HB[64] = { 7, 7, 5, 5, 6, 6, 4, 4, 5, 5, 7, 7, 4, 4, 6, 6,
                                      7, 7, 5, 5, 6, 6, 4, 4, 5, 5, 7, 7, 4, 4, 6, 6 ,
                                      -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
                                      -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
  static const int fechn_HB[64] = { 2, 6, 2, 6, 2, 6, 2, 6, 1, 5, 1, 5, 1, 5, 1, 5,
                                      3, 7, 3, 7, 3, 7, 3, 7, 0, 4, 0, 4, 0, 4, 0, 4,
                                      -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
                                      -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
  static const int fepga_HT[64] = { 6, 6, 4, 4, 7, 7, 5, 5, 4, 4, 6, 6, 5, 5, 7, 7,
                                      6, 6, 4, 4, 7, 7, 5, 5, 4, 4, 6, 6, 5, 5, 7, 7 ,
                             -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
                                      -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
  static const int fechn_HT[64] = { 4, 0, 4, 0, 4, 0, 4, 0, 7, 3, 7, 3, 7, 3, 7, 3,
                                      5, 1, 5, 1, 5, 1, 5, 1, 6, 2, 6, 2, 6, 2, 6, 2,
                                      -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
                                      -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
}


class OMACaloAnalysis :  public AnalysisTask{
 public:




  OMACaloAnalysis(const std::string& name,ISvcLocator* pSvcLocator );
  virtual ~OMACaloAnalysis ();
  StatusCode initialize() override;
  StatusCode finalize() override;
  StatusCode execute() override;
  StatusCode analyze(std::string& SaveSet, std::string Task) override;

private :

  void resetMonitors();
  bool iniBook();
  CaloMonitor* getAutoCaloMonitor(std::string monName,
                                  double lR=0., double uR=255.,
                                  std::string dName="PEDESTAL", std::string dType="OFFSET",
                                  std::string hName="NONE"    , std::string hType="ANALYSIS");
  bool monitorChannel(LHCb::CaloCellID id);
  double deltaDB( LHCb::CaloCellID id , double value);
  void shortBinLabels( AIDA::IHistogram2D* h);
  void setBinLabels( AIDA::IHistogram2D* h);
  void storeAll(TFile* tf, bool clone=false);
  void store( TFile* tf, AIDA::IHistogram1D* h1 );
  void store( TFile* tf, AIDA::IHistogram2D* h2 );
  std::string tree( std::string name);
  void alarmReport();
  StatusCode init();
  void reset();
  void histDBPage(std::string,std::string,bool reset=true);
  void buildHistDB();
  OnlineHistogram* OnlineHistogramOnPage(OnlineHistPage* page, std::string hName,std::string style,
                                         double x0=0.,double y0=0.,double x1=1.,double y1=1.,
                                         std::string page2display="");

  void logOut(std::string log);
  bool FillCondDBTable();

  StatusCode profileAnalysis( CaloMonitor* monitor );
  StatusCode momentAnalysis( CaloMonitor* monitor );
  StatusCode histoAnalysis(   CaloMonitor* monitor );
  bool isDeclared(std::string name){
    for(std::vector<std::string>::iterator it = m_decl.begin(); m_decl.end() != it ; ++it){
      if( *it == name )return true;
    }
    return false;
  }
  void clone1D(std::string lunIn , std::string lunOut="");
  void declareHisto(std::string name,AIDA::IHistogram1D* h){
    if( !m_mon )return;
    if( isDeclared( name ) )return;
    declareInfo(name, h , Gaudi::Utils::Histos::htitle( h ) );
    m_decl.push_back( name );
    return;
  }
  void declareHisto(std::string name,AIDA::IHistogram2D* h){
    if( !m_mon )return;
    if( isDeclared( name ) )return;
    declareInfo(name, h , Gaudi::Utils::Histos::htitle( h ) );
    m_decl.push_back( name );
    return;
  }
  std::string trending( std::string mon,int imon);
  TH1D* addToTrending(AIDA::IHistogram1D* h, std::string stamp,double val, double err=0.);
  TH2D* addToTrending(AIDA::IHistogram2D* h, std::string stamp, int bin, double val, double err=0.);

  std::string getUnit(LHCb::CaloCellID id){
    std::ostringstream sid;
    if( m_loc ){
      int feb = m_calo->cardNumber( id );
      sid<< "crate" << format("%02i", m_calo->cardCrate( feb ) ) << "/"
         << "feb"   << format("%02i", m_calo->cardSlot( feb  ) )<< "/" ;
      int chan  = m_calo->cardColumn( id) + nColCaloCard * m_calo->cardRow( id );
      sid << chan;
    }else{
      sid <<  id.index() ;
    }
    return sid.str();
  }
  const LHCb::CaloCellID getID(int index);
  // Prs fit functions :
  bool _fjump( FILE* fid, std::string pattern );
  std::string _utos( unsigned int u );
  bool WriteFile(std::vector<int> vfile,std::string m_OffsetFile);
  std::vector<int> getFromFile( std::string m_OffsetFile );
  //double fg3(double*x,double*par);
  //double fg2(double*x,double*par);
  int prsBinCor(int bin, int G);
  std::pair<int,int> prsMap(int feb,int local);
  int indmap(int crate, int feb, int channel, int subchannel);

  std::string toUpper(std::string str){
    std::string uStr( str );
    std::transform( str.begin() , str.end() , uStr.begin () , ::toupper ) ;
    return uStr;
  }
  void inPrsAutoUpdate();
  void outPrsAutoUpdate();
  bool prsAutoUpdate(CaloMonitor* monitor){
    if("Prs" == m_detectorName     &&
       m_prsUpd[monitor->parity()]  &&
       monitor->dataType() == CaloMonitorDataType::MEAN &&
       monitor->dataName() == CaloMonitorDataName::PEDESTAL &&
       !m_prsGainsE.empty() && !m_prsGainsO.empty() &&
       m_uConfFiler != "")return true;
    return false;
  }

  std::string getTime(){
    std::ostringstream stimer("");
    stimer      << m_time.year(true) << "/"
                << format( "%02u" ,m_time.month(true) +1) << "/"
                << format( "%02u" ,m_time.day(true) ) << " "
                << format( "%02u" , m_time.hour(true) )<< ":"
                << format( "%02u" ,m_time.minute(true) ) << ":"
                << format( "%02u" ,m_time.second(true) );
    return stimer.str();
  }
  void loadCaloMonitors();
  void trendLogHeader();

  //
  DeCalorimeter* m_calo;
  std::string m_detector;
  typedef std::map<std::string, CaloMonitor*>  MonMap;
  MonMap m_monMap;
  TFile* m_tf;
  TFile* m_tf2;
  int m_bad;
  //
  std::string m_summary;
  std::string m_detectorName;
  std::vector<std::string> m_monitors;
  std::string m_lab;
  bool m_full;
  bool m_log;
  bool m_skipKnown;
  bool m_storeInSaveSet;
  bool m_copyInInput;
  bool m_storeFit;
  std::string m_sDir;
  std::string m_anaTask;
  double m_refCount;
  double m_ledCount;
  double m_pedCount;
  std::string m_source;
  std::vector<std::string> m_hisCount;
  double m_stat;
  std::vector<int> m_feb;
  std::vector<int> m_tell1;
  std::vector<int> m_crate;
  bool m_loc;
  std::string m_fit;
  double m_chi2CutOff;
  Gaudi::Time m_time ;
  bool m_revert;
  bool m_first;
  bool m_mon;
  long m_count;
  IGauchoMonitorSvc* m_pGauchoMonitorSvc; ///< Online Gaucho Monitoring Service
  std::ostringstream m_mylog;
  std::vector<AIDA::IHistogram1D*> m_cloned;
  std::map<std::string,AIDA::IHistogram1D*> m_trendMap;
  std::map<std::string,AIDA::IHistogram1D*> m_trendValMap;
  std::map<std::string,AIDA::IHistogram1D*> m_statMap;
  int m_trending;
  std::string m_trend;
  std::string m_timeStamp;
  std::map<int,std::string> m_controls;
  std::vector<std::string> m_controlsDir;
  std::string m_prefPage;
  std::map<LHCb::CaloCellID,double> m_prsGainsO;
  std::map<LHCb::CaloCellID,double> m_prsGainsE;
  int m_subChannel;
  std::vector<std::string> m_splitAna;
  std::map<std::string,TF1> m_fitMap;
  std::vector<TH1D*> m_inter;
  std::map<std::string,bool> m_prsUpd;
  std::string m_uConfFiler;
  std::string m_rConfFiler;
  unsigned int m_upd;
  bool m_build;
  bool m_comment;
  double m_uRange;
  std::string m_sDirPath;
  std::string m_dbTable;
  bool m_updIncomplete ;
  int m_nBackUp;
  std::vector<int> m_prsVect[8];
  std::vector<std::string> m_prsComment;
  bool m_prsOut;
  bool m_newref;
  bool m_force;
  std::map<std::string,std::vector<std::string> > m_short;
  std::vector<std::string> m_decl;
};

#endif // CALOCALIB_OMACALOANALYSIS_H
