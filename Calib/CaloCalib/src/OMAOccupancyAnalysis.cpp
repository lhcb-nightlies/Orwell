// Include files 
#include <string>
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiAlg/Fill.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "OnlineHistDB/OnlineHistDB.h"
#include "OnlineHistDB/OnlineHistogram.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "Kernel/CaloCellID.h"
#include "TFile.h"
#include "TSystem.h"
#include "TCanvas.h"
#include "TDatime.h"
#include "OMAOccupancyAnalysis.h"
#include <time.h>
#include "CaloDb.h"
#include "ECALmap.h"
#include "HCALmap.h"

using namespace LHCb;
using namespace Gaudi::Units;
using namespace std;

#define NECELLS 6016
#define NEZ  4
#define NEX 64
#define NEY 64
char pmnm_e[NECELLS][8];
double pmg0_e[NECELLS];
double pmal_e[NECELLS];
int cwdac_e[NECELLS];
int cwpow_e[NECELLS];
double HV_e[NECELLS];
double HVst_e[NECELLS];
int mask_e[NECELLS];
double pmg0new_e[NECELLS];
double HVnew_e[NECELLS];
double ernew_e[NECELLS]; // !! ECAL relative error on resulting gain, = eRAT/aRAT !!

#define NHCELLS 1488
char pmnm_h[NHCELLS][8];
double pmg0_h[NHCELLS];
double pmal_h[NHCELLS];
int cwdac_h[NHCELLS];
int cwpow_h[NHCELLS];
double HV_h[NHCELLS];
double HVst_h[NHCELLS];
int mask_h[NHCELLS];
double pmg0new_h[NHCELLS];
double HVnew_h[NHCELLS];
double ernew_h[NHCELLS]; // !! HCAL relative error on resulting gain, = eRAT/aRAT !!


//-----------------------------------------------------------------------------
// Implementation file for class : OMAOccupancyAnalysis
//
// 2015-03-12 : Jean-Francois Marchand
//-----------------------------------------------------------------------------

const unsigned int BitsCol     = 6 ;
const unsigned int BitsRow     = 6 ;
const unsigned int BitsArea    = 2 ;
const unsigned int ShiftCol    = 0 ;
const unsigned int ShiftRow    = ShiftCol  + BitsCol  ;
const unsigned int ShiftArea   = ShiftRow  + BitsRow  ;
const unsigned int MaskCol  = ( ( ( (unsigned int) 1 ) << BitsCol ) - 1 ) << ShiftCol;
const unsigned int MaskRow  = ( ( ( (unsigned int) 1 ) << BitsRow ) - 1 ) << ShiftRow;
const unsigned int MaskArea = ( ( ( (unsigned int) 1 ) << BitsArea ) - 1  ) << ShiftArea ;


// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( OMAOccupancyAnalysis )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
OMAOccupancyAnalysis::OMAOccupancyAnalysis( const std::string& name,
                                      ISvcLocator* pSvcLocator)
: AnalysisTask ( name , pSvcLocator )
				  ,m_first(true)
				  ,m_count(0)
				  ,m_monsvc(false)
				  ,m_sDirPath("") 
				  ,m_magfieldsvc(0) {

  m_detectorName = LHCb::CaloAlgUtils::CaloNameFromAlg( name );
  declareProperty("LEDHist"              , m_LEDHistName  = m_detectorName+"Calib/Profile/Ratio/1");
  declareProperty("OccHistInner"         , m_OccHistInnerName  = m_detectorName+"Occupancy/occ4");
  declareProperty("OccHistMiddle"        , m_OccHistMiddleName  = m_detectorName+"Occupancy/occ4");
  declareProperty("OccHistOuter"         , m_OccHistOuterName  = m_detectorName+"Occupancy/occ4");
  declareProperty("cellHist"             , m_cellHistName = m_detectorName+"Occupancy/cell");
  declareProperty("HistDBBuildPage"      , m_build = false); // expert usage
  declareProperty("ProduceSaveSet"       , m_storeInSaveSet = true ); // Store Output histos in a dedicated SaveSet 
  declareProperty("SaveSetDir"           , m_sDir = "/hist/Savesets" ); // Store Output histos in a dedicated SaveSet 
  declareProperty("UpdateHV"             , m_updateHVDb = false);
  declareProperty("UpdateHVfromOccupancy", m_occBased = true);
  declareProperty("HVDBTime"             , m_hvdbTime = "2015-09-07 12:00:00");
  declareProperty("MaskedCells"          , m_maskedCells = { } );
  declareProperty("PrintCurrentDBvalues" , m_readDBnow = false);

  // set default analysis task name (Saveset directory)
  m_anaTask = "CaloOccupancyAnalysis";  

  debug() << " properties declared " << endmsg;
  // check MonitorSvc is at work
  m_monsvc  = pSvcLocator->existsService("MonitorSvc");
  if (m_monsvc) info() << "MonitoringSvc is at work" << endmsg;	

  debug() << "Service Locator " << pSvcLocator << endmsg;
  
}
//=============================================================================
// Destructor
//=============================================================================
OMAOccupancyAnalysis::~OMAOccupancyAnalysis() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode OMAOccupancyAnalysis::initialize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  m_trendDataOccupancy.clear();

  // Check MonitorSvc is available 
  if( m_monsvc )
    if( serviceLocator()->service("MonitorSvc", m_pGauchoMonitorSvc, false).isFailure())
      return Error("Cannot load MonitorSvc",StatusCode::FAILURE);

  iniBook();      

  if (m_detectorName=="Ecal") { m_det = 0; }
  else if (m_detectorName=="Hcal") { m_det = 1; }
  else {
    error() << "DetectorType "<< m_detectorName << " not known for plotting 2D occupancy maps" << endmsg; 
    return StatusCode::FAILURE;
  }

  // must be executed last, error printed already by AnalysisTask
  //  if ( (AnalysisTask::initialize()).isFailure() ) return StatusCode::FAILURE;
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> End of initialize" << endmsg;
  //return  AnalysisTask::initialize();
  StatusCode sc = AnalysisTask::initialize(); 
  if ( sc.isFailure() ) return sc;

  //  m_magfieldsvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );

  return StatusCode::SUCCESS;
}


//=============================================================================     
StatusCode OMAOccupancyAnalysis::execute(){
  // update gaucho at each pass (each saveset)
  debug() << "---> in execute()" << endmsg;
  if ( m_monsvc ) {
    if (msgLevel( MSG::DEBUG))
      debug() << "Update All" << endmsg;
    m_pGauchoMonitorSvc->updateAll(false); 
  }  
  return StatusCode::SUCCESS;
}


// first pass initialization ========
StatusCode OMAOccupancyAnalysis::init(){

  debug() << "---> in init()" << endmsg;
  m_first = false;
  m_count = 0;
  debug() << "---> end of init()" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode OMAOccupancyAnalysis::analyze(std::string& SaveSet, std::string Task) {

  //////// Preparation to real work 
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Analyze" << endmsg;

  // first pass init
  if (m_first)
    if (init().isFailure()) return Error("Analysis failed",StatusCode::SUCCESS);

  // pass counter
  m_count++;

  // output saveset path :
  char year[5];
  char month[3];
  char day[3]; 
  char hour[3];
  char min[3];
  char sec [3];
  time_t rawTime=time(NULL); 
  struct tm* timeInfo = localtime(&rawTime); 
  ::strftime(year, sizeof(year),"%Y", timeInfo); 
  ::strftime(month, sizeof(month),"%m", timeInfo);   
  ::strftime(day, sizeof(day),"%d", timeInfo); 
  ::strftime(hour, sizeof(hour),"%H", timeInfo); 
  ::strftime(min, sizeof(min),"%M", timeInfo); 
  ::strftime(sec, sizeof(sec),"%S", timeInfo); 
  m_sDirPath = m_sDir  + "/" + year + "/" + m_partition + "/" + m_anaTask + "/" + month + "/" + day;

  // Mandatory: call default analyze method
  StatusCode sc = AnalysisTask::analyze(SaveSet, Task);
  if ( sc.isFailure() ) return sc; 

  // Check we do have a SaveSet to analyse
  if( m_savesetName == "" )return Warning("No saveset to analyze",StatusCode::SUCCESS);

  // derive the timestamp from the input saveset
  int i1 = m_savesetName.find_last_of("/") +1 ; 
  std::string file = m_savesetName.substr(i1,std::string::npos);
  //int i2 = file.find_last_of("_") ; 
  int i2 = file.find_first_of("-") ; 
  int i3 = file.find_last_of(".");
  if( i2 == i3 || i2 == (int) std::string::npos || i3 == (int) std::string::npos)m_timeStamp = "00000000T000000";
  else m_timeStamp = file.substr(i2,i3-i2);
  
  // Open the SaveSet
  info() << "Opening root file " << SaveSet << endmsg;
  m_finput = new TFile(SaveSet.c_str(),"READ");  
  if ( ! m_finput->IsOpen()){
    error() << " Unable to open input file : " << SaveSet  << endmsg;
    return StatusCode::SUCCESS;
  }

  bool isLEDFile = true;
  info() <<" Extracting  histo : " <<  m_LEDHistName << " from Saveset " << endmsg;    
  TProfile* ledHist = (TProfile*) m_finput->Get( m_LEDHistName.c_str() );
  if( 0 == ledHist ) {
    error() << "HISTO "<< m_LEDHistName << " NOT FOUND" << endmsg; 
    isLEDFile = false;
  }
  info() <<" Extracting  histo : " <<  m_OccHistInnerName << " from Saveset " << endmsg;    
  TProfile* occHistInner = (TProfile*) m_finput->Get( m_OccHistInnerName.c_str() );
  if( 0 == occHistInner )  error() << "HISTO "<< m_OccHistInnerName << " NOT FOUND" << endmsg; 
  info() <<" Extracting  histo : " <<  m_OccHistMiddleName << " from Saveset " << endmsg;    
  TProfile* occHistMiddle = (TProfile*) m_finput->Get( m_OccHistMiddleName.c_str() );
  if( 0 == occHistMiddle )  error() << "HISTO "<< m_OccHistMiddleName << " NOT FOUND" << endmsg; 
  info() <<" Extracting  histo : " <<  m_OccHistOuterName << " from Saveset " << endmsg;    
  TProfile* occHistOuter = (TProfile*) m_finput->Get( m_OccHistOuterName.c_str() );
  if( 0 == occHistOuter )  error() << "HISTO "<< m_OccHistOuterName << " NOT FOUND" << endmsg; 
  info() <<" Extracting  histo : " <<  m_cellHistName << " from Saveset " << endmsg;    
  TProfile* cellHist = (TProfile*) m_finput->Get( m_cellHistName.c_str() );
  if( 0 == cellHist )  error() << "HISTO "<< m_cellHistName << " NOT FOUND" << endmsg; 
  
  std::vector<float> occupancies;

  int nbins = 0;
  if (occHistInner) nbins = occHistInner->GetNbinsX();
  else if (occHistMiddle) nbins = occHistMiddle->GetNbinsX();
  else if (occHistOuter) nbins = occHistOuter->GetNbinsX();
  else if (cellHist) nbins = cellHist->GetNbinsX();
  else if (ledHist) nbins = ledHist->GetNbinsX();
  else {
    error() << "nbins == 0" << endmsg;
    return StatusCode::FAILURE;
  }
  if (nbins>12000) nbins=12000;
  verbose() << "---> Number of bins= " << nbins << endmsg;



  //-----------------------------------------------------------
  // First loop to get proper normalization of the occupancy...
  double sum_occ = 0;
  for (int i=1 ; i<nbins+1 ; i++) {
    unsigned int cellID = (unsigned int)i-1;
    const unsigned int area = ( cellID & MaskArea  ) >> ShiftArea; //0: outer, 1: middle, 2: inner
    const unsigned int col  = ( cellID & MaskCol  ) >> ShiftCol;
    const unsigned int row  = ( cellID & MaskRow  ) >> ShiftRow;
    if ( area==0 && ( ( m_det==0 && ( (col<16 && row<8+6 ) ||
				      (col<16 && row>43+6) ||
				      (col>47 && row<8+6 ) ||
				      (col>47 && row>43+6) ) ) || 
		      ( m_det==1 && ( (col<=7 && row<=8) ||
				      (col<=7 && row>=23) ||
				      (col>=24 && row<=8) ||
				      (col>=24 && row>=23)) ) ) ) {
      double occupancy = 0;
      occupancy = occHistOuter->GetBinContent(i);
      sum_occ += occupancy;
    }
  }
  std::cout << "----------> sum_occ current = " << sum_occ << std::endl;

  m_led2d->Reset();
  m_ledError2d->Reset();
  m_occ2d->Reset();
  m_occError2d->Reset();
  m_ratioOcc->Reset();
  m_ratioOccError->Reset();
  m_ratioLED->Reset();
  m_ratioLEDError->Reset();
  m_occLED->Reset();
  m_cellsRef->Reset();
  m_cellsRefAgeing->Reset();
  m_ratioHVRef->Reset();
  m_ratioHVRefLog->Reset();

  //for (int i=1 ; i<occHistInner->GetNbinsX() ; i++) {
  for (int i=1 ; i<nbins+1 ; i++) {
    //for (int i=0 ; i<nbins ; i++) {

    unsigned int cellID = (unsigned int)i-1;
    const unsigned int area = ( cellID & MaskArea  ) >> ShiftArea; //0: outer, 1: middle, 2: inner
    const unsigned int col  = ( cellID & MaskCol  ) >> ShiftCol;
    const unsigned int row  = ( cellID & MaskRow  ) >> ShiftRow;
    //LHCb::CaloCellID id = LHCb::CaloCellID(m_detectorName, area, row, col);

    double led = 0;
    if (ledHist) led = ledHist->GetBinContent(i);
    double ledError = 0;
    if (ledHist && led!=0) ledError = ledHist->GetBinError(i) *1./led;
    double occupancy = 0;
    if (area==0 && occHistOuter)  occupancy = occHistOuter->GetBinContent(i);
    if (area==1 && occHistMiddle) occupancy = occHistMiddle->GetBinContent(i);
    if (area==2 && occHistInner)  occupancy = occHistInner->GetBinContent(i);
    double occupancyError = 0;
    if (area==0 && occHistOuter)  occupancyError = occHistOuter->GetBinError(i);
    if (area==1 && occHistMiddle) occupancyError = occHistMiddle->GetBinError(i);
    if (area==2 && occHistInner)  occupancyError = occHistInner->GetBinError(i);
    double cell = 0;
    if (cellHist) cell = cellHist->GetBinContent(i);

    if (sum_occ!=0) occupancy = occupancy * 1./sum_occ;
    else 
      error() << "Normalization factor for current data is zero... check" << endmsg;

    occupancies.push_back(occupancy);
    occupancies.push_back(occupancyError);
    occupancies.push_back(led);
    occupancies.push_back(ledError);
    occupancies.push_back(cell);


    for (int m=1 ; m<int(((6*1./(m_det*2+1))*1./(area+1))+1) ; m++) {
      for (int n=1 ; n<int(((6*1./(m_det*2+1))*1./(area+1))+1) ; n++) {
	int xbin = int((col+(32*1./(m_det+1))*area)*(6*1./(m_det*2+1))*1./(area+1)+m);
	int ybin = int((row+(32*1./(m_det+1))*area)*(6*1./(m_det*2+1))*1./(area+1)-(36*1./(m_det*5+1))+n);

	if ( ( m_det==0 && ( (area==0 && !(xbin>=97 && ybin>=97 && xbin<=288 && ybin<=216) ) ||
			     (area==1 && (xbin>=97 && ybin>=97 && xbin<=288 && ybin<=216) && 
			      !(xbin>=145 && ybin>=121 && xbin<=240 && ybin<=192) ) ||
			     (area==2 && (xbin>=145 && ybin>=121 && xbin<=240 && ybin<=192) && 
			      !(xbin>=177 && ybin>=145 && xbin<=208 && ybin<=168) ) ) )
	     || ( m_det==1 && ( (area==0 && !(xbin>=17 && ybin>=13 && xbin<=48 && ybin<=40) ) ||
				(area==1 && (xbin>=17 && ybin>=13 && xbin<=48 && ybin<=40) ) ) ) ) {
	  
	  //if (led!=0) {
	  m_led2d      ->SetBinContent( xbin, ybin, led );
	  m_ledError2d ->SetBinContent( xbin, ybin, ledError );
	  //}
	  //if (occupancy!=0) {
	  m_occ2d      ->SetBinContent( xbin, ybin, occupancy );
	  m_occError2d ->SetBinContent( xbin, ybin, occupancyError );
	  //}
	}
      }
    }

  }

  MakeTrendingHistos(occupancies,m_timeStamp,isLEDFile);
  occupancies.clear();
  m_count++;

  if (m_build) { // Re-build histDB pages
    std::string path = "/Calorimeters/Analysis/OccupancyData/" + m_detectorName + "/";
    std::string spname = path +  m_detectorName +"Analysis";
    //std::string sprefix = m_anaTask +"/"+ name() + "/";
    //std::string shName = sprefix + m_detectorName + "MonitorSummary/";
    info() << "Rebuilding histDB page : '" << spname + "Summary" << "'"<<endmsg;
    OnlineHistPage* spage = dbSession()->getPage(spname + "Summary") ;
    spage->removeAllHistograms();
    OnlineHistogramOnPage( spage , "CaloOccupancyAnalysis/"+m_detectorName+"OccupancyAnalysis/Trending/Occupancy2DRatio",        0.00, 0.51, 0.49, 1.00 );  
    OnlineHistogramOnPage( spage , "CaloOccupancyAnalysis/"+m_detectorName+"OccupancyAnalysis/Trending/LED2DRatio",              0.51, 0.51, 1.00, 1.00 );  
    OnlineHistogramOnPage( spage , "CaloOccupancyAnalysis/"+m_detectorName+"OccupancyAnalysis/Trending/Occupancy2DRatioError",   0.00, 0.00, 0.49, 0.49 );  
    OnlineHistogramOnPage( spage , "CaloOccupancyAnalysis/"+m_detectorName+"OccupancyAnalysis/Trending/OccLED",                  0.51, 0.00, 1.00, 0.49, spname +"Trending");
    //OnlineHistogramOnPage( spage , "CaloOccupancyAnalysis/"+m_detectorName+"OccupancyAnalysis/Trending/OccLED",                  0.51, 0.00, 1.00, 0.49, spname +"Trending");

    info() << "Rebuilding histDB page : '" << spname + "ChangeSummary" << "'"<<endmsg;
    OnlineHistPage* spage1 = dbSession()->getPage(spname + "ChangeSummary") ;
    spage1->removeAllHistograms();
    OnlineHistogramOnPage( spage1 , "CaloOccupancyAnalysis/"+m_detectorName+"OccupancyAnalysis/Summary/CellsRef",                 0.00, 0.51, 0.49, 1.00 );
    OnlineHistogramOnPage( spage1 , "CaloOccupancyAnalysis/"+m_detectorName+"OccupancyAnalysis/Summary/CellsRefAgeing",           0.51, 0.51, 1.00, 1.00 );
    OnlineHistogramOnPage( spage1 , "CaloOccupancyAnalysis/"+m_detectorName+"OccupancyAnalysis/Summary/HVratioRef",               0.00, 0.00, 0.49, 0.49 );
    OnlineHistogramOnPage( spage1 , "CaloOccupancyAnalysis/"+m_detectorName+"OccupancyAnalysis/Summary/HVratioRefLog",            0.51, 0.00, 1.00, 0.49 );

  }

  //close input file
  debug() << "closing input file " << m_finput << endmsg;
  m_finput->Close();
  delete m_finput;  

  dbSession()->refresh();


  // --------------------------------------------------------
  // Produce a dedicated SaveSet  when requested
  if( m_storeInSaveSet ){
    if (msgLevel( MSG::DEBUG))debug() << "Produce a dedicated Saveset in " << m_sDirPath << endmsg;   
    // create dir path
    void *dir = gSystem->OpenDirectory(m_sDirPath.c_str());
    if (dir == 0)gSystem->mkdir(m_sDirPath.c_str(),true);
    std::string set = m_sDirPath + "/" + m_anaTask + m_timeStamp + ".root";
    m_tf2 = new TFile( set.c_str() , "UPDATE");   
    info() << "Store the analysis histos in a dedicated SaveSet '" << set << "'" <<endmsg;
    storeAll(m_tf2);
    m_tf2->Close("R");
    delete m_tf2;
    gSystem->FreeDirectory(dir);
    m_tf2 = NULL;
  }



  // analyzing a Saveset should be seen as an 'event' for MonitorSvc
  execute().ignore();

  debug() << "<===== END of Analyze" << endmsg;

  if( sc.isFailure() ) return sc;
  return StatusCode::SUCCESS;
}

//=============================================================================
// Finalization
//=============================================================================
StatusCode OMAOccupancyAnalysis::finalize() {
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;  

  //trend if ( trend() ) m_trendTool->closeFile();

  return AnalysisTask::finalize();
}


bool OMAOccupancyAnalysis::iniBook(){
  if ( msgLevel(MSG::DEBUG) ) debug() << "book summary histo" << endmsg;
  int nx = 0, ny = 0;
  if (m_detectorName=="Ecal") { nx = 384; ny = 312; }
  if (m_detectorName=="Hcal") { nx = 64; ny = 52; }
  m_led2d          = Gaudi::Utils::Aida2ROOT::aida2root( book2D( "/LED2D",                         "LED2D",                 -3878.4, 3878.4, nx, -3151.2,3151.2, ny));
  m_ledError2d     = Gaudi::Utils::Aida2ROOT::aida2root( book2D( "/LEDError2D",                    "LEDError2D",            -3878.4, 3878.4, nx, -3151.2,3151.2, ny));
  m_occ2d          = Gaudi::Utils::Aida2ROOT::aida2root( book2D( "/Occupancy2D",                   "Occupancy2D",           -3878.4, 3878.4, nx, -3151.2,3151.2, ny));
  m_occError2d     = Gaudi::Utils::Aida2ROOT::aida2root( book2D( "/OccupancyError2D",              "OccupancyError2D",      -3878.4, 3878.4, nx, -3151.2,3151.2, ny));
  m_ratioOcc       = Gaudi::Utils::Aida2ROOT::aida2root( book2D( "Trending/Occupancy2DRatio",      "Occupancy ratio",              -3878.4, 3878.4, nx, -3151.2,3151.2, ny));
  m_ratioOccError  = Gaudi::Utils::Aida2ROOT::aida2root( book2D( "Trending/Occupancy2DRatioError", "Error on Occupancy ratio",     -3878.4, 3878.4, nx, -3151.2,3151.2, ny));
  m_ratioLED       = Gaudi::Utils::Aida2ROOT::aida2root( book2D( "Trending/LED2DRatio",            "LED ratio ",                   -3878.4, 3878.4, nx, -3151.2,3151.2, ny));
  m_ratioLEDError  = Gaudi::Utils::Aida2ROOT::aida2root( book2D( "Trending/LED2DRatioError",       "Error on LED ratio",           -3878.4, 3878.4, nx, -3151.2,3151.2, ny));
  m_occLED         = Gaudi::Utils::Aida2ROOT::aida2root( book2D( "Trending/OccLED",                "Occupancy ratio vs LED ratio",  0.8, 1.2, 400, 0.8, 1.2, 400));
  m_cellsRef       = Gaudi::Utils::Aida2ROOT::aida2root( book2D( "Summary/CellsRef",               "Updated cells - All",    -3878.4, 3878.4, nx, -3151.2,3151.2, ny));
  m_cellsRefAgeing = Gaudi::Utils::Aida2ROOT::aida2root( book2D( "Summary/CellsRefAgeing",         "Updated cells - HV_{new} / HV_{reference} > 1",    -3878.4, 3878.4, nx, -3151.2,3151.2, ny));
  m_ratioHVRef     = Gaudi::Utils::Aida2ROOT::aida2root( book1D( "Summary/HVratioRef",             "HV_{new} / HV_{reference}",     0.99, 1.01, 200));
  m_ratioHVRefLog  = Gaudi::Utils::Aida2ROOT::aida2root( book1D( "Summary/HVratioRefLog",          "HV_{new} / HV_{reference}",     0.99, 1.01, 200));
  return true;
}

//=============================================================================

void OMAOccupancyAnalysis::MakeTrendingHistos(std::vector<float> occupancies, std::string m_timeStamp, bool isLEDFile){

  if (msgLevel( MSG::DEBUG)) debug() << "Making the trending histograms" << endmsg;  

  //Preparation for the classical trending
  std::vector< std::vector<float> > vec ;
  m_trendDataOccupancy.push_back(occupancies);
  vec = m_trendDataOccupancy;

  std::tm tm1;
  sscanf(m_timeStamp.c_str(),"-%4d%2d%2dT%2d%2d%2d",&tm1.tm_year,&tm1.tm_mon,&tm1.tm_mday,&tm1.tm_hour,&tm1.tm_min,&tm1.tm_sec);
  TDatime tdt = TDatime(tm1.tm_year, tm1.tm_mon, tm1.tm_mday, tm1.tm_hour, tm1.tm_min, tm1.tm_sec);

  int con = isLHCBCALOdbConnected();
  int succ = 0;
  char erm[1024];
  if (0==con) succ = LHCBCALOdbConnectUser("CALO_OP", "LHCBONR_CALODB", "LHCbCALO_OP", erm);
  debug() << erm << endmsg;
  if (0!=succ) {
    debug() << "Cannot connect CaloDb" << endmsg;
    return;
  }	

  char zs_e[NECELLS];
  int xs_e[NECELLS];
  int ys_e[NECELLS];
  
  char zs_h[NHCELLS];
  int xs_h[NHCELLS];
  int ys_h[NHCELLS];

  if (m_det==0) { // ECAL
    for (int ih_e=0; ih_e<NECELLS; ++ih_e) {
      int icell_e = iECAL2cellID(ih_e);
      int iz_e;
      zxy_E(icell_e, iz_e, xs_e[ih_e], ys_e[ih_e]);
      char cz_e[4] = {' ','O','M','I'};
      zs_e[ih_e] = cz_e[iz_e];
    }

    if (m_readDBnow) {
      time_t     now = time(0);
      struct tm  tstruct;
      char       buf[80];
      tstruct = *localtime(&now);
      strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
      //std::cout << "currentDateTime()=" << buf << std::endl;
      UInt_t  tim = static_cast<UInt_t >(time(NULL));
      TDatime tdtcurrent(tim);
      debug() << "Current Time ---> " << tdtcurrent.AsSQLString() << endmsg;
      succ = ECALGetChanDAC_date(tdtcurrent.AsSQLString(), 1, zs_e,xs_e,ys_e, cwdac_e,cwpow_e, HV_e, HVst_e, erm);
      debug() << "ECAL DAC info: " << erm << endmsg;
      succ = ECALGetPM_date(tdtcurrent.AsSQLString(), zs_e,xs_e,ys_e, &pmnm_e[0][0],8,pmg0_e,pmal_e, erm);
      debug() << "ECAL PMT calib: " << erm << endmsg;
      for (int i=0 ; i<NECELLS ; i++) {
	verbose() << "HV_e_now[" << i << "]= " << HV_e[i] << "; pmal_e[" << i << "]= " << pmal_e[i] << ";" << endmsg;
      }    
    }

    debug() << "Time ---> " << tdt.AsSQLString() << endmsg;
    succ = ECALGetChanDAC_date(tdt.AsSQLString(), 1, zs_e,xs_e,ys_e, cwdac_e,cwpow_e, HV_e, HVst_e, erm); // 1 meand hv_old, 3 means both new and old
    debug() << "ECAL DAC info: " << erm << endmsg;
    succ = ECALGetPM_date(tdt.AsSQLString(), zs_e,xs_e,ys_e, &pmnm_e[0][0],8,pmg0_e,pmal_e, erm);
    debug() << "ECAL PMT calib: " << erm << endmsg;
    for (int i=0 ; i<NECELLS ; i++) {
      verbose() << "HV_e[" << i << "]= " << HV_e[i] << "; pmal_e[" << i << "]= " << pmal_e[i] << ";" << endmsg;
    }    
  }

  if (m_det==1) { // HCAL
    for(int ih_h=0; ih_h<NHCELLS; ++ih_h){
      int icell_h = iHCAL2cellID(ih_h);
      int iz_h;
      zxy_H(icell_h, iz_h, xs_h[ih_h], ys_h[ih_h]);
      if (2==iz_h) zs_h[ih_h]='I';
      else zs_h[ih_h]='O';
    }
    debug() << "Time ---> " << tdt.AsSQLString() << endmsg;
    succ = HCALGetChanDAC_date(tdt.AsSQLString(), 1, zs_h, xs_h, ys_h, cwdac_h,cwpow_h, HV_h, HVst_h, erm);
    debug() << "HCAL DAC info: " << erm << endmsg;
    succ = HCALGetPM_date(tdt.AsSQLString(), zs_h, xs_h, ys_h, &pmnm_h[0][0],8,pmg0_h,pmal_h, erm);
    debug() << "HCAL PMT calib: " << erm << endmsg;    
    for (int i=0 ; i<NHCELLS ; i++) {
      verbose() << "HV_h[" << i << "]= " << HV_h[i] << "; pmal_h[" << i << "]= " << pmal_h[i] << ";" << endmsg;
    }
  }

  m_magfieldsvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );
  if (m_magfieldsvc) debug() << "Magnet polarity isDown: " << m_magfieldsvc->isDown() << endmsg;
  else error() << "no magnetic filed svc found" << endmsg;

  std::vector<float> ref;
  std::string refName;
  if (m_magfieldsvc->isDown())  refName = "/home/jmarchan/calib/referenceDown.root";
  if (!m_magfieldsvc->isDown()) refName = "/home/jmarchan/calib/referenceUp.root";
  TFile *refFile = new TFile(refName.c_str(),"READ");
  if ( refFile->IsOpen() ) verbose() << "File opened successfully" << endmsg;
  else verbose() << "file: " << refName << " not found" << endmsg;

  std::string histName = "histo" + m_detectorName;
  TH1F* h = (TH1F*) refFile->Get(histName.c_str());
  for (int i=0 ; i<h->GetNbinsX() ; i++) {
    ref.push_back(h->GetBinContent(i+1));
  }
  refFile->Close();
		  
  //-----------------------------------------------------------
  // First loop to get proper normalization of the occupancy...
  double sum_occ = 0;
  for (unsigned int i=0 ; i<occupancies.size() ; i++) {
    int type = i%5; // 0: occupancy, 1: occupancy error, 2: led, 3: led error, 4: cell
    unsigned int cellID = (unsigned int)i/5;
    const unsigned int area = ( cellID & MaskArea  ) >> ShiftArea; //0: outer, 1: middle, 2: inner
    const unsigned int col  = ( cellID & MaskCol  ) >> ShiftCol;
    const unsigned int row  = ( cellID & MaskRow  ) >> ShiftRow;
    if (ref.at(i)==0) continue;
    if (type==4) {
      if ( area==0 && ( ( m_det==0 && ( (col<16 && row<8+6 ) ||
					(col<16 && row>43+6) ||
					(col>47 && row<8+6 ) ||
					(col>47 && row>43+6) ) ) || 
			( m_det==1 && ( (col<=7 && row<=8) ||
					(col<=7 && row>=23) ||
					(col>=24 && row<=8) ||
					(col>=24 && row>=23)) ) ) ) {
	double occupancy = 0;
	occupancy = ref.at(i-4);
	sum_occ += occupancy;
      }
    }
  }
  std::cout << "----------> sum_occ reference = " << sum_occ << std::endl;


  for (unsigned int i=0 ; i<occupancies.size() ; i++) {

    int type = i%5; // 0: occupancy, 1: occupancy error, 2: led, 3: led error, 4: cell
    unsigned int cellID = (unsigned int)i/5;
    const unsigned int area = ( cellID & MaskArea  ) >> ShiftArea; //0: outer, 1: middle, 2: inner
    const unsigned int col  = ( cellID & MaskCol  ) >> ShiftCol;
    const unsigned int row  = ( cellID & MaskRow  ) >> ShiftRow;

    if (ref.at(i)==0) continue;
       
    double ratioOcc = 0, ratioLED = 0, led1 = 0, led2 = 0, occupancy1 = 0, occupancy2 = 0, n2_1 = 0, n2_2 = 0, ratioOccError = 0, ratioLEDError = 0;
    if (type==4) {
      n2_2 = occupancies.at(i);
      n2_1 = ref.at(i);
      led2 = occupancies.at(i-2);
      led1 = ref.at(i-2);
      occupancy2 = occupancies.at(i-4);
      occupancy1 = ref.at(i-4);
      if (sum_occ!=0) occupancy1 = occupancy1 * 1./sum_occ;
      else 
	error() << "Normalization factor for reference is zero... check" << endmsg;
      if (fabs(led1)>0) ratioLED = led2 * 1./led1;
      if (fabs(occupancy1)>0) ratioOcc = occupancy2 * 1./occupancy1;

      // ref i-4   occupancy
      // ref i-3   occupancyError
      // ref i-2   LED
      // ref i-1   LEDError
      // ref i     n2
      
      double n11 = occupancy1 * n2_1;
      double n12 = occupancy2 * n2_2;
      double n21 = n2_1;
      double n22 = n2_2;
      double p1 = n11*1./n21;
      double p2 = n12*1./n22;
      double error0 = sqrt(p1*(1-p1)*1./n21) * 1./occupancy1;
      double error1 = sqrt(p2*(1-p2)*1./n22) * 1./occupancy2;
      ratioOccError = sqrt(error0*error0 + error1*error1);
      m_occLED->Fill( ratioLED, ratioOcc );

      double errorLED1 = ref.at(i-1);
      double errorLED2 = occupancies.at(i-1);
      ratioLEDError = sqrt(errorLED1*errorLED1 + errorLED2*errorLED2);

      if ( m_occBased ) {
	if ( fabs(ratioOcc-1)>3*ratioOccError && ratioOccError<0.01 && ratioOccError!=0 ) {
	  verbose() << "cellID: " << cellID << " to be changed - ratioOcc= " << ratioOcc << " - ratioOccError= " << ratioOccError << endmsg;
	  
	  if (m_det==0) { // ECAL
	    int iEcal = cellID2iECAL(cellID);
	    HV_e[iEcal] = HV_e[iEcal] * pow(1./ratioOcc, 1*1./(pmal_e[iEcal]));
	    m_ratioHVRef->Fill( pow(ratioOcc, 1*1./(pmal_e[iEcal])) );
	    m_ratioHVRefLog->Fill( pow(ratioOcc, 1*1./(pmal_e[iEcal])) );
	  }
	  if (m_det==1) { // HCAL
	    int iHcal = cellID2iHCAL(cellID);
	    HV_h[iHcal] = HV_h[iHcal] * pow(1./ratioOcc, 1./pmal_h[iHcal]);
	    m_ratioHVRef->Fill( pow(ratioOcc, 1*1./(pmal_h[iHcal])) );
	    m_ratioHVRefLog->Fill( pow(ratioOcc, 1*1./(pmal_h[iHcal])) );
	  }
	} 
      } else {
	//if ( fabs(ratioLED-1)>0.01 && ratioLED!=0 ) {
	if ( ratioLED!=0 ) {
	  verbose() << "cellID: " << cellID << " to be changed - ratioLED= " << ratioLED << " - ratioLEDError= " << ratioLEDError << endmsg;

	  bool isMasked = false;
	  if (m_maskedCells.size()%5==0 && m_maskedCells.size()/5>0) { 
	    int numberMaskedCells = m_maskedCells.size()/5;
	    for (int k=0 ; k<numberMaskedCells ; k++) 
	      if ( area==m_maskedCells.at(k*5+0) && (col>=m_maskedCells.at(k*5+1) && row>=m_maskedCells.at(k*5+2) && col<=m_maskedCells.at(k*5+3) && row<=m_maskedCells.at(k*5+4)) ) isMasked = true;
	  }
	  //if (m_maskedCells.size()%4==0 && m_maskedCells.size()/4>0) { 
	  //  int numberMaskedCells = m_maskedCells.size()/4;
	  //  for (int k=0 ; k<numberMaskedCells ; k++)
	  //    if ( area==0 && (col>=m_maskedCells.at(k*4+0) && row>=m_maskedCells.at(k*4+1) && col<=m_maskedCells.at(k*4+2) && row<=m_maskedCells.at(k*4+3)) ) isMasked = true;
	  //}

	  if (m_det==0 //&& ratioLEDError<0.01// && 
	      && !isMasked
//	      && !(area==0 &&  ( (col>=52 && row>=18 && col<=55 && row<=21) ||
//				 (col>=60 && row>=22 && col<=63 && row<=25) ))
//	      !(area==0 &&  ( (col>=4  && row>=26 && col<=11 && row<=29 ) || 
//			      (col>=12 && row>=50 && col<=19 && row<=53) ||
//			      (col>=24 && row>=54 && col<=27 && row<=57) ||
//			      (col>=60 && row>=54 && col<=63 && row<=57) ||
//			      (col>=48 && row>=6  && col<=51 && row<=13) ||
//			      (col>=60 && row>=18 && col<=63 && row<=25) ||
//			      (col>=60 && row>=6  && col<=63 && row<=9 ) ||
//			      (col>=44 && row>=10 && col<=47 && row<=13) ||
//			      (col>=20 && row>=10 && col<=23 && row<=13) ||
//			      (col>=8  && row>=18 && col<=11 && row<=21) 
//			      ) ) 
	      ) { // ECAL - exclude outer region for the moment since the LED are not stable
	    int iEcal = cellID2iECAL(cellID);
	    HV_e[iEcal] = HV_e[iEcal] * pow(1./ratioLED, 1*1./(pmal_e[iEcal]));	    
	    m_ratioHVRef->Fill( pow(1./ratioLED, 1*1./(pmal_e[iEcal])) );
	    m_ratioHVRefLog->Fill( pow(1./ratioLED, 1*1./(pmal_e[iEcal])) );
	  }
	  if (m_det==1) {// && ratioLEDError<0.02) { // HCAL
	    int iHcal = cellID2iHCAL(cellID);
	    HV_h[iHcal] = HV_h[iHcal] * pow(1./ratioLED, 1./pmal_h[iHcal]);
	    m_ratioHVRef->Fill( pow(1./ratioLED, 1*1./(pmal_h[iHcal])) );
	    m_ratioHVRefLog->Fill( pow(1./ratioLED, 1*1./(pmal_h[iHcal])) );
	  }
	} 
      }

      

      for (int m=1 ; m<int(((6*1./(m_det*2+1))*1./(area+1))+1) ; m++) {
	for (int n=1 ; n<int(((6*1./(m_det*2+1))*1./(area+1))+1) ; n++) {
	  int xbin = int((col+(32*1./(m_det+1))*area)*(6*1./(m_det*2+1))*1./(area+1)+m);
	  int ybin = int((row+(32*1./(m_det+1))*area)*(6*1./(m_det*2+1))*1./(area+1)-(36*1./(m_det*5+1))+n);

	  if ( ( m_det==0 && ( (area==0 && !(xbin>=97 && ybin>=97 && xbin<=288 && ybin<=216) ) ||
			       (area==1 && (xbin>=97 && ybin>=97 && xbin<=288 && ybin<=216) && 
				!(xbin>=145 && ybin>=121 && xbin<=240 && ybin<=192) ) ||
			       (area==2 && (xbin>=145 && ybin>=121 && xbin<=240 && ybin<=192) && 
				!(xbin>=177 && ybin>=145 && xbin<=208 && ybin<=168) ) ) )
	       || ( m_det==1 && ( (area==0 && !(xbin>=17 && ybin>=13 && xbin<=48 && ybin<=40) ) ||
				  (area==1 && (xbin>=17 && ybin>=13 && xbin<=48 && ybin<=40) ) ) ) ) {

	    m_ratioOcc      ->SetBinContent( xbin, ybin, ratioOcc );
	    m_ratioLED      ->SetBinContent( xbin, ybin, ratioLED );
	    m_ratioOccError ->SetBinContent( xbin, ybin, ratioOccError );
	    m_ratioLEDError ->SetBinContent( xbin, ybin, ratioLEDError );

	    if ( m_occBased ) {
	      if ( fabs(ratioOcc-1)>3*ratioOccError && ratioOccError<0.01 && ratioOccError!=0 ) {
		if (m_det==0) {
		  int iEcal = cellID2iECAL(cellID);
		  m_cellsRef      ->SetBinContent( xbin, ybin, pow(1./ratioOcc, 1*1./pmal_e[iEcal]) );
		  if (pow(1./ratioOcc, 1*1./pmal_e[iEcal])>1) 
		    m_cellsRefAgeing->SetBinContent( xbin, ybin, pow(1./ratioOcc, 1*1./pmal_e[iEcal]) );
		}
		if (m_det==1) {
		  int iHcal = cellID2iHCAL(cellID);
		  m_cellsRef      ->SetBinContent( xbin, ybin, pow(1./ratioOcc, 1*1./pmal_e[iHcal]) );
		  if (pow(1./ratioOcc, 1*1./pmal_e[iHcal])>1)
		    m_cellsRefAgeing->SetBinContent( xbin, ybin, pow(1./ratioOcc, 1*1./pmal_e[iHcal]) );
		}
	      } 
	    } else {
	      //if ( fabs(ratioLED-1)>0.01 && ratioLED!=0 ) {
	      if ( ratioLED!=0 ) {

		bool isMasked = false;
		if (m_maskedCells.size()%5==0 && m_maskedCells.size()/5>0) { 
		  int numberMaskedCells = m_maskedCells.size()/5;
		  for (int k=0 ; k<numberMaskedCells ; k++)
		    if ( area==m_maskedCells.at(k*5+0) && (col>=m_maskedCells.at(k*5+1) && row>=m_maskedCells.at(k*5+2) && col<=m_maskedCells.at(k*5+3) && row<=m_maskedCells.at(k*5+4)) ) isMasked = true;
		}
		//if (m_maskedCells.size()%4==0 && m_maskedCells.size()/4>0) { 
		//  int numberMaskedCells = m_maskedCells.size()/4;
		//  for (int k=0 ; k<numberMaskedCells ; k++)
		//    if ( area==0 && (col>=m_maskedCells.at(k*4+0) && row>=m_maskedCells.at(k*4+1) && col<=m_maskedCells.at(k*4+2) && row<=m_maskedCells.at(k*4+3)) ) isMasked = true;
		//}
		if (m_det==0 //&& ratioLEDError<0.01 //&& 
		    && !isMasked
		    //		    && !(area==0 &&  ( (col>=52 && row>=18 && col<=55 && row<=21) ||
		    //				       (col>=60 && row>=22 && col<=63 && row<=25) ))
		    //		    !(area==0 &&  ( (col>=4  && row>=26 && col<=11 && row<=29 ) || 
		    //				    (col>=12 && row>=50 && col<=19 && row<=53) ||
		    //				    (col>=24 && row>=54 && col<=27 && row<=57) ||
		    //				    (col>=60 && row>=54 && col<=63 && row<=57) ||
		    //				    (col>=48 && row>=6  && col<=51 && row<=13) ||
		    //				    (col>=60 && row>=18 && col<=63 && row<=25) ||
		    //				    (col>=60 && row>=6  && col<=63 && row<=9 ) ||
		    //				    (col>=44 && row>=10 && col<=47 && row<=13) ||
		    //				    (col>=20 && row>=10 && col<=23 && row<=13) ||
		    //				    (col>=8  && row>=18 && col<=11 && row<=21) 
		    //				    ) ) 		    
		    //area!=0
		    ) {// Exclude ECAL outer region for the moment since the LED are not stable) {
		  int iEcal = cellID2iECAL(cellID);
		  m_cellsRef      ->SetBinContent( xbin, ybin, pow(1./ratioLED, 1*1./pmal_e[iEcal]) );
		  if (pow(1./ratioLED, 1*1./pmal_e[iEcal])>1)
		    m_cellsRefAgeing      ->SetBinContent( xbin, ybin, pow(1./ratioLED, 1*1./pmal_e[iEcal]) );
		}
		if (m_det==1) {// && ratioLEDError<0.02) {
		  int iHcal = cellID2iHCAL(cellID);
		  m_cellsRef      ->SetBinContent( xbin, ybin, pow(1./ratioLED, 1./pmal_h[iHcal]) );//1 );
		  if (pow(1./ratioLED, 1./pmal_h[iHcal])>1)
		    m_cellsRefAgeing      ->SetBinContent( xbin, ybin, pow(1./ratioLED, 1./pmal_h[iHcal]) );//1 );
		}
	      } 
	    }	 

	  }
	}
      }
    }
  }
  
  succ = LHCBCALOdbClose(erm);
  debug() << erm << endmsg;
  if(0!=succ){
    debug() << "Cannot disconnect CaloDb" << endmsg;
    return;
  }
 
  // Write new HV to DB
  if (m_updateHVDb && (isLEDFile && !m_occBased) ) {
    
    con = isLHCBCALOdbConnected();
    if(!con){
      succ=LHCBCALOdbConnectUser("CALODB_ADMIN", "LHCBONR_CALODB", "aDm1n", erm);
      debug() << erm << endmsg;
      if(0!=succ){
	debug() << "Cannot connect CaloDb" << endmsg;
	return;
      }
    }
    
    if (m_det==0) { // ECAL
      //verbose() << "before: " << HV_e[6015] << endmsg;
      //HV_e[6015]= 0.7;
      //HV_e[6015]= 0.668381;
      //verbose() << "after: " << HV_e[6015] << endmsg;
      succ = ECALStoreDACSettings(zs_e, xs_e, ys_e, HV_e, HVst_e, erm);
      info() << "ECAL write HV: " << erm << endmsg;
      for (int i=0 ; i<NECELLS ; i++) {
	verbose() << "HV_enew[" << i << "]= " << HV_e[i] << endmsg;
      }
    }
    if (m_det==1) { // HCAL
      //HV_h[1487]= 1.08906;
      //HV_h[1487]= 1.;
      succ = HCALStoreDACSettings(zs_h, xs_h, ys_h, HV_h, HVst_h, erm);
      info() << "HCAL write HV: " << erm << endmsg;
      for (int i=0 ; i<NHCELLS ; i++) {
	verbose() << "HV_hnew[" << i << "]= " << HV_h[i] << endmsg;
      }
    }

    if(!con){
      succ = LHCBCALOdbClose(erm);
      debug() << erm << endmsg;
      if (0!=succ) {
	debug() << "Cannot disconnect CaloDb" << endmsg;
	return;
      }
    }
    
  }

  return;
}

OnlineHistogram* OMAOccupancyAnalysis::OnlineHistogramOnPage(OnlineHistPage* page, std::string hName, double x0, double y0, double x1, double y1, std::string page2display){

  if( !page ) return NULL;

  OnlineHistogram* oh  = dbSession()->getHistogram(hName);
  if( !oh ) {
    warning() << "Cannot load OnlineHistogram " + hName << endmsg;
    return NULL;
  }
  // set style
  //int un = 1;   
  std::string colz    = "COLZ";
  std::string labelX  = "X [mm]";
  std::string labelY  = "Y [mm]";
  float labelXSize    = 0.05;
  float labelYSize    = 0.05;
  float marginL   = 0.08/(x1-x0);
  float marginR   = 0.12;
  float offsetX   = 1.50;
  float offsetY   = 1.50;
  float zmaxError = 0.015;
  float zmax      = 1.05;
  float zmin      = 0.95;
  float zmaxhv    = 1.005;
  float zminhv    = 0.995;
  float zero      = 0.;
  int log         = 1;
  float xmax      = 1.005;
  float xmin      = 0.995;

  if (hName=="CaloOccupancyAnalysis/"+m_detectorName+"OccupancyAnalysis/Trending/OccLED") {
    labelX = "LED ratio";    
    labelY = "Occupancy ratio";
  }

  oh->unsetAllDisplayOptions();
  oh->unsetPage();
  oh->getTask()->setSubDetectors("CALO",toUpper(m_detectorName));
  
  //oh->setDisplayOption("LOGZ", (void*)& un);
  oh->setDisplayOption("DRAWOPTS", (void*)& colz);
  oh->setDisplayOption("MARGIN_LEFT", (void*)& marginL );
  oh->setDisplayOption("MARGIN_RIGHT", (void*)& marginR );
  oh->setDisplayOption("LAB_X_SIZE", (void*)& labelXSize);
  oh->setDisplayOption("LAB_Y_SIZE", (void*)& labelYSize);
  oh->setDisplayOption("LABEL_X", (void*)& labelX);
  oh->setDisplayOption("LABEL_Y", (void*)& labelY);
  oh->setDisplayOption("TIT_X_OFFS", (void*)& offsetX);
  oh->setDisplayOption("TIT_Y_OFFS", (void*)& offsetY);

  if (hName=="CaloOccupancyAnalysis/"+m_detectorName+"OccupancyAnalysis/Trending/LED2DRatio" || 
      hName=="CaloOccupancyAnalysis/"+m_detectorName+"OccupancyAnalysis/Trending/Occupancy2DRatio") {
    oh->setDisplayOption("ZMIN", (void*)& zmin);
    oh->setDisplayOption("ZMAX", (void*)& zmax);
  }
  if (hName=="CaloOccupancyAnalysis/"+m_detectorName+"OccupancyAnalysis/Trending/Occupancy2DRatioError" ) {
    oh->setDisplayOption("ZMIN", (void*)& zero);
    oh->setDisplayOption("ZMAX", (void*)& zmaxError);
  } 
  if (hName=="CaloOccupancyAnalysis/"+m_detectorName+"OccupancyAnalysis/Summary/CellsRef" ) {
    oh->setDisplayOption("ZMIN", (void*)& zminhv);
    oh->setDisplayOption("ZMAX", (void*)& zmaxhv);
  }
  if (hName=="CaloOccupancyAnalysis/"+m_detectorName+"OccupancyAnalysis/Summary/CellsRefAgeing" ) {
    oh->setDisplayOption("ZMIN", (void*)& zminhv);
    oh->setDisplayOption("ZMAX", (void*)& zmaxhv);
  }
  int optioni;
  if (hName=="CaloOccupancyAnalysis/"+m_detectorName+"OccupancyAnalysis/Summary/HVratioRef" ) {
    optioni = 111111;
    oh->setDisplayOption("STATS", &optioni);
  }
  if (hName=="CaloOccupancyAnalysis/"+m_detectorName+"OccupancyAnalysis/Summary/HVratioRefLog" ) {
    optioni = 111111;
    oh->setDisplayOption("STATS", &optioni);
    oh->setDisplayOption("LOGY", (void*)& log);
    oh->setDisplayOption("XMIN", (void*)& xmin);
    oh->setDisplayOption("XMAX", (void*)& xmax);
  }

  if ( page2display != "" ) oh->setPage2display( page2display );

  oh->saveDisplayOptions();
  oh->saveHistDisplayOptions();
  if( x0>=0 && y0 >= 0 && x1 >=0 && y1 >= 0) page->declareHistogram(oh,x0,y0,x1,y1);
  page->save();
  return oh;
}



void OMAOccupancyAnalysis::storeAll(TFile* tf){
  store( tf, histo2D(HistoID( "/LED2D" ) ) );
  store( tf, histo2D(HistoID( "/LEDError2D" ) ) );
  store( tf, histo2D(HistoID( "/Occupancy2D" ) ) );
  store( tf, histo2D(HistoID( "/OccupancyError2D" ) ) );

  store( tf, histo2D(HistoID( "Trending/Occupancy2DRatio" ) ) );
  store( tf, histo2D(HistoID( "Trending/LED2DRatio") ) );
  store( tf, histo2D(HistoID( "Trending/Occupancy2DRatioError") ) );
  store( tf, histo2D(HistoID( "Trending/LED2DRatioError") ) );
  store( tf, histo2D(HistoID( "Trending/OccLED") ) );

  store( tf, histo2D(HistoID( "Summary/CellsRef") ) );
  store( tf, histo2D(HistoID( "Summary/CellsRefAgeing") ) );
  store( tf, histo1D(HistoID( "Summary/HVratioRef") ) );
  store( tf, histo1D(HistoID( "Summary/HVratioRefLog") ) );
}

//--------------------------------------------
void OMAOccupancyAnalysis::store( TFile* tf, AIDA::IHistogram2D* h2 ){
  if( NULL == h2 )return;
  
  const TH2D* th2 = Gaudi::Utils::Aida2ROOT::aida2root( h2 );
  std::string nam = "/" + std::string( th2->GetName());
  int index = nam.find_last_of(".")+1;
  if(index !=0)nam =  nam.substr(0,index-1)+"/"+nam.substr(index,std::string::npos);
  //nam = nam.substr(0,index-1)+"/" + nam;
  // check the histo does not yet exists
  if( NULL != tf){
    TH2D* check = NULL;
    check = (TH2D*) tf->Get( nam.c_str() );
    if(  NULL !=  check ){
      if (msgLevel( MSG::DEBUG))debug() << "Histograms '" <<nam
                <<"' already saved in " << tf << " - do not overwrite"<< endmsg;
      return;
    }
  }  
  if (msgLevel( MSG::DEBUG) )debug() << "storing histo '" << nam << "' in " << tf << endmsg;
  std::string newName = tree(nam); 
  TH2D* out = new TH2D( *th2 );
  out->SetName( newName.c_str() );
  out->Write( NULL , TObject::kOverwrite );
  delete out;
}

void OMAOccupancyAnalysis::store( TFile* tf, AIDA::IHistogram1D* h1 ){
  if( NULL == h1 )return;
  
  const TH1D* th1 = Gaudi::Utils::Aida2ROOT::aida2root( h1 );
  std::string nam = "/" + std::string( th1->GetName());
  int index = nam.find_last_of(".")+1;
  if(index !=0)nam =  nam.substr(0,index-1)+"/"+nam.substr(index,std::string::npos);
  //nam = nam.substr(0,index-1)+"/" + nam;
  // check the histo does not yet exists
  if( NULL != tf){
    TH1D* check = NULL;
    check = (TH1D*) tf->Get( nam.c_str() );
    if(  NULL !=  check ){
      if (msgLevel( MSG::DEBUG))debug() << "Histograms '" <<nam
                <<"' already saved in " << tf << " - do not overwrite"<< endmsg;
      return;
    }
  }  
  if (msgLevel( MSG::DEBUG) )debug() << "storing histo '" << nam << "' in " << tf << endmsg;
  std::string newName = tree(nam); 
  TH1D* out = new TH1D( *th1 );
  out->SetName( newName.c_str() );
  out->Write( NULL , TObject::kOverwrite );
  delete out;
}

std::string OMAOccupancyAnalysis::tree( std::string name){
  unsigned int index = name.find_last_of("/") +1 ;
  std::string full = name.substr(0,index-1);
  std::string newName = name.substr(index,std::string::npos);
  
  int p,i;
  std::string fil,cur,s;
  TDirectory *gDir = gDirectory;

  std::list<std::string> lpath;
  i = 1;
  if ( (p=full.find(":",0)) != -1 ) {
    fil = full.substr(0,p);
    i = p+1;
    fil += ":/";
    gDirectory->cd(fil.c_str());
  }
  while ( (p = full.find("/",i)) != -1) {
    s = full.substr(i,p-i);
    lpath.push_back(s);
    i = p+1;
  }
  lpath.push_back( full.substr(i,full.length()-i) );
  if ( full.substr(0,1) == "/") {
    gDirectory->cd("/");
  }

  std::list<std::string>::const_iterator litr;
  for(litr=lpath.begin(); litr!=lpath.end(); ++litr) {
    cur = *litr;
    if (! gDirectory->GetKey(litr->c_str()) ) {
      gDirectory->mkdir(litr->c_str());
    }
    gDirectory->cd(litr->c_str());
  }
  gDirectory = gDir;
  gDirectory->cd(full.c_str());
  return newName;
}
