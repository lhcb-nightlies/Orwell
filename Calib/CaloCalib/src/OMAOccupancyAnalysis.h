#ifndef OMAOCCUPANCYANALYSIS_H
#define OMAOCCUPANCYANALYSIS_H 1

// Include files
#include "GaudiKernel/DeclareFactoryEntries.h"
#include "OMAlib/AnalysisTask.h"
#include "CaloDet/DeCalorimeter.h"
#include "Gaucho/IGauchoMonitorSvc.h"
#include "CaloMonitor.h"
#include <string>
#include <vector>
#include "Trending/ITrendingTool.h"
#include "Kernel/ILHCbMagnetSvc.h"
// ROOT includes
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TProfile.h>
#include <TMath.h>
#include <TSystem.h>

/** @class OMAOccupancyAnalysis OMAOccupancyAnalysis.h
 *
 *  @author Jean-Francois Marchand
 *  @date   2015-03-12
 */
class OMAOccupancyAnalysis : public AnalysisTask {
 public:

  /// Standard constructor
  OMAOccupancyAnalysis( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~OMAOccupancyAnalysis( ); ///< Destructor
  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode analyze(std::string& SaveSet, std::string Task) override; ///< Algorithm analyze
  StatusCode finalize() override;    ///< Algorithm finalization
  StatusCode execute() override;

 protected:

 private:

  bool iniBook();

  DeCalorimeter* m_calo;

  //names
  std::string m_caloregion;
  std::string m_detectorName;
  std::string m_LEDHistName;
  std::string m_OccHistInnerName;
  std::string m_OccHistMiddleName;
  std::string m_OccHistOuterName;
  std::string m_cellHistName;

  StatusCode init();

  bool m_first;
  long m_count;
  //Long trending done online only if the monitoring service's om
  bool m_monsvc;
  IGauchoMonitorSvc* m_pGauchoMonitorSvc; ///< Online Gaucho Monitoring Service

  TH2D* m_led2d;
  TH2D* m_ledError2d;
  TH2D* m_occ2d;
  TH2D* m_occError2d;
  TH2D* m_ratioOcc;
  TH2D* m_ratioOccError;
  TH2D* m_ratioLED;
  TH2D* m_ratioLEDError;
  TH2D* m_occLED;
  TH2D* m_cellsRef;
  TH2D* m_cellsRefAgeing;
  TH1D* m_ratioHVRef;
  TH1D* m_ratioHVRefLog;

  TFile* m_finput;

  std::string m_tfile;
  std::string m_tfileOcc;
  std::string m_tfileLED;

  std::vector< std::vector<float> > m_trendDataOccupancy;

  bool m_build;
  int m_det;

  std::string toUpper(std::string str){
    std::string uStr( str );
    std::transform( str.begin() , str.end() , uStr.begin () , ::toupper ) ;
    return uStr;
  }

  void MakeTrendingHistos(std::vector<float> occupancies, std::string m_timeStamp, bool isLEDFile);
  OnlineHistogram* OnlineHistogramOnPage(OnlineHistPage* page, std::string hName,
                                         double x0=0.,double y0=0.,double x1=1.,double y1=1.,
                                         std::string page2display="");

  bool m_storeInSaveSet;
  std::string m_sDirPath;
  std::string m_sDir;
  std::string m_anaTask;
  std::string m_timeStamp;
  TFile* m_tf2;
  void storeAll(TFile* tf);
  void store( TFile* tf, AIDA::IHistogram1D* h1 );
  void store( TFile* tf, AIDA::IHistogram2D* h2 );
  std::string tree( std::string name);

  bool m_updateHVDb;
  bool m_occBased;
  std::string m_hvdbTime;
  ILHCbMagnetSvc* m_magfieldsvc ;

  std::vector<unsigned int>  m_maskedCells;
  bool m_readDBnow;
};
#endif // OMAOCCUPANCYANALYSIS_H
