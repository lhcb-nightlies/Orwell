// Include files 
#include <string>

// from Gaudi
#include "GaudiKernel/AlgFactory.h"
#include <string>
#include "GaudiAlg/Fill.h"
#include "OnlineHistDB/OnlineHistDB.h"
#include "OnlineHistDB/OnlineHistogram.h"
// local
#include "OMAPi0Analysis.h"

//-----------------------------------------------------------------------------
// Implementation file for class : OMAPi0Analysis
//
// 2011-09-20 : Benoit VIAUD 
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( OMAPi0Analysis )

// Assume a gaussian signal peak
Double_t pi0Gauss(Double_t* x, Double_t* par){
  if( par[1] < 0) par[1]=-par[1];
  return TMath::Gaus(x[0],par[0],par[1]);
}

// Background function: pol 2nd order.
Double_t background(Double_t* x, Double_t* par) {
  return par[0] + par[1]*x[0]+par[2]*x[0]*x[0];
}

// Sum of background and peak function
Double_t fitFunction(Double_t* x, Double_t* par) {
    
  // The method below does not work. Try later to pass the m_* via *par[]
  double Pi0_dn=par[7]; 
  double Pi0_up=par[8];
  double Pi0_Dbin=(Pi0_up-Pi0_dn)/par[9];
  TF1 backFcnAux("backFcnAux", background, Pi0_dn, Pi0_up, 3);
  TF1 signalFcnAux("signalFcnAux", pi0Gauss, Pi0_dn, Pi0_up, 2);
  backFcnAux.SetParameters(par);
  signalFcnAux.SetParameters(&par[3]);
  double integralSig=signalFcnAux.Integral(Pi0_dn, Pi0_up);
  double integralBkg=backFcnAux.Integral(Pi0_dn, Pi0_up);
  
  return  Pi0_Dbin*par[6]*background(x,par)/integralBkg  + Pi0_Dbin*par[5]*pi0Gauss(x,&par[3])/integralSig;

}



//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
OMAPi0Analysis::OMAPi0Analysis( const std::string& name,
                                      ISvcLocator* pSvcLocator)
  : AnalysisTask ( name , pSvcLocator )
    ,m_monsvc(false)
    ,m_count( 1 ){
  declareProperty("TrendPeriod"     ,  m_trending = 168); // # consecutive Savesets (def. 168h with gran = 6 & 10mn/saveset)
  declareProperty("xLabelsOptions"  , m_lab ="" );           // Summary Histo label options (""/"v")  
  declareProperty("minEventFit"     , m_minEventFit =5000 ); // Minimum number of event to perform the fit
  declareProperty("maxErr"          , m_maxErr = 20 ); // Maximum fit error on the Pi0 mass (larger not included in plots )
  declareProperty("SamplingGran"    , m_splgran=6);// Sampling granularity. m_splgran=10 means 10 saversets are merged.
  declareProperty("LongTrending"    , m_trend=false);
  declareProperty("LongTrendingFile",m_tfile="LHCb_Pi0Ana_");
  declareProperty("OffLineTrend"    , m_OffLineTrend="");
  declareProperty("CaloRegion"      ,  m_caloregion = "Whole");
  declareProperty("FitRange_dn"     , m_FitRange_dn=75.);  
  declareProperty("FitRange_up"     , m_FitRange_up=195.);  
  declareProperty("HistToFit"       ,  m_histPi0Name = "CaloMoniDst/ResolvedPi0Mon/4");// Tell which mass distrib must be fitter
                                                                                  // All Pi0 (default) or those in 
                                                                                  // Inner/Middle/Outer
  debug() << " properties declared " << endmsg;
  m_name = name;
  m_gran = m_splgran;
  // check MonitorSvc is at work
  m_monsvc  = pSvcLocator->existsService("MonitorSvc");
  if(m_monsvc)info() << "MonitoringSvc is at work" << endmsg;	

  debug() << "Service Locator " << pSvcLocator << endmsg;
  
}
//=============================================================================
// Destructor
//=============================================================================
OMAPi0Analysis::~OMAPi0Analysis() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode OMAPi0Analysis::initialize() {
 
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  // for offline usage over N savesets (N < gran)
  if( m_inputFiles.size() !=0 &&  m_inputFiles.size() < m_splgran)m_splgran=m_inputFiles.size();
  m_gran = m_splgran;
  
  // fit function :
  m_fitFcn = TF1("fitFcn",fitFunction,m_FitRange_dn,m_FitRange_up,10);
  m_fitFcn.SetNpx(500);
  // "reasonable" starting point
  m_fitBkg_p0=0.;
  m_fitBkg_p1=1.;
  m_fitBkg_p2=-1.;
  m_pi0MassPDG=135.;
  m_pi0MassSig=10.;
  m_nSig=1000.;
  m_nBkg=30000.;
  m_fitFcn.SetParName(0,"p0_{Bkg}");
  m_fitFcn.SetParName(1,"p1_{Bkg}");
  m_fitFcn.SetParName(2,"p2_{Bkg}");
  m_fitFcn.SetParName(3,"#m_{pi}");
  m_fitFcn.SetParName(4,"#sigma_{pi}");
  m_fitFcn.SetParName(5,"N_{sig}");
  m_fitFcn.SetParName(6,"N_{Bkg}");
  m_fitFcn.SetParName(7,"lowHistoEdge");
  m_fitFcn.SetParName(8,"highHistoEdge");
  m_fitFcn.SetParName(9,"NHistoBins");


  //Pi0 histogram and data vector
  m_trendDataMass.clear();
  m_trendDataMassErr.clear();
  m_trendDataMassRel.clear();
  m_trendDataMassRelErr.clear();
  m_trendDataSigma.clear();
  m_trendDataSigmaErr.clear();
  m_trendDataNsig.clear();
  m_trendDataNsigErr.clear();
  m_trendDataNbkg.clear();
  m_trendDataNbkgErr.clear();
  m_trendDataSoB.clear();
  m_trendDataSoBErr.clear();
  m_trendDataChi2.clear();
  m_trendDataChi2Err.clear();
  m_trendDataEnt.clear();
  m_trendDataEntErr.clear();
  
  //Number of passes...
  m_count = 1;

  // Check MonitorSvc is available 
  if( m_monsvc )
    if( serviceLocator()->service("MonitorSvc", m_pGauchoMonitorSvc, false).isFailure())
      return Error("Cannot load MonitorSvc",StatusCode::FAILURE);


  //Fitted histogram default configuration
  m_nbbins   = 100;
  m_lowedge  = 0.;
  m_highedge = 250.;
  
  //initialize nb of events processed
  m_Entries = 0 ;

  //Fit results
  m_fitPi0_chi2=0.;
  m_fitPi0_M  = 135.;
  m_fitPi0_MErr  = 0. ;
  m_fitPi0_Sig=0.;
  m_fitPi0_SigErr=0.;
  m_nSigErr=0.;
  m_nBkgErr=0.;
  
  // Hardcoded for a test asked by Irina (May 5th 2011). 
  // Will do it clean if this kind of comparison becomes permanent
  m_refMass=135.00;
  m_refMassErr=0.8; 

  /// book & register (global summaries, trending, ...)
  iniBook();      
  // Same for the Long Trending tool               
  m_trendTool =  tool<ITrendingTool>("TrendingTool","TrendingTool",this);  
  if(m_trend && (m_monsvc || m_OffLineTrend != "") ) openTrendFile();

  // must be executed last, error printed already by AnalysisTask
  return  AnalysisTask::initialize();
  //  if ( (AnalysisTask::initialize()).isFailure() ) return StatusCode::FAILURE;
  //return StatusCode::SUCCESS;
}


//=============================================================================     
StatusCode OMAPi0Analysis::execute(){
  // update gaucho at each pass (each saveset)
  if ( m_monsvc ){
    if (msgLevel( MSG::DEBUG))debug() << "Update All" << endmsg;
    m_pGauchoMonitorSvc->updateAll(false); 
  }  
  return StatusCode::SUCCESS;
}


//=============================================================================
// Main execution
//=============================================================================
StatusCode OMAPi0Analysis::analyze(std::string& SaveSet, std::string Task) {

  
  //////// Preparation to real work
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Analyze" << endmsg;
  // Mandatory: call default analyze method
  StatusCode sc = AnalysisTask::analyze(SaveSet, Task);  
  if( m_savesetName == "" && m_taskname == "")return Warning("No saveset nor task to analyze",StatusCode::SUCCESS);
 
  // Calculate the time stamp for trending histograms. Derived from the input file name.
  // Here it will just be the run number if one runs on the savesets per run. 
  int i1 = m_savesetName.find_last_of("/") +1 ; 
  std::string file = m_savesetName.substr(i1,std::string::npos);
  int i2 = file.find_first_of("-") ; 
  int i3 = file.find_last_of(".");
  if( i2 == i3 || i2 == (int) std::string::npos || i3 == (int) std::string::npos)m_timeStamp = "00000000T000000";
  else m_timeStamp = file.substr(i2,i3-i2);

  // Open the SaveSet
  info() << "Opening root file " << SaveSet << endmsg;
  m_finput = new TFile(SaveSet.c_str(),"READ");  
  if ( ! m_finput->IsOpen()){
    error() << " Unable to open input file : " << SaveSet  << endmsg;
    return StatusCode::SUCCESS;
  }

  // Extract/merge the histos we'll fit
  info() <<" Extracting/Merging  histo : " <<  m_histPi0Name << " from Saveset "<< m_count << endmsg;    
  TH1D* auxh= (TH1D*) m_finput->Get( m_histPi0Name.c_str() );
  if( 0 == auxh){ error() << "HISTO "<< m_histPi0Name <<" NOT FOUND" <<endmsg; return StatusCode::FAILURE; }   

  info() << "   -  found " << auxh->GetSum() << " entries to be added in the local merged histo " << endmsg;  
  if( m_count == 1 && auxh -> GetSum() == 0)return StatusCode::SUCCESS; // have to start with a non-empty histo
  if(m_count == 1){//Build the Histo used for the fit, can be done by merging a group of runs/savesets. 
    info() << "  - Creating the local histo and start merging" << endmsg;
    m_nbbins=auxh->GetNbinsX();
    m_lowedge=auxh->GetBinLowEdge(1);
    m_highedge=auxh->GetBinLowEdge(1)+auxh->GetNbinsX()*auxh->GetBinWidth(1);
    m_mPi0.SetBins(m_nbbins,m_lowedge,m_highedge);
    m_mPi0.Sumw2();
    info() << "       - " << m_nbbins << " bins in ["<<m_lowedge<<","<<m_highedge<<"] range"<<endmsg;
  }
  // merging histo
  m_mPi0.Add(&m_mPi0,auxh,+1.,+1.);
  info() << "Adding savesets " << m_count << "/" << m_gran << "  => Histo statistics : " << m_mPi0.GetSum() << endmsg;
  
  if( m_count%m_gran==0 ){
    if(m_mPi0.GetSum() < m_minEventFit){
      m_gran++; // increase the granularity for next saveset
      warning() << "Not enough statistics ("<<m_mPi0.GetSum()<<") to perform the fit - wait for another saveset" << endmsg;
    }else{
      bool fitStatus = Pi0MassFit();
      if( fitStatus ){   
        // Fill the Trending histos
        info() << "Produce trending plots" << endmsg;
        sc = PlotTrending();
        // reset histogram
        m_mPi0.Reset();
        m_gran=m_splgran; 
      }else{
        m_gran++; // increase the granularity for next saveset
        warning() << "Fit failed - wait for more statistics from another saveset" << endmsg;
      }
    } 
  }

  m_count++;


  //close input file
  m_finput->Close("R");
  delete m_finput;  
  m_finput=NULL;
  // analyzing a Saveset should be seen as an 'event' for MonitorSvc
  execute().ignore();
  return sc;
}

//=============================================================================
// Finalization
//=============================================================================
StatusCode OMAPi0Analysis::finalize() {
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;
  //finalize 2Dviews
  //A good place to delete some histograms. 
  // Nothing to do for the moment. 
  if( trend() )trendTool()->closeFile();
  AnalysisTask::finalize();
  return StatusCode::SUCCESS;
}

//=============================================================================
// Trending Plots
//=============================================================================
StatusCode OMAPi0Analysis::PlotTrending() {
  info() << "Publish the results in a Trending histogram" << endmsg;
  info() << "@@@Pi0Ana ######################################################" << endmsg;
  info() << "@@@Pi0Ana #                                                    #" << endmsg;
  info() << "@@@Pi0Ana #               SAVESET NUMBER  "           << m_count  << endmsg;
  info() << "@@@Pi0Ana #                                                    #" << endmsg;
  info() << "@@@Pi0Ana #         " << m_timeStamp  << endmsg;
  info() << "@@@Pi0Ana #                                                    #" << endmsg;
  info() << "@@@Pi0Ana #     Entries Pi0  = "  << m_Entries                   << endmsg;
  info() << "@@@Pi0Ana #                                                    #" << endmsg;
  info() << "@@@Pi0Ana #     Fit N Pi0 sig    = "   <<   m_nSig      << " +- " << m_nSigErr        << endmsg;
  info() << "@@@Pi0Ana #     Fit N Pi0 bkg    = "   <<   m_nBkg      << " +- " << m_nBkgErr        << endmsg;
  info() << "@@@Pi0Ana #     Fit N Pi0 M      = "   <<   m_fitPi0_M  << " +- " << m_fitPi0_MErr    << endmsg;
  info() << "@@@Pi0Ana #     Fit N Pi0 Sig    = "   <<   m_fitPi0_Sig<< " +- " << m_fitPi0_SigErr  << endmsg;
  info() << "@@@Pi0Ana #     Fit N Pi0 Chi2   = "   <<   m_fitPi0_chi2    << endmsg;
  info() << "@@@Pi0Ana #                                                    #" << endmsg;
  info() << "@@@Pi0Ana ######################################################" << endmsg;
  info() << "@@@Pi0AnaSum "<< m_count << " | " << m_timeStamp << " | " 
         <<   m_fitPi0_M  << " +- " << m_fitPi0_MErr  << " | "  
         <<   m_fitPi0_Sig<< " +- " << m_fitPi0_SigErr << endmsg;

  ///// Trending histograms

  if(m_fitPi0_MErr>m_maxErr) {
    info() << "WARNING: Not a reasonable fit result: do not plot that point " <<endmsg;
  }else{
    MakeTrendingHistos("hMass");
    MakeTrendingHistos("hMassRel");
    MakeTrendingHistos("hSigma");
    MakeTrendingHistos("hNsig");
    MakeTrendingHistos("hNbkg");
    MakeTrendingHistos("hSoB");
    MakeTrendingHistos("hChi2");
    MakeTrendingHistos("hEnt");
  }
  //END: Trending histograms
  
  return StatusCode::SUCCESS;
}

bool OMAPi0Analysis::iniBook(){

  if (msgLevel( MSG::DEBUG))debug() << "book summary histo" << endmsg;

  if( m_trending<0)return true;

  // trends booking

  AIDA::IHistogram1D* hMass = book1D("/TrendingMass", " Fitted mass of the Pi0 (trending)", 0., (double) m_trending, m_trending);
  Gaudi::Utils::Aida2ROOT::aida2root(hMass)->SetName( std::string(name()+"/TrendingMass").c_str() );

  AIDA::IHistogram1D* hMassRel = book1D("/TrendingMassRel", " Relative Pi0 mass variation wrt one ref (trending)", 0., (double) m_trending, m_trending);
  Gaudi::Utils::Aida2ROOT::aida2root(hMassRel)->SetName( std::string(name()+"/TrendingMassRel").c_str() );

  AIDA::IHistogram1D* hSigma = book1D("/TrendingSigma", " Fitted sigma of the Pi0 mass (trending)", 0., (double) m_trending, m_trending);
  Gaudi::Utils::Aida2ROOT::aida2root( hSigma )->SetName( std::string(name()+"/TrendingSigma").c_str() );

  AIDA::IHistogram1D* hNsig = book1D("/TrendingNsig", " Number of fitted pi0 (trending)", 0., (double) m_trending, m_trending);
  Gaudi::Utils::Aida2ROOT::aida2root( hNsig )->SetName( std::string(name()+"/TrendingNsig").c_str() );


  AIDA::IHistogram1D* hNbkg = book1D("/TrendingNbkg", " Fitted Number of bkg to the Pi0 (trending)", 0., (double) m_trending, m_trending);
  Gaudi::Utils::Aida2ROOT::aida2root( hNbkg )->SetName( std::string(name()+"/TrendingNbkg").c_str() );

  AIDA::IHistogram1D* hChi2 = book1D("/TrendingChi2", " Chi2 of the Pi0 mass fit (trending)", 0., (double) m_trending, m_trending);
  Gaudi::Utils::Aida2ROOT::aida2root( hChi2 )->SetName( std::string(name()+"/TrendingChi2").c_str() );


  AIDA::IHistogram1D* hSoB = book1D("/TrendingSob", " Fitted Signal over bkg ratio in the Pi0 mass distrib (trending)", 0., (double) m_trending, m_trending);
  Gaudi::Utils::Aida2ROOT::aida2root( hSoB )->SetName( std::string(name()+"/TrendingSob").c_str() );


  AIDA::IHistogram1D* hEnt = book1D("/TrendingEnt", " Total number of events in the Pi0 sample (trending)", 0., (double) m_trending, m_trending);
  Gaudi::Utils::Aida2ROOT::aida2root( hEnt )->SetName( std::string(name()+"/TrendingEnt").c_str() );

  m_trendPi0Mass= hMass ;
  m_trendPi0MassRel= hMassRel ;
  m_trendPi0Sigma= hSigma ;
  m_trendPi0Nsig= hNsig ;
  m_trendPi0Nbkg= hNbkg ;
  m_trendPi0Chi2= hChi2 ;
  m_trendPi0SoB= hSoB ;
  m_trendPi0Ent= hEnt ;

 
  return true;
}

bool OMAPi0Analysis:: Pi0MassFit(){

  //if (msgLevel( MSG::DEBUG))debug() << "In the fitter..." << endmsg;

  // Full histo
  m_Entries = m_mPi0.GetSum();
  info() << "Performing the fit on " << m_Entries << " entries" << endmsg;

  // "reasonable" starting point
  m_fitBkg_p0=0.;
  m_fitBkg_p1=1.;
  m_fitBkg_p2=-1.;
  m_pi0MassPDG=135.;
  m_pi0MassSig=10.;
  m_nSig=1000.;
  m_nBkg=30000.;

  m_fitFcn.SetParameter(0,m_fitBkg_p0);
  m_fitFcn.SetParameter(1,m_fitBkg_p1);
  m_fitFcn.SetParameter(2,m_fitBkg_p2);
  m_fitFcn.SetParameter(3,m_pi0MassPDG);
  m_fitFcn.SetParameter(4,m_pi0MassSig);
  m_fitFcn.SetParameter(5,m_nSig);
  m_fitFcn.SetParameter(6,m_nBkg);
  //Histo info for Normalization purpose. 
  m_fitFcn.SetParameter(7,m_lowedge);
  m_fitFcn.SetParameter(8,m_highedge);
  m_fitFcn.SetParameter(9,double(m_nbbins));
  m_fitFcn.FixParameter(7,m_lowedge);
  m_fitFcn.FixParameter(8,m_highedge);
  m_fitFcn.FixParameter(9,double(m_nbbins));

  // Help the fit by sticking to the zone where the
  // parameterization works

  info() << "  - fit the ["<<m_FitRange_dn<<","<<m_FitRange_up<<"] range"<<endmsg;

  //First pass
  m_fitFcn.FixParameter(3,m_pi0MassPDG);
  m_fitFcn.FixParameter(4,m_pi0MassSig);
  m_fitFcn.FixParameter(5,m_nSig);

  m_mPi0.Fit(&m_fitFcn,"S","",m_FitRange_dn,m_FitRange_up);
  m_fitFcn.ReleaseParameter(3);
  m_mPi0.Fit(&m_fitFcn,"S","",m_FitRange_dn,m_FitRange_up);
  m_fitFcn.ReleaseParameter(4);
  m_fitFcn.ReleaseParameter(5);
  m_fitFcn.SetParLimits(4,0.,999.);
  bool fitStatus = m_mPi0.Fit(&m_fitFcn,"S","",m_FitRange_dn,m_FitRange_up)->IsValid();

  //Collecting the results
  Double_t para[10];
  m_fitFcn.GetParameters(para);
  Double_t paraErr[7];
  paraErr[0]=m_fitFcn.GetParError(0);
  paraErr[1]=m_fitFcn.GetParError(1);
  paraErr[2]=m_fitFcn.GetParError(2);
  paraErr[3]=m_fitFcn.GetParError(3);
  paraErr[4]=m_fitFcn.GetParError(4);
  paraErr[5]=m_fitFcn.GetParError(5);
  paraErr[6]=m_fitFcn.GetParError(6);

  m_nBkg = para[6];
  m_nBkgErr = paraErr[6];
  m_nSig  = para[5];
  m_nSigErr  = paraErr[5];
  m_fitPi0_M=para[3];
  m_fitPi0_Sig=para[4];
  m_fitPi0_MErr=paraErr[3];
  m_fitPi0_SigErr=paraErr[4];

  //S over B and error propagation (neglect correlations )
  m_SoB=0;
  m_SoBErr=0;
  if(m_nBkg>0){
    m_SoB=m_nSig/m_nBkg;
    m_SoBErr=1./m_nBkg*sqrt(m_nSigErr*m_nSigErr+m_SoB*m_SoB*m_nBkgErr*m_nBkgErr);
  }
  m_fitPi0_chi2 = ( m_fitFcn.GetNDF() !=0 ) ? m_fitFcn.GetChisquare()/m_fitFcn.GetNDF() : 0;
  return fitStatus;
}



void OMAPi0Analysis::MakeTrendingHistos(std::string var){

  if (msgLevel( MSG::DEBUG))debug() << "Making the trending histograms" << endmsg;  

  //The men in the middle
  AIDA::IHistogram1D* h=NULL;
  double val=0;
  double valerr=0;
  std::vector<std::pair<std::string,double> > vec ;
  std::vector<std::pair<std::string,double> > vecErr;
  
  if(var=="hMass"){
    h=m_trendPi0Mass;
    val=m_fitPi0_M;
    valerr=m_fitPi0_MErr;

   //Long trending tool
    std::vector<float> MassValues;
    MassValues.push_back((float) val);
    MassValues.push_back((float) valerr);
    MassValues.push_back((float) m_fitPi0_Sig);
    MassValues.push_back((float) m_fitPi0_SigErr);
    if(m_trend && (m_monsvc || m_OffLineTrend !="") ) LongTrendTool( MassValues );

    //Preparation for the classical trending
    m_trendDataMass.push_back(std::make_pair( m_timeStamp,  val));
    m_trendDataMassErr.push_back(std::make_pair( m_timeStamp,  valerr));
    vec=m_trendDataMass;
    vecErr=m_trendDataMassErr;


  }else if(var=="hMassRel"){
    h=m_trendPi0MassRel;
    val=(m_fitPi0_M-m_refMass)/m_refMass;
    valerr=std::sqrt(m_fitPi0_M*m_fitPi0_M/m_refMass/m_refMass*m_refMassErr*m_refMassErr+m_fitPi0_MErr*m_fitPi0_MErr);
    valerr=valerr/m_refMass;
    m_trendDataMassRel.push_back(std::make_pair( m_timeStamp,  val));
    m_trendDataMassRelErr.push_back(std::make_pair( m_timeStamp,  valerr));
    vec=m_trendDataMassRel;
    vecErr=m_trendDataMassRelErr;
  }else if(var=="hSigma"){
    h=m_trendPi0Sigma;
    val=m_fitPi0_Sig;
    valerr=m_fitPi0_SigErr;
    m_trendDataSigma.push_back(std::make_pair( m_timeStamp,  val));
    m_trendDataSigmaErr.push_back(std::make_pair( m_timeStamp,  valerr));
    vec=m_trendDataSigma;
    vecErr=m_trendDataSigmaErr;

  }else if(var=="hNsig"){
    h=m_trendPi0Nsig;
    val=m_nSig;
    valerr=m_nSigErr;
    m_trendDataNsig.push_back(std::make_pair( m_timeStamp,  val));
    m_trendDataNsigErr.push_back(std::make_pair( m_timeStamp,  valerr));
    vec=m_trendDataNsig;
    vecErr=m_trendDataNsigErr;    
    
  }else if(var=="hNbkg"){
    h=m_trendPi0Nbkg;
    val=m_nBkg;
    valerr=m_nBkgErr;
    m_trendDataNbkg.push_back(std::make_pair( m_timeStamp,  val));
    m_trendDataNbkgErr.push_back(std::make_pair( m_timeStamp,  valerr));
    vec=m_trendDataNbkg;
    vecErr=m_trendDataNbkgErr;    
    
  }else if(var=="hSoB"){
    h=m_trendPi0SoB;
    if(m_SoB<=1){  
      val=m_SoB;
      valerr=m_SoBErr;
    }else{
      val=0;
      valerr=0;      
    }
    m_trendDataSoB.push_back(std::make_pair( m_timeStamp,  val));
    m_trendDataSoBErr.push_back(std::make_pair( m_timeStamp,  valerr));
    vec=m_trendDataSoB;
    vecErr=m_trendDataSoBErr;    
    
  }else if(var=="hChi2"){
    h=m_trendPi0Chi2;
    val=m_fitPi0_chi2;
    m_trendDataChi2.push_back(std::make_pair( m_timeStamp,  val));
    vec=m_trendDataChi2;
    vecErr=m_trendDataChi2;
    
  }else if(var=="hEnt"){
    h=m_trendPi0Ent;
    val=m_Entries;
    m_trendDataEnt.push_back(std::make_pair( m_timeStamp,  val));
    vec=m_trendDataEnt;
    vecErr=m_trendDataEnt;    
  }else{
    info() << "ERROR: The Trending Histo function were not called properly" << endmsg;  
  }  


  if( NULL != h ){
    

    TH1D* th = Gaudi::Utils::Aida2ROOT::aida2root( h );

    // check the vector

    if( (int) vec.size() > m_trending ){
      std::vector<std::pair<std::string,double> >::iterator it = vec.begin();
      std::vector<std::pair<std::string,double> >::iterator it2 = vecErr.begin();
      vec.erase( it );
      vecErr.erase( it2 );
    }
    
    // update the histos
    int mbin = m_trending-vec.size()+1;
    for(std::vector<std::pair<std::string,double> >::iterator it = vec.begin() ; vec.end() != it ; ++it){
      if(mbin<1 ){mbin++;continue;}
      std::string lab = (*it).first;
      double value = (*it).second;
      th->SetBinContent(mbin, value );
      if( "" != lab) th->GetXaxis()->SetBinLabel( mbin , lab.c_str() ); 
      mbin++;
    }
    mbin = m_trending-vec.size()+1;

    if(var!="hChi2" && var!="hEnt"){
      
      for(std::vector<std::pair<std::string,double> >::iterator it = vecErr.begin() ; vecErr.end() != it ; ++it){
        if(mbin<1 ){mbin++;continue;}
        std::string lab = (*it).first;
        double value = (*it).second;
        th->SetBinError(mbin, value );
        mbin++;
      }

    }
    
    th->GetXaxis()->LabelsOption( m_lab.c_str() );
  
  }
  


  return;
}

void OMAPi0Analysis::LongTrendTool(std::vector<float> val){

  trendTool()->write( val );

  return;
}

void OMAPi0Analysis::openTrendFile(){
  if( m_trend ){

    std::string tfile;
    tfile=m_tfile+m_caloregion;

    std::vector<std::string> tags;
    tags.push_back("mean");
    tags.push_back("meanErr");
    tags.push_back("sigma");
    tags.push_back("sigmaErr");
    if( trendTool()->openWrite( tfile, tags) )info() << "opening trending file " << tfile << endmsg;
    else{
      warning() << "cannot open trending file " << tfile << endmsg;
      m_trend = false;
    }
  }
}





//=============================================================================

