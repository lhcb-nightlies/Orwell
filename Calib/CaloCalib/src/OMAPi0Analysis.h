// $Id: OMAPi0Analysis.h,v 1.4 2009/11/03 12:37:13 odescham Exp $
#ifndef OMAPI0ANALYSIS_H
#define OMAPI0ANALYSIS_H 1

// Include files

#include "GaudiKernel/DeclareFactoryEntries.h"
#include "OMAlib/AnalysisTask.h"
#include "Gaucho/IGauchoMonitorSvc.h"
#include "AIDA/IHistogram1D.h"
#include "Trending/ITrendingTool.h"
#include "GaudiUtils/Aida2ROOT.h"

// ROOT includes
#include "TFitResult.h"
#include <TFile.h>
#include <TH1.h>
#include <TF1.h>
#include <TMath.h>
#include <TSystem.h>
#include <string>
#include <vector>


/** @class OMAPi0Analysis OMAPi0Analysis.h
 *
 *
 *  @author Aurelien Martens
 *  @date   2009-04-11
 */
class OMAPi0Analysis : public AnalysisTask {
 public:

  /// Standard constructor
  OMAPi0Analysis( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~OMAPi0Analysis( ); ///< Destructor
  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode analyze(std::string& SaveSet, std::string Task) override; ///< Algorithm analyze
  StatusCode finalize() override;    ///< Algorithm finalization
  StatusCode execute() override;

  // Fit and Trending
  StatusCode PlotTrending();

  //Long Trending
  bool trend(){return m_trend;}
  void openTrendFile();
  void LongTrendTool(std::vector<float> val);
  ITrendingTool* trendTool(){return m_trendTool;}

protected:

private:

  bool iniBook();
  bool Pi0MassFit();
  void MakeTrendingHistos(std::string var);


  //names
  std::string m_caloregion;
  std::string m_histPi0Name;
  std::string m_name;
  std::string m_timeStamp;
  std::string m_lab;


  TH1D m_mPi0 ;
  TF1  m_fitFcn;

  //Long trending done online only if the monitoring service's om
  bool m_monsvc;
  IGauchoMonitorSvc* m_pGauchoMonitorSvc; ///< Online Gaucho Monitoring Service
  long m_count;


  std::vector<std::pair<std::string,double> > m_trendDataMass;
  std::vector<std::pair<std::string,double> > m_trendDataMassErr;
  std::vector<std::pair<std::string,double> > m_trendDataMassRel;
  std::vector<std::pair<std::string,double> > m_trendDataMassRelErr;
  std::vector<std::pair<std::string,double> > m_trendDataSigma;
  std::vector<std::pair<std::string,double> > m_trendDataSigmaErr;
  std::vector<std::pair<std::string,double> > m_trendDataNsig;
  std::vector<std::pair<std::string,double> > m_trendDataNsigErr;
  std::vector<std::pair<std::string,double> > m_trendDataNbkg;
  std::vector<std::pair<std::string,double> > m_trendDataNbkgErr;
  std::vector<std::pair<std::string,double> > m_trendDataSoB;
  std::vector<std::pair<std::string,double> > m_trendDataSoBErr;
  std::vector<std::pair<std::string,double> > m_trendDataChi2;
  std::vector<std::pair<std::string,double> > m_trendDataChi2Err;
  std::vector<std::pair<std::string,double> > m_trendDataEnt;
  std::vector<std::pair<std::string,double> > m_trendDataEntErr;


  AIDA::IHistogram1D* m_trendPi0Mass;
  AIDA::IHistogram1D* m_trendPi0MassRel;
  AIDA::IHistogram1D* m_trendPi0Sigma;
  AIDA::IHistogram1D* m_trendPi0Nsig;
  AIDA::IHistogram1D* m_trendPi0Nbkg;
  AIDA::IHistogram1D* m_trendPi0SoB;
  AIDA::IHistogram1D* m_trendPi0Chi2;
  AIDA::IHistogram1D* m_trendPi0Ent;

  int  m_minEventFit;
  double  m_maxErr;

  TFile* m_finput;

  int m_trending;

  //Fit variables
  double m_FitRange_dn;
  double m_FitRange_up;

  int m_nbbins;
  double m_lowedge;
  double m_highedge;

  int m_Entries;


  double m_fitBkg_p0;
  double m_fitBkg_p1;
  double m_fitBkg_p2;
  double m_pi0MassPDG;
  double m_pi0MassSig;

  //Fitted parameters
  double m_nSig;
  double m_nBkg;
  double m_nSigErr;
  double m_nBkgErr;
  double m_SoB;
  double m_SoBErr;
  double m_fitPi0_M;
  double m_fitPi0_MErr;
  double m_fitPi0_Sig;
  double m_fitPi0_SigErr;
  double m_fitPi0_chi2;

  //Other
  double m_refMass;
  double m_refMassErr;
  unsigned int m_splgran;

  //Long Trending
  bool m_trend;
  ITrendingTool* m_trendTool;
  std::string m_tfile;


  //For Offline studies/development
  std::string m_OffLineTrend;

  //
  unsigned int m_gran;
  };
#endif // OMAPI0ANALYSIS_H
