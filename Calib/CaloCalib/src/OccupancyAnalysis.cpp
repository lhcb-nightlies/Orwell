// Include files
// STD and STL
#include <algorithm>
#include <iostream>
#include <fstream>
#include <chrono>
#include <memory>
#include <thread>
#include <chrono>
#include <vector>
#include <string>
#include <mutex>
#include <tuple>
#include <map>
#include <set>
#include <time.h>

// Boost Regex, cast and FileSystem
#include <boost/optional/optional_io.hpp>
#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/optional.hpp>
#include <boost/filesystem.hpp>

// Boost Qi, Spirit and Phoenix
//#define FUSION_MAX_VECTOR_SIZE 20
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_object.hpp>
#include <boost/spirit/include/support_istream_iterator.hpp>
#include <boost/fusion/adapted.hpp>

// from Gaudi
#include <GaudiKernel/AlgFactory.h>
#include <DetDesc/Condition.h>
#include <DetDesc/RunChangeIncident.h>

// OT detector element
#include <OTDet/DeOTDetector.h>

#include <GaudiKernel/MsgStream.h>
#include <GaudiKernel/ServiceLocatorHelper.h>

// local
#include "OccupancyAnalysis.h"
//-----------------------------------------------------------------------------
// Implementation file for class : OccupancyAnalysis
//
// 2015-05-05 : Jean-Francois Marchand
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( OccupancyAnalysis )

namespace {
   namespace fs = boost::filesystem;
   using namespace LHCb;
   using std::unique_ptr;
   using std::endl;
   using std::cout;
   using std::vector;
   using std::string;
   using std::ofstream;
   using std::to_string;
   using std::pair;
   using std::make_pair;
   using std::tuple;
   using std::make_tuple;

   const std::string EOR = "EOR";
}

namespace sp = boost::spirit;
namespace qi = sp::qi;
namespace ph = boost::phoenix;

//=============================================================================
OccupancyAnalysis::OccupancyAnalysis( const string& name, ISvcLocator* pSvcLocator)
  : AnalysisTask ( name , pSvcLocator )
  ,m_first(true)
  ,m_run{0},
  m_incidentSvc{nullptr},
  m_lhcState{nullptr},
  m_fillNumber{nullptr},
  m_sDirPath("")
{
  declareProperty("XMLFilePath", m_xmlFilePath = "/group/calo/OccupancyAnalysis/xml/" );
  declareProperty("epsFilePath", m_epsFilePath = "/group/calo/OccupancyAnalysis/eps/" );
  declareProperty("InitialTime", m_initialTime  = 0);
  declareProperty("RunOnline",   m_RunOnline  = true);
  declareProperty("SaveSetDir",  m_sDir = "/hist/Savesets" ); // Store Output histos in a dedicated SaveSet
  declareProperty("DoRunByRun",  m_doRunByRun = false); // for testing purpose only...
  declareProperty("CheckLumi",   m_checkLumi = true);
  declareProperty("LEDDuration", m_ledDuration = 75);
  m_anaTask = "OccupancyAnalysis";
  m_SSetService = 0;
}

//=============================================================================
OccupancyAnalysis::~OccupancyAnalysis()
{

}

//=============================================================================
StatusCode OccupancyAnalysis::initialize()
{
   // Init things for this task
   // must be run here
   if (!fs::exists(m_xmlFilePath)) {
      fs::create_directories(m_xmlFilePath);
   }
   if (!fs::exists(m_epsFilePath)) {
      fs::create_directories(m_epsFilePath);
   }

   StatusCode sc = AnalysisTask::initialize(); // must be executed first
   if ( sc.isFailure() ) return sc;  // error printed already by AnalysisTask

   if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

   // IncidentSvc
   updMgrSvc();
   m_incidentSvc = service("IncidentSvc");
   if (!m_incidentSvc) {
      error() << "Failed to retrieve IndicentSvc" << endmsg;
      return StatusCode::FAILURE;
   }

   // Values from DIM
   m_lhcState   = unique_ptr<LHCState>{new LHCState{}};
   m_fillNumber = unique_ptr<FillNumber>{new FillNumber{}};

   // Regex and lambda to extract version from filename vN.xml
   boost::regex re("^v([0-9]+)\\.xml$");
   auto version = [re, this](const string& filename) {
     boost::smatch matches;
     auto flags = boost::match_default;
     if (!fs::is_regular_file(fs::path{m_xmlFilePath} / fs::path{filename}))
       return 0u;
     boost::regex_match(begin(filename), end(filename), matches, re, flags);
     if (!matches.empty()) {
       return boost::lexical_cast<unsigned int>(matches[1]);
     } else {
       return 0u;
     }
   };

   // Store versions
   std::transform(fs::directory_iterator{fs::path{m_xmlFilePath}},
                  fs::directory_iterator{}, std::inserter(m_versions, end(m_versions)),
                  [version](const fs::directory_entry& e) {
                     return version(e.path().filename().string());
                  });

   // Read XML to get previous numbers.
   for (auto v : m_versions) {
      auto r = readXML(xmlFileName(v));

      const auto lhcState = std::get<1>(r);
      if (lhcState)
  if ( msgLevel(MSG::DEBUG) ) debug() << "Init... lhcState= " << *lhcState << endmsg;
      const auto fillNumber = std::get<2>(r);
      if (fillNumber)
  if ( msgLevel(MSG::DEBUG) ) debug() << "Init... fillNumber= " << *fillNumber << endmsg;
   }
   if (m_SSetService == 0)
   {
//  	 char *utgid = getenv("UTGID");
//  	 if (utgid == 0)
//  	 {
//  		 utgid = "UTGIDnotDefined";
//  	 }
    m_SSetLoc = "...........";
    m_SSetService = new DimService("LHCb/OccupancyAnalysis/SAVESETLOCATION",(char*)m_SSetLoc.c_str());
   }
   if ( msgLevel(MSG::DEBUG) ) debug() << "==> End of initialize" << endmsg;
   return sc;
}

// first pass initialization ========
StatusCode OccupancyAnalysis::init(){

  m_first = false;

  info() << "initialization successful" << endmsg;
  return StatusCode::SUCCESS;
}

//=============================================================================
StatusCode OccupancyAnalysis::start()
{
  // The thread is started here to ensure that everything is ready when we need it.
  m_thread = std::thread([this]{
        while (true) {
          auto r = m_queue.get();
          debug() << "---> in start(), r.first= " << r.first << endmsg;
          if (r.first == "stop") {
            debug() << "---> in start(), break" << endmsg;
            continue;//			       break;
          } else {
            debug() << "---> in start(), mergeFiles()" << endmsg;
            mergeFiles(r.first, r.second);
          }
        }
      });
  return StatusCode::SUCCESS;
}

//=============================================================================
StatusCode OccupancyAnalysis::execute()
{
  if ( msgLevel(MSG::DEBUG) ) info() << "==> Execute" << endmsg;

  if (m_run) {
    // Fire RunChange incident to load XML used in data.
    m_incidentSvc->fireIncident(RunChangeIncident(name(), m_run));
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
StatusCode OccupancyAnalysis::analyze (string& saveSet, string task)
{

  // first pass init
  //if(m_first)
  //if(init().isFailure() )return Error("Analysis failed",StatusCode::SUCCESS);

  debug() << "---> analyse: " << saveSet << endmsg;

  //AnalysisTask::analyze( saveSet, task ); // init Saveset & Task naming
  //if( m_savesetName == "" && m_taskname == "")return Warning("No saveset nor task to analyze",StatusCode::SUCCESS);

  // output saveset path :
  char year[5];
  char month[3];
  char day[3];
  time_t rawTime=time(NULL);
  struct tm* timeInfo = localtime(&rawTime);
  ::strftime(year, sizeof(year),"%Y", timeInfo);
  ::strftime(month, sizeof(month),"%m", timeInfo);
  ::strftime(day, sizeof(day),"%d", timeInfo);
  m_sDirPath = m_sDir  + "/" + year + "/" + m_partition + "/" + m_anaTask + "/" + month + "/" + day;

  // We do the work on a thread so the DIM thread is not blocked if we want to
  // to wait for start to have completed.
  m_queue.put(make_pair(saveSet, task));
  return StatusCode::SUCCESS;
}

//=============================================================================
StatusCode OccupancyAnalysis::mergeFiles(string saveSet, string task)
{
   if ( msgLevel(MSG::DEBUG) ) debug() << "==> Analyze" << endmsg;

   // Mandatory: call default analyze method
   StatusCode sc = AnalysisTask::analyze(saveSet, task);
   if ( sc.isFailure() ) return sc;

   // Get latest version, bootstrap if needed
   //const FileVersion latest = latestVersion();
   boost::optional<unsigned int> latest = latestVersion();

   boost::optional<std::string> prevlhcState;
   boost::optional<int> prevfillNumber;

   //if (latest == 0) {
   if (!latest) {
     latest = 0;

     boost::optional<std::string> lhcState;
     boost::optional<int> fillNumber;
     //writeXML(latest, latest, lhcState, fillNumber);
     writeXML(*latest, *latest, lhcState, fillNumber);
   } else {
     verbose() << "--------> latest= " << latest << endmsg;
     // Parse the file to get the info from the previous calibration.
     //std::tie(m_run, prevlhcState, prevfillNumber) = readXML(xmlFileName(latest));
     std::tie(m_run, prevlhcState, prevfillNumber) = readXML(xmlFileName(*latest));
   }

   long int timeFile = 0;

   // Figure out which files we need.
   if(m_RunOnline){
      const string fileName{fs::path{saveSet}.filename().string()};
      boost::regex re(string("CaloDAQOccupancy") + "-(?<run>\\d+)-(?<time>[\\dT]+)(-EOR)?\\.root$");
      boost::smatch matches;
      auto flags = boost::match_default;
      info() << "Filename: " << saveSet << endmsg;
      if (!boost::regex_match(begin(fileName), end(fileName), matches, re, flags)) {
  error() << "Bad SaveSet filename: " << saveSet << endmsg;
  return StatusCode::FAILURE;
      } else {
  // Get run number
  m_run = boost::lexical_cast<unsigned int>(matches.str("run"));
  info() << "Filename: " << saveSet << ", run: " << m_run << endmsg;
      }

      // Parse timestamp
      std::tm tm;
      bzero(&tm, sizeof(std::tm));
      strptime(matches.str("time").c_str(), "%Y%m%dT%H%M%S", &tm);
      timeFile = std::mktime(&tm);
   }

   boost::optional<std::string> LHCstate = std::string(*m_lhcState->lhcState());
   boost::optional<int> LHCbFill = m_fillNumber->fillNumber();

   if ( msgLevel(MSG::DEBUG) ) {
     debug() << "==> In mergeFiles()... Current LHCstate: " << *LHCstate << endmsg;
     debug() << "==> In mergeFiles()... Current LHCbFill: " << *LHCbFill << endmsg;
     if (prevlhcState) debug() << "==> In mergeFiles()... Previous LHCstate: " << *prevlhcState << endmsg;
     if (prevfillNumber) debug() << "==> In mergeFiles()... Previous LHCbFill: " << *prevfillNumber << endmsg;
   }

   std::string LHCstate_fail("DIM_FAIL");
   std::string LHCstate_dump("DUMP");
   std::string LHCstate_physics("PHYSICS");
   //std::string LHCstate_prev = *prevlhcState;
   std::string LHCstate_prev("");
   if (prevlhcState)  LHCstate_prev = *prevlhcState;

   int LHCbFill_fail = -1;
   //int LHCbFill_prev = *prevfillNumber;
   int LHCbFill_prev = 0;
   if (prevfillNumber) LHCbFill_prev = *prevfillNumber;

   UInt_t time_SOF = 0; // time of start of fill
   UInt_t time_EOF = 0; // time of   end of fill
   int ifill = 0;       // last fill number

   std::string currentLHCstate = *LHCstate;

   if (m_doRunByRun) {
     verbose() << "Run by run..." << endmsg;
     // First check if end of run...
     // then do everything...
     std::string str2 ("EOR");
     std::size_t found = saveSet.find(str2);
     if (found!=std::string::npos) {
       Do_EOF(ifill,timeFile-3600, timeFile+3600, m_run);
     }
   } else {
     verbose() << "Fill by fill..." << endmsg;

     if ( currentLHCstate.length()>0 && currentLHCstate.compare(LHCstate_fail)!=0 &&
	  LHCbFill!=0 && *LHCbFill!=LHCbFill_fail ) {
       verbose() << "Fill by fill... 0 " << endmsg;
       
       if ( currentLHCstate.compare(LHCstate_prev)!=0 || *LHCbFill!=LHCbFill_prev ) {
	 verbose() << "Fill by fill... 1" << endmsg;
	 
	 ifill = *LHCbFill;
	 
	 verbose() << "Fill by fill... 2" << endmsg;
	 
	 if (ifill==LHCbFill_prev)
	   std::cout << "State detected: --> " << currentLHCstate << " fill " << ifill << std::endl;
	 else
	   if (prevlhcState) std::cout << "State change: " << *prevlhcState << " fill " << *prevfillNumber
				       << " --> " << currentLHCstate << " fill " << ifill << std::endl;
	 
	 std::size_t found = LHCstate_prev.find(LHCstate_physics);
	 
	 if ( found!=string::npos ){
	   std::cout << "EOR action" << std::endl;
	   //get_LPC_info(ifill, time_SOF, time_EOF);
	   //Do_EOF(ifill,time_SOF, time_EOF, m_run);
	   get_LPC_info(*prevfillNumber, time_SOF, time_EOF);
	   Do_EOF(*prevfillNumber,time_SOF, time_EOF, m_run);
	 }
       }
     }
   }

   verbose() << "Will write new xml file..." << endmsg;
   //writeXML(*latest + 1, m_run, LHCstate, LHCbFill);
   writeXML(*latest, m_run, LHCstate, LHCbFill);
   //writeXML( (*latest + 1 > 10 ) ? 0 : *latest + 1, m_run, LHCstate, LHCbFill);
   verbose() << "New xml file written..." << endmsg;

   return StatusCode::SUCCESS;
}

//=============================================================================
fs::path OccupancyAnalysis::xmlFileName(FileVersion v) const
{
  if (v==10) v=0;
  //verbose() << "---> FileVersion = " << *v << endmsg;
  return fs::path(m_xmlFilePath) / (string("v") + to_string(v) + ".xml");
}

//=============================================================================
fs::path OccupancyAnalysis::writeXML(const FileVersion version,
            const unsigned int run,
            const boost::optional<std::string> lhcState,
            const boost::optional<int> fillNumber)
{

  StatusCode sc = StatusCode::SUCCESS ;

  fs::path filename = xmlFileName(version);
  ofstream file(filename.string().c_str());

  info() << "Writing the global t0 XML for online to " << filename.string() << endmsg ;

  file << "<!-- run: " << run << " -->" << endl;
  if (lhcState) file << "<!-- lhcState: " << *lhcState << " -->" << endl;
  if (fillNumber) file << "<!-- fillNumber: " << *fillNumber << " -->" << endl;
  file.close();

  // Insert the version we wrote.
  m_versions.insert(version);
  return filename;
}

//=============================================================================
StatusCode OccupancyAnalysis::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;
  // Stop worker thread
  m_queue.put(make_pair("stop", ""));
  m_thread.join();
  if (m_SSetService != 0)
  {
    delete m_SSetService;
  }
  return AnalysisTask::finalize();  // must be called after all other actions
}

//=============================================================================
//unsigned int OccupancyAnalysis::latestVersion() const
boost::optional<unsigned int> OccupancyAnalysis::latestVersion() const
{
  boost::optional<unsigned int> r;
  // Find highest version
  auto v = std::max_element(begin(m_versions), end(m_versions));
  if (v == end(m_versions)) {
    debug() << "Latest version: 0" << endmsg;
    //return 0;
  } else {
    //unsigned int r = *v;
    r = *v;
    debug() << "Latest version: " << r << endmsg;
    //return r;
  }
  return r;
}

//=============================================================================
tuple<unsigned int, boost::optional<std::string>, boost::optional<int> >
OccupancyAnalysis::readXML(const fs::path& xmlFile)
{
  using namespace qi;

  // open file, disable skipping of whitespace
  std::ifstream in(xmlFile.string().c_str());
  in.unsetf(std::ios::skipws);

  // wrap istream into iterator
  sp::istream_iterator file_begin(in);
  sp::istream_iterator file_end;

  unsigned int run = 0;
  boost::optional<std::string>   LHCstate;
  boost::optional<int>    LHCbFill;

  bool full = qi::phrase_parse(file_begin, file_end,
            "<!-- run:" >> uint_[ph::ref(run) = _1] >> sp::as_string[+(char_ - '>')]  >> '>'
            >> *(qi::lit("<!-- lhcState:") >> sp::as_string[*(char_-"-")][ph::ref(LHCstate) = _1] >>  char_("-") >> sp::as_string[+(char_ - '>')]  >> '>' )
            >> *(qi::lit("<!-- fillNumber:") >> int_[ph::ref(LHCbFill) = _1] >> sp::as_string[+(char_ - '>')]  >> '>'),
            qi::space);

  if (!full || file_begin != file_end) {
    warning() << "XML file " << xmlFile << " could not be parsed to obtain previous T0" << endmsg;
    return make_tuple(0, LHCstate, LHCbFill);
  }

  return make_tuple(run, LHCstate, LHCbFill);

}

//=============================================================================
void OccupancyAnalysis::Do_EOF(int fillNumber, UInt_t t1, UInt_t t2, uint runNumber){

  if ( msgLevel(MSG::DEBUG) ) debug() << "End-Of-Fill action for fill " << fillNumber << endmsg;

  if (fillNumber<=0 && !m_doRunByRun) return;

  std::vector< std::string > history_led;
  std::vector< std::string > history_occ;
  history_led.clear();
  history_occ.clear();


  //-- Get intervals of time according to luminosity
  timePeriod timeRange = findPeriod(fillNumber);


  ///***************** TEST *******************/
  //timePeriod timeRange;
  //std::vector<int> tStart, tStop;
  //tStart.clear();
  //tStop.clear();
  //tStart.push_back(1429920000);
  //tStop.push_back(1430726310);//1430400524);//1430214911);
  //timeRange.timeStart = tStart;
  //timeRange.timeStop = tStop;
  //if (!m_doRunByRun) {
  //  t1 = 1429920000;
  //  t2 = 1430726310;//1430214911;
  //}
  ///***************** TEST *******************/


  //-- Build list of LED files and list of Occ files passing luminosity criteria
  buildList(t1, t2, history_led, history_occ, timeRange, runNumber); // readout relevant savesets
  if ( msgLevel(MSG::DEBUG) ) debug() << history_led.size() << " " << history_occ.size() << endmsg;

  //-- Merging all the files in one file per fill
  if ( msgLevel(MSG::DEBUG) ) debug() << "=======> MERGING <========" << endmsg;

  // Produce a dedicated Saveset
  if (msgLevel( MSG::DEBUG))debug() << "Produce a dedicated Saveset in " << m_sDirPath << endmsg;
  void *dir = gSystem->OpenDirectory(m_sDirPath.c_str());
  if (dir == 0) gSystem->mkdir(m_sDirPath.c_str(),true);

  string fillNumberStr = boost::lexical_cast<string>( fillNumber );
  string runNumberStr = boost::lexical_cast<string>( runNumber );
  std::string set;

  char year[5];
  char month[3];
  char day[3]; 
  char hour[3];
  char min[3];
  char sec [3];
  time_t rawTime=time(NULL); 
  struct tm* timeInfo = localtime(&rawTime); 
  ::strftime(year, sizeof(year),"%Y", timeInfo); 
  ::strftime(month, sizeof(month),"%m", timeInfo);   
  ::strftime(day, sizeof(day),"%d", timeInfo); 
  ::strftime(hour, sizeof(hour),"%H", timeInfo); 
  ::strftime(min, sizeof(min),"%M", timeInfo); 
  ::strftime(sec, sizeof(sec),"%S", timeInfo); 
  std::string time = "-" + string(year) + string(month) + string(day) + "T" + string(hour) + string(min) + string(sec) ;

  if (m_doRunByRun) set = m_sDirPath + "/" + m_anaTask + "_Run_" + runNumberStr + time + ".root";
  else              set = m_sDirPath + "/" + m_anaTask + "_Fill_" + fillNumberStr + time + ".root";
  m_TargetFile = new TFile( set.c_str() , "UPDATE");

  for (unsigned int i=0 ; i<history_led.size() ; i++) {
    //TFile* file = new TFile(history_led.at(i).c_str(),"READ");
    TFile* file = TFile::Open( history_led.at(i).c_str() );
    if ( msgLevel(MSG::DEBUG) ) debug() << history_led.at(i).c_str() << endmsg;
    if (file!=NULL) haddFile(m_TargetFile, file);
    //if (file) file->Close();
    delete file;
  }
  for (unsigned int i=0 ; i<history_occ.size() ; i++) {
    //TFile* file = new TFile(history_occ.at(i).c_str(),"READ");
    TFile* file = TFile::Open( history_occ.at(i).c_str() );
    if ( msgLevel(MSG::DEBUG) ) debug() << history_occ.at(i).c_str() << endmsg;
    if (file!=NULL) haddFile(m_TargetFile, file);
    //if (file) file->Close();
    delete file;
  }

  if ( msgLevel(MSG::DEBUG) ) debug() << "Number of LED files: " << history_led.size() << endmsg;
  if ( msgLevel(MSG::DEBUG) ) debug() << "Number of Occ files: " << history_occ.size() << endmsg;
  if ( msgLevel(MSG::DEBUG) ) debug() << "--- cleanup led fill " << fillNumber << " ..." << endmsg;
  history_led.clear();
  if ( msgLevel(MSG::DEBUG) ) debug() << "--- cleanup occ fill " << fillNumber << " ..." << endmsg;
  history_occ.clear();

  m_TargetFile->Close("R");
  delete m_TargetFile;
  gSystem->FreeDirectory(dir);
  m_SSetLoc = set;
  m_SSetService->updateService((char*)m_SSetLoc.c_str());
  m_TargetFile = NULL;

}


//=============================================================================
void OccupancyAnalysis::buildList(UInt_t &t1, UInt_t &t2, std::vector<std::string>& files_led, std::vector<std::string>& files_occ, timePeriod timeRange, uint runNumber){

  if ( msgLevel(MSG::DEBUG) ) debug() << "in buildList begin:  " << files_led.size() << " " << files_occ.size() << endmsg;
  char topdir_led[256];
  char topdir_occ[256];
  UInt_t oneday = 24*60*60;
  //for(UInt_t t=t1-oneday; t<t2; t+=oneday) {
  for(UInt_t t=t1-oneday; t<t2+oneday; t+=oneday) {
    TDatime tdt(t);
    sprintf(topdir_led, "/hist/Savesets/%4.4d/LHCb/CaloDAQCalib/%2.2d/%2.2d/",     tdt.GetYear(), tdt.GetMonth(), tdt.GetDay());
    sprintf(topdir_occ, "/hist/Savesets/%4.4d/LHCb/CaloDAQOccupancy/%2.2d/%2.2d/", tdt.GetYear(), tdt.GetMonth(), tdt.GetDay());

    if ( msgLevel(MSG::DEBUG) ) debug() << topdir_led << endmsg;
    printf("%s\n",topdir_led);
    list_dir(topdir_led, files_led, timeRange, runNumber);
    if ( msgLevel(MSG::DEBUG) ) debug() << topdir_occ << endmsg;
    printf("%s\n",topdir_occ);
    list_dir(topdir_occ, files_occ, timeRange, runNumber);
  }
  if ( msgLevel(MSG::DEBUG) ) debug() << "in buildList end:  " << files_led.size() << " " << files_occ.size() << endmsg;

}

//=============================================================================
void OccupancyAnalysis::haddFile( TFile *output, TFile *input) {

  std::vector<std::string> detectorName;
  detectorName.clear();
  detectorName.push_back("Ecal");
  detectorName.push_back("Hcal");

  for (unsigned int i=0 ; i<detectorName.size() ; i++) {

    TProfile* ledHist_input     = (TProfile*) input->Get( TString(detectorName.at(i)+"Calib/Profile/LED/1") );
    TProfile* ledPinHist_input  = (TProfile*) input->Get( TString(detectorName.at(i)+"Calib/Profile/Ratio/1") );
    TProfile* occ0_input        = (TProfile*) input->Get( TString(detectorName.at(i)+"Occupancy/occ0") );
    TProfile* occ1_input        = (TProfile*) input->Get( TString(detectorName.at(i)+"Occupancy/occ1") );
    TProfile* occ2_input        = (TProfile*) input->Get( TString(detectorName.at(i)+"Occupancy/occ2") );
    TProfile* occ3_input        = (TProfile*) input->Get( TString(detectorName.at(i)+"Occupancy/occ3") );
    TProfile* occ4_input        = (TProfile*) input->Get( TString(detectorName.at(i)+"Occupancy/occ4") );
    TProfile* occ5_input        = (TProfile*) input->Get( TString(detectorName.at(i)+"Occupancy/occ5") );
    TH1D*     cell_input        = (TH1D*)     input->Get( TString(detectorName.at(i)+"Occupancy/cell") );

    if (!ledHist_input && !ledPinHist_input && !(occ0_input && occ1_input && occ2_input && occ3_input && occ4_input && occ5_input && cell_input) ) {
      if ( msgLevel(MSG::DEBUG) ) debug() << "No input TProfile/TH1D... in " << detectorName.at(i) << endmsg;
      continue;//      return;
    }
    
    if (ledPinHist_input && ledHist_input) {
      ledPinHist_input->SetName("1");
      ledHist_input->SetName("1");
      TDirectory* dirLEDPIN_0 = output->GetDirectory( TString(detectorName.at(i)+"Calib") );
      //std::cout << dirLEDPIN_0 << std::endl;
      if (dirLEDPIN_0==NULL) {
	dirLEDPIN_0 = output->mkdir( TString(detectorName.at(i)+"Calib/Profile/LED") );
	dirLEDPIN_0 = output->mkdir( TString(detectorName.at(i)+"Calib/Profile/Ratio") );
	TDirectory* dirLEDPIN_1 = output->GetDirectory( TString(detectorName.at(i)+"Calib/Profile/Ratio") );
	bool cd_1 = output->cd(dirLEDPIN_1->GetPath());
	if (cd_1) {
	  dirLEDPIN_1->WriteTObject(ledPinHist_input);
	} else {
	  if ( msgLevel(MSG::DEBUG) ) debug() << "Cannot cd to " << detectorName.at(i) << "Calib/Profile/Ratio" << endmsg;
	  return;
	}
	TDirectory* dirLEDPIN_2 = output->GetDirectory( TString(detectorName.at(i)+"Calib/Profile/LED") );
	bool cd_2 = output->cd(dirLEDPIN_2->GetPath());
	if (cd_2) {
	  dirLEDPIN_2->WriteTObject(ledHist_input);
	} else {
	  if ( msgLevel(MSG::DEBUG) ) debug() << "Cannot cd to " << detectorName.at(i) << "Calib/Profile/LED" << endmsg;
	  return;
	}
      } else {
	TDirectory* dirLEDPIN_1 = output->GetDirectory( TString(detectorName.at(i)+"Calib/Profile/Ratio") );
	if (dirLEDPIN_1==NULL) {
	  if ( msgLevel(MSG::DEBUG) ) debug() << detectorName.at(i) << "Calib exists but " << detectorName.at(i) << "Calib/Profile/Ratio/ doesn't..." << endmsg;
	  return;
	} else {
	  bool cd_1 = output->cd(dirLEDPIN_1->GetPath());
	  if (cd_1) {
	    TProfile* ledPinHist_output = (TProfile*) output->Get( TString(detectorName.at(i)+"Calib/Profile/Ratio/1") );
	    if (!ledPinHist_output) {
	      if ( msgLevel(MSG::DEBUG) ) debug() << detectorName.at(i) << "Calib/Profile/Ratio/1 not found for output file..." << endmsg;
	      return;
	    } else {
	      ledPinHist_output->Add(ledPinHist_input);
	      dirLEDPIN_1->WriteTObject(ledPinHist_output,0,"WriteDelete");
	    }
	  } else {
	    if ( msgLevel(MSG::DEBUG) ) debug() << "Cannot cd to " << detectorName.at(i) << "Calib/Profile/Ratio" << endmsg;
	    return;
	  }
	}

	TDirectory* dirLED_1 = output->GetDirectory( TString(detectorName.at(i)+"Calib/Profile/LED") );
	if (dirLED_1==NULL) {
	  if ( msgLevel(MSG::DEBUG) ) debug() << detectorName.at(i) << "Calib exists but " << detectorName.at(i) << "Calib/Profile/LED/ doesn't..." << endmsg;
	  return;
	} else {
	  bool cd_1 = output->cd(dirLED_1->GetPath());
	  if (cd_1) {
	    TProfile* ledHist_output = (TProfile*) output->Get( TString(detectorName.at(i)+"Calib/Profile/LED/1") );
	    if (!ledHist_output) {
	      if ( msgLevel(MSG::DEBUG) ) debug() << detectorName.at(i) << "Calib/Profile/LED/1 not found for output file..." << endmsg;
	      return;
	    } else {
	      ledHist_output->Add(ledHist_input);
	      dirLED_1->WriteTObject(ledHist_output,0,"WriteDelete");
	    }
	  } else {
	    if ( msgLevel(MSG::DEBUG) ) debug() << "Cannot cd to " << detectorName.at(i) << "Calib/Profile/LED" << endmsg;
	    return;
	  }
	}

      }
    }
    

    if (occ0_input && occ1_input && occ2_input && occ3_input && occ4_input && occ5_input && cell_input) {
      occ0_input->SetName("occ0");
      occ1_input->SetName("occ1");
      occ2_input->SetName("occ2");
      occ3_input->SetName("occ3");
      occ4_input->SetName("occ4");
      occ5_input->SetName("occ5");
      cell_input->SetName("cell");
      TDirectory* dirOcc_0 = output->GetDirectory( TString(detectorName.at(i)+"Occupancy") );
      if (dirOcc_0==NULL) {
	dirOcc_0 = output->mkdir(TString(detectorName.at(i)+"Occupancy"));
	bool cd_0 = output->cd(dirOcc_0->GetPath());
	if (cd_0) {
	  dirOcc_0->WriteTObject(occ0_input);
	  dirOcc_0->WriteTObject(occ1_input);
	  dirOcc_0->WriteTObject(occ2_input);
	  dirOcc_0->WriteTObject(occ3_input);
	  dirOcc_0->WriteTObject(occ4_input);
	  dirOcc_0->WriteTObject(occ5_input);
	  dirOcc_0->WriteTObject(cell_input);
	} else {
	  if ( msgLevel(MSG::DEBUG) ) debug() << "Cannot cd to " << detectorName.at(i) << "Occupancy" << endmsg;
	  return;
	}
      } else {
	TDirectory* dirOcc_1 = output->GetDirectory( TString(detectorName.at(i)+"Occupancy") );
	bool cd_0 = output->cd(dirOcc_1->GetPath());
	if (cd_0) {
	  TProfile* occ0_output = (TProfile*) output->Get( TString(detectorName.at(i)+"Occupancy/occ0") );
	  TProfile* occ1_output = (TProfile*) output->Get( TString(detectorName.at(i)+"Occupancy/occ1") );
	  TProfile* occ2_output = (TProfile*) output->Get( TString(detectorName.at(i)+"Occupancy/occ2") );
	  TProfile* occ3_output = (TProfile*) output->Get( TString(detectorName.at(i)+"Occupancy/occ3") );
	  TProfile* occ4_output = (TProfile*) output->Get( TString(detectorName.at(i)+"Occupancy/occ4") );
	  TProfile* occ5_output = (TProfile*) output->Get( TString(detectorName.at(i)+"Occupancy/occ5") );
	  TH1D*     cell_output = (TH1D*)     output->Get( TString(detectorName.at(i)+"Occupancy/cell") );
	  if ( !(occ0_output && occ1_output && occ2_output && occ3_output && occ4_output && occ5_output) ) {
	    if ( msgLevel(MSG::DEBUG) ) debug() << detectorName.at(i) << "Occupancy/TProfiles not found for output file..." << endmsg;
	    return;
	  } else {
	    occ0_output->Add(occ0_input);
	    occ1_output->Add(occ1_input);
	    occ2_output->Add(occ2_input);
	    occ3_output->Add(occ3_input);
	    occ4_output->Add(occ4_input);
	    occ5_output->Add(occ5_input);
	    cell_output->Add(cell_input);
	    dirOcc_1->WriteTObject(occ0_output,0,"WriteDelete");
	    dirOcc_1->WriteTObject(occ1_output,0,"WriteDelete");
	    dirOcc_1->WriteTObject(occ2_output,0,"WriteDelete");
	    dirOcc_1->WriteTObject(occ3_output,0,"WriteDelete");
	    dirOcc_1->WriteTObject(occ4_output,0,"WriteDelete");
	    dirOcc_1->WriteTObject(occ5_output,0,"WriteDelete");
	    dirOcc_1->WriteTObject(cell_output,0,"WriteDelete");
	  }
	} else {
	  if ( msgLevel(MSG::DEBUG) ) debug() << "Cannot cd to " << detectorName.at(i) << "Occupancy" << endmsg;
	  return;
	}
      }
    }
  }
  
}

//=============================================================================
void OccupancyAnalysis::list_dir (const char *path, std::vector<std::string>& fileList, timePeriod timeRange, uint runNumber) {

  struct dirent *entry;
  //int ret = 1;
  DIR *dir;
  dir = opendir(path);
  if (dir==NULL) {
    if ( msgLevel(MSG::DEBUG) ) debug() << "Directory: " << path << " doesn't exist..." << endmsg;
    return;
  }
  
  //---  std::string str2 (".root");
  while ((entry = readdir (dir)) != NULL) {
    std::string str(entry->d_name);
    
    unsigned int runNumberFile = 0;
    const string fileName{fs::path{str}.filename().string()};
    boost::regex re1(string("CaloDAQCalib") + "-(?<run>\\d+)-(?<time>[\\dT]+)(-EOR)?\\.root$");
    boost::regex re2(string("CaloDAQOccupancy") + "-(?<run>\\d+)-(?<time>[\\dT]+)(-EOR)?\\.root$");
    boost::smatch matches;
    auto flags = boost::match_default;
    
    if ( !boost::regex_match(begin(fileName), end(fileName), matches, re1, flags) &&  // LED
	 !boost::regex_match(begin(fileName), end(fileName), matches, re2, flags)     // Occupancy
	 ) {
      debug() << "Bad SaveSet filename: " << str << endmsg;
      continue;
    } else {
      // Get run number
      runNumberFile = boost::lexical_cast<unsigned int>(matches.str("run"));
      if (runNumberFile==0) continue;
      
      verbose() << "in list_dir entry name:   " << entry->d_name << endmsg;
      
      char str[300];
      strcpy (str,const_cast<char*>(path));
      strcat (str,entry->d_name);
      
      if (m_doRunByRun) {
	if (runNumberFile==runNumber) {
	  fileList.push_back(str);
	}
      } else {
	timeFile time = getInitTime(str);
	verbose() << "time.timeStart= " << time.timeStart << "  time.timeStop= " << time.timeStop << endmsg;
	verbose() << "timeRange.timeStart.size()= " << timeRange.timeStart.size() << endmsg;
	if (timeRange.timeStart.size()>0)
	  for (unsigned int i=0 ; i<timeRange.timeStart.size() ; i++)
	    verbose() << "timeRange.timeStart.at(i)= " << timeRange.timeStart.at(i) << endmsg;
	

	bool inTimeRange = false;

	if (boost::regex_match(begin(fileName), end(fileName), matches, re1, flags)) { //CaloDAQCalib
	  if (time.timeStop-15*60 > timeRange.timeStartLED &&
	      time.timeStop < timeRange.timeStopLED ) {
	    inTimeRange = true;	   
	  }
	}

	
	for (unsigned int i=0 ; i<timeRange.timeStart.size() ; i++) {
	  if ( timeRange.timeStop.at(i)-timeRange.timeStart.at(i)>600 ) {
	    debug() << "time.timeStart= " << time.timeStart << "  time.timeStop= " << time.timeStop << endmsg;
	    debug() << "timeRange.timeStart.at(i)= " << timeRange.timeStart.at(i) << "  timeRange.timeStop.at(i)= " << timeRange.timeStop.at(i) << endmsg;
	    //---  if (boost::regex_match(begin(fileName), end(fileName), matches, re1, flags)) { //CaloDAQCalib
	    //---    if (  time.timeStop-15*60>timeRange.timeStart.at(i) &&
	    //---  	    time.timeStop<timeRange.timeStop.at(i) ) {
	    //---  	// Savesets rootfile in time range, remove the file from /tmp/
	    //---  	inTimeRange = true;
	    //---    }
	    //---  }
	    if (boost::regex_match(begin(fileName), end(fileName), matches, re2, flags)) { //CaloDAQOccupancy
	      if (  time.timeStart>timeRange.timeStart.at(i) &&
		    time.timeStop<timeRange.timeStop.at(i) ) {
		// Savesets rootfile in time range, remove the file from /tmp/
		inTimeRange = true;
	      }
	    }
	  }
	}
	if (inTimeRange == true) fileList.push_back(str);
	else
	  debug() << "file not inTimeRange..." << endmsg;
	
      }
    }
  }
}

//=============================================================================
timeFile OccupancyAnalysis::getInitTime(const char* file) {
  timeFile time;
  TFile* tfile = TFile::Open(file);
  if (tfile==NULL) {
    time.timeStart = 0;
    time.timeStop = 0;
    return time;
  }

  TDirectory* dir = 0;
  if (strstr(file, "CaloDAQOccupancy") != NULL) {
    dir = gFile->GetDirectory("EcalOccupancy");
  } else if (strstr(file, "CaloDAQCalib") != NULL) {
    dir = gFile->GetDirectory("EcalCalib");
  }

  if (dir) {
    TH1D* tInit = (TH1D*) dir->Get("timeInit");
    TH1D* tEnd  = (TH1D*) dir->Get("timeEnd");
    if (tInit && tEnd) {
      time.timeStart = atoi(tInit->GetTitle());
      time.timeStop = atoi(tEnd->GetTitle());
    } else {
      time.timeStart = 0;
      time.timeStop = 0;
    }
  } else {
    time.timeStart = 0;
    time.timeStop = 0;
  }
  if (tfile) delete tfile;
  return time;
}


//=============================================================================
timePeriod OccupancyAnalysis::findPeriod(int fillNumber) {

  timePeriod time;
  char fileName[400];
  //sprintf(fileName,"/afs/cern.ch/lhcb/online/TFC/LPCfiles/%d/%d_lumi_LHCb.txt",fillNumber,fillNumber);
  sprintf(fileName,"/group/online/tfc/LPCfiles/%d/%d_lumi_LHCb.txt",fillNumber,fillNumber);
  FILE * file = fopen(fileName,"r");

  if (!file) {
    if ( msgLevel(MSG::DEBUG) ) debug() << "No lumi file found..." << endmsg;
    std::vector<int> tStart(0), tStop(0);
    time.timeStart = tStart;
    time.timeStop = tStop;
    time.timeStartLED = 0;
    time.timeStopLED = 0;
    return time;
  }

  int unixTime, runStat;
  float instLumi, instLumiError, specLumi, specLumiError;
  char lin[999];
  std::vector<int> x;
  std::vector<float> y;
  std::vector<float> dy10;
  std::vector<float> dy50;
  std::vector<float> dy;
  std::vector<int> tStart, tStop;
  float avLumi = 0;
  int timeStart = 0, timeStop = 0;
  int indexStart = -1;
  int indexStop = -1;
  int tStartLED = 0;
  int tStopLED = 0;

  while (fgets(lin,999,file)) {
    sscanf(lin,"%d%d%f%f%f%f",&unixTime,&runStat,&instLumi,&instLumiError,&specLumi,&specLumiError);

    x.push_back(unixTime);
    y.push_back(instLumi);
    avLumi += instLumi;

    int size = x.size();
    if (size>50)  dy50.push_back(fabs(y.at(size-1) - y.at(size-51))*1./(x.at(size-1)-x.at(size-51)));
    else          dy50.push_back(0);
    if (size>10)  dy10.push_back(fabs(y.at(size-1) - y.at(size-11))*1./(x.at(size-1)-x.at(size-11)));
    else          dy10.push_back(0);
    if (size>10)  dy.push_back(fabs(y.at(size-1) - y.at(size-11))*0.1/fabs(y.at(size-11)));
    else          dy.push_back(0);

    if (size>50 && m_checkLumi) {
      //if ( (fabs(y.at(size-1) - y.at(size-3))>5  &&
      //       fabs(y.at(size-1) - y.at(size-51))>30) ) {
      if (// fabs(y.at(size-1) - y.at(size-11))>0.1*fabs(y.at(size-11)) ||
	   fabs(y.at(size-1) - y.at(size-11))*1./(x.at(size-1)-x.at(size-11)) > 0.2  ||
	   fabs(y.at(size-1) - y.at(size-51))*1./(x.at(size-1)-x.at(size-51)) > 0.02  ) {
	
	indexStop = size-1;
	
	if (indexStart!=-1 && indexStart+2<indexStop) {
	  indexStop = size-1;
	  timeStart = x.at(indexStart);
	  timeStop = x.at(indexStop-50);
	  
	  tStart.push_back(timeStart);
	  tStop.push_back(timeStop);
	  verbose() << "timeStart= " << timeStart << "  -  timeStop= " << timeStop << endmsg;
	  
	  indexStart = indexStop;//-1;
	  indexStop = -1;
	} else {
	  indexStart = size-1;
	}
	
      }
    }
  }

  if (!m_checkLumi) {
    timeStart = x.at(0);
    unsigned int sizex = x.size();
    timeStop = x.at(sizex-1);
    tStart.push_back(timeStart);
    tStop.push_back(timeStop);
  }

  unsigned int sizex = x.size();
  if ( (x.at(sizex-1) - (15+m_ledDuration)*60) > (x.at(0) + 2*60*60) ) {// Check if LED start after begin of fill + 2 hours
    tStartLED = x.at(sizex-1) - (15+m_ledDuration)*60;
  } else {
    tStartLED = x.at(sizex-1) - 15*60;
    verbose() << "------> LED duration too small: x.at(sizex-1) - (15+m_ledDuration)*60 = " << x.at(sizex-1) - (15+m_ledDuration)*60 <<  "     x.at(0) + 60*60 = " << x.at(0) + 60*60 << endmsg;
  }
  
  tStopLED = x.at(sizex-1) - 15*60;
  std::cout << "Begin fill: " << x.at(0) << " End fill: " << x.at(sizex-1) << "  -   Begin LED: " << tStartLED << " End LED: " << tStopLED << std::endl;

  const Int_t n = x.size();
  std::vector<float> xf(x.begin(), x.end());
  TCanvas* c0 = new TCanvas();//"c0","c0",1400,400);
  TMultiGraph *mg = new TMultiGraph();
//  TGraph* lumi = new TGraph(n,X,Y);
  TGraph* lumi = new TGraph( n, &(xf[0]), &(y[0]) );

  lumi->SetMarkerStyle(20);
  lumi->SetMarkerSize(0.4);
  mg->Add(lumi);
  mg->Draw("ap");
  mg->GetXaxis()->SetTimeDisplay(1);
  mg->GetXaxis()->SetNdivisions(-503);
  mg->GetXaxis()->SetTimeFormat("%d/%m/%Y %H:%M %F 1970-01-01 00:00:00");   //2000-02-28 13:00:01");

  char text[30];
  sprintf(text,"Fill %d",fillNumber);

  for (unsigned int i=0 ; i<tStart.size() ; i++) {
    if ( tStop.at(i)-tStart.at(i)>600 ) {
      TGraph *gr1 = new TGraph(2);
      gr1->SetFillColor(6);
      gr1->SetFillStyle(3005);
      gr1->SetLineColor(6);
      gr1->SetLineWidth(-603);
      gr1->SetPoint(0,tStart.at(i),0);
      gr1->SetPoint(1,tStart.at(i),430);
      gr1->SetPoint(2,tStop.at(i),430);
      gr1->SetPoint(3,tStop.at(i),0);
      gr1->Draw("FSame");
    }
  }
  string fillNumberStr = boost::lexical_cast<string>( fillNumber );
  std::string set = m_epsFilePath + "lumiTest_" + fillNumberStr + ".eps";
  c0->SaveAs(set.c_str());


  TCanvas* c1 = new TCanvas();
  TMultiGraph *mg1 = new TMultiGraph();
  TGraph* der10 = new TGraph( n, &(xf[0]), &(dy10[0]) );
  gPad->SetLogy();
  mg1->Add(der10);
  mg1->Add(der10);
  mg1->Draw("ap");
  mg1->GetXaxis()->SetTimeDisplay(1);
  mg1->GetXaxis()->SetNdivisions(-503);
  mg1->GetXaxis()->SetTimeFormat("%d/%m/%Y %H:%M %F 1970-01-01 00:00:00");   //2000-02-28 13:00:01");
  std::string set1 = m_epsFilePath + "deriv10_" + fillNumberStr + ".eps";
  c1->SaveAs(set1.c_str());


  TCanvas* c2 = new TCanvas();
  TMultiGraph *mg2 = new TMultiGraph();
  TGraph* der50 = new TGraph( n, &(xf[0]), &(dy50[0]) );
  gPad->SetLogy();
  mg2->Add(der50);
  mg2->Add(der50);
  mg2->Draw("ap");
  mg2->GetXaxis()->SetTimeDisplay(1);
  mg2->GetXaxis()->SetNdivisions(-503);
  mg2->GetXaxis()->SetTimeFormat("%d/%m/%Y %H:%M %F 1970-01-01 00:00:00");   //2000-02-28 13:00:01");
  std::string set2 = m_epsFilePath + "deriv50_" + fillNumberStr + ".eps";
  c2->SaveAs(set2.c_str());

  TCanvas* c3 = new TCanvas();
  TMultiGraph *mg3 = new TMultiGraph();
  TGraph* delta = new TGraph( n, &(xf[0]), &(dy[0]) );
  gPad->SetLogy();
  mg3->Add(delta);
  mg3->Add(delta);
  mg3->Draw("ap");
  mg3->GetXaxis()->SetTimeDisplay(1);
  mg3->GetXaxis()->SetNdivisions(-503);
  mg3->GetXaxis()->SetTimeFormat("%d/%m/%Y %H:%M %F 1970-01-01 00:00:00");   //2000-02-28 13:00:01");
  std::string set3 = m_epsFilePath + "deltay_" + fillNumberStr + ".eps";
  c3->SaveAs(set3.c_str());

  if (file) fclose(file);

  time.timeStart = tStart;
  time.timeStop = tStop;
  time.timeStartLED = tStartLED;
  time.timeStopLED = tStopLED;

  return time;

}


//============================================================================================
bool OccupancyAnalysis::get_LPC_info(int ifill, UInt_t & time_SOF, UInt_t & time_EOF){
  printf("reading LPC info for fill %d ...\n",ifill);

  int np = 0;
  double minlumi = 1e34, maxlumi = -1e34, avlumi = 0;
  char fnam[256], str[256];

  sprintf(fnam,"/group/online/tfc/LPCfiles/%d/%d_lumi_LHCb.txt",ifill,ifill);

  sleep(120);

  FILE* f1 = fopen(fnam,"r");
  if (!f1) {
    debug() << "Lumi file not found" << endmsg;
    return false;
  }
  UInt_t unixTime,runStat;
  double instLumi, instLumiError, specLumi, specLumiError;
  while (&str[0]==fgets(&str[0], 255, f1)) {
    int nit = sscanf(str,"%u %u %lf %lf %lf %lf", &unixTime, &runStat, &instLumi, &instLumiError, &specLumi, &specLumiError);
    if (6==nit) {
      np++;
      avlumi += instLumi;
      if(minlumi>instLumi) minlumi = instLumi;
      if(maxlumi<instLumi) maxlumi = instLumi;
    }
  }
  fclose(f1);

  avlumi/=np;

  if ( msgLevel(MSG::DEBUG) )
    debug() << "The LPC info in " << fnam << ": " << np << " points, lumi " << minlumi << " < " << avlumi << " < " << maxlumi << endmsg;

  time_SOF = time_EOF = 0;
  double thrlumi = minlumi + (avlumi-minlumi)*0.5;

  FILE* f2 = fopen(fnam,"r");
  if (!f2) return false;
  double prevlumi = 0;
  while (&str[0]==fgets(&str[0], 255, f2)) {
    int nit = sscanf(str,"%u %u %lf %lf %lf %lf", &unixTime, &runStat, &instLumi, &instLumiError, &specLumi, &specLumiError);
    if (6==nit) {
      if (0==time_SOF && prevlumi<thrlumi && instLumi>=thrlumi) {
  time_SOF = unixTime;
      }
      if (prevlumi>thrlumi && instLumi<=thrlumi) {
  time_EOF = unixTime;
      }
      prevlumi = instLumi;
    }
  }
  fclose(f2);

  TDatime tdt_SOF(time_SOF);
  char c_SOF[64];
  strncpy(c_SOF, tdt_SOF.AsSQLString(), 60);

  TDatime tdt_EOF(time_EOF);
  char c_EOF[64];
  strncpy(c_EOF, tdt_EOF.AsSQLString(), 60);

  if ( msgLevel(MSG::DEBUG) )
    debug() << "Fill " << ifill << ": start " << c_SOF << ", end " << c_EOF << endmsg;

  return true;
}
