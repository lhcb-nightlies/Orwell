#ifndef __OccupancyAnalysis_h
#define __OccupancyAnalysis_h

// STD & STL
#include <string>
#include <memory>
#include <fstream>
#include <iostream>
#include <chrono>
#include <mutex>
#include <queue>
#include <deque>
#include <thread>
#include <condition_variable>
#include <unordered_set>
#include <unordered_map>

// BOOST
#include <boost/filesystem.hpp>

// Gaudi
#include <GaudiKernel/IIncidentSvc.h>
#include <GaudiKernel/IEventProcessor.h>
#include <GaudiKernel/IDetDataSvc.h>

// Detector
#include <OTDet/DeOTDetector.h>

// OMALib
#include <OMAlib/AnalysisTask.h>

// dim
#include <dic.hxx>
#include <dis.hxx>

// ROOT
#include <TH1D.h>
#include <TH2D.h>
#include "TCanvas.h"
#include <TF1.h>
#include <TFile.h>
#include <TString.h>
#include <TProfile.h>
#include <TMultiGraph.h>
#include "TSystemDirectory.h"
#include "TSystem.h"
#include "TGraph.h"
#include "TPad.h"

#include "dirent.h"

struct timeFile {
  int timeStart;
  int timeStop;
} ;

struct timePeriod {
  std::vector<int> timeStart;
  std::vector<int> timeStop;
  int timeStartLED;
  int timeStopLED;
} ;


template<class T, class Container = std::deque<T> >
class Queue {
public:
   Queue() : m_mutex{}, m_condition{}, m_queue{} {}

   T get() {
      std::unique_lock<std::mutex> lock{m_mutex};
      m_condition.wait(lock, [this] ()-> bool {return not m_queue.empty();});
      T tmp = std::move(m_queue.front());
      m_queue.pop();
      return tmp;
   }

   template<class ITEM>
   void put(ITEM&& i) {
      std::unique_lock<std::mutex> lock{m_mutex};
      m_queue.push(std::forward<ITEM>(i));
      m_condition.notify_one();
   }

private:

   std::mutex m_mutex;
   std::condition_variable m_condition;
   std::queue<T, Container> m_queue;

};


/** @class OccupancyAnalysis OccupancyAnalysis.h
 *
 *
 *  @author Lucia Grillo
 *  @date   2015-01-08
 */

class DeOTDetector;

class OccupancyAnalysis : public AnalysisTask {

public:

   /// Standard constructor
   OccupancyAnalysis( const std::string& name, ISvcLocator* pSvcLocator );

   virtual ~OccupancyAnalysis( ); ///< Destructor

   StatusCode initialize() override;    ///< Algorithm initialization
   StatusCode start     () override;    ///< Algorithm start
   StatusCode execute   () override;    ///< Algorithm execution
   StatusCode finalize  () override;    ///< Algorithm finalization
   StatusCode analyze   (std::string& saveSet,
                         std::string task) override; ///< Algorithm analyze

   typedef unsigned int FileVersion ;

   DimService *m_SSetService;
   std::string m_SSetLoc;
private:

   bool m_first;
   StatusCode init();

   bool m_RunOnline;
   long long m_initialTime;

   // Local storage of current run number
   unsigned int m_run;

   bool m_doRunByRun;
   bool m_checkLumi;
   std::string m_xmlFilePath;
   std::string m_xmlFileName;
   std::string m_epsFilePath;
   int m_ledDuration;

   // Synchronisation
   std::thread m_thread;
   Queue<std::pair<std::string, std::string>> m_queue;

   // Detector
   SmartIF<IIncidentSvc> m_incidentSvc;

   static constexpr char  lhcState_default = 0;
   static constexpr int   fillNumber_default = 0;


   // Class to listen to LHC/State from DIM
   class LHCState : public DimInfo {
   public :
     LHCState()
       : DimInfo("LHC/State", lhcState_default),
       m_lhcState{0} {}
     boost::optional<char*> lhcState() const {
       boost::optional<char*> r;
         if (*m_lhcState != lhcState_default) {
    r = m_lhcState;
         }
         return r;
       }
   private:
       void infoHandler() override {
         m_lhcState = getString();
  std::cout << "in h: lhcState= " << getString() << std::endl;
       }
       char* m_lhcState;
   };

   // Class to listen to LHCbStatus/FillNumber from DIM
   class FillNumber : public DimInfo {
   public :
     FillNumber()
       : DimInfo("LHCbStatus/FillNumber", fillNumber_default),
       m_fillNumber{0} {}
       boost::optional<int> fillNumber() const {
         boost::optional<int> r;
         if (m_fillNumber != fillNumber_default) {
    r = m_fillNumber;
         }
         return r;
       }
   private:
       void infoHandler() override {
         m_fillNumber = getInt();
       }
       int m_fillNumber;
   };

   std::unique_ptr<LHCState>   m_lhcState;
   std::unique_ptr<FillNumber> m_fillNumber;

   std::string m_sDirPath;
   std::string m_sDir;
   std::string m_anaTask;
   TFile* m_TargetFile;


   std::unordered_set<unsigned int> m_versions;

   boost::filesystem::path xmlFileName( FileVersion v ) const ;
   double readCondDB();

   std::tuple<unsigned int, boost::optional<std::string>, boost::optional<int> >
     readXML(const boost::filesystem::path& xmlFile);

   boost::filesystem::path writeXML(const FileVersion previous, const unsigned int run,
                                    const boost::optional<std::string> lhcState,
                                    const boost::optional<int> fillNumber);

   //unsigned int latestVersion() const;
   boost::optional<unsigned int> latestVersion() const;

   // This one does all the work.
   StatusCode mergeFiles(std::string saveSet, std::string task);

   void Do_EOF(int fillNumber, UInt_t t1, UInt_t t2, uint runNumber);
   void buildList(UInt_t &t1, UInt_t &t2, std::vector<std::string>& files_led, std::vector<std::string>& files_occ, timePeriod timeRange, uint runNumber);
   void haddFile( TFile *output, TFile *input);
   void list_dir (const char *path, std::vector<std::string>& fileList, timePeriod timeRange, uint runNumber);
   timeFile getInitTime(const char* file);
   timePeriod findPeriod(int fillNumber);
   bool get_LPC_info(int ifill, UInt_t & time_SOF, UInt_t & time_EOF);

};

#endif // OccupancyAnalysis_H
