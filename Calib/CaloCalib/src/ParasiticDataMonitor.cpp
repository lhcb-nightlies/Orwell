// $Id: $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 
#include "CaloUtils/CaloAlgUtils.h"
// local
#include "ParasiticDataMonitor.h"

//-----------------------------------------------------------------------------
// Implementation file for class : ParasiticDataMonitor
//
// 2011-09-04 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( ParasiticDataMonitor )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ParasiticDataMonitor::ParasiticDataMonitor( const std::string& name,
                                            ISvcLocator* pSvcLocator)
  : GaudiHistoAlg ( name , pSvcLocator ),
    m_nChannels(0),
    m_count(0),
    m_trend(false){

  declareProperty ( "Detector"  , m_det   = DeCalorimeterLocation::Ecal ) ;
  declareProperty ( "RawLocation", m_raw = LHCb::RawEventLocation::Default);
  declareProperty ( "ParasiticCardMap", m_cardMap);
  declareProperty ( "AllowNonParasiticData" , m_force=false);
  declareProperty ( "OnlineMode"            , m_online);
  declareProperty ( "Profile1D"             , m_p1D=true);
  declareProperty ( "ProfileError"          , m_prof = "");
  declareProperty ( "TrendStat"             , m_trendStat=1500);
  declareProperty ( "TrendFile"             , m_trendFile="LHCb_CaloCrateTimer" );
  declareProperty ( "TrendTolerance"        , m_trendTol=0.);
  declareProperty ( "Logout"                , m_logout=false);
  declareProperty ( "Histo1D"               , m_h1D=false);
  declareProperty ( "HistoMin"              , m_min=1400.);
  declareProperty ( "HistoMax"              , m_max=2400.);
  declareProperty ( "HistoBin"              , m_bin=500);
  declareProperty ( "Threshold"             , m_thr=-256);

  m_det    = LHCb::CaloAlgUtils::DeCaloLocation( name ) ;
  m_packedType=LHCb::RawBank::EcalPacked;

	// check whether MonitorSvc is at work (offline/online)
 	m_online  = pSvcLocator->existsService("MonitorSvc");
  if(m_online)info() << "MonitoringSvc is at work" << endmsg;
  setHistoDir( name );  
}
//=============================================================================
// Destructor
//=============================================================================
ParasiticDataMonitor::~ParasiticDataMonitor() {} 


//=============================================================================
bool ParasiticDataMonitor::parseCardMap( ){

  for( CARDMAP::iterator it = m_cardMap.begin(); m_cardMap.end() != it ; ++it){

    std::string scode =  it->first;
    int icode;
    std::istringstream iscode( scode );
    iscode >> icode;

    const std::vector<std::string>& svec = it->second;

    // CHECK FEBs
    int crate = icode / 16;
    int slot  = icode % 16;
    int index = m_calo->cardIndexByCode( crate , slot );
    int tell1 = m_calo->cardToTell1( index );
    
    if( !m_calo->isParasiticCard( index ) && !m_force ){
      warning() << "The requested FEB ["<<icode<<"] is not a parasitic FEB - Skip it" << endmsg;
      continue;
    }else if( !m_calo->isParasiticCard( index ) &&  m_force ){
      info() << "The  standard FEB [" << icode << "] will be monitored" << endmsg;
    }else 
      info() << "The parasitic FEB [" << icode << "] will be monitored" << endmsg;

    // CHECK CHANNELs
 
    if( svec.empty() ){
      // monitor all channels
      int nc =64;
      if( m_caloName == "Ecal" || m_caloName == "Hcal" )nc=32;      
      for(int i=0;i<nc;++i){
        std::ostringstream oflag("");
        oflag << "FEB#"<<format("%03i",icode)<<"-Channel#"<<format("%02i",i) ;
        std::string flag=oflag.str();
        m_readoutMap[tell1][icode ][i]= flag;
        m_dataMap[flag]=statData(m_thr);
      }
      m_nChannels += nc;
      info() << "Will monitor all "<< nc << " channels of the parasitic FEB code=[" << icode << "]"<<endmsg;
    }
    else{
      info() << "Will monitor " << svec.size() << " channels of the parasitic FEB code=["<<icode<<"]"<<endmsg;
      m_nChannels += svec.size();
      for( std::vector<std::string>::const_iterator iv = svec.begin(); svec.end() != iv ; ++iv){
        std::string flag = *iv;
        int index = flag.find_first_of(":");
        if( (int)std::string::npos == index ){
          warning() << "Bad syntax for ParasiticCardMap" <<endmsg;
          warning() << "Must be of the form :  " <<endmsg;
          warning() << "{ '193'     : {'0 : 1st channel',' 1 : 2nd channel', ...},"<<endmsg;  
          warning() << "{ '257'     : {'0 : 1st channel',' 1 : 2nd channel', ...},"<<endmsg; 
          warning() << "{ 'febCode' : {'channelIndex : flag', '...'}}; " << endmsg;  
          return false; 
        } 
        int icode; 
        std::istringstream iscode( scode );
        iscode >> icode;
        std::string schan = flag.substr(0,index);
        std::string sdesc    = flag.substr(index+1,flag.length());
        std::map<std::string,statData>::iterator it = m_dataMap.find( sdesc );
        if( it != m_dataMap.end() ){
          warning() << "BAD SETTING - The flag '"<< sdesc << "' is defined for 2 different channels "<<endmsg;
          return false;
        }
        int ichan;
        std::istringstream ischan( schan );
        ischan >> ichan;
        m_readoutMap[ tell1 ][ icode ][ichan]=sdesc;
        m_dataMap[sdesc]=statData(m_thr);
      }    
    }
  }
  return true;
}


//=============================================================================
// Initialization
//=============================================================================
StatusCode ParasiticDataMonitor::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;


  m_count = 0;

  // get DeCalorimter
  m_calo = getDet<DeCalorimeter>( m_det );
  if( 0 == m_calo ) { return StatusCode::FAILURE; }
  m_caloName = LHCb::CaloAlgUtils::CaloNameFromAlg( m_det );

  
  //----- Prepare the partial decoding
  const std::vector<CardParam>& cards = m_calo->cardParams();

  
  // automatic selection of parasitic FEBs if not provided by options
  if( m_cardMap.empty() ){
    std::vector<std::string> dummy;
    for( std::vector<CardParam>::const_iterator it=cards.begin(); cards.end() != it ;++it){      
      if( it->isParasitic()) m_cardMap[ Gaudi::Utils::toString( it->code() ) ] = dummy;
    }
  }

  //-- convert cardMap -> readoutMap
  if( !parseCardMap() )return StatusCode::SUCCESS; 

  //-- select TELL1 sourceID
  if( m_readoutMap.empty() ){
    warning() << "No TELL1 to be decoded - skip" << endmsg;
    return StatusCode::SUCCESS;
  }


  // Define histos
  if( m_p1D){
    m_profile = bookProfile1D(m_caloName+"DataMonitor",name() + "Data Profile", 0., double(m_nChannels), m_nChannels,m_prof);
    int k=1;
    m_tprofile = Gaudi::Utils::Aida2ROOT::aida2root( m_profile );
    for( std::map<std::string,statData>::iterator it = m_dataMap.begin() ; m_dataMap.end() !=it ; ++it ){
      std::string flag = it->first;
      m_tprofile->GetXaxis()->SetBinLabel( k, flag.c_str() );
      k++;
    }
  }
  

  // get the trending tool
  m_trendTool =  tool<ITrendingTool>("TrendingTool","TrendingTool",this);  
  m_trendTags.clear();
  std::vector<float> trendThresholds;
  for( std::map<std::string,statData>::iterator it = m_dataMap.begin() ; m_dataMap.end() !=it ; ++it ){
    std::string tag = it->first;
    m_trendTags.push_back( name() + " : '" +tag+"' (mean)" );
    m_trendTags.push_back( name() + " : '" +tag+"' (rms)" );
    trendThresholds.push_back( m_trendTol );
    trendThresholds.push_back( m_trendTol );
  }
  
  m_trend = false;
  if( m_trendFile != "" && !m_trendTags.empty() ){
    if( !m_online && m_trendFile[0]!='/')
      warning() << "Warning : offline mode writing online trendFile - check your options" << endmsg;
    else if(  m_online && m_trendFile[0]=='/')
      warning() << "Warning : online mode writing offline trendFile - check your options" << endmsg;
    else{
      m_trend = trendTool()->openWrite( m_trendFile, m_trendTags)&& trendTool()->setThresholds( trendThresholds );
      if( m_trend ){
        info() << "opening trending file '" << m_trendFile << "'"<<endmsg;
        info() << "Mean/RMS trending for " << m_trendTags << endmsg;
        info() << "based on "<< m_trendStat << " events - tolerance=" << m_trendTol << endmsg;
      } else
        warning()<<" Failed setting the trending tool" << endmsg;
    }
  } else 
    warning() << "No trending file"<<endmsg;  
  
  // Define packed banks
  m_packedType=LHCb::RawBank::EcalPacked;//default
  if( m_caloName=="Hcal") m_packedType=LHCb::RawBank::HcalPacked;
  if( m_caloName=="Prs") m_packedType=LHCb::RawBank::PrsPacked;


  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode ParasiticDataMonitor::execute() {
  
  // get RawEvent
  m_banks = NULL;
  LHCb::RawEvent* rawEvt = NULL ;
  if( exist<LHCb::RawEvent>( m_raw ) ){
    rawEvt= get<LHCb::RawEvent>( m_raw );
  }else  {
    Warning("WARNING : rawEvent not found").ignore();
    counter("Parasitic data monitor")+=0;
    return StatusCode::SUCCESS;
  }

  // get RawBanks
  m_banks= &rawEvt->banks(  m_packedType );
  if ( 0 == m_banks || 0 == m_banks->size() ) {
    Warning(m_caloName+" packed banks not found").ignore();
    counter("Parasitic data monitor")+=0;
    return StatusCode::SUCCESS;
  }

  int nFebs=0;
  bool found=false;
  // Decode parasitic data from RawBank
  for( std::vector<LHCb::RawBank*>::const_iterator itB = m_banks->begin(); itB != m_banks->end() ; ++itB ) {
    LHCb::RawBank* bank = *itB;
    int sourceID       = bank->sourceID();
    for( READOUTMAP::iterator it=m_readoutMap.begin(); m_readoutMap.end()!=it;++it){
      int source = it->first;
      if( source >= 0 && source != sourceID )continue;
      unsigned int* data = bank->data();
      int size           = bank->size()/4;  // Bank size is in bytes

      const std::map< int, std::map<int,std::string>  >& cards = m_readoutMap[ sourceID ];
      

      if( m_caloName == "Prs" ){
        // 1MHz decoding Prs ==================

        while( 0 != size ) {
          unsigned int word = *data++;
          size--;
          // Read bank header
          int lenTrig = word & 0x7F;
          int lenAdc  = (word >> 7 ) & 0x7F;
          int code  = (word >>14 ) & 0x1FF;
          nFebs++;
          bool keep = false;
          std::map<int,std::map<int,std::string> > ::const_iterator itc = cards.find( code );
          if( itc != cards.end() )keep = true;          
          
          /*
          int crate = code / 16;
          int slot  = code % 16;
          if(keep)info() << "tell1/crate/slot/code " << sourceID << " "<<crate << " " << slot << " " << code << " " << keep << endmsg;
          */

          // skip the trigger bits
          int nSkip = (lenTrig+3)/4;  //== Length in byte, with padding
          size -= nSkip;
          data     += nSkip;


          // ... and read data
          int offset   = 32;  //== force read the first word, to have also the debug for it.
          unsigned int lastData = 0;
          while ( 0 < lenAdc ) {
            if ( 32 == offset ) {
              lastData =  *data++;
              size--;
              offset = 0;
            }
            int adc = ( lastData >> offset ) & 0x3FF;
            unsigned int num = ( lastData >> (offset+10) ) & 0x3F;
            lenAdc--;
            offset += 16;            
            if( keep ){
              //info() << " --> " << num << endmsg;
              found = true;
              const std::map<int,std::string>& channels = itc->second;
              std::map<int,std::string>::const_iterator itch = channels.find( num );
              if( itch != channels.end() ){
                std::string flag = itch->second;
                m_dataMap[flag].add(double(adc));
              //if(m_logout)info() << "Adding " << adc << " to " << flag << endmsg;
              }              
            }
          }
        }
      }else if (m_caloName=="Ecal" || m_caloName=="Hcal"){
        // 1MHz decoding  Ecal/Hcal =================
        while( 0 != size ) {
          unsigned int word = *data++;
          size--;
          // Read bank header
          int lenTrig = word & 0x7F;
          int code    = (word >> 14 ) & 0x1FF;
          int crate = code / 16;
          int slot  = code % 16;
          int index = m_calo->cardIndexByCode( crate,slot);
          nFebs++;
          
          bool isParasitic = m_calo->isParasiticCard( index ) ;
          bool keep = false;
          std::map<int,std::map<int,std::string> > ::const_iterator itc = cards.find( code );
          if( itc != cards.end() )keep = true;        
          
          // First skip trigger bank ...
          int nSkip = (lenTrig+3)/4;   //== is in bytes, with padding
          data     += nSkip;
          size     -= nSkip;
          unsigned int pattern  = *data++;
          int offset   = 0;
          unsigned int lastData = *data++;
          size -= 2;
          // ... and read data
          for (unsigned int bitNum = 0; 32 > bitNum; bitNum++ ) {
            int adc;
            if ( 31 < offset ) {
            offset  -= 32;
            lastData =  *data++;
            size--;
            }
            if ( 0 == ( pattern & (1<<bitNum) ) ) {  //.. short coding
              adc = ( ( lastData >> offset ) & 0xF ) ;
              if( !isParasitic) adc -= 8;
              offset += 4;
            } else {
              adc =  ( ( lastData >> offset ) & 0xFFF ); 
              if ( 24 == offset ) adc &= 0xFF;
              if ( 28 == offset ) adc &= 0xF;  //== clean-up extra bits
              offset += 12;
              if ( 32 < offset ) {  //.. get the extra bits on next word
                lastData = *data++;
                size--;
                offset -= 32;        
                int temp = (lastData << (12-offset) ) & 0xFFF;
                adc += temp;
              }
              if( !isParasitic) adc -= 256;
            }
            if( keep ){
              found=true;
              const std::map<int,std::string>& channels = itc->second;
              std::map<int,std::string>::const_iterator itch = channels.find( bitNum );
              if( itch != channels.end() ){
                std::string flag = itch->second;
                m_dataMap[flag].add(double(adc));
              //if(m_logout)info() << "Adding " << adc << " to " << flag << endmsg;
              }
            }
          }
        }
      }
    }
  }


  counter("Parasitic data monitor")+=(found ? 1 : 0);  
  if( !found ){
    counter("Requested data not found") += 1;
    debug() << "Requested data not found" << endmsg;
    return StatusCode::SUCCESS;
  }


  m_count++;
  bool reset = (m_count == m_trendStat );
  std::vector<float> trendVal;
  // Process the monitoring
  int k = 0;
  for( std::map<std::string,statData>::iterator it = m_dataMap.begin() ; m_dataMap.end() !=it ; ++it ){
    std::string flag = it->first;
    statData& data = it->second;
    if( m_p1D)fill( m_profile, k , data.value(),1.);
    k++;

    // histo1D
    if ( m_h1D )plot1D( data.value(), Gaudi::Utils::toString(k) , "Parastitic data '"+flag+"'" , m_min,m_max,m_bin);

    // prepare trending vectors
    if( reset ){
      trendVal.push_back((float)data.mean());
      trendVal.push_back((float)data.rms());    
      if( m_logout )info() << data.nReset() << " - : " << flag << "  mean=" << data.mean() << " rms=" << data.rms() << endmsg;
      data.reset();
      m_count = 0;
    }
  }
  if(reset && m_trend){
    if( trendVal.size() != m_trendTags.size())
      Warning("Problem filling  trending value").ignore();
    else{
      trendTool()->write( trendVal );
    }
  }
  
  counter("Decoded FEBS") += nFebs;



  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode ParasiticDataMonitor::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;
  if(m_trend)trendTool()->closeFile();
  return GaudiHistoAlg::finalize();  // must be called after all other actions
}

//=============================================================================

