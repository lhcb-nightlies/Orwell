// $Id: $
#ifndef PARASITICDATAMONITOR_H
#define PARASITICDATAMONITOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "CaloDet/DeCalorimeter.h"
#include "Event/RawEvent.h"
#include <TProfile.h>
#include "GaudiUtils/Aida2ROOT.h"
#include "Trending/ITrendingTool.h"

/** @class ParasiticDataMonitor ParasiticDataMonitor.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2011-09-04
 */
class ParasiticDataMonitor : public GaudiHistoAlg {
public:
  /// Standard constructor
  ParasiticDataMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~ParasiticDataMonitor( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization


  // statData
  class statData{
 public:
    statData(double threshold=-256){
      m_nreset=0;
      m_thr=threshold;
      reset();
    }

    void add(double val){
      if( val < m_thr)return;
      m_val=val;
      m_sum+=val;
      m_sqsum+=val*val;
      m_count++;
    }
    double value(){return m_val;}
    double mean(){
      return (m_count>0) ? m_sum / double( m_count) : 0;
    }
    double rms(){
      double m = mean();
      m *= m;
      double mq=(m_count>0) ? m_sqsum / double( m_count) : 0;
      return ((mq-m)>0) ? sqrt(mq-m) : 0 ;
    }
    int entries(){return m_count;}
    int nReset(){return m_nreset;}
    void reset(){
      m_val=0;
      m_sum=0;
      m_sqsum=0;
      m_count=0;
      m_nreset++;
    }


  private:
    double m_val;
    double m_sum;
    double m_sqsum;
    int m_count;
    int m_nreset;
    double m_thr;
  };



  ITrendingTool* trendTool(){return m_trendTool;}

protected:

private:
  typedef std::map<std::string,std::vector<std::string> > CARDMAP;
  typedef std::map<int, std::map< int , std::map< int,std::string > > >  READOUTMAP; // [Tell1[FEB[Channel]]]:flag
  typedef std::map<std::string,statData> DATAMAP; // flag:statData
  bool parseCardMap( );
  std::string    m_det;
  DeCalorimeter* m_calo;
  const std::vector<LHCb::RawBank*>* m_banks;
  std::string m_raw;
  LHCb::RawBank::BankType m_packedType;
  CARDMAP    m_cardMap;
  READOUTMAP m_readoutMap;
  DATAMAP    m_dataMap;
  bool m_online;
  bool m_force;
  AIDA::IProfile1D*  m_profile;
  TProfile* m_tprofile;
  bool m_p1D;
  std::string m_prof;
  std::string m_caloName;
  int m_nChannels;
  int m_count;
  int m_trendStat;
  ITrendingTool* m_trendTool;
  std::string m_trendFile;
  double m_trendTol;
  bool m_trend;
  std::vector<std::string> m_trendTags;
  bool m_logout;
  bool m_h1D;
  int m_bin;
  double m_min;
  double m_max;
  double m_thr;
};
#endif // PARASITICDATAMONITOR_H

