// $Id: SpdL0Timing.h,v 1.1 2008-10-15 16:31:26 odescham Exp $
#ifndef SPDL0TIMING_H
#define SPDL0TIMING_H 1

// Include files
// from Gaudi
#include "CaloDAQ/ICaloDataProvider.h"
#include "GaudiKernel/IEventTimeDecoder.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
// from CaloDet
#include "Event/RawEvent.h"
#include "Event/L0DUBase.h"
// from CaloDAQ
#include "CaloDAQ/ICaloTriggerAdcsFromRaw.h"
#include "CaloDAQ/ICaloTriggerBitsFromRaw.h"
// from DAQEvent
#include "Event/RawEvent.h"
#include "Event/L0ProcessorData.h"
#include "L0Interfaces/IL0DUFromRawTool.h"
#include "CaloUtils/Calo2Dview.h"

/** @class SpdL0Timing SpdL0Timing.h
 *
 *
 *  @author Hugo Ruiz
 *  @date   2008-07-09
 */
class SpdL0Timing : public Calo2Dview {
public:
  /// Standard constructor
  SpdL0Timing( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~SpdL0Timing( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

private:

  // Calo Data Provider
  ICaloDataProvider* m_daq;

  // L0DU from raw
  IL0DUFromRawTool* m_fromRaw;

  // Names
  std::string m_readoutTool;

};
#endif // SPDL0TIMING_H
