
if [ $1 ]; then
export det=$1
else
    read -p "OMA Alarms : subdetector (CALO/L0) ? [CALO] : " -e det
    if [ "$det" == "" ]; then
        export det="CALO"
    fi
fi

/group/online/scripts/dumpOMAlarms -s $det

export n=`/group/online/scripts/dumpOMAlarms -s $det | grep "There are 0 messages"  | wc -l`
if [ "$n" != "1" ]; then
    echo ""
    read -p "Clear alarm(s) ? [MSG ID] : " -e id
    if [ "$id" != "" ]; then
        /group/online/scripts/dumpOMAlarms -c -s $det -u HIST_WRITER -p histeggia194  -m $id
    fi
fi

