#!/bin/bash

clear

rm -f  $ORWELLROOT/options/ODINTimeFilter.opts

echo "// ODIN TIME FILTERING" > $ORWELLROOT/options/ODINTimeFilter.opts

# run setupUser if not yet done
echo $ORWELLROOT
if [ "$ORWELLROOT" = "" ]; then
    source /group/calo/sw/scripts/setupUser.sh
fi

echo "--------------------------"
echo "CaloTiming settings script"
echo "--------------------------"

tag="None"
def=`grep -i requested $ORWELLOPTS/inputRawData.opts  | awk -F { '{print $2}' | awk -F } '{print $1}'`
if [ "$def" = "" ];then
def="00000"
fi
cd $ORWELLOPTS
read -p  "Which run(s) to analyze ? [$def] : " -e run
if [  "$run" = "" ]; then
export run="$def"
fi
if [ "$run" != "$def" ]; then
    echo '----- getting requested run'
. /group/calo/sw/scripts/getrun.sh $run
    echo '---------------------------'
fi


# facultative BCID selection :

read -p  "BCID single value (return if no selection) ? [None] : " -e bcid  

if [ "$bcid" != "" ]; then
    if [ "$bcid" != "None" ]; then
        echo "ODINTimeFilter.BCIDSelector = true;  "      >> $ORWELLROOT/options/ODINTimeFilter.opts
        echo "ODINTimeFilter.bRange = ( $bcid , $bcid   ); // BCID interval"   >> $ORWELLROOT/options/ODINTimeFilter.opts
        echo "//" >> $ORWELLROOT/options/ODINTimeFilter.opts
    fi
fi   

    
# Define event selector
read -p  "Event selector (Time, EventId, None, HandMade)? [Time] : " -e sel
if [  "$sel" = "" ]; then
export sel="Time"
fi
if [ "$sel" != "Time" ]; then
    if [ "$sel" != "EventId" ]; then
        if [ "$sel" != "None" ]; then
            if [ "$sel" != "HandMade" ]; then
                echo "wrong selector"
		return 0 2>/dev/null 
            fi
        fi
    fi
fi


if [ "$sel" = "None" ]; then
    echo "NO filtering"
    tag=`date +%Y`""`date +%m`""`date +%d`"T"`date +%H`""`date +%M`""`date +%S`
    echo "ODINTimeFilter.timeSelector = false;" >> $ORWELLROOT/options/ODINTimeFilter.opts
    echo "ODINTimeFilter.eventSelector = false;" >> $ORWELLROOT/options/ODINTimeFilter.opts
fi;


# define interval

if [ "$sel" = "Time" ]; then

    tag=""

    read -p  "Year interval ? ["`date +%Y`" "`date +%Y`"] : " -e year 
    if [  "$year" = "" ]; then
        export year="`date +%Y` `date +%Y`"
    fi

    iyear=""
    virg=""
    count=0
    for i in $year; do
	if [ "$virg" = "" ]; then 
	    tag=$tag$i 
	fi
	iyear=$iyear$virg$i
	let count=count+1
	virg=","
    done
    if [ $count != 2 ]; then
	echo "wrong syntax - should be two int values separated with a blank"
        return 0 2>/dev/null 
    fi

    read -p  "Month interval ? ["`date +%m`" "`date +%m`"] : " -e month 
    if [  "$month" = "" ]; then
        export month="`date +%m` `date +%m`"
    fi


    imonth=""
    virg=""
    count=0
    for i in $month; do
	if [ "$virg" = "" ]; then 
	    tag=$tag$i 
	fi
	imonth=$imonth$virg$i
	let count=count+1
	virg=","
    done
    if [ $count != 2 ]; then
	echo "wrong syntax - should be two int values separated with a blank"
	return 0 2>/dev/null 
    fi


    read -p  "Day interval ? ["`date +%d`" "`date +%d`"] : " -e day 
    if [  "$day" = "" ]; then
        export day="`date +%d` `date +%d`"
    fi

    iday="" 
    virg=""
    count=0
    for i in $day; do
	if [ "$virg" = "" ]; then 
	    tag=$tag$i 
	fi
	iday=$iday$virg$i
	let count=count+1
	virg="," 
    done 
    if [ $count != 2 ]; then
	echo "wrong syntax - should be two int values separated with a blank"
	return 0 2>/dev/null  
    fi 

    tag="${tag}T"

    read -p  "Hour interval ? ["`date +%H`" "`date +%H`"] : " -e hour 
    if [  "$hour" = "" ]; then
        export hour="`date +%H` `date +%H`"
    fi

    ihour=""
    virg=""
    count=0
    for i in $hour; do
	if [ "$virg" = "" ]; then 
	    tag=$tag$i 
	fi
	ihour=$ihour$virg$i
	let count=count+1
	virg=","
    done
    if [ $count != 2 ]; then
	echo "wrong syntax - should be two int values separated with a blank"
	return 0 2>/dev/null 
    fi


    read -p  "Minute interval ? ["`date +%M`" "`date +%M`"] : " -e minute
    if [  "$minute" = "" ]; then
        export minute="`date +%M` `date +%M`"
    fi

    iminute=""
    virg=""
    count=0
    for i in $minute; do
	if [ "$virg" = "" ]; then 
	    tag=$tag$i 
	fi
        iminute=$iminute$virg$i
        let count=count+1
        virg=","
    done
    if [ $count != 2 ]; then
	echo "wrong syntax - should be two int values separated with a blank"
	return 0 2>/dev/null 
    fi


    read -p  "Second interval ? [00 59] : " -e second
    if [  "$second" = "" ]; then
        export second="00 59"
    fi

    isecond=""
    virg=""
    count=0
    for i in $second; do
	if [ "$virg" = "" ]; then 
	    tag=$tag$i 
	fi
	isecond=$isecond$virg$i
	let count=count+1
	virg=","
    done
    if [ $count != 2 ]; then
	echo "wrong syntax - should be two int values separated with a blank"
	return 0 2>/dev/null 
    fi 


    echo "ODINTimeFilter.eventSelector = false;"    >> $ORWELLROOT/options/ODINTimeFilter.opts    
    echo "ODINTimeFilter.timeSelector = true;"      >> $ORWELLROOT/options/ODINTimeFilter.opts
    echo "ODINTimeFilter.yRange = ( $iyear    );"   >> $ORWELLROOT/options/ODINTimeFilter.opts
    echo "ODINTimeFilter.mRange = ( $imonth   );"   >> $ORWELLROOT/options/ODINTimeFilter.opts
    echo "ODINTimeFilter.dRange = ( $iday     );"   >> $ORWELLROOT/options/ODINTimeFilter.opts
    echo "ODINTimeFilter.hRange = ( $ihour    );"   >> $ORWELLROOT/options/ODINTimeFilter.opts
    echo "ODINTimeFilter.mnRange= ( $iminute  );"   >> $ORWELLROOT/options/ODINTimeFilter.opts
    echo "ODINTimeFilter.sRange = ( $isecond  );"   >> $ORWELLROOT/options/ODINTimeFilter.opts
fi

# EventId selection
if [ "$sel" = "EventId" ]; then 
    read -p  "EventId interval ? [0 9999] : " -e event 
    if [  "$event" = "" ]; then
        export event="0 9999"
    fi

    tag=""

    ievent=""
    virg=""
    sep="-"
    count=0
    for i in $event; do
	tag=$tag$i$sep 
	ievent=$ievent$virg$i
	let count=count+1
	virg=","
	sep=""
    done
    if [ $count != 2 ]; then
	echo "wrong syntax - should be two int values separated with a blank"
	return 0 2>/dev/null 
    fi


    echo "ODINTimeFilter.timeSelector = false;" >> $ORWELLROOT/options/ODINTimeFilter.opts
    echo "ODINTimeFilter.eventSelector = true;" >> $ORWELLROOT/options/ODINTimeFilter.opts
    echo "ODINTimeFilter.eRange = ( $ievent    );">> $ORWELLROOT/options/ODINTimeFilter.opts

fi

if [ "$sel" = "HandMade" ]; then 
    echo ""
    echo "Fill $ORWELLROOT/options/ODINTimeFilter.opts template according to your need"
    echo "ODINTimeFilter.timeSelector = true;" >> $ORWELLROOT/options/ODINTimeFilter.opts
    echo "ODINTimeFilter.yRange = (`date +%Y`,`date +%Y`);// Year range (inclusive)"   >> $ORWELLROOT/options/ODINTimeFilter.opts
    echo "ODINTimeFilter.mRange = (`date +%m`,`date +%m`);// Month range"   >> $ORWELLROOT/options/ODINTimeFilter.opts
    echo "ODINTimeFilter.dRange = (`date +%d`,`date +%d`);// Day range"   >> $ORWELLROOT/options/ODINTimeFilter.opts
    echo "ODINTimeFilter.hRange = (`date +%H`,`date +%H`);// Hour range"   >> $ORWELLROOT/options/ODINTimeFilter.opts
    echo "ODINTimeFilter.mnRange= (`date +%M`,`date +%M`);// Minute range"   >> $ORWELLROOT/options/ODINTimeFilter.opts
    echo "ODINTimeFilter.sRange = (00,59);// Second range"   >> $ORWELLROOT/options/ODINTimeFilter.opts
    echo ""   >> $ORWELLROOT/options/ODINTimeFilter.opts
    echo "ODINTimeFilter.eventSelector = false;" >> $ORWELLROOT/options/ODINTimeFilter.opts
    echo "ODINTimeFilter.eRange = (0,9999);// EventID range">> $ORWELLROOT/options/ODINTimeFilter.opts

    cd $ORWELLROOT/options
    pico ODINTimeFilter.opts
    tag=`date +%Y``date +%m``date +%d`"T"`date +%H``date +%M``date +%S`

fi 


dqot='"'
echo "HistogramPersistencySvc.OutputFile = ${dqot}CaloTiming_${tag}.root${dqot};" >>  $ORWELLROOT/options/ODINTimeFilter.opts 
echo "EcalTimingAnalysis.InputFiles   = {${dqot}CaloTiming_${tag}.root${dqot}};" >  $ORWELLROOT/options/InputRoot.opts 
echo "HcalTimingAnalysis.InputFiles   = {${dqot}CaloTiming_${tag}.root${dqot}};" >>  $ORWELLROOT/options/InputRoot.opts 
echo "PrsTimingAnalysis.InputFiles   = {${dqot}CaloTiming_${tag}.root${dqot}};" >>  $ORWELLROOT/options/InputRoot.opts 




runs=`grep -i requested $ORWELLOPTS/inputRawData.opts  | awk -F { '{print $2}' | awk -F } '{print $1}'`
echo "---------------------------------------------------------" 
echo "Your setting to be applied on run(s) {$runs} looks like :"
echo "-------------------------------------------------------- " 
more $ORWELLROOT/options/ODINTimeFilter.opts 
more $ORWELLROOT/options/InputRoot.opts 
echo "--------------------------------" 
echo
read -p "Are you happy with that (y,n,quit) ? [y] : " -e ok
if [  "$ok" = "" ]; then
    export ok="y"
fi

if [ "$ok" = "y" ]; then
cd $ORWELLROOT/work/
echo "---------------------------"
echo " Producing CaloTiming histo"
echo "---------------------------"
../$CMTCONFIG/Orwell.exe  $ORWELLOPTS/CaloTiming.opts
sleep 5
echo "---------------------------"
echo " Analyzing CaloTiming histo"
echo "---------------------------"
../$CMTCONFIG/Orwell.exe $ORWELLOPTS/OMACaloTiming.opts

echo
echo "------------------------------------------------------------------------"
echo " Root file output is '"'$ORWELLROOT/work/CaloTiming_'"$tag"'.root'"'"
echo "------------------------------------------------------------------------"

fi;
if [ "$ok" = "n" ]; then
echo re-running
clear
/group/calo/sw/scripts/caloTimer.sh
fi;

