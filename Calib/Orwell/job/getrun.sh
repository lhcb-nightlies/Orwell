

export dquot='"'
export quot="'"
export dat=`date +%F-%H-%M-%S`
export output="inputRawData.opts"

if [ ! -d /tmp/$USER ]; then
mkdir /tmp/$USER
fi
export ok=false


if [ $1 -a -f $1 ]; then
    echo "Run list will be read from file [$1]"
    cp $1 /tmp/$USER/run.list
#elif [ -f ./run.list ]; then
#    echo 'Run list will be read from file [run.list]'
#    cp run.list /tmp/$USER/run.list
elif [ $1 -a ! $(echo "$1" | grep -E "^[0-9]+$") ]; then
    echo 'Badly defined run list  ...'
    return
else
    export ok=true
fi

if [ $1 ]; then
    
    if [ -f /tmp/$USER/run.list ]; then
	echo 'Looking for run(s) : ' `more /tmp/$USER/run.list`
    else
	echo 'Looking for run(s) : ' $@
	echo "$@" > /tmp/$USER/run.list
    fi
else
    export last=`ls -tr /hist/Savesets/ByRun/L0DUDAQMon/*/*/ | tail -1 | awk -F "run" '{print $2}' | awk -F ".root" '{print $1}'`
    read -p "No run specified ! Do you want to get last run [$last] (y/n) ? [y] : " -e getlast
    if [ "$getlast" == "" ]; then
	export getlast="y"
    fi

    if [ "$getlast" == "n" ]; then
	echo "stop here ..."
	return
    fi
    echo 'Looking for run(s) : ' $last
    echo "$last" > /tmp/$USER/run.list
fi



if [ $last ]; then
    export year=`date +%Y`
else
    read -p  "Which data taking year ? ["`date +%Y`"] : " -e year 
    if [  "$year" = "" ]; then
	export year=`date +%Y`
    fi
fi

read -p  "Which data type (RAW,DIGI,DST) ? [RAW] : " -e typ 
if [  "$typ" = "" ]; then
export typ="RAW"
fi

read -p  "Which data stream (FULL,LUMI,EXPRESS) ? [FULL] : " -e stream
if [  "$stream" = "" ]; then
export stream="FULL"
fi



echo 'Looking for run {'`more /tmp/$USER/run.list`'} taken in period {'$year'}'


echo ' ' > $output
echo '//----------------------------------------------------------' >> $output
echo '// EventSelector setting automatically created  ('$dat')'     >> $output
if [ $ok=true ]; then
echo '// for requested run = {'$@'} to be found  in daqarea'        >> $output  
echo '// (run list saved in run.list.'$dat')'                       >> $output 
else
echo '// for run as in run.list.'$dat                               >> $output  
fi
echo '//----------------------------------------------------------' >> $output
echo ' ' >> $output
echo '#include "$STDOPTS/RawDataIO.opts" ' >>   $output
echo 'ApplicationMgr.EvtMax = -1;  '  >> $output
echo 'EventSelector.FirstEvent = 1; ' >> $output
echo 'EventSelector.Input += {' >> $output


export sep=''
export none=true
for run in `more /tmp/$USER/run.list`; do
    export found=false
    export path=/daqarea/lhcb/data/$year
    if [ "$year" = "ALL" -o  "$year" = "All"  -o  "$year" = "all" ]; then
	export path=`ls -d /daqarea/lhcb/data/20*`
    fi 
    for y in $path ; do

	echo ' '
#	echo 'Looking for run '$run'  in '$path'/RAW/$stream'

	if [ 0 != `ls -d $path/RAW/$stream/*/$run 2>/dev/null | wc -l` ] ; then
	    export t1=`ls -d $path/RAW/$stream/*/$run`
	else
            if [ 0 != `ls -d $path/RAW/$stream/*/*/$run 2>/dev/null| wc -l` ] ;  then
                export t1=`ls -d $path/RAW/$stream/*/*/$run`   
            else
                export t1=`ls -d /group/calo/caloUsers/odescham/data/$run 2>/dev/null`
            fi
	fi 

	if  [ "" != $t1"" ] ; then 
	    echo 'The run '$run' has been found in '$t1
	    for i in $t1/* ; do
		export found=true
		export none=false
                if [ "$typ" = "RAW" ]; then
		echo $sep$dquot'DATA='$quot'file://'$i$quot'  SVC='$quot'LHCb::MDFSelector'$quot$dquot >> $output
                else
		echo $sep$dquot'DATA='$quot'file://'$i$quot'  TYP='$quot'POOL_ROOT_TREE'$quot' OPT='$quot'READ'$quot$dquot >> $output
                fi
		export sep=','
	    done
	else
	    echo $t1' directory not found'
	fi
    done
    if [ $found = false ]; then
	echo 'WARNING : The run '$run' has not been found '$y'/RAW/'
    fi
done
echo '};' >> $output

if [ $none = true ]; then
    echo ""
    echo "--------------------------------------------------------------------"
    echo 'None of run in {'$@'} have been found in '$y'/RAW/'
    echo 'NO OUTPUT ...'
    echo "--------------------------------------------------------------------"
    echo ""
    rm -f $output
else
    echo ""
    echo "--------------------------------------------------------------------"
    echo "Created '$output' with EventSelector settings for the requested data"
    echo "--------------------------------------------------------------------"
    echo ""
fi


if $ok; then

    read -p "Store request history (y/n) ? [n] : " -e sav
    if [ "$sav" == "" ]; then
	export sav="n"
    fi
    if [ "$sav" == "y" ]; then
	echo "Requested list of run is stored in file 'run.list."$dat"'"
	mv /tmp/$USER/run.list run.list.$dat
    fi
fi
rm -f /tmp/$USER/run.list
unset last



