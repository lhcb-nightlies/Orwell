

source /group/calo/sw/scripts/setOrwell.sh

if [ $1 ];then
export vsn=$1
else
export vsn=$OrwellVsn
fi

echo $vsn


echo "... check local Orwell installation"
source /group/calo/sw/scripts/setupUser.sh

echo "... reseting ..." 

cp -R $Calo_release_area/Orwell_$vsn/Calib/Orwell/$vsn/$CMTCONFIG ~/cmtuser/myOrwell_$vsn/Calib/Orwell/$vsn/.


cd ~/cmtuser/myOrwell_$vsn/Calib/Orwell/$vsn/cmt 
source ./setup.sh
cd ~/cmtuser/myOrwell_$vsn/Calib/Orwell/$vsn/options 