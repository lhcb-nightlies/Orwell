export OrwellVsn=v2r6

if [ "$CMTCONFIG" = "slc4_amd64_gcc34" ]; then
echo Warning : force SLC4
. /group/calo/sw/cmtuser/Orwell_${OrwellVsn}/Calib/Orwell/${OrwellVsn}/cmt/setupCalo_slc4.vars
else
. /group/calo/sw/cmtuser/Orwell_${OrwellVsn}/Calib/Orwell/${OrwellVsn}/cmt/setupCalo.vars
fi

