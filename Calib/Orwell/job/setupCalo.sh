source  /sw/lib/scripts/ExtCMT.sh v1r20p20090520
#source  /sw/lib/scripts/ExtCMT.sh v1r20p20070208

export LHCBHOME=/sw/lib
export LHCBRELEASES=${LHCBHOME}/lhcb
export LHCBBIN=${LHCBHOME}/bin
export LHCBPYTHON=${LHCBHOME}/scripts/python
export LHCBSCRIPTS=${LHCBHOME}/scripts
#
export MYSITEROOT=/sw/lib
export SITEROOT=/sw/lib
export PATH=${PATH}:$MYSITEROOT/scripts
export CMT_DIR=$MYSITEROOT/contrib
#export CMTCONFIG=$CMTDEB


export User_release_area=/group/calo/sw/cmtuser
export CMTPROJECTPATH=${User_release_area}:${LHCb_release_area}:${LCG_release_area}


echo "User_release_area is set to: " ${User_release_area}
echo "CMTPROJECTPATH is set to   : " ${CMTPROJECTPATH}

#----------------------
#source /group/calo/sw/scripts/setOrwell.sh
# for new installation
export OrwellVsn="v2r6"
source $LHCBSCRIPTS/setenvProject.sh Orwell  ${OrwellVsn}
cd /group/calo/sw/cmtuser/Orwell_${OrwellVsn}/Calib/Orwell/${OrwellVsn}/cmt/.
#. ./setup.sh
echo  $OrwellVsn
#----------------------
export MYSITEROOT=/sw/lib
#source /group/calo/sw/scripts/setup.sh

setenvOrwell  ${OrwellVsn}
echo "setting up the application ... wait"


echo "setting up Orwell application ... wait"
if [ -f ./Calib/Orwell/${OrwellVsn}/cmt/setupCalo.vars ]; then
 echo "source setupCalo.vars"
 #source /group/calo/sw/scripts/setOrwell.sh
 source ./Calib/Orwell/${OrwellVsn}/cmt/setupCalo.vars
else
 echo "source setup.sh"
 source ./Calib/Orwell/${OrwellVsn}/cmt/setup.sh
fi




cd $ORWELLOPTS/

echo "... ready to play ! " 
