#!/bin/bash
#
export USER=online
export HOME=/home/${USER}
export MYSITEROOT=/sw/lib
source /sw/lib/LbLogin.sh
source /sw/oracle/set_oraenv.sh

export UTGID=LHCb_MONA0801_CaloOccupancyAnalysis_00
export DIM_DNS_NODE="mona08"
export LOGFIFO=/tmp/logCaloOccupancyAna.fifo

. /group/calo/sw/scripts/setOrwell.sh

exec -a ${UTGID} ${ORWELLROOT}/${CMTCONFIG}/Orwell.exe ${ORWELLOPTS}/AutoCaloOccupancyAnalysis.opts -msgsvc=LHCb::FmcMessageSvc


