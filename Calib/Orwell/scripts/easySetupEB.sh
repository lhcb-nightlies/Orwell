#!/bin/bash
# avoid picking with mouse the IP number for setupEB
#
setEB | grep -m 1 'inet addr' | awk -F : '{print $2}' | awk -F Bcast '{print $1}' > temp
setEB temp
rm -f temp 
