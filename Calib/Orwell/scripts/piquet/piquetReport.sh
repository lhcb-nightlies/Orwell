#!/bin/bash

path='/group/calo/sw/scripts'



echo "            ++++++++++++++++++++++++++++++ "
echo "            +  DAILY CALO PIQUET REPORT  + "
echo "            ++++++++++++++++++++++++++++++ "
echo ""
echo "author : "`whoami`
echo "date   : "`date +%Y/%m/%d" "%H:%M`
echo ""

echo "   ## Summary of the last day operation and machine status ##"
echo "   ## ---------------------------------------------------- ##"
$path/piquet/fillStat.sh $1 1.5   # statistics over 36h
echo ""
echo "   ** machine status ** : look on the screen above"
echo ""

echo "   ## ----------------##"
echo "   ## Hardware status ##"
echo "   ## ----------------##"
echo "   ** LV controls **"
$path/piquet/utils/openPanel.sh "INFDAI1_UI_lbRackMaratonCrateStatus.sh" "" "y" "pass"
$path/piquet/utils/openPanel.sh "INFDAI1_UI_lbRackRackStatus.sh"  '[select CaloA/CaloC]' 
echo "   ** LV-HV & LV-VFE & Agilent & HV **"
$path/piquet/utils/openPanel.sh "CA2_UI_FSM.sh"  "[open Calo_HV_TOP]" 
echo "   ** SPD  temperature **"
$path/piquet/utils/openPanel.sh "CA2_UI_FSM.sh"  '[open Calo/Calo_DCS/PRS_DCS & click on "Spd Temperatures Overview"]' 
echo "   ** PRS  temperature"
$path/piquet/utils/openPanel.sh "PSDCST1_UI_FSM.sh" '[open PSDCST1 & click on  "View status"]'
echo ""
echo "   ## ---------------##"
echo "   ## Data integrity ##"
echo "   ## ---------------##"
echo "   ** L0 Calo Error rate **"
$path/piquet/utils/openPresenter.sh '/Shift/L0CALO: Status'                      'history' '' 'pass'
$path/piquet/utils/openPresenter.sh '/L0Calo/5. Electron/3. Emulator Comparison' 'history' '' 'pass'
$path/piquet/utils/openPresenter.sh '/L0Calo/6. Photon/3. Emulator Comparison'   'history'
echo ""
echo "   ** Readout status **"
$path/piquet/utils/openPresenter.sh '/Calorimeters/CaloReadoutStatus'            'history'
echo ""
echo "   ** Calibration & Monitoring **"
if [ "${HOSTNAME:0:4}" == "plus" ]; then
    ${path}/CheckMonitoring.sh
else
    echo "   ... Checking monitoring status on plus machine ..."
    ${path}/piquet/utils/CheckMonitoringOnPlus.sh
    echo ""
fi
echo "   ** Calibration Analysis : Pedestal, noise, LED stability **"
$path/piquet/utils/openPresenter.sh '/Calorimeters/Analysis/CalibrationData/AnalysisSummary'            'online'
echo "   ** Monitoring Analysis  **"
$path/piquet/utils/openPresenter.sh '/Calorimeters/Analysis/MonitoringData/AnalysisSummary'            'online'
echo "   ** Calo display : : hot & dead channels **"
$path/piquet/utils/openPresenter.sh '/Calorimeters/Calo2DView'            'history'
echo "   ** Ecal Crate timing **"
$path/piquet/utils/openPresenter.sh '/Calorimeters/Ecal/CrateTiming/Profile'            'history'
echo "   ## -------------------- ##"
echo "   ## Check Auto HV update ##"
echo "   ## -------------------- ##"
$path/piquet/lastHVChange.sh 'both' 'last'
read -p " ... press a key to continue ..."
echo "   ## ------------------ ##"
echo "   ## Check XCal current ##"
echo "   ## ------------------ ##"
echo "   ** Check Hcal current (close root window to resume) **"
${path}/hcalScripts/hcalcurr_view.sh 
echo "   ** Check Ecal current **"
$path/piquet/utils/openPanel.sh "LBECSINFO_UI_lbTrending.sh"  '[open LBECSINFO/CALO/ECAL/ECALintegratorsA/C]' 

# === bye
echo
echo "      ++++++++++++++++++++++ "
echo "      ++ REPORT COMPLETED ++ "
echo "      ++++++++++++++++++++++ "
echo
read -p " ... press a key to clean open windows ..."
echo
# === CLEANING === #
read -p  "  ?? Close all presenter windows  (y/n) ? [y] : " -e ok
if [ "$ok" == "y" -o "$ok" == "" ]; then
    pid=`ps | grep -i presenter | awk -F ' ' '{print $1}'`   
    for t in `echo $pid`; do
        kill -9 $t
    done
    pid=`ps | grep -i hcalcurr_view | awk -F ' ' '{print $1}'`   
    for t in `echo $pid`; do
        kill -9 $t
    done
fi
if [ -d /opt/WinCC_OA ]; then
    read -p  "  ?? Close all PVSS windows  (y/n) ? [y] : " -e ok
    if [ "$ok" == "y" -o "$ok" == "" ]; then
	pid=`ps | grep -i WCCOAui | awk -F ' ' '{print $1}'`   
	for t in `echo $pid`; do
            kill -9 $t
	done
    fi
fi
