#!/bin/bash

pannel=""
if [  $1 ]; then 
    pannel=$1
else
    echo "you must give a path"
    exit
fi
info=""
if [  "$2" != "" ]; then 
    info=$2
fi

def="y"
if [ "$3" != "" ]; then 
    def=$3
fi
pass=""
if [  "$4" != "" ]; then 
    pass=$4
fi

short='/group/calo/picketshortcuts/'
echo "  >> from ${short}ui/${pannel} "${info}
if [ -d /opt/WinCC_OA ]; then
    read -p  "  ?? Open pvss panel (y/n) ? [${def}] : " -e ok
    if [ "$ok" == ""  ]; then
	ok=${def}
    fi
    if [ "$ok" == "y"  ]; then
	echo "  ... launching ${pannel}"
	${short}ui/${pannel}
	if [ "$pass" != "pass" ]; then
	    read -p "...press a key to continue ..."
	fi
    fi
else
    echo "  ... PVSS cannot be launch on that machine"
	if [ "$pass" != "pass" ]; then
	    read -p "... press a key to continue ..."
	fi
fi
echo ""
