#!/bin/bash


page=""
if [  "$1" ]; then 
    page=$1
else
    echo "you must give the page path"
    exit
fi
mode="history"
if [  "$2" != "" ]; then 
    mode=$2
fi

def="y"
if [ "$3" != "" ]; then 
    def=$3
fi
pass=""
if [  "$4" != "" ]; then 
    pass=$4
fi

short='/group/calo/picketshortcuts/'
echo "  >> from Presenter:${page} (${mode} mode)"
read -p  "  ?? Open presenter (y/n) ? [${def}] : " -e ok
if [ "$ok" == ""  ]; then
    ok=${def}
fi
if [ "$ok" == "y"  ]; then
    if [ "$mode" == "history" ]; then
	    echo "  ... launching ${page} in history mode (last 10mn saveset by default - change the time interval)"
    elif [ "$mode" == "online" ]; then
	echo "  ... launching ${page} in online mode - current status"
    else
	echo "  ... launching ${page} in $mode mode"
    fi
    /group/calo/sw/scripts/piquet/utils/presenterPage.sh "$page" "$mode"
    if [ "$pass" != "pass" ]; then
	read -p " ... press a key to continue ..."
    fi
fi
echo ""

