export OrwellVsn=v2r10

if [ -d /group/calo/sw/cmtuser/Orwell_${OrwellVsn}/Calib/Orwell/${OrwellVsn} ]; then
    export OrwellPath="/group/calo/sw/cmtuser/Orwell_${OrwellVsn}/Calib/Orwell/${OrwellVsn}"
else
    export OrwellPath="/group/calo/sw/cmtuser/Orwell_${OrwellVsn}/Calib/Orwell/"
fi

if [ "$CMTCONFIG" = "slc4_amd64_gcc34" ]; then
echo Warning : force SLC4
. $OrwellPath/cmt/setupCalo_slc4.vars
else
. $OrwellPath/cmt/setupCalo.vars
fi

